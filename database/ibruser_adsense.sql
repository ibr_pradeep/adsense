-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 25, 2015 at 05:25 AM
-- Server version: 5.5.42-37.1
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ibruser_adsense`
--

-- --------------------------------------------------------

--
-- Table structure for table `analytics_detail`
--

CREATE TABLE IF NOT EXISTS `analytics_detail` (
  `analytics_detail_id` int(11) NOT NULL,
  `service_email` varchar(255) NOT NULL,
  `key_filename` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` bit(1) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `analytics_detail`
--

INSERT INTO `analytics_detail` (`analytics_detail_id`, `service_email`, `key_filename`, `status`, `is_deleted`, `created_date`, `updated_date`) VALUES
(3, '539518501004-q7ljpvkjla1cv653c11u0uqvfrb0b9k2@developer.gserviceaccount.com', 'anlytics/1506181129/client_secrets.p12', 0, b'0', '2015-06-18 00:00:00', '2015-06-18 04:57:32'),
(5, 'nowayitshouldwork@gmail.com', 'anlytics/1506232023/mdm.cer', 0, b'0', '2015-06-23 20:23:54', '2015-06-23 14:53:54');

-- --------------------------------------------------------

--
-- Table structure for table `api_configration`
--

CREATE TABLE IF NOT EXISTS `api_configration` (
  `id` int(11) NOT NULL,
  `api_id` varchar(255) DEFAULT NULL,
  `app_secret` varchar(255) DEFAULT NULL,
  `permissions` varchar(255) DEFAULT NULL,
  `redirect_url` varchar(255) DEFAULT NULL,
  `service_provider` varchar(255) DEFAULT NULL,
  `is_deleted` bit(1) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `api_configration`
--

INSERT INTO `api_configration` (`id`, `api_id`, `app_secret`, `permissions`, `redirect_url`, `service_provider`, `is_deleted`, `created_date`, `updated_date`) VALUES
(1, '1654266621474610', '965e9d971d30e5b47791365e178ae39a', 'public_profile, publish_actions ', 'http://216.172.184.121/~ibruser/adsense/index.php/user', 'Facebook', b'0', '2015-06-22 11:10:55', '2015-06-22 05:40:55'),
(6, '1y3e1UHZyWjuhd1iaTPWBg6fr', 'WedOfldMAaDDPlQCoZME89kve2oRcF9QPIdxvutjP3PfQCFJrG', '', '', 'Twitter', b'0', '2015-06-22 12:22:26', '2015-06-22 06:52:26');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `title`, `is_deleted`, `create_date`, `update_date`) VALUES
(1, 'abc 123', '0', '2015-05-22 07:23:37', '2015-05-21 11:34:38'),
(2, 'test123', '0', '2015-05-21 14:57:44', '2015-05-21 12:26:30'),
(3, 'test', '0', '2015-05-21 14:56:58', '2015-05-21 12:26:58'),
(4, 'zvxcvcx ', '0', '2015-05-23 10:06:45', '2015-05-23 09:36:45'),
(5, 'aaaa2', '0', '2015-06-08 18:07:23', '2015-06-08 12:32:15'),
(6, 'ans122', '0', '2015-06-19 15:30:50', '2015-06-19 10:00:50'),
(7, 'Foods', '1', '2015-06-22 14:24:14', '2015-06-22 08:54:14'),
(8, 'cloths', '1', '2015-06-22 14:27:06', '2015-06-22 08:57:06'),
(9, 'new CAT', '1', '2015-06-23 20:28:33', '2015-06-23 14:58:33');

-- --------------------------------------------------------

--
-- Table structure for table `paypal_details`
--

CREATE TABLE IF NOT EXISTS `paypal_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `paypal_email` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paypal_details`
--

INSERT INTO `paypal_details` (`id`, `user_id`, `paypal_email`, `created_date`, `updated_date`) VALUES
(4, 22, 'sulbha@ibrinfotech.com', '2015-05-26 12:18:58', '2015-05-26 11:48:58'),
(13, 39, 'sulbha@paypal.com', '2015-06-19 16:32:21', '2015-06-19 11:02:21'),
(14, 48, 'munish.41@gmail.com', '2015-06-23 20:33:54', '2015-06-23 15:03:54'),
(15, 47, 'dewanishan@gmail.com', '0000-00-00 00:00:00', '2015-06-24 18:13:28'),
(16, 51, 'sulbha123@gmail.com', '0000-00-00 00:00:00', '2015-06-25 10:13:41');

-- --------------------------------------------------------

--
-- Table structure for table `paypal_transaction_details`
--

CREATE TABLE IF NOT EXISTS `paypal_transaction_details` (
  `id` int(11) NOT NULL,
  `mc_gross` varchar(255) DEFAULT NULL,
  `protection_eligibility` varchar(255) DEFAULT NULL,
  `payer_id` varchar(255) DEFAULT NULL,
  `tax` varchar(255) DEFAULT NULL,
  `payment_date` varchar(255) DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `charset` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `mc_fee` varchar(255) DEFAULT NULL,
  `notify_version` varchar(255) DEFAULT NULL,
  `custom` varchar(255) DEFAULT NULL,
  `payer_status` varchar(255) DEFAULT NULL,
  `business` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `payer_email` varchar(255) DEFAULT NULL,
  `verify_sign` varchar(255) DEFAULT NULL,
  `txn_id` varchar(255) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `receiver_email` varchar(255) DEFAULT NULL,
  `payment_fee` varchar(255) DEFAULT NULL,
  `receiver_id` varchar(255) DEFAULT NULL,
  `txn_type` varchar(255) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `mc_currency` varchar(255) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `residence_country` varchar(255) DEFAULT NULL,
  `test_ipn` varchar(255) DEFAULT NULL,
  `handling_amount` varchar(255) DEFAULT NULL,
  `transaction_subject` varchar(255) DEFAULT NULL,
  `payment_gross` varchar(255) DEFAULT NULL,
  `shipping` varchar(255) DEFAULT NULL,
  `auth` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paypal_transaction_details`
--

INSERT INTO `paypal_transaction_details` (`id`, `mc_gross`, `protection_eligibility`, `payer_id`, `tax`, `payment_date`, `payment_status`, `charset`, `first_name`, `mc_fee`, `notify_version`, `custom`, `payer_status`, `business`, `quantity`, `payer_email`, `verify_sign`, `txn_id`, `payment_type`, `last_name`, `receiver_email`, `payment_fee`, `receiver_id`, `txn_type`, `item_name`, `mc_currency`, `item_number`, `residence_country`, `test_ipn`, `handling_amount`, `transaction_subject`, `payment_gross`, `shipping`, `auth`) VALUES
(1, '20.50', 'Ineligible', '48JMWRAFUJYQY', '0.00', '06:02:22 Jun 24, 2015 PDT', 'Completed', 'utf-8', 'test', '0.89', '3.8', '', 'verified', 'sulbha2814-facilitator@gmail.com', '1', 'sulbha@paypal.com	', 'AUjr39UTV0bGNmdhbbBkRD-UHHF.AurIJQTx7qTYHwZZl.0hKd09AE9B', '31A14479GN7712043', 'instant', 'buyer', 'sulbha2814-facilitator@gmail.com', '0.89', 'TDA6WHJJSZLJ2', 'web_accept', 'Demo Product', 'USD', '', 'US', '1', '0.00', '', '20.50', '0.00', 'AXoK9xf8R1kJF8TCNd6zZc6YxV6upeW10LDRviE1mF6EE4JNQ5dOOXEcGATHLWMzd3jKRuxadSefR2JlDVUfYXA');

-- --------------------------------------------------------

--
-- Table structure for table `share_link`
--

CREATE TABLE IF NOT EXISTS `share_link` (
  `sharelink_id` int(11) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scheduler_datetime` varchar(200) NOT NULL,
  `scheduler_status` bit(1) NOT NULL,
  `Scheduler_Set` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `share_link`
--

INSERT INTO `share_link` (`sharelink_id`, `category_id`, `user_id`, `link`, `content`, `image`, `is_deleted`, `create_date`, `update_date`, `scheduler_datetime`, `scheduler_status`, `Scheduler_Set`) VALUES
(19, '2', '0', 'http://dev.ibrinfotech.com/adsense/in', '', NULL, '1', '2015-05-26 14:51:33', '2015-05-26 14:21:33', '2015-05-28 17:00:19', b'0', 'Yes'),
(20, '2', '0', 'http://www.dsfsdfsdf.com', '', NULL, '0', '2015-05-26 14:51:49', '2015-05-26 14:21:49', '2015-05-26 16:00:35', b'0', 'Yes'),
(21, '1', '0', 'http://www.dsfsdfsdf.com', '', NULL, '0', '2015-05-26 17:14:31', '2015-05-26 16:44:31', '2015-05-26 17:30:47', b'0', 'Yes'),
(22, '2', '0', 'http://www.ibrinfotech.com', '', NULL, '0', '2015-05-26 17:14:47', '2015-05-26 16:44:47', '2015-06-27 04:30:31', b'1', 'Yes'),
(23, '2', '0', 'https://developers.google.com/gdata/docs/auth/overview', '', NULL, '1', '2015-06-09 14:27:18', '2015-06-09 08:57:18', '2015-06-09 14:07:19', b'0', 'Yes'),
(24, '2', '0', 'https://developers.google.com/analytics/devguides/reporting/core/v2/gdataAuthentication', '', NULL, '0', '2015-06-09 14:27:46', '2015-06-09 08:57:46', '2015-06-09 14:07:19	', b'0', 'Yes'),
(25, '3', '0', 'http://www.google.co.in/', '', NULL, '1', '2015-06-09 14:44:15', '2015-06-09 09:14:15', '2015-06-09 14:45:19', b'0', 'Yes'),
(26, '0', '29', 'https://github.com/joeauty/Google-API-Client-CodeIgniter-Spark/blob/master/examples/analytics/demo/index.php', '', NULL, '0', '2015-06-09 14:45:33', '2015-06-09 09:15:33', '2015-06-09 14:45:19', b'1', 'Yes'),
(27, '0', '37', 'http://www.magzim.com', '', NULL, '0', '2015-06-09 14:55:10', '2015-06-09 09:25:10', '', b'1', ''),
(28, '0', '37', 'http://www.test.com  Hi how  testing ', '', NULL, '0', '2015-06-09 15:03:48', '2015-06-09 09:33:48', '', b'1', ''),
(29, '0', '37', 'http://www.magzim.com ', '', NULL, '0', '2015-06-09 15:58:07', '2015-06-09 10:28:07', '2015-06-09 16:00:00', b'1', 'Yes'),
(30, '0', '37', 'http://www.magzim.com test tweet twett', '', NULL, '0', '2015-06-09 15:59:22', '2015-06-09 10:29:22', '2015-06-09 16:00:05', b'1', 'Yes'),
(31, '0', '37', 'http://www.magzim.com', '', NULL, '0', '2015-06-09 17:09:42', '2015-06-09 11:39:42', '2015-06-09 17:11:23', b'1', 'Yes'),
(32, '0', '37', 'http://www.magzim.com', '', NULL, '1', '2015-06-09 17:34:00', '2015-06-09 12:04:00', '2015-06-09 17:35:42', b'1', 'Yes'),
(33, '0', '37', 'http://www.magzim.com', '', NULL, '0', '2015-06-09 17:37:35', '2015-06-09 12:07:35', '2015-06-09 17:39:42', b'1', 'Yes'),
(34, '1', '0', 'http://www.google.com <br> hi sulbha', '', NULL, '0', '2015-06-12 15:07:04', '2015-06-12 09:37:04', '2015-06-12 15:10:27', b'0', 'Yes'),
(35, '2', '0', 'https://www.google.co.in/?gfe_rd=cr&ei=Sb16VZP4LYSAoAP6i4DIBw&gws_rd=ssl#q=encryption+and+decryption+in+php', '', NULL, '0', '2015-06-12 16:45:22', '2015-06-12 11:15:22', '2015-06-12 17:55:47', b'1', 'Yes'),
(36, '2', '0', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', '', NULL, '0', '2015-06-12 16:46:12', '2015-06-12 11:16:12', '2015-06-12 17:50:39', b'1', 'Yes'),
(37, '5', '0', 'https://www.google.co.in/?gfe_rd=cr&ei=o796VeuVA5PK8AfpnYHYBQ&gws_rd=ssl', '', NULL, '0', '2015-06-12 16:46:59', '2015-06-12 11:16:59', '2015-06-12 17:50:28', b'1', 'Yes'),
(38, '2', '0', 'https://play.google.com/apps/publish/signup/', '', NULL, '0', '2015-06-12 16:57:00', '2015-06-12 11:27:00', '2015-06-12 17:00:50', b'0', 'Yes'),
(39, '1', '0', 'https://developer.android.com/distribute/googleplay/developer-console.html', '', NULL, '0', '2015-06-12 16:59:32', '2015-06-12 11:29:32', '2015-06-12 17:00:21', b'0', 'Yes'),
(40, '3', '0', 'http://developer.android.com/distribute/googleplay/start.html', '', NULL, '0', '2015-06-12 17:33:40', '2015-06-12 12:03:40', '2015-06-12 18:30:27', b'1', 'Yes'),
(41, '2', '0', 'http://developer.android.com/distribute/googleplay/developer-console.html', '', NULL, '0', '2015-06-12 17:42:26', '2015-06-12 12:12:26', '2015-06-12 18:30:18', b'1', 'Yes'),
(42, '3', '0', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', '', NULL, '0', '2015-06-12 17:44:08', '2015-06-12 12:14:08', '2015-06-12 18:30:01', b'1', 'Yes'),
(43, '2', '0', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', '', NULL, '0', '2015-06-12 17:44:26', '2015-06-12 12:14:26', '2015-06-12 18:00:18', b'1', 'Yes'),
(44, '1', '0', 'http://www.dsfsdfsdf.com', '', NULL, '0', '2015-06-12 17:58:23', '2015-06-12 12:28:23', '2015-06-12 18:30:15', b'1', 'Yes'),
(45, '2', '0', 'http://www.xyz.com', '', NULL, '0', '2015-06-12 18:01:29', '2015-06-12 12:31:29', '2015-06-12 18:30:22', b'1', 'Yes'),
(46, '2', '0', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', '', NULL, '0', '2015-06-12 18:08:25', '2015-06-12 12:38:25', '2015-06-12 18:30:17', b'1', 'Yes'),
(47, '1', '0', 'https://github.com/jedisct1/libsodium-php', '', NULL, '0', '2015-06-12 18:08:56', '2015-06-12 12:38:56', '2015-06-12 18:30:50', b'1', 'Yes'),
(48, '2', '0', 'http://www.tutorialspoint.com/php/', '', NULL, '0', '2015-06-13 11:16:20', '2015-06-13 05:46:20', '2015-06-13 11:20:56', b'0', 'Yes'),
(49, '0', '42', 'http://www.w3schools.com/php/', '', NULL, '0', '2015-06-13 11:40:23', '2015-06-13 06:10:23', '', b'1', ''),
(50, '0', '42', 'http://www.tutorialspoint.com/php/', '', NULL, '0', '2015-06-13 11:44:23', '2015-06-13 06:14:23', '', b'1', ''),
(51, '3', '0', 'https://developers.facebook.com/apps/1654266621474610/roles/test-users/', '', NULL, '0', '2015-06-13 11:45:59', '2015-06-13 06:15:59', '2015-06-13 11:45:15', b'0', 'Yes'),
(52, '2', '42', 'http://www.dsfsdfsdf.com', '', NULL, '0', '2015-06-13 11:46:43', '2015-06-13 06:16:43', '', b'0', ''),
(53, '2', '0', 'http://www.dsfsdfsdf.com', '', NULL, '0', '2015-06-13 11:49:46', '2015-06-13 06:19:46', '', b'1', ''),
(54, '3', '0', 'http://www.dsfsdfsdf.com', '', NULL, '0', '2015-06-13 11:50:34', '2015-06-13 06:20:34', '', b'1', ''),
(55, '5', '0', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', '', NULL, '0', '2015-06-13 11:51:07', '2015-06-13 06:21:07', '', b'1', ''),
(56, '1', '0', 'https://www.google.co.in/?gfe_rd=cr&ei=BMJ7VdCwIYfK8AfL3YCABA&gws_rd=ssl#q=php+tutorial', '', NULL, '0', '2015-06-13 11:56:31', '2015-06-13 06:26:31', '', b'1', ''),
(57, '1', '0', 'http://www.jkscholarship.in/', '', NULL, '0', '2015-06-13 12:41:26', '2015-06-13 07:11:26', '2015-06-13 13:00:15', b'0', 'Yes'),
(58, '0', '39', 'http://www.jkscholarship.in/', '', NULL, '0', '2015-06-13 12:41:59', '2015-06-13 07:11:59', '', b'1', ''),
(59, '1', '0', 'http://www.jkscholarship.in/', '', NULL, '0', '2015-06-13 13:05:34', '2015-06-13 07:35:34', '2015-06-13 13:30:24', b'0', 'Yes'),
(60, '1', '0', 'http://php.net/strpos', '', NULL, '0', '2015-06-16 13:36:55', '2015-06-16 08:06:56', '2015-06-16 14:30:46', b'0', 'Yes'),
(61, '0', '36', 'http://stackoverflow.com/questions/19562903/remove-padding-from-columns-in-bootstrap-3', '', NULL, '0', '2015-06-16 13:37:25', '2015-06-16 08:07:25', '2015-06-16 14:30:22', b'1', 'Yes'),
(62, '0', '39', 'http://stackoverflow.com/questions/4366730/check-if-string-contains-specific-words', '', NULL, '0', '2015-06-16 13:37:42', '2015-06-16 08:07:42', '', b'1', ''),
(63, '1', '0', 'http://www.programmerinterview.com/index.php/php-questions/find-if-string-contains-another-string-php/', '', NULL, '0', '2015-06-16 13:38:25', '2015-06-16 08:08:25', '2015-06-16 14:00:16', b'0', 'Yes'),
(64, '1', '0', 'http://www.maxi-pedia.com/string+contains+substring+php', '', NULL, '0', '2015-06-16 14:55:42', '2015-06-16 09:25:42', '2015-06-16 15:00:35', b'1', 'Yes'),
(65, '0', '45', 'https://gist.github.com/Integralist/1391440', '', NULL, '0', '2015-06-16 14:56:10', '2015-06-16 09:26:10', '', b'1', ''),
(66, '2', '0', 'http://www.yopmail.net/en/', '', NULL, '0', '2015-06-16 14:58:41', '2015-06-16 09:28:41', '2015-06-16 16:00:24', b'1', 'Yes'),
(67, '2', '0', 'https://plugins.jquery.com/chosen/', '', NULL, '0', '2015-06-16 17:20:01', '2015-06-16 11:50:01', '2015-06-16 17:20:47', b'0', 'Yes'),
(68, '1', '0', 'https://github.com/harvesthq/chosen/releases', '', NULL, '0', '2015-06-16 18:02:14', '2015-06-16 12:32:14', '', b'1', ''),
(69, '0', '43', 'https://plugins.jquery.com/chosen/', '', NULL, '0', '2015-06-16 18:09:13', '2015-06-16 12:39:13', '', b'1', ''),
(70, '0', '38', 'http://www.w3schools.com/cssref/css3_pr_word-wrap.asp', '', NULL, '0', '2015-06-16 18:36:13', '2015-06-16 13:06:13', '', b'1', ''),
(71, '1', '39', 'http://www.dsfsdfsdf.com', '', NULL, '0', '2015-06-16 18:59:48', '2015-06-16 13:29:48', '', b'0', ''),
(72, '0', '38', 'https://ellislab.com/codeigniter/user-guide/tutorial/index.html', '', NULL, '0', '2015-06-16 19:00:08', '2015-06-16 13:30:08', '', b'1', ''),
(73, '0', '44', 'https://ellislab.com/codeigniter/user-guide/general/controllers.html', '', NULL, '0', '2015-06-16 19:03:39', '2015-06-16 13:33:39', '', b'1', ''),
(74, '0', '44', 'https://ellislab.com/codeigniter/user-guide/general/controllers.html', '', NULL, '0', '2015-06-16 19:28:00', '2015-06-16 13:58:00', '', b'1', ''),
(75, '2', '38', 'https://ellislab.com/codeigniter/user-guide/tutorial/index.html', '', NULL, '0', '2015-06-16 19:29:50', '2015-06-16 13:59:50', '', b'0', ''),
(76, '1', '0', 'http://216.172.184.121/~ibruser/adsense/index.php/usercategory', '', NULL, '0', '2015-06-16 19:30:51', '2015-06-16 14:00:51', '', b'1', ''),
(77, '0', '38', 'http://216.172.184.121/~ibruser/adsense/index.php/usercategory234234234', '', NULL, '0', '2015-06-16 19:31:49', '2015-06-16 14:01:49', '', b'1', ''),
(78, '0', '44', 'http://localhost/phpmyadmin/#PMAURL-17:sql.php?db=adsense&table=share_link&server=1&target=&token=0a28a2e9399b242c68ddf62a204edb57', '', NULL, '0', '2015-06-16 19:33:02', '2015-06-16 14:03:02', '', b'1', ''),
(79, '0', '44', 'http://localhost/phpmyadmin/#PMAURL-17:sql.php?db=adsense&table=share_link&server=1&target=&token=0a28a2e9399b242c68ddf62a204edb57', '', NULL, '0', '2015-06-16 19:37:36', '2015-06-16 14:07:36', '', b'1', ''),
(80, '', '36;38;39;43;44', 'http://php.net/manual/en/function.array-diff.php', '<?php\r\n$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");\r\n$a2=array("e"=>"red","f"=>"green","g"=>"blue");\r\n\r\n$result=array_diff($a1,$a2);\r\nprint_r($result);\r\n?>', 'assets/uploads/1434546029_Penguins.jpg', '0', '2015-06-17 18:30:29', '2015-06-17 13:00:29', '', b'1', ''),
(81, '', '36;38;39;43;44', 'http://php.net/manual/en/function.array-diff.php', '<?php\r\n$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");\r\n$a2=array("e"=>"red","f"=>"green","g"=>"blue");\r\n\r\n$result=array_diff($a1,$a2);\r\nprint_r($result);\r\n?>', 'assets/uploads/1434546147_Penguins.jpg', '0', '2015-06-17 18:32:27', '2015-06-17 13:02:27', '', b'1', ''),
(82, '1;2;3', '', 'http://www.jkscholarship.in/', 'abc', 'assets/uploads/1434546191_Chrysanthemum.jpg', '0', '2015-06-17 18:33:11', '2015-06-17 13:03:11', '', b'1', ''),
(83, '1', '36;38;39;43', 'http://php.net/manual/en/function.array-diff.php', '<!DOCTYPE html>\r\n<html>\r\n<body>\r\n\r\n<?php\r\n$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");\r\n$a2=array("e"=>"red","f"=>"green","g"=>"blue");\r\n\r\n$result=array_diff($a1,$a2);\r\nprint_r($result);\r\n?>\r\n\r\n</body>\r\n</html>', 'assets/uploads/1434546286_s1.jpg', '0', '2015-06-17 18:34:46', '2015-06-17 13:04:46', '', b'0', ''),
(84, '1;2;3', '43', 'https://gator4216.hostgator.com:2083/cpsess5986508604/3rdparty/phpMyAdmin/import.php', 'hello how are u ', 'assets/uploads/1434609583_r3.jpg', '0', '2015-06-18 12:09:43', '2015-06-18 06:39:43', '', b'0', ''),
(85, '1', '43', 'http://www.google.com/', 'hellooooooooooooooooooo', 'assets/uploads/1434609794_r3.jpg', '0', '2015-06-18 12:13:14', '2015-06-18 06:43:14', '', b'0', ''),
(86, '', '36;43', 'http://www.google.com/', 'hellooooooooo', 'assets/uploads/1434609901_r3.jpg', '0', '2015-06-18 12:15:01', '2015-06-18 06:45:01', '', b'1', ''),
(87, '', '43;44', 'https://www.google.co.in/?gfe_rd=cr&ei=bGiCVfXNIsqFoAOI-4G4AQ&gws_rd=ssl', 'hello', 'assets/uploads/1434610017_r3.jpg', '0', '2015-06-18 12:16:57', '2015-06-18 06:46:57', '', b'1', ''),
(88, '', '36', 'http://www.dsfsdfsdf.com', 'fsdf', 'assets/uploads/1434610100_r3.jpg', '0', '2015-06-18 12:18:20', '2015-06-18 06:48:20', '', b'1', ''),
(89, '2;4', '', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', 'asfdg', 'assets/uploads/1434610250_icon_twitter_small.jpg', '0', '2015-06-18 12:20:50', '2015-06-18 06:50:50', '2015-06-19 12:20:32', b'0', 'Yes'),
(90, '1;2;3;4', '', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', 'asdf', 'assets/uploads/1434610363_icon_twitter_small.jpg', '0', '2015-06-18 12:22:43', '2015-06-18 06:52:43', '2015-06-18 12:22:29', b'0', 'Yes'),
(91, '', '43;44;46', 'http://216.172.184.121/~ibruser/', 'hellooooooooooooooooooo dear', 'assets/uploads/1434610571_r1.jpg', '0', '2015-06-18 12:26:11', '2015-06-18 06:56:12', '', b'1', ''),
(92, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434610877_r4.jpg', '0', '2015-06-18 12:31:17', '2015-06-18 07:01:17', '', b'1', ''),
(93, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611050_r4.jpg', '0', '2015-06-18 12:34:10', '2015-06-18 07:04:10', '', b'1', ''),
(94, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611162_r4.jpg', '0', '2015-06-18 12:36:02', '2015-06-18 07:06:02', '', b'1', ''),
(95, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611228_r4.jpg', '0', '2015-06-18 12:37:08', '2015-06-18 07:07:08', '', b'1', ''),
(96, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611441_r4.jpg', '0', '2015-06-18 12:40:41', '2015-06-18 07:10:41', '', b'1', ''),
(97, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611460_r4.jpg', '0', '2015-06-18 12:41:00', '2015-06-18 07:11:00', '', b'1', ''),
(98, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611506_r4.jpg', '0', '2015-06-18 12:41:46', '2015-06-18 07:11:46', '', b'1', ''),
(99, '', '43', 'https://developers.facebook.com/', 'hw r u .....................', 'assets/uploads/1434611571_r3.jpg', '0', '2015-06-18 12:42:51', '2015-06-18 07:12:51', '', b'1', ''),
(100, '', '43', 'https://developers.facebook.com/', 'hw r u .....................', 'assets/uploads/1434611779_r3.jpg', '0', '2015-06-18 12:46:19', '2015-06-18 07:16:19', '', b'1', ''),
(101, '', '36;43', 'https://www.google.co.in/', 'how r u.............', 'assets/uploads/1434611834_s2.jpg', '0', '2015-06-18 12:47:14', '2015-06-18 07:17:14', '', b'1', ''),
(102, '', '36;43', 'https://www.google.co.in/', 'how r u.............', 'assets/uploads/1434612416_s2.jpg', '0', '2015-06-18 12:56:56', '2015-06-18 07:26:56', '', b'1', ''),
(103, '', '36;43', 'https://www.google.co.in/', 'how r u.............', 'assets/uploads/1434612976_s2.jpg', '0', '2015-06-18 13:06:16', '2015-06-18 07:36:16', '', b'1', ''),
(104, '', '36;43', 'https://www.google.co.in/', 'how r u.............', 'assets/uploads/1434613273_s2.jpg', '0', '2015-06-18 13:11:13', '2015-06-18 07:41:13', '', b'1', ''),
(105, '', '43', 'http://www.sanwebe.com/2012/02/post-to-facebook-page-wall-using-php-graph', 'hiiiiiiiiiiiiii', 'assets/uploads/1434613586_s1.jpg', '0', '2015-06-18 13:16:26', '2015-06-18 07:46:26', '', b'1', ''),
(106, '', '36;38;43', 'http://www.dsfsdfsdf.com', 'fsdf', 'assets/uploads/1434613823_s1.jpg', '0', '2015-06-18 13:20:23', '2015-06-18 07:50:23', '', b'1', ''),
(107, '', '36;38;43', 'http://www.dsfsdfsdf.com', 'fsdf', 'assets/uploads/1434613848_s1.jpg', '0', '2015-06-18 13:20:48', '2015-06-18 07:50:48', '', b'1', ''),
(108, '2', '43', 'http://www.dsfsdfsdf.com', 'gfdfg', 'assets/uploads/1434613869_r4.jpg', '0', '2015-06-18 13:21:09', '2015-06-18 07:51:09', '', b'0', ''),
(109, '', '38;43', 'http://www.dsfsdfsdf.com', 'fghfgh', 'assets/uploads/1434613941_r2.jpg', '0', '2015-06-18 13:22:21', '2015-06-18 07:52:21', '', b'1', ''),
(110, '', '43', 'http://www.dsfsdfsdf.com', 'gjfgjhfgjh', 'assets/uploads/1434614234_r3.jpg', '0', '2015-06-18 13:27:14', '2015-06-18 07:57:14', '', b'1', ''),
(111, '1;2', '36', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', 'ab', 'assets/uploads/1434614401_', '0', '2015-06-18 13:30:01', '2015-06-18 08:00:01', '2015-06-01 13:29:53', b'0', 'Yes'),
(112, '1', '43', 'http://www.jkscholarship.in', 'ab', 'assets/uploads/1434614620_', '0', '2015-06-18 13:33:40', '2015-06-18 08:03:40', '2015-06-18 13:33:32', b'0', 'Yes'),
(113, '1', '43', 'http://www.jkscholarship.in', '', 'assets/uploads/1434615155_', '0', '2015-06-18 13:42:35', '2015-06-18 08:12:35', '2015-06-18 13:43:37', b'0', 'Yes'),
(114, '', '43', 'http://www.jkscholarship.in', '', 'assets/uploads/1434615227_', '0', '2015-06-18 13:43:47', '2015-06-18 08:13:47', '', b'1', ''),
(115, '', '43', 'http://www.jkscholarship.in', 'hi please see this', 'assets/uploads/1434615445_', '0', '2015-06-18 13:47:25', '2015-06-18 08:17:25', '2015-06-18 13:48:00', b'1', 'Yes'),
(116, '1;2;3;4;5', '', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', 'abc', 'assets/uploads/1434616176_textbanner.jpg', '0', '2015-06-18 13:59:36', '2015-06-18 08:29:36', '2015-06-18 14:00:26', b'0', 'Yes'),
(117, '', '43', 'http://www.dsfsdfsdf.com', '', 'assets/uploads/1434616229_', '0', '2015-06-18 14:00:29', '2015-06-18 08:30:29', '2015-06-18 14:05:10', b'1', 'Yes'),
(118, '', '43', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', '', 'assets/uploads/1434616257_', '0', '2015-06-18 14:00:57', '2015-06-18 08:30:57', '', b'1', ''),
(119, '', '43', 'http://www.jkscholarship.in', '', 'assets/uploads/1434616806_', '0', '2015-06-18 14:10:06', '2015-06-18 08:40:06', '', b'1', ''),
(120, '1;2;3', '43', 'http://www.dsfsdfsdf.com', '', 'assets/uploads/1434616950_', '0', '2015-06-18 14:12:30', '2015-06-18 08:42:30', '', b'0', ''),
(121, '', '43', 'https://developers.facebook.com/products/ads/', '', 'assets/uploads/1434617944_', '0', '2015-06-18 14:29:04', '2015-06-18 08:59:04', '', b'1', ''),
(122, '', '45', 'https://developers.facebook.com/docs/ads-for-websites', '', 'assets/uploads/1434618187_', '0', '2015-06-18 14:33:07', '2015-06-18 09:03:07', '', b'1', ''),
(123, '', '43', 'http://php.net/manual/en/tutorial.php', '', 'assets/uploads/1434618234_', '0', '2015-06-18 14:33:54', '2015-06-18 09:03:54', '', b'1', ''),
(124, '1;2;3;4;5', '', 'https://docs.angularjs.org/tutorial/step_01', '', 'assets/uploads/1434618391_', '0', '2015-06-18 14:36:31', '2015-06-18 09:06:31', '', b'1', ''),
(125, '1;2;3;4;5', '', 'https://docs.angularjs.org/tutorial/step_01', '', 'assets/uploads/1434618430_', '0', '2015-06-18 14:37:10', '2015-06-18 09:07:10', '', b'1', ''),
(126, '', '38', 'https://wordpress.org/support/topic/plugin-site-creation-wizard-problem-in-getting-started', 'hi see this......', 'assets/uploads/1434618527_', '0', '2015-06-18 14:38:47', '2015-06-18 09:08:47', '', b'1', ''),
(127, '', '38', 'http://www.rtu.ac.in/RTU/', 'hiii ', 'assets/uploads/1434618644_', '0', '2015-06-18 14:40:44', '2015-06-18 09:10:44', '', b'1', ''),
(128, '', '', 'http://www.ibrinfotech.com/', '', 'assets/uploads/1434619599_', '1', '2015-06-18 14:56:39', '2015-06-18 09:26:39', '', b'1', ''),
(129, '', '38', 'http://www.magzim.com', '', 'assets/uploads/1434659934_', '0', '2015-06-19 02:08:54', '2015-06-18 20:38:54', '', b'1', ''),
(130, '', '38', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', '', 'assets/uploads/1434660079_', '0', '2015-06-19 02:11:19', '2015-06-18 20:41:19', '', b'1', ''),
(131, '', '47', 'http://www,magzim.com', '', 'assets/uploads/1434695488_', '0', '2015-06-19 12:01:28', '2015-06-19 06:31:28', '', b'1', ''),
(132, '1', '', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701489_banner.jpg', '0', '2015-06-19 13:41:29', '2015-06-19 08:11:29', '', b'1', ''),
(133, '1', '', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701631_banner.jpg', '0', '2015-06-19 13:43:51', '2015-06-19 08:13:51', '', b'1', ''),
(134, '1', '43', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701664_', '0', '2015-06-19 13:44:24', '2015-06-19 08:14:24', '', b'0', ''),
(135, '', '43', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701726_', '0', '2015-06-19 13:45:26', '2015-06-19 08:15:26', '', b'1', ''),
(136, '', '43', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701842_banner.jpg', '0', '2015-06-19 13:47:22', '2015-06-19 08:17:22', '', b'1', ''),
(137, '', '43', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701853_', '0', '2015-06-19 13:47:33', '2015-06-19 08:17:33', '', b'1', ''),
(138, '', '43', 'http://www.jabong.com/kids/?source=topnav_kids', '', 'assets/uploads/1434701941_', '0', '2015-06-19 13:49:01', '2015-06-19 08:19:01', '', b'1', ''),
(139, '', '43', 'http://www.jabong.com/kids/?source=topnav_kids', '', 'assets/uploads/1434701968_', '0', '2015-06-19 13:49:28', '2015-06-19 08:19:28', '', b'1', ''),
(140, '', '43', 'http://www.jabong.com/kids/?source=topnav_kids', 'hiiiiiiiiii', 'assets/uploads/1434702029_banner.jpg', '0', '2015-06-19 13:50:29', '2015-06-19 08:20:29', '', b'1', ''),
(141, '', '39', 'http://example.com', 'test on server', 'assets/uploads/1434706494_11233771_717705995051325_7723230737118797739_n.jpg', '0', '2015-06-19 15:04:54', '2015-06-19 09:34:54', '', b'1', ''),
(142, '', '39', 'http://example.com', 'test on server', 'assets/uploads/1434706669_11233771_717705995051325_7723230737118797739_n.jpg', '0', '2015-06-19 15:07:49', '2015-06-19 09:37:49', '', b'1', ''),
(143, '', '39', 'http://example.com', 'test on server', 'assets/uploads/1434706754_11233771_717705995051325_7723230737118797739_n.jpg', '0', '2015-06-19 15:09:14', '2015-06-19 09:39:14', '', b'1', ''),
(144, '', '38', 'http://example.com', 'test on server', 'assets/uploads/1434706793_11233771_717705995051325_7723230737118797739_n.jpg', '0', '2015-06-19 15:09:53', '2015-06-19 09:39:53', '', b'1', ''),
(145, '', '43', 'http://www.jabong.com/nbastore/?source=topnav_sports', 'hii check this...', 'assets/uploads/1434707604_banner.jpg', '0', '2015-06-19 15:23:24', '2015-06-19 09:53:24', '', b'1', ''),
(146, '', '43', 'http://www.jabong.com/nbastore/?source=topnav_sports', 'hii check this...', 'assets/uploads/1434707606_banner.jpg', '0', '2015-06-19 15:23:26', '2015-06-19 09:53:26', '', b'1', ''),
(147, '', '38', 'http://www.jabong.com/nbastore/?source=topnav_sports', 'hey...', 'assets/uploads/1434707691_banner.jpg', '1', '2015-06-19 15:24:51', '2015-06-19 09:54:51', '', b'1', ''),
(148, '', '43', 'http://www.magzim.com', '', NULL, '0', '2015-06-19 16:03:14', '2015-06-19 10:33:14', '', b'1', ''),
(149, '', '43', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', '', NULL, '0', '2015-06-19 16:04:12', '2015-06-19 10:34:12', '', b'1', ''),
(150, '', '', 'http://www.magzim.com', '', NULL, '1', '2015-06-19 16:05:30', '2015-06-19 10:35:30', '2015-06-19 16:10:59', b'1', 'Yes'),
(151, '', '43', 'http://www.storypick.com', '', NULL, '0', '2015-06-19 16:17:01', '2015-06-19 10:47:01', '2015-06-19 16:18:14', b'1', 'Yes'),
(152, '', '47', 'http://www.storypick.com', '', NULL, '0', '2015-06-19 16:20:12', '2015-06-19 10:50:12', '2015-06-19 16:29:59', b'1', 'Yes'),
(153, '', '39', 'http://test-test.com', 'post with schedule', 'assets/uploads/1434711959_11209760_717403471748244_452279927463402649_n.jpg', '0', '2015-06-19 16:35:59', '2015-06-19 11:05:59', '2015-06-19 16:40:23', b'1', 'Yes'),
(154, '', '47', 'http://www.scoopwhoop.com', '', NULL, '0', '2015-06-19 16:41:31', '2015-06-19 11:11:31', '2015-06-19 16:44:15', b'1', 'Yes'),
(155, '', '38', 'http://test.com', 'sfgs', NULL, '0', '2015-06-19 16:42:56', '2015-06-19 11:12:56', '', b'1', ''),
(156, '1', '', 'http://test1.com', 'dddd', 'assets/uploads/1434712420_18604_717403568414901_3417955052131641437_n.jpg', '0', '2015-06-19 16:43:40', '2015-06-19 11:13:40', '', b'1', ''),
(157, '1', '', 'http://test.com', 'ghjhjkhjhkjhjhj', NULL, '0', '2015-06-19 17:20:56', '2015-06-19 11:50:56', '', b'1', ''),
(158, '', '38', 'http://test.com', 'ghgfh', NULL, '0', '2015-06-19 17:21:12', '2015-06-19 11:51:12', '', b'1', ''),
(159, '1', '', 'http://test.com', 'rgdfg', NULL, '0', '2015-06-19 17:24:19', '2015-06-19 11:54:19', '', b'1', ''),
(160, '1', '', 'http://www.flipkart.com/asus-zenfone-2/p/itme6pvuadqxxjfc?pid=MOBE6CG6ZEJKCYGM&cmpid=content_mobile_8965229628_gmc_pla&tgi=sem%2C1%2CG%2', 'helloooooo', 'assets/uploads/1434714956_banner.jpg', '0', '2015-06-19 17:25:56', '2015-06-19 11:55:56', '', b'1', ''),
(161, '', '38', 'http://www.flipkart.com/asus-zenfone-2/p/itme6pvuadqxxjfc?pid=MOBE6CG6ZEJKCYGM&cmpid=content_mobile_8965229628_gmc_pla&tgi=sem%2C1%2CG%2C112140', 'hiiiiiiiiiii', 'assets/uploads/1434715424_banner.jpg', '0', '2015-06-19 17:33:44', '2015-06-19 12:03:44', '', b'1', ''),
(162, '1', '', 'http://www.amazon.in/?tag=googhydrabk-21&ref_=pd_mn_ABKror1112', 'hello......', 'assets/uploads/1434715841_banner.jpg', '0', '2015-06-19 17:40:41', '2015-06-19 12:10:41', '', b'1', ''),
(163, '', '38;39', 'http://test.com', 'sgfsg', NULL, '0', '2015-06-19 17:49:57', '2015-06-19 12:19:57', '', b'1', ''),
(164, '1', '', 'http://test_t.com', 'fgdgdg', NULL, '0', '2015-06-19 17:51:03', '2015-06-19 12:21:03', '', b'1', ''),
(165, '1', '', 'http://do-test.com', 'do test posting', NULL, '0', '2015-06-19 18:06:25', '2015-06-19 12:36:25', '', b'1', ''),
(166, '1', '', 'http://test-do.com', 'test by do', NULL, '0', '2015-06-19 18:06:46', '2015-06-19 12:36:46', '', b'1', ''),
(167, '1', '', 'http://test-do.com', 'to test', NULL, '0', '2015-06-19 18:08:21', '2015-06-19 12:38:21', '', b'1', ''),
(168, '1', '', 'http://test-do.com', 'to test', NULL, '0', '2015-06-19 18:09:20', '2015-06-19 12:39:20', '', b'1', ''),
(169, '1', '', 'http://test-do.com', 'jghjdkhjghfdjkhgjh', NULL, '0', '2015-06-19 18:10:00', '2015-06-19 12:40:00', '', b'1', ''),
(170, '1', '', 'http://test-do.com', 'jghjdkhjghfdjkhgjh', NULL, '0', '2015-06-19 18:10:30', '2015-06-19 12:40:30', '', b'1', ''),
(171, '1', '', 'http://do-test.com', 'cetegory posting', NULL, '0', '2015-06-19 18:11:03', '2015-06-19 12:41:03', '', b'1', ''),
(172, '1', '', 'http://www.amazon.com/gift-cards/b/ref=nav_cs_gc?ie=UTF8&node=2238192011', 'test', 'assets/uploads/1434777858_Screenshot (19).png', '0', '2015-06-20 10:54:18', '2015-06-20 05:24:18', '', b'1', ''),
(173, '', '43', 'http://www.amazon.com/gift-cards/b/ref=nav_cs_gc?ie=UTF8&node=2238192011', 'asdd', 'assets/uploads/1434778134_Screenshot (19).png', '0', '2015-06-20 10:58:54', '2015-06-20 05:28:54', '', b'1', ''),
(174, '', '44', 'http://www.snapdeal.com/offers/deal-of-the-day?HookID=1', 'as', 'assets/uploads/1434778288_Mind-Music.jpg', '0', '2015-06-20 11:01:28', '2015-06-20 05:31:28', '', b'1', ''),
(175, '1', '43', 'http://www.snapdeal.com/offers/deal-of-the-day?HookID=1', 'aghhhh', 'assets/uploads/1434778437_Mind-Music.jpg', '0', '2015-06-20 11:03:57', '2015-06-20 05:33:57', '2015-06-20 11:04:00', b'0', 'Yes'),
(176, '1', '43', 'http://www.snapdeal.com/offers/deal-of-the-day?HookID=1', 'aghhhh', 'assets/uploads/1434778440_Mind-Music.jpg', '0', '2015-06-20 11:04:00', '2015-06-20 05:34:00', '2015-06-20 11:04:00', b'0', 'Yes'),
(177, '1', '', 'http://test-ya.com', 'dfds', 'assets/uploads/1434778491_Screenshot__19_.png', '0', '2015-06-20 11:04:51', '2015-06-20 05:34:51', '', b'1', ''),
(178, '1', '', 'http://www.dsfsdfsdf.com', 'asdf', 'assets/uploads/1434778550_Mind-Music.jpg', '0', '2015-06-20 11:05:50', '2015-06-20 05:35:50', '', b'1', ''),
(179, '1', '', 'http://www.flipkart.com/asus-zenfone-2/p/itme6pvuadqxxjfc?pid=MOBE6CG6ZEJKCYGM&cmpid=content_mobile_8965229628_gmc_pla&tgi=sem%2C1%2CG%2C11214', 'sh', 'assets/uploads/1434778747_Mind-Music.jpg', '0', '2015-06-20 11:09:07', '2015-06-20 05:39:07', '2015-06-20 11:09:00', b'0', 'Yes'),
(180, '1', '', 'http://test-testt.com', 'test-test-test', NULL, '0', '2015-06-20 11:09:33', '2015-06-20 05:39:33', '', b'1', ''),
(181, '1', '', 'http://www.flipkart.com/asus-zenfone-2/p/itme6pvuadqxxjfc?pid=MOBE6CG6ZEJKCYGM&cmpid=content_mobile_8965229628_gmc_pla&tgi=sem%2C1%2CG%2C11214', 'sh', 'assets/uploads/1434778846_Mind-Music.jpg', '1', '2015-06-20 11:10:46', '2015-06-20 05:40:46', '2015-06-20 11:11:00', b'0', 'Yes'),
(182, '', '38', 'http://www.flipkart.com/asus-zenfone-2/p/itme6pvuadqxxjfc?pid=MOBE6CG6ZEJKCYGM&cmpid=content_mobile_8965229628_gmc_pla&tgi=sem%2C1%2CG%2C11214', 'as', 'assets/uploads/1434779979_screenshot-1.jpg', '1', '2015-06-20 11:29:39', '2015-06-20 05:59:39', '', b'1', ''),
(183, '1', '', 'https://sites.google.com/site/shoestyleambur/', 'can', 'assets/uploads/1434780636_Mind-Music.jpg', '0', '2015-06-20 11:40:36', '2015-06-20 06:10:36', '', b'1', ''),
(184, '1', '', 'https://www.facebook.com/', 'hjhgjh', NULL, '0', '2015-06-20 12:25:42', '2015-06-20 06:55:42', '', b'1', ''),
(185, '1', '', 'https://www.facebook.com/', 'facebook sharing', NULL, '0', '2015-06-20 12:27:14', '2015-06-20 06:57:14', '', b'1', ''),
(186, '1', '44', 'http://www.amazon.in/Adidas-Unisex-Scarlet-White-Sneakers/dp/B00V4KVKZY?tag=googinhydr18418-21&kpid=B00V4KVKZY&tag=googinkenshoo-21&ascsubtag=6d7e', 'hiiiiii', 'assets/uploads/1434783459_custom-product-search.png', '0', '2015-06-20 12:27:39', '2015-06-20 06:57:39', '', b'1', ''),
(187, '1', '', 'http://do-test.com', 'fgdhh', NULL, '0', '2015-06-20 12:29:16', '2015-06-20 06:59:16', '', b'1', ''),
(188, '1', '', 'https://ftploy.com/', 'dhghfh', NULL, '0', '2015-06-20 12:30:06', '2015-06-20 07:00:06', '', b'1', ''),
(189, '1', '', 'http://www.amazon.in/Adidas-Unisex-Scarlet-White-Sneakers/dp/B00V4KVKZY?tag=googinhydr18418-21&kpid=B00V4KVKZY&tag=googinkenshoo-21&ascsubtag=6d7e', 'ssssssss', NULL, '0', '2015-06-20 12:32:31', '2015-06-20 07:02:31', '', b'1', ''),
(190, '1', '', 'http://www.amazon.in/Adidas-Unisex-Scarlet-White-Sneakers/dp/B00V4KVKZY?tag=googinhydr18418-21&kpid=B00V4KVKZY&tag=googinkenshoo-21&ascsubtag=6d7e', 'sssssss', NULL, '0', '2015-06-20 12:55:57', '2015-06-20 07:25:57', '', b'1', ''),
(191, '', '47', 'http://www.magzim.com', 'World’s Most Costliest Earthquakes', 'assets/uploads/1434797490_BruceWillis_Friends.png', '0', '2015-06-20 16:21:30', '2015-06-20 10:51:30', '2015-06-20 16:23:59', b'1', 'Yes'),
(192, '', '47', 'http://www.magzim.com', 'World’s Most Costliest Earthquakes!123 test', 'assets/uploads/1434797540_Friends.jpg', '0', '2015-06-20 16:22:20', '2015-06-20 10:52:20', '', b'1', ''),
(193, '', '47', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', 'World’s Most Costliest EarthquakesWorld’s Most Costliest Earthquakes', 'assets/uploads/1434797731_friends_Ross_Rachel.jpeg', '0', '2015-06-20 16:25:31', '2015-06-20 10:55:31', '', b'1', ''),
(194, '', '47', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', 'World’s Most Costliest EarthquakesWorld’s Most Costliest EarthquakesWorld’s Most Costliest EarthquakesWorld’s', 'assets/uploads/1434797958_friends_theme_song.jpg', '0', '2015-06-20 16:29:18', '2015-06-20 10:59:18', '', b'1', ''),
(195, '', '47', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', 'World’s Most Costliest EarthquakesWorld’s Most Costliest Earthquakes World’s Most Costliest EarthquakesWorld’s Most Costliest Earthquakes ', 'assets/uploads/1434798103_friends-centralperk.jpg', '0', '2015-06-20 16:31:43', '2015-06-20 11:01:43', '', b'1', ''),
(196, '', '47', 'http://www.storypick.com', 'World’s Most Costliest EarthquakesWorld’s Most Costliest Earthquakes ', 'assets/uploads/1434798233_friends_simpsons.jpg', '0', '2015-06-20 16:33:53', '2015-06-20 11:03:53', '', b'1', ''),
(197, '8', '', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', 'fasd', NULL, '0', '2015-06-22 14:27:34', '2015-06-22 08:57:34', '2015-06-23 14:27:23', b'0', 'Yes'),
(198, '1', '', 'http://www.graphicproducts.com/tutorials/five-s/', 'hi whats up...', 'assets/uploads/1435040368_Mind-Music.jpg', '0', '2015-06-23 11:49:28', '2015-06-23 06:19:28', '', b'1', ''),
(199, '', '42', 'http://www.graphicproducts.com/tutorials/five-s/', 'hiiiii', NULL, '0', '2015-06-23 11:51:57', '2015-06-23 06:21:57', '', b'1', ''),
(200, '', '45', 'http://www.graphicproducts.com/tutorials/five-s/', 'asdffg.....', NULL, '0', '2015-06-23 11:52:52', '2015-06-23 06:22:52', '', b'1', ''),
(201, '', '45', 'http://www.graphicproducts.com/tutorials/five-s/', 'asdfffg', 'assets/uploads/1435040747_Mind-Music.jpg', '0', '2015-06-23 11:55:47', '2015-06-23 06:25:48', '', b'1', ''),
(202, '', '45', 'http://www.jabong.com/men/shoes/Adidas/', 'as........', 'assets/uploads/1435040893_Mind-Music.jpg', '0', '2015-06-23 11:58:13', '2015-06-23 06:28:13', '', b'1', ''),
(203, '1', '', 'http:/ asddd.com', 'qq', NULL, '0', '2015-06-23 12:02:16', '2015-06-23 06:32:16', '', b'1', ''),
(204, '1', '', 'http://djvicky.in/index.xhtml', 'qq', NULL, '0', '2015-06-23 12:02:55', '2015-06-23 06:32:55', '', b'1', ''),
(205, '1', '', 'http://www.sdjaingirlscollege.com/UI/Aboutus.aspx?Page=aboutus', 'assddd', NULL, '0', '2015-06-23 12:05:16', '2015-06-23 06:35:16', '', b'1', ''),
(206, '1', '', 'http://www.amazon.in/?tag=googhydrabk-21&ref_=pd_mn_ABKror1112', 'assd', NULL, '0', '2015-06-23 12:08:17', '2015-06-23 06:38:17', '', b'1', '');

-- --------------------------------------------------------

--
-- Table structure for table `TwitterUpdate`
--

CREATE TABLE IF NOT EXISTS `TwitterUpdate` (
  `user_id` int(11) NOT NULL,
  `uname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_token` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_token_secret` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TwitterUpdate`
--

INSERT INTO `TwitterUpdate` (`user_id`, `uname`, `name`, `oauth_token`, `oauth_token_secret`) VALUES
(1, 'sulbha14', 'sulbha shrivastava', '456543101-lojBU0wRymm2RJfMsTwNE5spEzYjFxPlOVnX99ok', 'POaajMRp5EfkJr4TUqlITFolf3CG89r4JgTgAQAFcIbW4'),
(2, 'aminkhan1990', 'aminkhan', '342306792-oZIsWVlPTgDZp7DtJvoP710mCNLpgdQm9HcIeFIY', 'H2gho1uzrStKQgfqaSym7oMW0ralZhRaM1bFkEUgo1KbB'),
(3, 'sulbha14shri', 'sulbha', '3223150730-3ts1YVmdjpGk1iivMapooe4fZmMWb92KvnwmKjO', 'A4CCdrqyViRjcyIlKZzldLW3RDRY0Qq5OOiPj4l6bX5hB');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `social_id` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL COMMENT '0 for delete , 1 not delete',
  `type` enum('1','2') NOT NULL DEFAULT '2' COMMENT '1 for admin type , 2 for user',
  `activation_token` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `login_token` text,
  `login_token_secreat` text,
  `outh_provider` varchar(255) DEFAULT NULL,
  `referal_link` varchar(255) DEFAULT NULL,
  `percent_cut` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ac_holder_name` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `ifsc` varchar(255) DEFAULT NULL,
  `swift_bic` varchar(255) DEFAULT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `twitter_id` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `social_id`, `firstname`, `middlename`, `lastname`, `email`, `password`, `gender`, `is_deleted`, `type`, `activation_token`, `active`, `login_token`, `login_token_secreat`, `outh_provider`, `referal_link`, `percent_cut`, `create_date`, `update_date`, `ac_holder_name`, `bank_name`, `ifsc`, `swift_bic`, `account_no`, `paypal_email`, `twitter_id`, `facebook_id`) VALUES
(1, '0', 'Admin', NULL, NULL, 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', 'male', '0', '1', '', b'0', '', NULL, '', NULL, '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, '456543101', 'sulbha', '', 'sulbha14', '', '', '', '0', '2', '456543101', b'0', '456543101-XF1NM0cpb2lFICzfUBy7U4g4QMixfyi907tJeclf', '723LPIGmtVjbDCc1F4nmbBmmIQNoiKLsrjqTGZWh0vwKk', 'twitter', '51_sulbha14', NULL, '2015-06-25 11:24:19', '2015-06-25 05:54:19', 'sulbha shrivastava', 'SBI', 'gjg23423422', 'gsdfsdf dfg', '234234234 523', 'sulbha@gmail.com', 'sulbha14', NULL),
(52, '102258373449386', 'Elizabeth', NULL, 'Baoescu', '', '', 'female', '0', '2', '102258373449386', b'0', 'CAAXgiZCZAWNzIBAO1ZBvwZCVZBTgXlgZCFZCJaLVvGyyZB25ef5SkwE2RGXXZA76X1FBbZCF1lSpUJ7JL12ZAMIyYuhOdw92ZBtmdSCjry8lFqn5lOavULA3IBrisY5KBNQpZBF4JHyzQvRY9OfFwzYEsDwAmp1u1ZAOH9IDR2vgBhuwC4SK5Nl5QdcUEfWHLwVJZCBvq31r6ZCJpvYoIOjeAb03KdUS', NULL, 'facebook', '52_Elizabeth', NULL, '2015-06-25 11:31:49', '2015-06-25 06:01:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(53, '102613803409576', 'Sulbha', NULL, 'Shrivastava', '', '', 'female', '0', '2', '102613803409576', b'0', 'CAAXgiZCZAWNzIBAHACAZAOEfqDS2aVBhw4LtiDPlFK6I6fs2uUBe1xsZAbSVzdpbX5IYFDszoOk7lsDMibjR7RzGFGuZCckqRdkQjqkYJgZCkQMhfqZCBFey9coZCp1vAYpFv0Yy0dAveeHY8ZCt4kegOwrltolFiX5B3jwebnOZC3WZBUkXqilGg9TOS5ZClokSIqkLZBiLE4hKArMueoSLdxsD7', NULL, 'facebook', '53_Sulbha', NULL, '2015-06-25 11:37:37', '2015-06-25 06:07:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, ''),
(54, '3223150730', 'sulbha14shri', NULL, '', '', '', '', '0', '2', '3223150730', b'0', '3223150730-k5RMyTczCIbt4vCdGgbU5qKZY4zNvxDUskw1ASH', '3D2CXQ1fLlLrpyUezJvsGLRd8XJc0w8P3nKHi7GzxqA2I', 'twitter', '54_sulbha14shri', NULL, '2015-06-25 15:41:00', '2015-06-25 10:11:00', NULL, NULL, NULL, NULL, NULL, NULL, 'sulbha14shri', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_25june`
--

CREATE TABLE IF NOT EXISTS `user_25june` (
  `id` int(11) NOT NULL,
  `social_id` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL COMMENT '0 for delete , 1 not delete',
  `type` enum('1','2') NOT NULL DEFAULT '2' COMMENT '1 for admin type , 2 for user',
  `activation_token` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `login_token` text,
  `login_token_secreat` text,
  `outh_provider` varchar(255) DEFAULT NULL,
  `referal_link` varchar(255) DEFAULT NULL,
  `percent_cut` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_25june`
--

INSERT INTO `user_25june` (`id`, `social_id`, `firstname`, `middlename`, `lastname`, `email`, `password`, `gender`, `is_deleted`, `type`, `activation_token`, `active`, `login_token`, `login_token_secreat`, `outh_provider`, `referal_link`, `percent_cut`, `create_date`, `update_date`) VALUES
(1, '0', 'Admin', 'aa', 'aa', 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', 'male', '0', '1', '', b'0', '', NULL, '', NULL, '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '103406843331677', 'James', NULL, 'Putnamstein', NULL, '', 'male', '0', '2', '103406843331677', b'0', 'CAAWrpVdes54BAAIqlohTviky8bJZBrmj62dEV4d5RMbrrDuiBKhg659kgvZC46wzzCbJEdLSP0a92D8gANFZBzUcWeZAVmq13Dx8Y4c6z39vMKdAGC5ZA0jqhTKFu7wmPTuZACTz9KQvVEQon7T3xxNUmRTOydSdUAqdMK9dzHw1l3XYJRuqXSWtZAlBE7DgfZBjtXFpHvhFYA9f0ZBn1khSt', NULL, 'facebook', NULL, '30', '2015-06-05 15:20:16', '2015-06-05 09:50:16'),
(38, '3223150730', 'sulbha14shri', NULL, '', 'sulbha14shri', '', '', '0', '2', '3223150730', b'0', '3223150730-k5RMyTczCIbt4vCdGgbU5qKZY4zNvxDUskw1ASH', '3D2CXQ1fLlLrpyUezJvsGLRd8XJc0w8P3nKHi7GzxqA2I', 'twitter', '38_sulbha14shri', '10', '2015-06-12 17:30:18', '2015-06-12 12:00:18'),
(39, '102613803409576', 'Sulbha', NULL, 'Shrivastava', NULL, '', 'female', '0', '2', '102613803409576', b'0', 'CAAXgiZCZAWNzIBAAuWQBPVMGgPnLKFqPtL2wyMgbEBucnFRr98oQnb0ins5XbKiY7CViv4nJEgmRCpV5mnSboHtG6FhpusogPyFl6WBEFa2PpWZCUGB99qWWn3gmUL0hIdH9lPfFV6HvU8ydjSmJFPQnHZAwSTB1wvMDh2VHQimNupBARkYOq7cg1ubGQCZA42ekT7r8TGo4h1PdLN4TQ', NULL, 'facebook', '39_Sulbha', '25', '2015-06-12 18:40:00', '2015-06-12 13:10:00'),
(42, '1443245619329735', 'Tom', NULL, 'Narayananson', '', '', 'male', '0', '2', '1443245619329735', b'0', 'CAAXgiZCZAWNzIBAPqUZBPGw1Py12YmqKwM9jmf4SGR4vRyZB8BU04ZAfMSRO4TqXdVX5CW6ZBZBT47ibTdQMJZAi61WH7nIZCJ23AZCPXPThEKW4u8yS514EyhX9p4F480SZAw0tZAIXjVZCWQDa8MZCYTqL9R3NziAGNtSTPKVkRTiYIz7YVEyaaX7TugVJQb8SnNuNeD5q5dKWNuqOwkVOsZBVGZAx', NULL, 'facebook', '42_Tom', '10', '2015-06-12 19:01:50', '2015-06-12 13:31:50'),
(43, '102258373449386', 'Elizabeth', NULL, 'Baoescu', '', '', 'female', '0', '2', '102258373449386', b'0', 'CAAXgiZCZAWNzIBAGanP9EDY9zBk74OTLwDZCOPrs760lGg2CB8UO3tAA6VSDrcZBE12T0wjWiHEwCgMyHgmgxMbDlzEXsnQCsHMBZAQDE1o6FNlN4rrrIf5W41ZBTmR5EOZCntzNt0TIrEvrZCZCCgZCJRQq1NlAnyuYvSZCvYyqD5XNofZBZAESGWZC4z4ZBoTtTuxespDRzGZCNgBgH7iHAgGSxxXk', NULL, 'facebook', '43_Elizabeth', '20', '2015-06-12 19:03:07', '2015-06-12 13:33:07'),
(44, '456543101', 'sulbha14', NULL, '', 'sulbha14', '', '', '0', '2', '456543101', b'0', '456543101-XF1NM0cpb2lFICzfUBy7U4g4QMixfyi907tJeclf', '723LPIGmtVjbDCc1F4nmbBmmIQNoiKLsrjqTGZWh0vwKk', 'twitter', '44_sulbha14', '10', '2015-06-13 12:49:25', '2015-06-13 07:19:25'),
(45, '1424052347918674', 'Tom', NULL, 'Seligsteinman', '', '', 'male', '0', '2', '1424052347918674', b'0', 'CAAXgiZCZAWNzIBAOZARo89Ogc4jtUZCij1LU3YsnnBrbvzVlcbj9MR5mwfH0IMX8rbG0TrmZB562r4sTk1iktw0jyIkxFHuYRpfjIhXQsE2wRAz8gjtHM91eNy296KbJs9oxkvd0aS08JgQbu0mIUEfNvZA4ARD5i32SrH8ZCSHHuSZCkOz1gTiS4Ie60ZAE3sMFhPCGuBRFC7OxN48c0Xoxh', NULL, 'facebook', '45_Tom', '10', '2015-06-16 13:26:40', '2015-06-16 07:56:40'),
(46, '3237800810', 'anshu_ibr', NULL, '', 'anshu_ibr', '', '', '0', '2', '3237800810', b'0', '3237800810-trLaGjZOCcFa0MBDJ9uXnFzbb7aPO2xIFWNB7Sz', 'YfWXutyuVoWswqxQBkuOkYpnhbxbd9UdFX5fZc9DCH1Q3', 'twitter', '46_anshu_ibr', '10', '2015-06-16 17:08:26', '2015-06-16 11:38:26'),
(47, '95621715', 'dewanishan', '', '', 'dewanishan', '', '', '0', '2', '95621715', b'0', '95621715-nlB9MtjYmU8QpfZVaQIIf3wppIg7KhqCLxKFl58TD', 'wyVvL4ARImTK6uQqSJmLxJzUISW70M1WYmYLVUKVNtt3W', 'twitter', '47_dewanishan', '10', '2015-06-19 11:58:09', '2015-06-19 06:28:09'),
(48, '1042906272', 'mihtra', NULL, '', 'mihtra', '', '', '0', '2', '1042906272', b'0', '1042906272-WMcsK4t5iQZikblmC61VvF2PGCXu1jHskSGLqq3', 'xUtsvZAhfxVp8nbjsUhQL6fRM5guVLuyoNkHp2ab2fDjp', 'twitter', '48_mihtra', '10', '2015-06-20 18:19:51', '2015-06-20 12:49:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_backup`
--

CREATE TABLE IF NOT EXISTS `user_backup` (
  `id` int(11) NOT NULL,
  `social_id` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL COMMENT '0 for delete , 1 not delete',
  `type` enum('1','2') NOT NULL DEFAULT '2' COMMENT '1 for admin type , 2 for user',
  `activation_token` varchar(255) NOT NULL,
  `active` bit(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_backup`
--

INSERT INTO `user_backup` (`id`, `social_id`, `firstname`, `middlename`, `lastname`, `email`, `password`, `gender`, `is_deleted`, `type`, `activation_token`, `active`, `create_date`, `update_date`) VALUES
(1, '0', 'Admin', 'Admin', 'Admin', 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', 'male', '0', '1', '', b'0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, '854531801308988', 'Sulbha', NULL, 'Shrivastava', 'sulbha2814@gmail.com', '', 'female', '0', '2', '854531801308988', b'0', '0000-00-00 00:00:00', '2015-05-24 13:56:24'),
(18, '102772173393739', 'Sulbha', NULL, 'Shrivastava', NULL, '', 'female', '0', '2', '102772173393739', b'0', '0000-00-00 00:00:00', '2015-05-24 14:01:37'),
(19, '827775913968820', 'Mohit', NULL, 'Niwadunge', 'mohit.niwadunge@gmail.com', '', 'male', '0', '2', '827775913968820', b'0', '0000-00-00 00:00:00', '2015-05-25 09:24:27'),
(20, '883151818410900', 'Anshu', NULL, 'Meghawat', 'anshumeghawat@gmail.com', '', 'female', '0', '2', '883151818410900', b'0', '0000-00-00 00:00:00', '2015-05-25 09:28:29'),
(21, '910384699035046', 'Er Amin', NULL, 'Khan', 'aminkhan.1990@yahoo.com', '', 'male', '0', '2', '910384699035046', b'0', '0000-00-00 00:00:00', '2015-05-25 10:31:06'),
(22, '3223150730', 'sulbha14shri', NULL, '', 'sulbha14shri', '', '', '0', '2', '3223150730', b'0', '0000-00-00 00:00:00', '2015-05-25 17:51:32'),
(23, '883978708330111', 'Pradeep', NULL, 'Sharma', 'sharma.pradeep1989@gmail.com', '', 'male', '0', '2', '883978708330111', b'0', '0000-00-00 00:00:00', '2015-05-26 10:29:24'),
(24, '342306792', 'aminkhan1990', NULL, '', 'aminkhan1990', '', '', '0', '2', '342306792', b'1', '0000-00-00 00:00:00', '2015-05-27 09:51:04'),
(25, '103750459962577', 'Sulbha', NULL, 'Shrivastava', NULL, '', 'female', '0', '2', '103750459962577', b'0', '0000-00-00 00:00:00', '2015-06-03 08:13:30'),
(26, '856601217768713', 'Sulbha', NULL, 'Shrivastava', 'sulbha2814@gmail.com', '', 'female', '0', '2', '856601217768713', b'0', '0000-00-00 00:00:00', '2015-06-03 09:39:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_category`
--

CREATE TABLE IF NOT EXISTS `user_category` (
  `user_category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_category`
--

INSERT INTO `user_category` (`user_category_id`, `user_id`, `category_id`, `created_date`, `updated_date`) VALUES
(2, 38, '1', '2015-06-12 13:58:39', '2015-06-12 08:28:39'),
(3, 43, '1', '2015-06-13 11:28:16', '2015-06-13 05:58:16'),
(4, 44, '1;2;3;4;5', '2015-06-13 12:49:57', '2015-06-13 07:19:57'),
(23, 45, '1;2;3', '2015-06-16 14:51:00', '2015-06-16 09:21:00'),
(24, 39, '1', '2015-06-17 22:57:35', '2015-06-17 17:27:35'),
(25, 47, '1;2;3', '2015-06-19 11:58:24', '2015-06-19 06:28:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `analytics_detail`
--
ALTER TABLE `analytics_detail`
  ADD PRIMARY KEY (`analytics_detail_id`);

--
-- Indexes for table `api_configration`
--
ALTER TABLE `api_configration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `paypal_details`
--
ALTER TABLE `paypal_details`
  ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `user_id_2` (`user_id`);

--
-- Indexes for table `paypal_transaction_details`
--
ALTER TABLE `paypal_transaction_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `share_link`
--
ALTER TABLE `share_link`
  ADD PRIMARY KEY (`sharelink_id`), ADD KEY `category_id` (`category_id`), ADD KEY `category_id_2` (`category_id`);

--
-- Indexes for table `TwitterUpdate`
--
ALTER TABLE `TwitterUpdate`
  ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `uname` (`uname`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_25june`
--
ALTER TABLE `user_25june`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_backup`
--
ALTER TABLE `user_backup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_category`
--
ALTER TABLE `user_category`
  ADD PRIMARY KEY (`user_category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `analytics_detail`
--
ALTER TABLE `analytics_detail`
  MODIFY `analytics_detail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `api_configration`
--
ALTER TABLE `api_configration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `paypal_details`
--
ALTER TABLE `paypal_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `paypal_transaction_details`
--
ALTER TABLE `paypal_transaction_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `share_link`
--
ALTER TABLE `share_link`
  MODIFY `sharelink_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=207;
--
-- AUTO_INCREMENT for table `TwitterUpdate`
--
ALTER TABLE `TwitterUpdate`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `user_25june`
--
ALTER TABLE `user_25june`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `user_backup`
--
ALTER TABLE `user_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `user_category`
--
ALTER TABLE `user_category`
  MODIFY `user_category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
