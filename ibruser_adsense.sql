-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 18, 2015 at 04:56 AM
-- Server version: 5.5.42-37.1
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ibruser_adsense`
--

-- --------------------------------------------------------

--
-- Table structure for table `analytics_detail`
--

CREATE TABLE IF NOT EXISTS `analytics_detail` (
  `analytics_detail_id` int(11) NOT NULL,
  `title` varchar(225) DEFAULT NULL,
  `service_email` varchar(255) NOT NULL,
  `key_filename` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `is_deleted` bit(1) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `analytics_detail`
--

INSERT INTO `analytics_detail` (`analytics_detail_id`, `title`, `service_email`, `key_filename`, `status`, `is_deleted`, `created_date`, `updated_date`) VALUES
(3, NULL, '539518501004-q7ljpvkjla1cv653c11u0uqvfrb0b9k2@developer.gserviceaccount.com', 'anlytics/1506181129/client_secrets.p12', 0, b'0', '2015-06-18 00:00:00', '2015-06-18 04:57:32'),
(5, NULL, 'nowayitshouldwork@gmail.com', 'anlytics/1506232023/mdm.cer', 0, b'1', '2015-06-23 20:23:54', '2015-06-23 14:53:54'),
(6, NULL, '1056283348460-jhfqnisf18buieffokrbnrfjer59lqbi@developer.gserviceaccount.com', 'anlytics/1506301808/Test Aff-3d305e20620d.p12', 0, b'1', '2015-06-30 18:08:42', '2015-06-30 12:38:42'),
(7, NULL, '539518501004-q7ljpvkjla1cv653c11u0uqvfrb0b9k2@developer.gserviceaccount.com', 'anlytics/1507031711/client_secrets.p12', 0, b'1', '2015-07-03 17:11:12', '2015-07-03 11:41:12'),
(8, NULL, 'asdc@gmail.com', 'anlytics/1507031820/Screenshot (19).png', 0, b'1', '2015-07-03 18:20:35', '2015-07-03 12:50:35'),
(9, NULL, '1056283348460-jhfqnisf18buieffokrbnrfjer59lqbi@developer.gserviceaccount.com', 'anlytics/1507031829/Test Aff-3d305e20620d.p12', 0, b'1', '2015-07-03 18:29:53', '2015-07-03 12:59:54'),
(10, NULL, '38854994151-4pnri1scco1tjchm6m9t5h0vrm3nrmch@developer.gserviceaccount.com', 'anlytics/1507031957/Adsense Dashboard-cb4a5f083d60.p12', 0, b'1', '2015-07-03 19:57:38', '2015-07-03 14:27:38'),
(11, NULL, '38854994151-4pnri1scco1tjchm6m9t5h0vrm3nrmch@developer.gserviceaccount.com', 'anlytics/1507031958/Adsense Dashboard-cb4a5f083d60.p12', 0, b'0', '2015-07-03 19:58:34', '2015-07-03 14:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `api_configration`
--

CREATE TABLE IF NOT EXISTS `api_configration` (
  `id` int(11) NOT NULL,
  `api_id` varchar(255) DEFAULT NULL,
  `app_secret` varchar(255) DEFAULT NULL,
  `permissions` varchar(255) DEFAULT NULL,
  `redirect_url` varchar(255) DEFAULT NULL,
  `service_provider` varchar(255) DEFAULT NULL,
  `is_deleted` bit(1) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `api_configration`
--

INSERT INTO `api_configration` (`id`, `api_id`, `app_secret`, `permissions`, `redirect_url`, `service_provider`, `is_deleted`, `created_date`, `updated_date`) VALUES
(1, '1596833473899878', 'b77acff8a209a6fa335f40854874bdad', 'public_profile, publish_actions ', 'http://216.172.184.121/~ibruser/adsense/index.php/user', 'Facebook', b'0', '2015-06-22 11:10:55', '2015-06-22 05:40:55'),
(6, '1y3e1UHZyWjuhd1iaTPWBg6fr', 'WedOfldMAaDDPlQCoZME89kve2oRcF9QPIdxvutjP3PfQCFJrG', '', '', 'Twitter', b'0', '2015-06-22 12:22:26', '2015-06-22 06:52:26');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `title`, `is_deleted`, `create_date`, `update_date`) VALUES
(1, 'abc 123', '1', '2015-05-22 07:23:37', '2015-05-21 11:34:38'),
(2, 'test123', '1', '2015-05-21 14:57:44', '2015-05-21 12:26:30'),
(3, 'test', '1', '2015-05-21 14:56:58', '2015-05-21 12:26:58'),
(4, 'zvxcvcx ', '1', '2015-05-23 10:06:45', '2015-05-23 09:36:45'),
(5, 'aaaa2', '1', '2015-06-08 18:07:23', '2015-06-08 12:32:15'),
(6, 'ans122', '1', '2015-06-19 15:30:50', '2015-06-19 10:00:50'),
(7, 'Foods', '1', '2015-06-22 14:24:14', '2015-06-22 08:54:14'),
(8, 'cloths', '1', '2015-06-22 14:27:06', '2015-06-22 08:57:06'),
(9, 'new CAT', '1', '2015-06-23 20:28:33', '2015-06-23 14:58:33'),
(10, 'qwre', '1', '2015-07-03 18:10:41', '2015-07-03 12:40:41'),
(11, 'fwd', '1', '2015-07-03 18:10:50', '2015-07-03 12:40:50'),
(12, 'ast', '1', '2015-07-08 15:07:52', '2015-07-08 09:37:52'),
(13, 'Entertainment ', '1', '2015-07-21 00:32:20', '2015-07-20 19:02:20'),
(14, 'Entertainment ', '0', '2015-07-21 00:33:14', '2015-07-20 19:03:14'),
(15, 'Travel ', '0', '2015-07-21 00:33:57', '2015-07-20 19:03:57'),
(16, 'Lifestyle', '0', '2015-07-21 00:34:08', '2015-07-20 19:04:08'),
(17, 'Science & Tech', '0', '2015-07-21 00:34:31', '2015-07-20 19:04:32'),
(18, 'Relationships', '0', '2015-07-21 00:35:17', '2015-07-20 19:05:17'),
(19, 'Bizzare', '0', '2015-07-21 00:35:34', '2015-07-20 19:05:34'),
(20, 'test category1', '1', '2015-07-23 17:08:28', '2015-07-23 11:38:28');

-- --------------------------------------------------------

--
-- Table structure for table `paypal_details`
--

CREATE TABLE IF NOT EXISTS `paypal_details` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `paypal_email` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paypal_details`
--

INSERT INTO `paypal_details` (`id`, `user_id`, `paypal_email`, `created_date`, `updated_date`) VALUES
(4, 22, 'sulbha@ibrinfotech.com', '2015-05-26 12:18:58', '2015-05-26 11:48:58'),
(13, 39, 'sulbha@paypal.com', '2015-06-19 16:32:21', '2015-06-19 11:02:21'),
(14, 48, 'munish.41@gmail.com', '2015-06-23 20:33:54', '2015-06-23 15:03:54'),
(15, 47, 'dewanishan@gmail.com', '0000-00-00 00:00:00', '2015-06-24 18:13:28'),
(16, 51, 'sulbha123@gmail.com', '0000-00-00 00:00:00', '2015-06-25 10:13:41');

-- --------------------------------------------------------

--
-- Table structure for table `paypal_transaction_details`
--

CREATE TABLE IF NOT EXISTS `paypal_transaction_details` (
  `id` int(11) NOT NULL,
  `mc_gross` varchar(255) DEFAULT NULL,
  `protection_eligibility` varchar(255) DEFAULT NULL,
  `payer_id` varchar(255) DEFAULT NULL,
  `tax` varchar(255) DEFAULT NULL,
  `payment_date` varchar(255) DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `charset` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `mc_fee` varchar(255) DEFAULT NULL,
  `notify_version` varchar(255) DEFAULT NULL,
  `custom` varchar(255) DEFAULT NULL,
  `payer_status` varchar(255) DEFAULT NULL,
  `business` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `payer_email` varchar(255) DEFAULT NULL,
  `verify_sign` varchar(255) DEFAULT NULL,
  `txn_id` varchar(255) DEFAULT NULL,
  `payment_type` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `receiver_email` varchar(255) DEFAULT NULL,
  `payment_fee` varchar(255) DEFAULT NULL,
  `receiver_id` varchar(255) DEFAULT NULL,
  `txn_type` varchar(255) DEFAULT NULL,
  `item_name` varchar(255) DEFAULT NULL,
  `mc_currency` varchar(255) DEFAULT NULL,
  `item_number` varchar(255) DEFAULT NULL,
  `residence_country` varchar(255) DEFAULT NULL,
  `test_ipn` varchar(255) DEFAULT NULL,
  `handling_amount` varchar(255) DEFAULT NULL,
  `transaction_subject` varchar(255) DEFAULT NULL,
  `payment_gross` varchar(255) DEFAULT NULL,
  `shipping` varchar(255) DEFAULT NULL,
  `auth` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `paypal_transaction_details`
--

INSERT INTO `paypal_transaction_details` (`id`, `mc_gross`, `protection_eligibility`, `payer_id`, `tax`, `payment_date`, `payment_status`, `charset`, `first_name`, `mc_fee`, `notify_version`, `custom`, `payer_status`, `business`, `quantity`, `payer_email`, `verify_sign`, `txn_id`, `payment_type`, `last_name`, `receiver_email`, `payment_fee`, `receiver_id`, `txn_type`, `item_name`, `mc_currency`, `item_number`, `residence_country`, `test_ipn`, `handling_amount`, `transaction_subject`, `payment_gross`, `shipping`, `auth`) VALUES
(1, '1.14', 'Ineligible', 'EHQ3QTNLKA7TE', '0.00', '00:22:56 Jun 29, 2015 PDT', 'Pending', 'utf-8', 'toufiq', NULL, '3.8', '', 'verified', NULL, '1', 'imodify021@gmail.com', 'AFcWxV21C7fd0v3bYYYRCpSSRl31A9ZEQelME1R3naIRLsfZ5aRR68ya', '102870160M087422V', 'instant', 'shaikh', 'sulbha@gmail.com', NULL, NULL, 'web_accept', 'sulbha  sulbha14', 'USD', '', 'US', '1', '0.00', '', '1.14', '0.00', 'AAB6rFWrX1L7BRassO-CBhx0U4A4qcfd27DhbZeHwd3BAp1ch2yxFQbnbXVzquKcFE9215i-qDV-xfzjfrqtAIQ'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '0.27', 'Ineligible', 'EHQ3QTNLKA7TE', '0.00', '06:14:36 Aug 06, 2015 PDT', 'Pending', 'utf-8', 'toufiq', NULL, '3.8', '', 'verified', NULL, '1', 'imodify021@gmail.com', 'AFcWxV21C7fd0v3bYYYRCpSSRl31A.nT8jHGCWs1m0KmC9bC.PjwCwjT', '41G44243NA689674E', 'instant', 'shaikh', 'evaibhaw@gmail.com', NULL, NULL, 'web_accept', 'QuotesDetail', 'USD', '', 'US', '1', '0.00', '', '0.27', '0.00', 'AV.cAZvUG8wEV-Q3-d0ckvScgXnWkrQpo4cBgIiGO6FR-tRO2IoED.3L5I6SepBeVh6H0Xhki8pZPMhJXgrdobw'),
(4, '0.27', 'Ineligible', 'EHQ3QTNLKA7TE', '0.00', '06:14:36 Aug 06, 2015 PDT', 'Pending', 'utf-8', 'toufiq', NULL, '3.8', '', 'verified', NULL, '1', 'imodify021@gmail.com', 'AFcWxV21C7fd0v3bYYYRCpSSRl31A.nT8jHGCWs1m0KmC9bC.PjwCwjT', '41G44243NA689674E', 'instant', 'shaikh', 'evaibhaw@gmail.com', NULL, NULL, 'web_accept', 'QuotesDetail', 'USD', '', 'US', '1', '0.00', '', '0.27', '0.00', 'AV.cAZvUG8wEV-Q3-d0ckvScgXnWkrQpo4cBgIiGO6FR-tRO2IoED.3L5I6SepBeVh6H0Xhki8pZPMhJXgrdobw'),
(5, '0.27', 'Ineligible', 'EHQ3QTNLKA7TE', '0.00', '06:17:31 Aug 06, 2015 PDT', 'Pending', 'utf-8', 'toufiq', NULL, '3.8', '', 'verified', NULL, '1', 'imodify021@gmail.com', 'AK7YCU0G3HzI.qJ2AlHbXicRdu2DAzPo7t4dv8Pv7zKGRHRZSf7LAB9Z', '7MS28147A7776243F', 'instant', 'shaikh', 'evaibhaw@gmail.com', NULL, NULL, 'web_accept', 'QuotesDetail', 'USD', '', 'US', '1', '0.00', '', '0.27', '0.00', 'AD5A5HdT9nyykSUE-LUli7FvAEUoYv6n6C186D3-R0IlH2Vq17ZtuqPDlaOOZ.xrTKF4XjKr7tlRTDUR8RXaFHQ'),
(6, '15.73', 'Ineligible', 'EHQ3QTNLKA7TE', '0.00', '06:23:48 Aug 06, 2015 PDT', 'Pending', 'utf-8', 'toufiq', NULL, '3.8', '', 'verified', NULL, '1', 'imodify021@gmail.com', 'AlRrain7X9kw7IhtXHVvnmF6HmQjAEl8ffZ-XlKhtFmr8VTW61sAxyX4', '0ET89866HS840161U', 'instant', 'shaikh', 'nickoberoi007@gmail.com', NULL, NULL, 'web_accept', 'Nishant   oberoi', 'USD', '', 'US', '1', '0.00', '', '15.73', '0.00', 'A7gzMQ57CKkEOCNMZfnhh327pPLuK-eX7hzs56wpQFet.N7a8DDZW6TgWS8P7BYyyIpD7jwJ9gpBCd73YwtK13w'),
(7, '0.77', 'Ineligible', 'TWUTDAWJVSWZE', '0.00', '02:44:02 Aug 08, 2015 PDT', 'Completed', 'windows-1252', 'ishan', '0.33', '3.8', '', 'verified', 'singh84a@gmail.com', '0', 'dewanishan@gmail.com', 'ACUe-E7Hjxmeel8FjYAtjnx-yjHAAaxJK-M0e-YDBgCPOYQz2Rn3wec5', '8D896317462521055', 'instant', 'dewan', 'singh84a@gmail.com', '0.33', '9VG42TVGNFWVL', 'web_accept', 'Magzimontherock', 'USD', '', 'IN', NULL, NULL, '', '0.77', NULL, 'A-mrWVal7f6jbkgrWa6qSpKsiT9q0L.pcs7z0slD6pNzLPGsUQ7qQBZ7bnDchIc87BaAVa7XVNxaPnxR5AqpBkA');

-- --------------------------------------------------------

--
-- Table structure for table `share_link`
--

CREATE TABLE IF NOT EXISTS `share_link` (
  `sharelink_id` int(11) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `content` text NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `is_deleted` int(22) NOT NULL DEFAULT '0',
  `error` text,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `scheduler_datetime` varchar(200) NOT NULL,
  `scheduler_status` bit(1) NOT NULL,
  `Scheduler_Set` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=488 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `share_link`
--

INSERT INTO `share_link` (`sharelink_id`, `category_id`, `user_id`, `link`, `content`, `image`, `is_deleted`, `error`, `create_date`, `update_date`, `scheduler_datetime`, `scheduler_status`, `Scheduler_Set`) VALUES
(19, '', '', 'http://dev.ibrinfotech.com/adsense/in', '', NULL, 2, NULL, '2015-05-26 14:51:33', '2015-05-26 14:21:33', '2015-05-28 17:00:19', b'1', 'Yes'),
(20, '', '0', 'http://www.dsfsdfsdf.com', '', NULL, 1, NULL, '2015-05-26 14:51:49', '2015-05-26 14:21:49', '2015-05-26 16:00:35', b'1', 'Yes'),
(21, '', '0', 'http://www.dsfsdfsdf.com', '', NULL, 1, NULL, '2015-05-26 17:14:31', '2015-05-26 16:44:31', '2015-05-26 17:30:47', b'1', 'Yes'),
(22, '', '0', 'http://www.ibrinfotech.com', '', NULL, 1, NULL, '2015-05-26 17:14:47', '2015-05-26 16:44:47', '2015-06-27 04:30:31', b'1', 'Yes'),
(23, '', '0', 'https://developers.google.com/gdata/docs/auth/overview', '', NULL, 2, NULL, '2015-06-09 14:27:18', '2015-06-09 08:57:18', '2015-06-09 14:07:19', b'1', 'Yes'),
(24, '', '0', 'https://developers.google.com/analytics/devguides/reporting/core/v2/gdataAuthentication', '', NULL, 1, NULL, '2015-06-09 14:27:46', '2015-06-09 08:57:46', '2015-06-09 14:07:19	', b'1', 'Yes'),
(25, '', '0', 'http://www.google.co.in/', '', NULL, 2, NULL, '2015-06-09 14:44:15', '2015-06-09 09:14:15', '2015-06-09 14:45:19', b'1', 'Yes'),
(26, '0', '29', 'https://github.com/joeauty/Google-API-Client-CodeIgniter-Spark/blob/master/examples/analytics/demo/index.php', '', NULL, 1, NULL, '2015-06-09 14:45:33', '2015-06-09 09:15:33', '2015-06-09 14:45:19', b'1', 'Yes'),
(27, '0', '37', 'http://www.magzim.com', '', NULL, 1, NULL, '2015-06-09 14:55:10', '2015-06-09 09:25:10', '', b'1', ''),
(28, '0', '37', 'http://www.test.com  Hi how  testing ', '', NULL, 1, NULL, '2015-06-09 15:03:48', '2015-06-09 09:33:48', '', b'1', ''),
(29, '0', '37', 'http://www.magzim.com ', '', NULL, 1, NULL, '2015-06-09 15:58:07', '2015-06-09 10:28:07', '2015-06-09 16:00:00', b'1', 'Yes'),
(30, '0', '37', 'http://www.magzim.com test tweet twett', '', NULL, 1, NULL, '2015-06-09 15:59:22', '2015-06-09 10:29:22', '2015-06-09 16:00:05', b'1', 'Yes'),
(31, '0', '37', 'http://www.magzim.com', '', NULL, 1, NULL, '2015-06-09 17:09:42', '2015-06-09 11:39:42', '2015-06-09 17:11:23', b'1', 'Yes'),
(32, '0', '37', 'http://www.magzim.com', '', NULL, 2, NULL, '2015-06-09 17:34:00', '2015-06-09 12:04:00', '2015-06-09 17:35:42', b'1', 'Yes'),
(33, '0', '37', 'http://www.magzim.com', '', NULL, 1, NULL, '2015-06-09 17:37:35', '2015-06-09 12:07:35', '2015-06-09 17:39:42', b'1', 'Yes'),
(34, '', '0', 'http://www.google.com <br> hi sulbha', '', NULL, 1, NULL, '2015-06-12 15:07:04', '2015-06-12 09:37:04', '2015-06-12 15:10:27', b'1', 'Yes'),
(35, '', '0', 'https://www.google.co.in/?gfe_rd=cr&ei=Sb16VZP4LYSAoAP6i4DIBw&gws_rd=ssl#q=encryption+and+decryption+in+php', '', NULL, 1, NULL, '2015-06-12 16:45:22', '2015-06-12 11:15:22', '2015-06-12 17:55:47', b'1', 'Yes'),
(36, '', '0', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', '', NULL, 1, NULL, '2015-06-12 16:46:12', '2015-06-12 11:16:12', '2015-06-12 17:50:39', b'1', 'Yes'),
(37, '', '0', 'https://www.google.co.in/?gfe_rd=cr&ei=o796VeuVA5PK8AfpnYHYBQ&gws_rd=ssl', '', NULL, 1, NULL, '2015-06-12 16:46:59', '2015-06-12 11:16:59', '2015-06-12 17:50:28', b'1', 'Yes'),
(38, '', '0', 'https://play.google.com/apps/publish/signup/', '', NULL, 1, NULL, '2015-06-12 16:57:00', '2015-06-12 11:27:00', '2015-06-12 17:00:50', b'1', 'Yes'),
(39, '', '0', 'https://developer.android.com/distribute/googleplay/developer-console.html', '', NULL, 1, NULL, '2015-06-12 16:59:32', '2015-06-12 11:29:32', '2015-06-12 17:00:21', b'1', 'Yes'),
(40, '', '0', 'http://developer.android.com/distribute/googleplay/start.html', '', NULL, 1, NULL, '2015-06-12 17:33:40', '2015-06-12 12:03:40', '2015-06-12 18:30:27', b'1', 'Yes'),
(41, '', '0', 'http://developer.android.com/distribute/googleplay/developer-console.html', '', NULL, 1, NULL, '2015-06-12 17:42:26', '2015-06-12 12:12:26', '2015-06-12 18:30:18', b'1', 'Yes'),
(42, '', '0', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', '', NULL, 1, NULL, '2015-06-12 17:44:08', '2015-06-12 12:14:08', '2015-06-12 18:30:01', b'1', 'Yes'),
(43, '', '0', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', '', NULL, 1, NULL, '2015-06-12 17:44:26', '2015-06-12 12:14:26', '2015-06-12 18:00:18', b'1', 'Yes'),
(44, '', '0', 'http://www.dsfsdfsdf.com', '', NULL, 1, NULL, '2015-06-12 17:58:23', '2015-06-12 12:28:23', '2015-06-12 18:30:15', b'1', 'Yes'),
(45, '', '0', 'http://www.xyz.com', '', NULL, 1, NULL, '2015-06-12 18:01:29', '2015-06-12 12:31:29', '2015-06-12 18:30:22', b'1', 'Yes'),
(46, '', '0', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', '', NULL, 1, NULL, '2015-06-12 18:08:25', '2015-06-12 12:38:25', '2015-06-12 18:30:17', b'1', 'Yes'),
(47, '', '0', 'https://github.com/jedisct1/libsodium-php', '', NULL, 1, NULL, '2015-06-12 18:08:56', '2015-06-12 12:38:56', '2015-06-12 18:30:50', b'1', 'Yes'),
(48, '', '0', 'http://www.tutorialspoint.com/php/', '', NULL, 1, NULL, '2015-06-13 11:16:20', '2015-06-13 05:46:20', '2015-06-13 11:20:56', b'1', 'Yes'),
(49, '0', '42', 'http://www.w3schools.com/php/', '', NULL, 1, NULL, '2015-06-13 11:40:23', '2015-06-13 06:10:23', '', b'1', ''),
(50, '0', '42', 'http://www.tutorialspoint.com/php/', '', NULL, 1, NULL, '2015-06-13 11:44:23', '2015-06-13 06:14:23', '', b'1', ''),
(51, '', '0', 'https://developers.facebook.com/apps/1654266621474610/roles/test-users/', '', NULL, 1, NULL, '2015-06-13 11:45:59', '2015-06-13 06:15:59', '2015-06-13 11:45:15', b'1', 'Yes'),
(52, '', '42', 'http://www.dsfsdfsdf.com', '', NULL, 1, NULL, '2015-06-13 11:46:43', '2015-06-13 06:16:43', '', b'1', ''),
(53, '', '0', 'http://www.dsfsdfsdf.com', '', NULL, 1, NULL, '2015-06-13 11:49:46', '2015-06-13 06:19:46', '', b'1', ''),
(54, '', '0', 'http://www.dsfsdfsdf.com', '', NULL, 1, NULL, '2015-06-13 11:50:34', '2015-06-13 06:20:34', '', b'1', ''),
(55, '', '0', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', '', NULL, 1, NULL, '2015-06-13 11:51:07', '2015-06-13 06:21:07', '', b'1', ''),
(56, '', '0', 'https://www.google.co.in/?gfe_rd=cr&ei=BMJ7VdCwIYfK8AfL3YCABA&gws_rd=ssl#q=php+tutorial', '', NULL, 1, NULL, '2015-06-13 11:56:31', '2015-06-13 06:26:31', '', b'1', ''),
(57, '', '0', 'http://www.jkscholarship.in/', '', NULL, 1, NULL, '2015-06-13 12:41:26', '2015-06-13 07:11:26', '2015-06-13 13:00:15', b'1', 'Yes'),
(58, '0', '39', 'http://www.jkscholarship.in/', '', NULL, 1, NULL, '2015-06-13 12:41:59', '2015-06-13 07:11:59', '', b'1', ''),
(59, '', '0', 'http://www.jkscholarship.in/', '', NULL, 1, NULL, '2015-06-13 13:05:34', '2015-06-13 07:35:34', '2015-06-13 13:30:24', b'1', 'Yes'),
(60, '', '0', 'http://php.net/strpos', '', NULL, 1, NULL, '2015-06-16 13:36:55', '2015-06-16 08:06:56', '2015-06-16 14:30:46', b'1', 'Yes'),
(61, '0', '36', 'http://stackoverflow.com/questions/19562903/remove-padding-from-columns-in-bootstrap-3', '', NULL, 1, NULL, '2015-06-16 13:37:25', '2015-06-16 08:07:25', '2015-06-16 14:30:22', b'1', 'Yes'),
(62, '0', '39', 'http://stackoverflow.com/questions/4366730/check-if-string-contains-specific-words', '', NULL, 1, NULL, '2015-06-16 13:37:42', '2015-06-16 08:07:42', '', b'1', ''),
(63, '', '0', 'http://www.programmerinterview.com/index.php/php-questions/find-if-string-contains-another-string-php/', '', NULL, 1, NULL, '2015-06-16 13:38:25', '2015-06-16 08:08:25', '2015-06-16 14:00:16', b'1', 'Yes'),
(64, '', '0', 'http://www.maxi-pedia.com/string+contains+substring+php', '', NULL, 1, NULL, '2015-06-16 14:55:42', '2015-06-16 09:25:42', '2015-06-16 15:00:35', b'1', 'Yes'),
(65, '0', '45', 'https://gist.github.com/Integralist/1391440', '', NULL, 1, NULL, '2015-06-16 14:56:10', '2015-06-16 09:26:10', '', b'1', ''),
(66, '', '0', 'http://www.yopmail.net/en/', '', NULL, 1, NULL, '2015-06-16 14:58:41', '2015-06-16 09:28:41', '2015-06-16 16:00:24', b'1', 'Yes'),
(67, '', '0', 'https://plugins.jquery.com/chosen/', '', NULL, 1, NULL, '2015-06-16 17:20:01', '2015-06-16 11:50:01', '2015-06-16 17:20:47', b'1', 'Yes'),
(68, '', '0', 'https://github.com/harvesthq/chosen/releases', '', NULL, 1, NULL, '2015-06-16 18:02:14', '2015-06-16 12:32:14', '', b'1', ''),
(69, '0', '43', 'https://plugins.jquery.com/chosen/', '', NULL, 1, NULL, '2015-06-16 18:09:13', '2015-06-16 12:39:13', '', b'1', ''),
(70, '0', '38', 'http://www.w3schools.com/cssref/css3_pr_word-wrap.asp', '', NULL, 1, NULL, '2015-06-16 18:36:13', '2015-06-16 13:06:13', '', b'1', ''),
(71, '', '39', 'http://www.dsfsdfsdf.com', '', NULL, 1, NULL, '2015-06-16 18:59:48', '2015-06-16 13:29:48', '', b'1', ''),
(72, '0', '38', 'https://ellislab.com/codeigniter/user-guide/tutorial/index.html', '', NULL, 1, NULL, '2015-06-16 19:00:08', '2015-06-16 13:30:08', '', b'1', ''),
(73, '0', '44', 'https://ellislab.com/codeigniter/user-guide/general/controllers.html', '', NULL, 1, NULL, '2015-06-16 19:03:39', '2015-06-16 13:33:39', '', b'1', ''),
(74, '0', '44', 'https://ellislab.com/codeigniter/user-guide/general/controllers.html', '', NULL, 1, NULL, '2015-06-16 19:28:00', '2015-06-16 13:58:00', '', b'1', ''),
(75, '', '38', 'https://ellislab.com/codeigniter/user-guide/tutorial/index.html', '', NULL, 1, NULL, '2015-06-16 19:29:50', '2015-06-16 13:59:50', '', b'1', ''),
(76, '', '0', 'http://216.172.184.121/~ibruser/adsense/index.php/usercategory', '', NULL, 1, NULL, '2015-06-16 19:30:51', '2015-06-16 14:00:51', '', b'1', ''),
(77, '0', '38', 'http://216.172.184.121/~ibruser/adsense/index.php/usercategory234234234', '', NULL, 1, NULL, '2015-06-16 19:31:49', '2015-06-16 14:01:49', '', b'1', ''),
(78, '0', '44', 'http://localhost/phpmyadmin/#PMAURL-17:sql.php?db=adsense&table=share_link&server=1&target=&token=0a28a2e9399b242c68ddf62a204edb57', '', NULL, 1, NULL, '2015-06-16 19:33:02', '2015-06-16 14:03:02', '', b'1', ''),
(79, '0', '44', 'http://localhost/phpmyadmin/#PMAURL-17:sql.php?db=adsense&table=share_link&server=1&target=&token=0a28a2e9399b242c68ddf62a204edb57', '', NULL, 1, NULL, '2015-06-16 19:37:36', '2015-06-16 14:07:36', '', b'1', ''),
(80, '', '36;38;39;43;44', 'http://php.net/manual/en/function.array-diff.php', '<?php\r\n$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");\r\n$a2=array("e"=>"red","f"=>"green","g"=>"blue");\r\n\r\n$result=array_diff($a1,$a2);\r\nprint_r($result);\r\n?>', 'assets/uploads/1434546029_Penguins.jpg', 1, NULL, '2015-06-17 18:30:29', '2015-06-17 13:00:29', '', b'1', ''),
(81, '', '36;38;39;43;44', 'http://php.net/manual/en/function.array-diff.php', '<?php\r\n$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");\r\n$a2=array("e"=>"red","f"=>"green","g"=>"blue");\r\n\r\n$result=array_diff($a1,$a2);\r\nprint_r($result);\r\n?>', 'assets/uploads/1434546147_Penguins.jpg', 1, NULL, '2015-06-17 18:32:27', '2015-06-17 13:02:27', '', b'1', ''),
(82, '', '', 'http://www.jkscholarship.in/', 'abc', 'assets/uploads/1434546191_Chrysanthemum.jpg', 1, NULL, '2015-06-17 18:33:11', '2015-06-17 13:03:11', '', b'1', ''),
(83, '', '36;38;39;43', 'http://php.net/manual/en/function.array-diff.php', '<!DOCTYPE html>\r\n<html>\r\n<body>\r\n\r\n<?php\r\n$a1=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");\r\n$a2=array("e"=>"red","f"=>"green","g"=>"blue");\r\n\r\n$result=array_diff($a1,$a2);\r\nprint_r($result);\r\n?>\r\n\r\n</body>\r\n</html>', 'assets/uploads/1434546286_s1.jpg', 1, NULL, '2015-06-17 18:34:46', '2015-06-17 13:04:46', '', b'1', ''),
(84, '', '43', 'https://gator4216.hostgator.com:2083/cpsess5986508604/3rdparty/phpMyAdmin/import.php', 'hello how are u ', 'assets/uploads/1434609583_r3.jpg', 1, NULL, '2015-06-18 12:09:43', '2015-06-18 06:39:43', '', b'1', ''),
(85, '', '43', 'http://www.google.com/', 'hellooooooooooooooooooo', 'assets/uploads/1434609794_r3.jpg', 1, NULL, '2015-06-18 12:13:14', '2015-06-18 06:43:14', '', b'1', ''),
(86, '', '36;43', 'http://www.google.com/', 'hellooooooooo', 'assets/uploads/1434609901_r3.jpg', 1, NULL, '2015-06-18 12:15:01', '2015-06-18 06:45:01', '', b'1', ''),
(87, '', '43;44', 'https://www.google.co.in/?gfe_rd=cr&ei=bGiCVfXNIsqFoAOI-4G4AQ&gws_rd=ssl', 'hello', 'assets/uploads/1434610017_r3.jpg', 1, NULL, '2015-06-18 12:16:57', '2015-06-18 06:46:57', '', b'1', ''),
(88, '', '36', 'http://www.dsfsdfsdf.com', 'fsdf', 'assets/uploads/1434610100_r3.jpg', 1, NULL, '2015-06-18 12:18:20', '2015-06-18 06:48:20', '', b'1', ''),
(89, '', '', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', 'asfdg', 'assets/uploads/1434610250_icon_twitter_small.jpg', 1, NULL, '2015-06-18 12:20:50', '2015-06-18 06:50:50', '2015-06-19 12:20:32', b'1', 'Yes'),
(90, '', '', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', 'asdf', 'assets/uploads/1434610363_icon_twitter_small.jpg', 1, NULL, '2015-06-18 12:22:43', '2015-06-18 06:52:43', '2015-06-18 12:22:29', b'1', 'Yes'),
(91, '', '43;44;46', 'http://216.172.184.121/~ibruser/', 'hellooooooooooooooooooo dear', 'assets/uploads/1434610571_r1.jpg', 1, NULL, '2015-06-18 12:26:11', '2015-06-18 06:56:12', '', b'1', ''),
(92, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434610877_r4.jpg', 1, NULL, '2015-06-18 12:31:17', '2015-06-18 07:01:17', '', b'1', ''),
(93, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611050_r4.jpg', 1, NULL, '2015-06-18 12:34:10', '2015-06-18 07:04:10', '', b'1', ''),
(94, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611162_r4.jpg', 1, NULL, '2015-06-18 12:36:02', '2015-06-18 07:06:02', '', b'1', ''),
(95, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611228_r4.jpg', 1, NULL, '2015-06-18 12:37:08', '2015-06-18 07:07:08', '', b'1', ''),
(96, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611441_r4.jpg', 1, NULL, '2015-06-18 12:40:41', '2015-06-18 07:10:41', '', b'1', ''),
(97, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611460_r4.jpg', 1, NULL, '2015-06-18 12:41:00', '2015-06-18 07:11:00', '', b'1', ''),
(98, '', '43;44', 'http://216.172.184.121/~ibruser/', 'huu', 'assets/uploads/1434611506_r4.jpg', 1, NULL, '2015-06-18 12:41:46', '2015-06-18 07:11:46', '', b'1', ''),
(99, '', '43', 'https://developers.facebook.com/', 'hw r u .....................', 'assets/uploads/1434611571_r3.jpg', 1, NULL, '2015-06-18 12:42:51', '2015-06-18 07:12:51', '', b'1', ''),
(100, '', '43', 'https://developers.facebook.com/', 'hw r u .....................', 'assets/uploads/1434611779_r3.jpg', 1, NULL, '2015-06-18 12:46:19', '2015-06-18 07:16:19', '', b'1', ''),
(101, '', '36;43', 'https://www.google.co.in/', 'how r u.............', 'assets/uploads/1434611834_s2.jpg', 1, NULL, '2015-06-18 12:47:14', '2015-06-18 07:17:14', '', b'1', ''),
(102, '', '36;43', 'https://www.google.co.in/', 'how r u.............', 'assets/uploads/1434612416_s2.jpg', 1, NULL, '2015-06-18 12:56:56', '2015-06-18 07:26:56', '', b'1', ''),
(103, '', '36;43', 'https://www.google.co.in/', 'how r u.............', 'assets/uploads/1434612976_s2.jpg', 1, NULL, '2015-06-18 13:06:16', '2015-06-18 07:36:16', '', b'1', ''),
(104, '', '36;43', 'https://www.google.co.in/', 'how r u.............', 'assets/uploads/1434613273_s2.jpg', 1, NULL, '2015-06-18 13:11:13', '2015-06-18 07:41:13', '', b'1', ''),
(105, '', '43', 'http://www.sanwebe.com/2012/02/post-to-facebook-page-wall-using-php-graph', 'hiiiiiiiiiiiiii', 'assets/uploads/1434613586_s1.jpg', 1, NULL, '2015-06-18 13:16:26', '2015-06-18 07:46:26', '', b'1', ''),
(106, '', '36;38;43', 'http://www.dsfsdfsdf.com', 'fsdf', 'assets/uploads/1434613823_s1.jpg', 1, NULL, '2015-06-18 13:20:23', '2015-06-18 07:50:23', '', b'1', ''),
(107, '', '36;38;43', 'http://www.dsfsdfsdf.com', 'fsdf', 'assets/uploads/1434613848_s1.jpg', 1, NULL, '2015-06-18 13:20:48', '2015-06-18 07:50:48', '', b'1', ''),
(108, '', '43', 'http://www.dsfsdfsdf.com', 'gfdfg', 'assets/uploads/1434613869_r4.jpg', 1, NULL, '2015-06-18 13:21:09', '2015-06-18 07:51:09', '', b'1', ''),
(109, '', '38;43', 'http://www.dsfsdfsdf.com', 'fghfgh', 'assets/uploads/1434613941_r2.jpg', 1, NULL, '2015-06-18 13:22:21', '2015-06-18 07:52:21', '', b'1', ''),
(110, '', '43', 'http://www.dsfsdfsdf.com', 'gjfgjhfgjh', 'assets/uploads/1434614234_r3.jpg', 1, NULL, '2015-06-18 13:27:14', '2015-06-18 07:57:14', '', b'1', ''),
(111, '', '36', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', 'ab', 'assets/uploads/1434614401_', 1, NULL, '2015-06-18 13:30:01', '2015-06-18 08:00:01', '2015-06-01 13:29:53', b'1', 'Yes'),
(112, '', '43', 'http://www.jkscholarship.in', 'ab', 'assets/uploads/1434614620_', 1, NULL, '2015-06-18 13:33:40', '2015-06-18 08:03:40', '2015-06-18 13:33:32', b'1', 'Yes'),
(113, '', '43', 'http://www.jkscholarship.in', '', 'assets/uploads/1434615155_', 1, NULL, '2015-06-18 13:42:35', '2015-06-18 08:12:35', '2015-06-18 13:43:37', b'1', 'Yes'),
(114, '', '43', 'http://www.jkscholarship.in', '', 'assets/uploads/1434615227_', 1, NULL, '2015-06-18 13:43:47', '2015-06-18 08:13:47', '', b'1', ''),
(115, '', '43', 'http://www.jkscholarship.in', 'hi please see this', 'assets/uploads/1434615445_', 1, NULL, '2015-06-18 13:47:25', '2015-06-18 08:17:25', '2015-06-18 13:48:00', b'1', 'Yes'),
(116, '', '', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', 'abc', 'assets/uploads/1434616176_textbanner.jpg', 1, NULL, '2015-06-18 13:59:36', '2015-06-18 08:29:36', '2015-06-18 14:00:26', b'1', 'Yes'),
(117, '', '43', 'http://www.dsfsdfsdf.com', '', 'assets/uploads/1434616229_', 1, NULL, '2015-06-18 14:00:29', '2015-06-18 08:30:29', '2015-06-18 14:05:10', b'1', 'Yes'),
(118, '', '43', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', '', 'assets/uploads/1434616257_', 1, NULL, '2015-06-18 14:00:57', '2015-06-18 08:30:57', '', b'1', ''),
(119, '', '43', 'http://www.jkscholarship.in', '', 'assets/uploads/1434616806_', 1, NULL, '2015-06-18 14:10:06', '2015-06-18 08:40:06', '', b'1', ''),
(120, '', '43', 'http://www.dsfsdfsdf.com', '', 'assets/uploads/1434616950_', 1, NULL, '2015-06-18 14:12:30', '2015-06-18 08:42:30', '', b'1', ''),
(121, '', '43', 'https://developers.facebook.com/products/ads/', '', 'assets/uploads/1434617944_', 1, NULL, '2015-06-18 14:29:04', '2015-06-18 08:59:04', '', b'1', ''),
(122, '', '45', 'https://developers.facebook.com/docs/ads-for-websites', '', 'assets/uploads/1434618187_', 1, NULL, '2015-06-18 14:33:07', '2015-06-18 09:03:07', '', b'1', ''),
(123, '', '43', 'http://php.net/manual/en/tutorial.php', '', 'assets/uploads/1434618234_', 1, NULL, '2015-06-18 14:33:54', '2015-06-18 09:03:54', '', b'1', ''),
(124, '', '', 'https://docs.angularjs.org/tutorial/step_01', '', 'assets/uploads/1434618391_', 1, NULL, '2015-06-18 14:36:31', '2015-06-18 09:06:31', '', b'1', ''),
(125, '', '', 'https://docs.angularjs.org/tutorial/step_01', '', 'assets/uploads/1434618430_', 1, NULL, '2015-06-18 14:37:10', '2015-06-18 09:07:10', '', b'1', ''),
(126, '', '38', 'https://wordpress.org/support/topic/plugin-site-creation-wizard-problem-in-getting-started', 'hi see this......', 'assets/uploads/1434618527_', 1, NULL, '2015-06-18 14:38:47', '2015-06-18 09:08:47', '', b'1', ''),
(127, '', '38', 'http://www.rtu.ac.in/RTU/', 'hiii ', 'assets/uploads/1434618644_', 1, NULL, '2015-06-18 14:40:44', '2015-06-18 09:10:44', '', b'1', ''),
(128, '', '', 'http://www.ibrinfotech.com/', '', 'assets/uploads/1434619599_', 2, NULL, '2015-06-18 14:56:39', '2015-06-18 09:26:39', '', b'1', ''),
(129, '', '38', 'http://www.magzim.com', '', 'assets/uploads/1434659934_', 1, NULL, '2015-06-19 02:08:54', '2015-06-18 20:38:54', '', b'1', ''),
(130, '', '38', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', '', 'assets/uploads/1434660079_', 1, NULL, '2015-06-19 02:11:19', '2015-06-18 20:41:19', '', b'1', ''),
(131, '', '47', 'http://www,magzim.com', '', 'assets/uploads/1434695488_', 1, NULL, '2015-06-19 12:01:28', '2015-06-19 06:31:28', '', b'1', ''),
(132, '', '', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701489_banner.jpg', 1, NULL, '2015-06-19 13:41:29', '2015-06-19 08:11:29', '', b'1', ''),
(133, '', '', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701631_banner.jpg', 1, NULL, '2015-06-19 13:43:51', '2015-06-19 08:13:51', '', b'1', ''),
(134, '', '43', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701664_', 1, NULL, '2015-06-19 13:44:24', '2015-06-19 08:14:24', '', b'1', ''),
(135, '', '43', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701726_', 1, NULL, '2015-06-19 13:45:26', '2015-06-19 08:15:26', '', b'1', ''),
(136, '', '43', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701842_banner.jpg', 1, NULL, '2015-06-19 13:47:22', '2015-06-19 08:17:22', '', b'1', ''),
(137, '', '43', 'http://www.jabong.com/men/?source=topnav_men', '', 'assets/uploads/1434701853_', 1, NULL, '2015-06-19 13:47:33', '2015-06-19 08:17:33', '', b'1', ''),
(138, '', '43', 'http://www.jabong.com/kids/?source=topnav_kids', '', 'assets/uploads/1434701941_', 1, NULL, '2015-06-19 13:49:01', '2015-06-19 08:19:01', '', b'1', ''),
(139, '', '43', 'http://www.jabong.com/kids/?source=topnav_kids', '', 'assets/uploads/1434701968_', 1, NULL, '2015-06-19 13:49:28', '2015-06-19 08:19:28', '', b'1', ''),
(140, '', '43', 'http://www.jabong.com/kids/?source=topnav_kids', 'hiiiiiiiiii', 'assets/uploads/1434702029_banner.jpg', 1, NULL, '2015-06-19 13:50:29', '2015-06-19 08:20:29', '', b'1', ''),
(141, '', '39', 'http://example.com', 'test on server', 'assets/uploads/1434706494_11233771_717705995051325_7723230737118797739_n.jpg', 1, NULL, '2015-06-19 15:04:54', '2015-06-19 09:34:54', '', b'1', ''),
(142, '', '39', 'http://example.com', 'test on server', 'assets/uploads/1434706669_11233771_717705995051325_7723230737118797739_n.jpg', 1, NULL, '2015-06-19 15:07:49', '2015-06-19 09:37:49', '', b'1', ''),
(143, '', '39', 'http://example.com', 'test on server', 'assets/uploads/1434706754_11233771_717705995051325_7723230737118797739_n.jpg', 1, NULL, '2015-06-19 15:09:14', '2015-06-19 09:39:14', '', b'1', ''),
(144, '', '38', 'http://example.com', 'test on server', 'assets/uploads/1434706793_11233771_717705995051325_7723230737118797739_n.jpg', 1, NULL, '2015-06-19 15:09:53', '2015-06-19 09:39:53', '', b'1', ''),
(145, '', '43', 'http://www.jabong.com/nbastore/?source=topnav_sports', 'hii check this...', 'assets/uploads/1434707604_banner.jpg', 1, NULL, '2015-06-19 15:23:24', '2015-06-19 09:53:24', '', b'1', ''),
(146, '', '43', 'http://www.jabong.com/nbastore/?source=topnav_sports', 'hii check this...', 'assets/uploads/1434707606_banner.jpg', 1, NULL, '2015-06-19 15:23:26', '2015-06-19 09:53:26', '', b'1', ''),
(147, '', '38', 'http://www.jabong.com/nbastore/?source=topnav_sports', 'hey...', 'assets/uploads/1434707691_banner.jpg', 2, NULL, '2015-06-19 15:24:51', '2015-06-19 09:54:51', '', b'1', ''),
(148, '', '43', 'http://www.magzim.com', '', NULL, 1, NULL, '2015-06-19 16:03:14', '2015-06-19 10:33:14', '', b'1', ''),
(149, '', '43', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', '', NULL, 1, NULL, '2015-06-19 16:04:12', '2015-06-19 10:34:12', '', b'1', ''),
(150, '', '', 'http://www.magzim.com', '', NULL, 2, NULL, '2015-06-19 16:05:30', '2015-06-19 10:35:30', '2015-06-19 16:10:59', b'1', 'Yes'),
(151, '', '43', 'http://www.storypick.com', '', NULL, 1, NULL, '2015-06-19 16:17:01', '2015-06-19 10:47:01', '2015-06-19 16:18:14', b'1', 'Yes'),
(152, '', '47', 'http://www.storypick.com', '', NULL, 1, NULL, '2015-06-19 16:20:12', '2015-06-19 10:50:12', '2015-06-19 16:29:59', b'1', 'Yes'),
(153, '', '39', 'http://test-test.com', 'post with schedule', 'assets/uploads/1434711959_11209760_717403471748244_452279927463402649_n.jpg', 1, NULL, '2015-06-19 16:35:59', '2015-06-19 11:05:59', '2015-06-19 16:40:23', b'1', 'Yes'),
(154, '', '47', 'http://www.scoopwhoop.com', '', NULL, 1, NULL, '2015-06-19 16:41:31', '2015-06-19 11:11:31', '2015-06-19 16:44:15', b'1', 'Yes'),
(155, '', '38', 'http://test.com', 'sfgs', NULL, 1, NULL, '2015-06-19 16:42:56', '2015-06-19 11:12:56', '', b'1', ''),
(156, '', '', 'http://test1.com', 'dddd', 'assets/uploads/1434712420_18604_717403568414901_3417955052131641437_n.jpg', 1, NULL, '2015-06-19 16:43:40', '2015-06-19 11:13:40', '', b'1', ''),
(157, '', '', 'http://test.com', 'ghjhjkhjhkjhjhj', NULL, 1, NULL, '2015-06-19 17:20:56', '2015-06-19 11:50:56', '', b'1', ''),
(158, '', '38', 'http://test.com', 'ghgfh', NULL, 1, NULL, '2015-06-19 17:21:12', '2015-06-19 11:51:12', '', b'1', ''),
(159, '', '', 'http://test.com', 'rgdfg', NULL, 1, NULL, '2015-06-19 17:24:19', '2015-06-19 11:54:19', '', b'1', ''),
(160, '', '', 'http://www.flipkart.com/asus-zenfone-2/p/itme6pvuadqxxjfc?pid=MOBE6CG6ZEJKCYGM&cmpid=content_mobile_8965229628_gmc_pla&tgi=sem%2C1%2CG%2', 'helloooooo', 'assets/uploads/1434714956_banner.jpg', 1, NULL, '2015-06-19 17:25:56', '2015-06-19 11:55:56', '', b'1', ''),
(161, '', '38', 'http://www.flipkart.com/asus-zenfone-2/p/itme6pvuadqxxjfc?pid=MOBE6CG6ZEJKCYGM&cmpid=content_mobile_8965229628_gmc_pla&tgi=sem%2C1%2CG%2C112140', 'hiiiiiiiiiii', 'assets/uploads/1434715424_banner.jpg', 1, NULL, '2015-06-19 17:33:44', '2015-06-19 12:03:44', '', b'1', ''),
(162, '', '', 'http://www.amazon.in/?tag=googhydrabk-21&ref_=pd_mn_ABKror1112', 'hello......', 'assets/uploads/1434715841_banner.jpg', 1, NULL, '2015-06-19 17:40:41', '2015-06-19 12:10:41', '', b'1', ''),
(163, '', '38;39', 'http://test.com', 'sgfsg', NULL, 1, NULL, '2015-06-19 17:49:57', '2015-06-19 12:19:57', '', b'1', ''),
(164, '', '', 'http://test_t.com', 'fgdgdg', NULL, 1, NULL, '2015-06-19 17:51:03', '2015-06-19 12:21:03', '', b'1', ''),
(165, '', '', 'http://do-test.com', 'do test posting', NULL, 1, NULL, '2015-06-19 18:06:25', '2015-06-19 12:36:25', '', b'1', ''),
(166, '', '', 'http://test-do.com', 'test by do', NULL, 1, NULL, '2015-06-19 18:06:46', '2015-06-19 12:36:46', '', b'1', ''),
(167, '', '', 'http://test-do.com', 'to test', NULL, 1, NULL, '2015-06-19 18:08:21', '2015-06-19 12:38:21', '', b'1', ''),
(168, '', '', 'http://test-do.com', 'to test', NULL, 1, NULL, '2015-06-19 18:09:20', '2015-06-19 12:39:20', '', b'1', ''),
(169, '', '', 'http://test-do.com', 'jghjdkhjghfdjkhgjh', NULL, 1, NULL, '2015-06-19 18:10:00', '2015-06-19 12:40:00', '', b'1', ''),
(170, '', '', 'http://test-do.com', 'jghjdkhjghfdjkhgjh', NULL, 1, NULL, '2015-06-19 18:10:30', '2015-06-19 12:40:30', '', b'1', ''),
(171, '', '', 'http://do-test.com', 'cetegory posting', NULL, 1, NULL, '2015-06-19 18:11:03', '2015-06-19 12:41:03', '', b'1', ''),
(172, '', '', 'http://www.amazon.com/gift-cards/b/ref=nav_cs_gc?ie=UTF8&node=2238192011', 'test', 'assets/uploads/1434777858_Screenshot (19).png', 1, NULL, '2015-06-20 10:54:18', '2015-06-20 05:24:18', '', b'1', ''),
(173, '', '43', 'http://www.amazon.com/gift-cards/b/ref=nav_cs_gc?ie=UTF8&node=2238192011', 'asdd', 'assets/uploads/1434778134_Screenshot (19).png', 1, NULL, '2015-06-20 10:58:54', '2015-06-20 05:28:54', '', b'1', ''),
(174, '', '44', 'http://www.snapdeal.com/offers/deal-of-the-day?HookID=1', 'as', 'assets/uploads/1434778288_Mind-Music.jpg', 1, NULL, '2015-06-20 11:01:28', '2015-06-20 05:31:28', '', b'1', ''),
(175, '', '43', 'http://www.snapdeal.com/offers/deal-of-the-day?HookID=1', 'aghhhh', 'assets/uploads/1434778437_Mind-Music.jpg', 1, NULL, '2015-06-20 11:03:57', '2015-06-20 05:33:57', '2015-06-20 11:04:00', b'1', 'Yes'),
(176, '', '43', 'http://www.snapdeal.com/offers/deal-of-the-day?HookID=1', 'aghhhh', 'assets/uploads/1434778440_Mind-Music.jpg', 1, NULL, '2015-06-20 11:04:00', '2015-06-20 05:34:00', '2015-06-20 11:04:00', b'1', 'Yes'),
(177, '', '', 'http://test-ya.com', 'dfds', 'assets/uploads/1434778491_Screenshot__19_.png', 1, NULL, '2015-06-20 11:04:51', '2015-06-20 05:34:51', '', b'1', ''),
(178, '', '', 'http://www.dsfsdfsdf.com', 'asdf', 'assets/uploads/1434778550_Mind-Music.jpg', 1, NULL, '2015-06-20 11:05:50', '2015-06-20 05:35:50', '', b'1', ''),
(179, '', '', 'http://www.flipkart.com/asus-zenfone-2/p/itme6pvuadqxxjfc?pid=MOBE6CG6ZEJKCYGM&cmpid=content_mobile_8965229628_gmc_pla&tgi=sem%2C1%2CG%2C11214', 'sh', 'assets/uploads/1434778747_Mind-Music.jpg', 1, NULL, '2015-06-20 11:09:07', '2015-06-20 05:39:07', '2015-06-20 11:09:00', b'1', 'Yes'),
(180, '', '', 'http://test-testt.com', 'test-test-test', NULL, 1, NULL, '2015-06-20 11:09:33', '2015-06-20 05:39:33', '', b'1', ''),
(181, '', '', 'http://www.flipkart.com/asus-zenfone-2/p/itme6pvuadqxxjfc?pid=MOBE6CG6ZEJKCYGM&cmpid=content_mobile_8965229628_gmc_pla&tgi=sem%2C1%2CG%2C11214', 'sh', 'assets/uploads/1434778846_Mind-Music.jpg', 2, NULL, '2015-06-20 11:10:46', '2015-06-20 05:40:46', '2015-06-20 11:11:00', b'1', 'Yes'),
(182, '', '38', 'http://www.flipkart.com/asus-zenfone-2/p/itme6pvuadqxxjfc?pid=MOBE6CG6ZEJKCYGM&cmpid=content_mobile_8965229628_gmc_pla&tgi=sem%2C1%2CG%2C11214', 'as', 'assets/uploads/1434779979_screenshot-1.jpg', 2, NULL, '2015-06-20 11:29:39', '2015-06-20 05:59:39', '', b'1', ''),
(183, '', '', 'https://sites.google.com/site/shoestyleambur/', 'can', 'assets/uploads/1434780636_Mind-Music.jpg', 1, NULL, '2015-06-20 11:40:36', '2015-06-20 06:10:36', '', b'1', ''),
(184, '', '', 'https://www.facebook.com/', 'hjhgjh', NULL, 1, NULL, '2015-06-20 12:25:42', '2015-06-20 06:55:42', '', b'1', ''),
(185, '', '', 'https://www.facebook.com/', 'facebook sharing', NULL, 1, NULL, '2015-06-20 12:27:14', '2015-06-20 06:57:14', '', b'1', ''),
(186, '', '44', 'http://www.amazon.in/Adidas-Unisex-Scarlet-White-Sneakers/dp/B00V4KVKZY?tag=googinhydr18418-21&kpid=B00V4KVKZY&tag=googinkenshoo-21&ascsubtag=6d7e', 'hiiiiii', 'assets/uploads/1434783459_custom-product-search.png', 1, NULL, '2015-06-20 12:27:39', '2015-06-20 06:57:39', '', b'1', ''),
(187, '', '', 'http://do-test.com', 'fgdhh', NULL, 1, NULL, '2015-06-20 12:29:16', '2015-06-20 06:59:16', '', b'1', ''),
(188, '', '', 'https://ftploy.com/', 'dhghfh', NULL, 1, NULL, '2015-06-20 12:30:06', '2015-06-20 07:00:06', '', b'1', ''),
(189, '', '', 'http://www.amazon.in/Adidas-Unisex-Scarlet-White-Sneakers/dp/B00V4KVKZY?tag=googinhydr18418-21&kpid=B00V4KVKZY&tag=googinkenshoo-21&ascsubtag=6d7e', 'ssssssss', NULL, 1, NULL, '2015-06-20 12:32:31', '2015-06-20 07:02:31', '', b'1', ''),
(190, '', '', 'http://www.amazon.in/Adidas-Unisex-Scarlet-White-Sneakers/dp/B00V4KVKZY?tag=googinhydr18418-21&kpid=B00V4KVKZY&tag=googinkenshoo-21&ascsubtag=6d7e', 'sssssss', NULL, 1, NULL, '2015-06-20 12:55:57', '2015-06-20 07:25:57', '', b'1', ''),
(191, '', '47', 'http://www.magzim.com', 'World’s Most Costliest Earthquakes', 'assets/uploads/1434797490_BruceWillis_Friends.png', 1, NULL, '2015-06-20 16:21:30', '2015-06-20 10:51:30', '2015-06-20 16:23:59', b'1', 'Yes'),
(192, '', '47', 'http://www.magzim.com', 'World’s Most Costliest Earthquakes!123 test', 'assets/uploads/1434797540_Friends.jpg', 1, NULL, '2015-06-20 16:22:20', '2015-06-20 10:52:20', '', b'1', ''),
(193, '', '47', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', 'World’s Most Costliest EarthquakesWorld’s Most Costliest Earthquakes', 'assets/uploads/1434797731_friends_Ross_Rachel.jpeg', 1, NULL, '2015-06-20 16:25:31', '2015-06-20 10:55:31', '', b'1', ''),
(194, '', '47', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', 'World’s Most Costliest EarthquakesWorld’s Most Costliest EarthquakesWorld’s Most Costliest EarthquakesWorld’s', 'assets/uploads/1434797958_friends_theme_song.jpg', 1, NULL, '2015-06-20 16:29:18', '2015-06-20 10:59:18', '', b'1', ''),
(195, '', '47', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', 'World’s Most Costliest EarthquakesWorld’s Most Costliest Earthquakes World’s Most Costliest EarthquakesWorld’s Most Costliest Earthquakes ', 'assets/uploads/1434798103_friends-centralperk.jpg', 1, NULL, '2015-06-20 16:31:43', '2015-06-20 11:01:43', '', b'1', ''),
(196, '', '47', 'http://www.storypick.com', 'World’s Most Costliest EarthquakesWorld’s Most Costliest Earthquakes ', 'assets/uploads/1434798233_friends_simpsons.jpg', 1, NULL, '2015-06-20 16:33:53', '2015-06-20 11:03:53', '', b'1', ''),
(197, '8', '', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', 'fasd', NULL, 1, NULL, '2015-06-22 14:27:34', '2015-06-22 08:57:34', '2015-06-23 14:27:23', b'0', 'Yes'),
(198, '', '', 'http://www.graphicproducts.com/tutorials/five-s/', 'hi whats up...', 'assets/uploads/1435040368_Mind-Music.jpg', 1, NULL, '2015-06-23 11:49:28', '2015-06-23 06:19:28', '', b'1', ''),
(199, '', '42', 'http://www.graphicproducts.com/tutorials/five-s/', 'hiiiii', NULL, 1, NULL, '2015-06-23 11:51:57', '2015-06-23 06:21:57', '', b'1', ''),
(200, '', '45', 'http://www.graphicproducts.com/tutorials/five-s/', 'asdffg.....', NULL, 1, NULL, '2015-06-23 11:52:52', '2015-06-23 06:22:52', '', b'1', ''),
(201, '', '45', 'http://www.graphicproducts.com/tutorials/five-s/', 'asdfffg', 'assets/uploads/1435040747_Mind-Music.jpg', 1, NULL, '2015-06-23 11:55:47', '2015-06-23 06:25:48', '', b'1', ''),
(202, '', '45', 'http://www.jabong.com/men/shoes/Adidas/', 'as........', 'assets/uploads/1435040893_Mind-Music.jpg', 1, NULL, '2015-06-23 11:58:13', '2015-06-23 06:28:13', '', b'1', ''),
(203, '', '', 'http:/ asddd.com', 'qq', NULL, 1, NULL, '2015-06-23 12:02:16', '2015-06-23 06:32:16', '', b'1', ''),
(204, '', '', 'http://djvicky.in/index.xhtml', 'qq', NULL, 1, NULL, '2015-06-23 12:02:55', '2015-06-23 06:32:55', '', b'1', ''),
(205, '', '', 'http://www.sdjaingirlscollege.com/UI/Aboutus.aspx?Page=aboutus', 'assddd', NULL, 1, NULL, '2015-06-23 12:05:16', '2015-06-23 06:35:16', '', b'1', ''),
(206, '', '', 'http://www.amazon.in/?tag=googhydrabk-21&ref_=pd_mn_ABKror1112', 'assd', NULL, 1, NULL, '2015-06-23 12:08:17', '2015-06-23 06:38:17', '', b'1', ''),
(207, '', '', 'http://216.172.184.121/~', 'zzzzz', NULL, 1, NULL, '2015-06-27 17:41:11', '2015-06-27 12:11:11', '2015-06-29 18:00:57', b'1', 'Yes'),
(208, '', '56', 'http://www.magzim.com/15-interesting-facts-you-didnt-know-about-friends-tv-show/', '15 Interserting Facts You Never knew About FRIENDS TV show', 'assets/uploads/1435668447_Friends.jpg', 1, NULL, '2015-06-30 18:17:27', '2015-06-30 12:47:27', '2015-06-30 18:19:07', b'1', 'Yes'),
(209, '', '56', 'http://www.magzim.com/15-interesting-facts-you-didnt-know-about-friends-tv-show/', '15 Interserting Facts You Never knew About FRIENDS TV show', 'assets/uploads/1435669840_Friends-Netflix.jpg', 1, NULL, '2015-06-30 18:40:40', '2015-06-30 13:10:40', '', b'1', ''),
(210, '', '56', 'http://www.magzim.com/20-unknown-facts-you-didnt-know-about-adolf-hitler/', '20 Unknown Facts You Didn''t Know about Adolf Hitler', 'assets/uploads/1435669992_feminise-hitler.jpg', 1, NULL, '2015-06-30 18:43:12', '2015-06-30 13:13:12', '2015-06-30 18:44:55', b'1', 'Yes'),
(211, '', '', 'http://www.shopperdeals.in/', 'HIIII TESTING ', NULL, 1, NULL, '2015-07-01 11:02:29', '2015-07-01 05:32:29', '2015-07-01 01:00:55', b'1', 'Yes'),
(212, '', '', 'http://www.shopperdeals.in/1236', '', NULL, 1, NULL, '2015-07-01 11:08:00', '2015-07-01 05:38:00', '2015-08-01 14:34:00', b'1', 'Yes'),
(213, '', '', 'https://www.google.co.in/search?q=webgl&oq=webgl&aqs=chrome..69i57j0l5.3511j0j4&sourceid=chrome&es_sm=93&ie=UTF-8', 'aaa', NULL, 1, NULL, '2015-07-01 12:52:21', '2015-07-01 07:22:21', '', b'1', ''),
(214, '', '', 'https://www.google.co.in/?gfe_rd=cr&ei=tpaTVdCAK4HaugSEn4eoDg&gws_rd=ssl', 'hiiii', NULL, 1, NULL, '2015-07-01 12:59:06', '2015-07-01 07:29:06', '', b'1', ''),
(215, '', '', 'http://wiki.dreamhost.com/Htaccess_tricks', 'hello dear ', 'assets/uploads/1435740811_feature_bullet.jpg', 1, NULL, '2015-07-01 14:23:31', '2015-07-01 08:53:31', '2015-07-01 14:25:10', b'1', 'Yes'),
(216, '', '56', 'http://www.magzim.com/15-interesting-facts-you-didnt-know-about-friends-tv-show/', '15 Interserting Facts You Never knew About FRIENDS TV show  ', 'assets/uploads/1435741173_friends2.jpeg', 1, NULL, '2015-07-01 14:29:33', '2015-07-01 08:59:34', '2015-07-01 14:30:11', b'1', 'Yes'),
(217, '', '', 'http://www.google.com', 'demo', NULL, 1, NULL, '2015-07-01 14:37:32', '2015-07-01 09:07:32', '2015-07-01 14:40:06', b'1', 'Yes'),
(218, '', '56', 'https://twitter.com/sulbha14shri', '12365', NULL, 1, NULL, '2015-07-01 14:40:26', '2015-07-01 09:10:26', '2015-07-01 14:41:15', b'1', 'Yes'),
(219, '', '', 'https://twitter.com/sulbha14shri/', '1312', NULL, 1, NULL, '2015-07-01 14:41:26', '2015-07-01 09:11:26', '2015-07-01 14:42:00', b'1', 'Yes'),
(220, '', '54', 'https://en.wikipedia.org/wiki/WebGL', '123', NULL, 1, NULL, '2015-07-01 14:45:58', '2015-07-01 09:15:58', '', b'1', ''),
(221, '', '52', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', '12365', NULL, 1, NULL, '2015-07-01 14:57:06', '2015-07-01 09:27:06', '', b'1', ''),
(222, '', '52;57', 'https://twitter.com/', 'asdsdf', NULL, 1, NULL, '2015-07-01 15:03:30', '2015-07-01 09:33:30', '2015-07-01 15:04:00', b'1', 'Yes'),
(223, '', '57', 'http://216.172.184.121/~ibruser/', 'wef', NULL, 1, NULL, '2015-07-01 15:05:28', '2015-07-01 09:35:28', '', b'1', ''),
(224, '', '54', 'https://www.google.co.in/?gfe_rd=cr&ei=n7mTVfT5DNKk8gXproHYDQ&gws_rd=ssl', 'hi dearssssssssssssssss', 'assets/uploads/1435744778_DSC_0037.jpg', 1, NULL, '2015-07-01 15:29:38', '2015-07-01 09:59:38', '', b'1', ''),
(225, '', '', 'http://php.net/manual/en/language.oop5.php', 'hello ssssssssssss', 'assets/uploads/1435744977_IMG_2090.jpg', 1, NULL, '2015-07-01 15:32:57', '2015-07-01 10:02:57', '', b'1', ''),
(226, '', '', 'http://php.net/manual/en/language.oop5.basic.php', 'sfasdfadsfsadfsssssssssssssssssssssssssssssssssssssssss', 'assets/uploads/1435745170_IMG_2090.jpg', 1, NULL, '2015-07-01 15:36:10', '2015-07-01 10:06:10', '', b'1', ''),
(227, '', '54', 'http://stackoverflow.com/questions/16600708/how-do-you-encrypt-and-decrypt-a-php-string', 'fghfg', NULL, 1, NULL, '2015-07-01 15:46:44', '2015-07-01 10:16:44', '', b'1', ''),
(228, '', '54', 'http://php.net/manual/en/keyword.class.php', 'ooooooooopssssssssssss', NULL, 1, NULL, '2015-07-01 15:47:22', '2015-07-01 10:17:22', '', b'1', ''),
(229, '', '56', 'http://www.magzim.com', '5-10% of internet users are actually unable to control how much time they spend online because of psychological addiction.', NULL, 1, NULL, '2015-07-01 16:01:46', '2015-07-01 10:31:46', '2015-07-01 16:02:29', b'1', 'Yes'),
(230, '', '54', 'http://php.net/', 'aaaaaaaaaaaaaaaaaaaaaaaa', NULL, 1, NULL, '2015-07-01 16:08:43', '2015-07-01 10:38:43', '', b'1', ''),
(231, '', '54', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', 'hiiiiiiiiiiiiiiiiii', NULL, 1, NULL, '2015-07-01 16:11:51', '2015-07-01 10:41:51', '', b'1', ''),
(232, '', '', 'http://php.net/manual/en/keyword.class.php', 'utyutyu', NULL, 1, NULL, '2015-07-01 16:20:05', '2015-07-01 10:50:05', '', b'1', ''),
(233, '', '54', 'http://php.net/manual/en/index.php', 'ggggggggggg', NULL, 1, NULL, '2015-07-01 16:21:14', '2015-07-01 10:51:14', '', b'1', ''),
(234, '', '54', 'http://216.172.184.121/~ibruser/adsense/index.php/sharelink', 'sdfsdf', NULL, 1, NULL, '2015-07-01 16:46:02', '2015-07-01 11:16:02', '', b'1', ''),
(235, '', '54', 'http://php.net/manual/en/index.php', 'ss shrivastava', NULL, 1, NULL, '2015-07-01 16:47:12', '2015-07-01 11:17:12', '', b'1', ''),
(236, '', '57', 'https://www.google.co.in/?gfe_rd=cr&ei=3M2TVcroFuKpmQWQuJCIDg&gws_rd=ssl#q=about+5s+system', 'ab', NULL, 1, NULL, '2015-07-01 16:54:38', '2015-07-01 11:24:38', '', b'1', ''),
(237, '', '57', 'https://adityabirla.taleo.net/', '', NULL, 1, NULL, '2015-07-01 16:55:57', '2015-07-01 11:25:57', '2015-07-01 16:57:00', b'1', 'Yes'),
(238, '', '57', 'https://adityabirla.taleo.net/', 'https://adityabirla.taleo.net/', NULL, 1, NULL, '2015-07-01 16:57:41', '2015-07-01 11:27:41', '', b'1', ''),
(239, '', '52', 'https://adityabirla.taleo.net/', 'https://adityabirla.taleo.net/', NULL, 1, NULL, '2015-07-01 16:59:14', '2015-07-01 11:29:14', '2015-07-01 17:00:08', b'1', 'Yes'),
(240, '', '52', 'https://adityabirla.taleo.net/', 'https://adityabirla.taleo.net/https://adityabirla.taleo.net/', NULL, 1, NULL, '2015-07-01 17:00:40', '2015-07-01 11:30:40', '', b'1', ''),
(241, '', '', 'https://adityabirla.taleo.net/123', 'wwww', NULL, 1, NULL, '2015-07-01 17:02:04', '2015-07-01 11:32:04', '', b'1', ''),
(242, '', '', 'http://216.172.184.121/~ibruser/adsense/index.php/billing', '', NULL, 1, NULL, '2015-07-01 17:02:36', '2015-07-01 11:32:36', '2015-07-01 17:15:18', b'1', 'Yes'),
(243, '', '', 'https://developers.facebook.com/apps/1654266621474610/roles/test-users/', 'sulbha test', NULL, 1, NULL, '2015-07-01 17:06:50', '2015-07-01 11:36:50', '2015-07-01 17:10:37', b'1', 'Yes'),
(244, '', '', 'https://www.google.co.in/?gfe_rd=cr&ei=0NCTVeBP4a2ZBY7dntAP&gws_rd=ssl', 'ammmm', NULL, 1, NULL, '2015-07-01 17:07:03', '2015-07-01 11:37:03', '', b'1', ''),
(245, '', '', 'http://www.amazon.in/?tag=googhydrabk-21&ref_=pd_mn_ABKror1112', 'qqqqqq', NULL, 1, NULL, '2015-07-01 17:08:14', '2015-07-01 11:38:14', '2015-07-01 17:09:00', b'1', 'Yes'),
(246, '', '52', 'https://developers.facebook.com/products/ads/', 'test eli', NULL, 1, NULL, '2015-07-01 17:08:35', '2015-07-01 11:38:35', '2015-07-01 17:10:26', b'1', 'Yes'),
(247, '', '54', 'https://developers.facebook.com/tools/explorer/145634995501895/?method=GET&path=me%3Ffields%3Did%2Cname', 'hi sulbhaaaaaaaaaa', NULL, 1, NULL, '2015-07-01 17:09:17', '2015-07-01 11:39:17', '2015-07-01 17:10:08', b'1', 'Yes'),
(248, '', '', 'http://www.amazon.in/', '123', NULL, 1, NULL, '2015-07-01 17:11:57', '2015-07-01 11:41:57', '2015-07-01 17:12:00', b'1', 'Yes'),
(249, '', '', 'http://www.w3schools.com/', 'w3 tutorial learn ', NULL, 1, NULL, '2015-07-01 17:13:59', '2015-07-01 11:43:59', '2015-07-01 17:15:45', b'1', 'Yes'),
(250, '', '52', 'http://www.tizag.com/phpT/', 'learn php ', NULL, 1, NULL, '2015-07-01 17:14:52', '2015-07-01 11:44:52', '2015-07-01 17:15:40', b'1', 'Yes'),
(251, '', '57', 'http://www.amazon.in/gift-card-store/b/ref=nav_topnav_giftcert?ie=UTF8&node=3704982031', '123333333', NULL, 1, NULL, '2015-07-01 17:20:31', '2015-07-01 11:50:31', '2015-07-01 17:21:00', b'1', 'Yes'),
(252, '', '', 'http://laravel.com/docs/4.2/quick', 'leravel tutorial', NULL, 1, NULL, '2015-07-01 17:21:00', '2015-07-01 11:51:00', '2015-07-01 17:25:47', b'1', 'Yes'),
(253, '', '', 'http://tutorials.jenkov.com/angularjs/index.html', '', NULL, 1, NULL, '2015-07-01 17:22:07', '2015-07-01 11:52:07', '2015-07-01 17:24:55', b'1', 'Yes'),
(254, '', '57', 'http://www.amazon.in/Wedding-Engagement-Gift-Cards/b/ref=sv_gc_subnav_mainstore_2?ie=UTF8&node=4489235031', '11111111111', NULL, 1, NULL, '2015-07-01 17:22:33', '2015-07-01 11:52:33', '2015-07-01 17:23:00', b'1', 'Yes'),
(255, '', '57', 'http://www.amazon.in/Birthday-Gift-Cards/b/ref=sv_gc_subnav_mainstore_1?ie=UTF8&node=4489193031', '1111111', NULL, 1, NULL, '2015-07-01 17:24:31', '2015-07-01 11:54:31', '', b'1', ''),
(256, '', '', 'http://www.amazon.in/b/ref=sv_gc_subnav_mainstore_3?ie=UTF8&node=4489188031', 'qqqqqqqqq', NULL, 1, NULL, '2015-07-01 17:26:53', '2015-07-01 11:56:53', '', b'1', ''),
(257, '', '', 'http://www.amazon.in/123', 'abcd', NULL, 1, NULL, '2015-07-01 17:29:45', '2015-07-01 11:59:45', '2015-07-01 17:30:00', b'1', 'Yes'),
(258, '', '57', 'http://www.amazon.in/s/ref=lp_4489188031_nr_n_6?fst=as%3Aoff&rh=n%3A3704982031%2Cn%3A%213704985031%2Cn%3A%213704986031%2Cn%3A4489188031%2C', 'test test', NULL, 1, NULL, '2015-07-01 17:33:35', '2015-07-01 12:03:35', '', b'1', ''),
(259, '', '57', 'http://www.amazon.in/b/ref=sv_gc_subnav_mainstore_7?ie=UTF8&node=5620879031', '', NULL, 1, NULL, '2015-07-01 17:35:38', '2015-07-01 12:05:38', '2015-07-01 17:38:08', b'1', 'Yes'),
(260, '', '57', 'http://www.amazon.in/gp/goldbox/ref=nav_topnav_deals', '..........http://www.amazon.in/gp/goldbox/ref=nav_topnav_deals', NULL, 1, NULL, '2015-07-01 17:48:00', '2015-07-01 12:18:00', '', b'1', ''),
(261, '', '52', 'http://www.amazon.in/gp/goldbox/ref=nav_topnav_deals', 'http://www.amazon.in/gp/goldbox/ref=nav_topnav_deals', NULL, 1, NULL, '2015-07-01 17:49:34', '2015-07-01 12:19:34', '', b'1', ''),
(262, '', '', 'http://crontest.com', 'fgdfgdfg', NULL, 1, NULL, '2015-07-01 18:25:55', '2015-07-01 12:55:55', '2015-07-01 18:30:00', b'1', 'Yes'),
(263, '', '54', 'http://crontest.com', 'sdfgf', NULL, 1, NULL, '2015-07-01 18:26:32', '2015-07-01 12:56:32', '2015-07-01 18:30:00', b'1', 'Yes'),
(264, '', '53', 'http://crontest.com', 'dsgdfg', NULL, 1, NULL, '2015-07-01 18:26:49', '2015-07-01 12:56:49', '2015-07-01 18:30:43', b'1', 'Yes'),
(265, '', '57', 'http://ibrinfotech.com', 'Hi Check this', 'assets/uploads/1435755466_Screen_Shot_2015-07-01_at_3.27.35_pm.png', 1, NULL, '2015-07-01 18:27:46', '2015-07-01 12:57:46', '', b'1', ''),
(266, '', '', 'https://twitter.com/irbaaz_shaikh', 'Hi Cron It should not be disturbed', 'assets/uploads/1435755587_Screen_Shot_2015-06-30_at_12.45.29_pm.png', 1, NULL, '2015-07-01 18:29:47', '2015-07-01 12:59:47', '2015-07-01 18:28:00', b'1', 'Yes'),
(267, '', '52', 'http://www.dispostable.com', 'Hello test today there is no tomorrow', 'assets/uploads/1435755653_AppIcon.png', 1, NULL, '2015-07-01 18:30:53', '2015-07-01 13:00:53', '', b'1', ''),
(268, '', '', 'http://www.ebay.in/', 'hyo', 'assets/uploads/1435756456_Screen_Shot_2015-06-29_at_12.15.04_pm.png', 1, NULL, '2015-07-01 18:44:16', '2015-07-01 13:14:16', '2015-07-01 19:30:06', b'1', 'Yes'),
(269, '', '57', 'http://www.ebay.in/', 'Hello How are you ddoing', 'assets/uploads/1435756564_OutOfStock.JPG', 1, NULL, '2015-07-01 18:46:04', '2015-07-01 13:16:04', '', b'1', ''),
(270, '', '52', 'http://www.ebay.in/adsd', 'Hello how are you', 'assets/uploads/1435756592_OutOfStock.JPG', 1, NULL, '2015-07-01 18:46:32', '2015-07-01 13:16:32', '', b'1', ''),
(271, '', '52', 'http://www.ebay.in/asdfasdfadsfs', 'Lolz', 'assets/uploads/1435756647_Airtel_lgo.jpg', 1, NULL, '2015-07-01 18:47:27', '2015-07-01 13:17:27', '2015-07-01 18:49:06', b'1', 'Yes'),
(272, '', '', 'http://www.google.com', 'edfsa', NULL, 1, NULL, '2015-07-01 18:47:50', '2015-07-01 13:17:50', '', b'1', ''),
(273, '', '56', 'http://www.magzim.com/12-amazing-facts-about-apple/', 'Top 12 Amazing Facts About Apple iPhones You would love to know ', 'assets/uploads/1435776404_apple-logo-money.png', 1, NULL, '2015-07-02 00:16:45', '2015-07-01 18:46:45', '2015-07-02 00:17:17', b'1', 'Yes'),
(274, '', '', 'http://abpnews.abplive.in/', 'hi...??', NULL, 1, NULL, '2015-07-03 18:18:50', '2015-07-03 12:48:50', '', b'1', ''),
(275, '', '51;53', 'http://abpnews.abplive.in/', 'hi..?', NULL, 1, NULL, '2015-07-03 18:24:36', '2015-07-03 12:54:36', '', b'1', ''),
(276, '', '51;53', 'http:/asd.com', 'abcd', NULL, 1, NULL, '2015-07-03 18:26:37', '2015-07-03 12:56:37', '2015-07-03 18:27:00', b'1', 'Yes'),
(277, '', '', 'http:/aq.com', 'cate', 'assets/uploads/1435928268_Screenshot__19_.png', 1, NULL, '2015-07-03 18:27:48', '2015-07-03 12:57:48', '2015-07-03 18:28:00', b'1', 'Yes'),
(278, '', '', 'http://abpnews.abplive.in/gadgets/', 'direct post with image', 'assets/uploads/1435928658_Screenshot__18_.png', 1, NULL, '2015-07-03 18:34:18', '2015-07-03 13:04:18', '', b'1', ''),
(279, '', '', 'http:/abcd.com', ' image testing', 'assets/uploads/1435928852_Screenshot__16_.png', 1, NULL, '2015-07-03 18:37:32', '2015-07-03 13:07:32', '', b'1', ''),
(280, '', '', 'http://abpnews.abplive.in/gadgets/', 'testing', 'assets/uploads/1435928987_Screenshot__19_.png', 1, NULL, '2015-07-03 18:39:47', '2015-07-03 13:09:47', '2015-07-03 19:40:00', b'1', 'Yes'),
(281, '', '', 'http://abpnews.abplive.in/ajabgazab/', 'absnd', 'assets/uploads/1435929447_Screenshot__16_.png', 1, NULL, '2015-07-03 18:47:27', '2015-07-03 13:17:27', '2015-07-03 16:30:55', b'1', 'Yes'),
(282, '', '51', 'http://www.amazon.com/', 'qwweeewr', 'assets/uploads/1435930172_Screenshot__19_.png', 1, NULL, '2015-07-03 18:59:32', '2015-07-03 13:29:32', '2015-07-03 19:00:00', b'1', 'Yes'),
(283, '', '', 'http://timesofindia.indiatimes.com/home/headlines', 'abcd', NULL, 1, NULL, '2015-07-04 10:50:16', '2015-07-04 05:20:16', '2015-07-04 10:51:00', b'1', 'Yes'),
(284, '', '53;57', 'https://www.google.co.in/?gfe_rd=cr&ei=JHCXVezUFc6GvATq8abQAw&gws_rd=ssl#q=disaster+management', 'assd', NULL, 1, NULL, '2015-07-04 11:04:03', '2015-07-04 05:34:03', '', b'1', ''),
(285, '', '51;53', 'http://readersdigest.co.in/', 'ans...', NULL, 1, NULL, '2015-07-04 11:17:45', '2015-07-04 05:47:45', '', b'1', ''),
(286, '', '', 'http://readersdigest.co.in/', 'as...', NULL, 1, NULL, '2015-07-04 11:22:19', '2015-07-04 05:52:19', '', b'1', ''),
(287, '', '57', 'http:/abcd.com', 'abcd', NULL, 1, NULL, '2015-07-06 10:53:21', '2015-07-06 05:23:21', '', b'1', ''),
(288, '', '57', '', 'hi abc', NULL, 1, NULL, '2015-07-06 10:56:24', '2015-07-06 05:26:24', '', b'1', ''),
(289, '', '', '', 'asas', NULL, 1, NULL, '2015-07-06 10:56:55', '2015-07-06 05:26:55', '', b'1', ''),
(290, '', '57', '', 'is a 1999 action space shooter video game for Microsoft Windows. It was designed by Marc Michalik and Walter Wright and developed at GameFX, a small studio composed of former members of Looking Glass Studios. Originally titled Out of the Void, the project at first had no relationship to Sinistar, which was released by Williams in 1982. After licensing the franchise from Midway Games, GameFX developed the game as a sequel. The player''s goal in both installments is to use starships, weapons and power-ups to destroy the Sinistar, a large bio-mechanical machine. Unlike its predecessor, the sequel has full three-dimensional graphics and gameplay. Sinistar: Unleashed got a mixed reception when released: some critics lauded its graphics and new features, and several journalists felt that it stayed true to the feel of the original game, but other critics faulted the boss characters and repetitiveness of the gameplay.', NULL, 1, NULL, '2015-07-06 12:06:23', '2015-07-06 06:36:23', '', b'1', ''),
(291, '', '57', '', 'asfdghhjl;llllllllllllll', NULL, 1, NULL, '2015-07-06 12:08:10', '2015-07-06 06:38:10', '', b'1', ''),
(292, '', '57', 'http://www.amazon.com/gp/goldbox/ref=nav_cs_gb', 'asdhhjkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk kllll;;;;;;;vvvvvvvvvvvvvvvvvvvc sddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', NULL, 1, NULL, '2015-07-06 15:55:38', '2015-07-06 10:25:38', '', b'1', '');
INSERT INTO `share_link` (`sharelink_id`, `category_id`, `user_id`, `link`, `content`, `image`, `is_deleted`, `error`, `create_date`, `update_date`, `scheduler_datetime`, `scheduler_status`, `Scheduler_Set`) VALUES
(293, '', '57', 'https://en.wikipedia.org/wiki/Bank_fraud', 'In order to hide serious financial problems, some businesses have been known to use fraudulent bookkeeping to overstate sales and income, in', NULL, 1, NULL, '2015-07-06 16:00:55', '2015-07-06 10:30:55', '', b'1', ''),
(294, '', '', 'https://en.wikipedia.org/wiki/Bank', 'weeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeesddfff ffggg sd', NULL, 1, NULL, '2015-07-06 16:10:28', '2015-07-06 10:40:28', '', b'1', ''),
(295, '', '', 'https://en.wikipedia.org/wiki/Bank', 'weeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeesddfff ffggg sd', NULL, 1, NULL, '2015-07-06 16:10:36', '2015-07-06 10:40:36', '', b'1', ''),
(296, '', '57', 'http://www.amazon.com/stream/ref=nav_swm_arrow?pf_rd_p=2128485682&pf_rd_s=nav-sitewide-msg&pf_rd_t=4201&pf_rd_i=navbar-4201&pf_rd_m=ATVPDKIKX0DER', '', NULL, 1, NULL, '2015-07-07 11:45:58', '2015-07-07 06:15:58', '', b'1', ''),
(297, '', '53', 'http://test_.com', 'fg', NULL, 1, NULL, '2015-07-07 13:09:56', '2015-07-07 07:39:56', '', b'1', ''),
(298, '', '53', 'http://test_.com', 'fg', NULL, 2, '(#100) link is not properly formatted', '2015-07-07 13:10:44', '2015-07-07 07:40:44', '', b'1', ''),
(299, '', '53', 'http://test_.com', '', NULL, 2, '(#100) link is not properly formatted', '2015-07-07 13:12:11', '2015-07-07 07:42:11', '', b'1', ''),
(300, '', '53', 'http://test_.com', '', NULL, 2, '(#100) link is not properly formatted', '2015-07-07 13:12:13', '2015-07-07 07:42:13', '', b'1', ''),
(301, '', '53', 'http://test_.com', 'sdas', NULL, 2, '(#100) link is not properly formatted', '2015-07-07 13:13:35', '2015-07-07 07:43:35', '2015-07-07 13:15:00', b'1', 'Yes'),
(302, '', '53', 'https://www.facebook.com/', 'fdgf', NULL, 0, NULL, '2015-07-07 14:44:44', '2015-07-07 09:14:44', '', b'1', ''),
(303, '', '53', 'https://goo.gl/', 'sdfasdad', NULL, 0, NULL, '2015-07-07 15:22:00', '2015-07-07 09:52:00', '', b'1', ''),
(304, '', '53', 'https://www.facebook.com/', 'ashvin', NULL, 0, NULL, '2015-07-07 15:23:18', '2015-07-07 09:53:18', '', b'1', ''),
(305, '', '56', '', 'Hi', NULL, 0, NULL, '2015-07-07 17:25:07', '2015-07-07 11:55:07', '', b'1', 'Yes'),
(306, '', '56', '', 'test test', NULL, 0, NULL, '2015-07-07 17:25:55', '2015-07-07 11:55:55', '', b'1', ''),
(307, '', '56', 'http://www.magzim.com', 'test test test', NULL, 0, NULL, '2015-07-07 19:17:19', '2015-07-07 13:47:19', '', b'1', ''),
(308, '', '52', 'http://www.test.com', 'HI dhdhddkhsdfkhfskdf', NULL, 0, NULL, '2015-07-07 19:31:54', '2015-07-07 14:01:54', '', b'1', ''),
(309, '', '52', 'http://www.magzim.com', 'Testing ', NULL, 0, NULL, '2015-07-07 19:32:47', '2015-07-07 14:02:47', '2015-07-07 19:34:20', b'1', 'Yes'),
(310, '', '52', 'http://www.magzim.com', '', NULL, 0, NULL, '2015-07-07 19:52:13', '2015-07-07 14:22:13', '', b'1', ''),
(311, '', '56', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', 'hjvjhfhh', NULL, 0, NULL, '2015-07-07 19:53:11', '2015-07-07 14:23:11', '', b'1', ''),
(312, '', '52', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/nggallery/image/brown_tree_snake_boiga_irregularis_2-jpg', '', NULL, 0, NULL, '2015-07-07 20:05:01', '2015-07-07 14:35:01', '', b'1', ''),
(313, '', '52', 'http://www.storypick.com', 'dksdnsdknsll', 'assets/uploads/1436279855_Android-Pissing-on-Apples-logo.jpg', 0, NULL, '2015-07-07 20:07:35', '2015-07-07 14:37:35', '', b'1', ''),
(314, '', '57', 'http://www.amazon', 'asssssddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddnnnnnnnnnnn', NULL, 2, 'Code: 186 Error: Status is over 140 characters.', '2015-07-08 10:03:16', '2015-07-08 04:33:16', '', b'1', ''),
(315, '', '57', 'http://www.amazon.com/gift-cards/b/ref=nav_cs_gc?ie=UTF8&node=2238192011', 'asdhf', NULL, 0, NULL, '2015-07-08 10:06:08', '2015-07-08 04:36:08', '', b'1', ''),
(316, '', '57', 'https://www.google.co.in/?gfe_rd=cr&ei=nuecVaDdG4vBuATc1ITQBA&gws_rd=ssl#q=askew', 'assdsdfdgggggggggggggg', NULL, 2, 'Code: 220 Error: Your credentials do not allow access to this resource.', '2015-07-08 14:35:07', '2015-07-08 09:05:07', '', b'1', ''),
(317, '', '57', 'https://www.google.co.in/?gfe_rd=cr&ei=nuecVaDdG4vBuATc1ITQBA&gws_rd=ssl#q=askew', 'abcd', NULL, 2, 'Code: 220 Error: Your credentials do not allow access to this resource.', '2015-07-08 14:37:14', '2015-07-08 09:07:14', '2015-07-08 14:38:00', b'1', 'Yes'),
(318, '', '53', 'https://www.google.co.in/?gfe_rd=cr&ei=nuecVaDdG4vBuATc1ITQBA&gws_rd=ssl#q=askew', 'abcd', NULL, 0, NULL, '2015-07-08 14:37:47', '2015-07-08 09:07:47', '', b'1', 'Yes'),
(319, '', '57', 'http://www.flipkart.com/mens-footwear', 'aaa', NULL, 2, 'Code: 220 Error: Your credentials do not allow access to this resource.', '2015-07-08 14:39:13', '2015-07-08 09:09:13', '', b'1', ''),
(320, '', '57', 'http://www.amazon.com/unlocked-cell-phones/b/ref=sv_cps_2?ie=UTF8&node=2407749011', 'as', NULL, 2, 'Code: 220 Error: Your credentials do not allow access to this resource.', '2015-07-08 14:41:03', '2015-07-08 09:11:03', '', b'1', ''),
(321, '', '54', 'http://www.flipkart.com/mens-footwear', 'asd', NULL, 2, 'Code: 220 Error: Your credentials do not allow access to this resource.', '2015-07-08 14:45:11', '2015-07-08 09:15:11', '', b'1', ''),
(322, '', '53', 'http://www.amazon.com/gift-cards/b/ref=nav_cs_gc?ie=UTF8&node=2238192011', 'hii', NULL, 0, NULL, '2015-07-08 15:31:41', '2015-07-08 10:01:41', '', b'1', ''),
(323, '', '53', 'http://www.snapdeal.com/products/mens-footwear-sports-shoes?utm_source=earth_womens_footwear&Utm_content=227', 'ab', NULL, 0, NULL, '2015-07-08 15:38:02', '2015-07-08 10:08:02', '2015-07-08 15:38:00', b'1', 'Yes'),
(324, '', '53', 'http://www.snapdeal.com/products/mens-footwear-boots', 'boot', NULL, 0, NULL, '2015-07-08 15:41:39', '2015-07-08 10:11:39', '', b'1', ''),
(325, '', '53', 'http://www.snapdeal.com/products/mens-footwear-loafers#bcrumbLabelId:391', 'as', NULL, 0, NULL, '2015-07-08 15:43:04', '2015-07-08 10:13:04', '2015-07-08 15:43:00', b'1', 'Yes'),
(326, '', '53', 'http://www.snapdeal.com/products/mens-footwear-loafers#bcrumbLabelId:391', 'hiiiiiii', NULL, 0, NULL, '2015-07-08 15:43:40', '2015-07-08 10:13:40', '2015-07-08 15:44:00', b'1', 'Yes'),
(327, '', '53', 'http://www.snapdeal.com/products/mens-footwear-loafers#bcrumbLabelId:391', 'xxxxxx', NULL, 0, NULL, '2015-07-08 15:45:02', '2015-07-08 10:15:02', '', b'1', ''),
(328, '', '57', 'http://www.snapdeal.com/products/mens-footwear-loafers#bcrumbLabelId:391', 'qqqq', NULL, 0, NULL, '2015-07-08 15:47:18', '2015-07-08 10:17:18', '2015-07-08 15:48:00', b'1', 'Yes'),
(329, '', '61', 'http://www.snapdeal.com/products/footwear#bcrumbLabelId:458', 'zzzz', NULL, 2, 'Code: 220 Error: Your credentials do not allow access to this resource.', '2015-07-08 15:56:29', '2015-07-08 10:26:29', '', b'1', ''),
(330, '', '53', 'http://www.snapdeal.com/products/womens-footwear#bcrumbLabelId:18', 'snapdeal', NULL, 0, NULL, '2015-07-08 15:59:16', '2015-07-08 10:29:16', '2015-07-08 16:00:00', b'1', 'Yes'),
(331, '', '61', 'http://www.snapdeal.com/products/kids-footwear', 'qq', NULL, 0, NULL, '2015-07-08 16:02:31', '2015-07-08 10:32:31', '', b'1', ''),
(332, '', '53', 'http://www.snapdeal.com/products/kids-footwear?ref=', 'post', NULL, 0, NULL, '2015-07-08 16:05:12', '2015-07-08 10:35:12', '2015-07-08 16:05:00', b'1', 'Yes'),
(333, '', '53', 'http://www.snapdeal.com/products/kids-footwear', 'new', NULL, 0, NULL, '2015-07-08 16:06:36', '2015-07-08 10:36:36', '', b'1', ''),
(334, '', '61', 'http://www.snapdeal.com/info/orderStatus', 'qw', NULL, 0, NULL, '2015-07-08 16:08:29', '2015-07-08 10:38:29', '', b'1', ''),
(335, '', '61', 'http://www.snapdeal.com/help', 'help', NULL, 0, NULL, '2015-07-08 16:09:33', '2015-07-08 10:39:33', '2015-07-08 16:09:00', b'1', 'Yes'),
(336, '', '', 'http://www.snapdeal.com/products/home-kitchen-home-decoratives', 'decor', NULL, 0, NULL, '2015-07-08 16:12:38', '2015-07-08 10:42:38', '2015-07-08 16:13:00', b'1', 'Yes'),
(337, '', '', 'http://www.snapdeal.com/products/mobiles', 'previous', NULL, 0, NULL, '2015-07-08 16:16:26', '2015-07-08 10:46:26', '2015-07-08 16:16:00', b'1', 'Yes'),
(338, '', '', 'http://www.snapdeal.com/products/girls-clothing', 'gl cloths', NULL, 0, NULL, '2015-07-08 16:21:51', '2015-07-08 10:51:51', '2015-07-08 16:22:00', b'1', 'Yes'),
(339, '', '', 'http://www.snapdeal.com/products/womens-footwear#bcrumbLabelId:18', 'aq', NULL, 0, NULL, '2015-07-08 16:27:18', '2015-07-08 10:57:18', '', b'1', ''),
(340, '', '', 'http://www.snapdeal.com/products/girls-clothing', 'ggg', NULL, 0, NULL, '2015-07-08 16:29:12', '2015-07-08 10:59:12', '', b'1', ''),
(341, '', '', 'http://www.snapdeal.com/products/sports-hobbies-fitness', 'aaa', NULL, 0, NULL, '2015-07-08 16:34:14', '2015-07-08 11:04:14', '', b'1', ''),
(342, '', '63', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', 'Top 10 Myths About Sex debunked ', 'assets/uploads/1436442934_PENIS-SIZE.jpg', 0, NULL, '2015-07-09 17:25:34', '2015-07-09 11:55:34', '', b'1', ''),
(343, '', '61', 'https://www.abcd.org.uk/links', 'assdjdjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjffffffffffffffffffffffffffffffffffffffff', 'assets/uploads/1436518821_Mind-Music.jpg', 0, NULL, '2015-07-10 14:30:21', '2015-07-10 09:00:21', '', b'1', ''),
(344, '', '63', '', 'Crying is for plain women. Pretty women go shopping.', NULL, 0, NULL, '2015-07-10 17:03:36', '2015-07-10 11:33:36', '', b'1', ''),
(345, '', '63', '', 'I believe that if life gives u lemons, you should make lemonade.And try to find somebody whose life has given them vodka, and have a party', NULL, 0, NULL, '2015-07-10 17:04:22', '2015-07-10 11:34:22', '', b'1', ''),
(346, '', '63', '', 'Experience is the child of thought, and thought is the child of action.', NULL, 0, NULL, '2015-07-10 17:06:34', '2015-07-10 11:36:34', '', b'1', ''),
(347, '', '63', '', 'Be thankful for the bad things in life. They open your eyes to see the good things you weren''t paying attention to before.', NULL, 0, NULL, '2015-07-10 17:08:20', '2015-07-10 11:38:20', '', b'1', ''),
(348, '', '63', 'http://www.magzim.com/8-biggest-mistakes-men-make-in-bed/', '8 Silly Mistakes Men Do In Bed During Sex', 'assets/uploads/1436528614_kissing_calories.jpg', 0, NULL, '2015-07-10 17:13:34', '2015-07-10 11:43:34', '', b'1', ''),
(349, '', '63', 'http://www.magzim.com/10-incredible-health-benefits-of-kissing/', '10 Incredible Health Benefits of Kissing ', 'assets/uploads/1436529198_Kissing_Happy.jpg', 0, NULL, '2015-07-10 17:23:18', '2015-07-10 11:53:18', '2015-07-10 18:00:00', b'1', 'Yes'),
(350, '', '63', 'http://www.magzim.com/20-unknown-facts-you-didnt-know-about-adolf-hitler/', '20 Unknown Facts You Didn''t Know about Adolf', 'assets/uploads/1436631503_123j.jpg', 0, NULL, '2015-07-11 21:48:23', '2015-07-11 16:18:23', '', b'1', ''),
(351, '', '63', '', 'Always end the day with a positive thought. No matter how hard things were, tomorrow is a fresh opportunity to make it better.', NULL, 0, NULL, '2015-07-11 23:18:16', '2015-07-11 17:48:16', '', b'1', ''),
(352, '', '63', '', 'Bananas are known to lower the risk of heart attacks and strokes, as well as decrease your risk of getting cancer, according to the FDA.', NULL, 0, NULL, '2015-07-11 23:19:04', '2015-07-11 17:49:04', '', b'1', ''),
(353, '', '63', '', 'When a baby kangaroo is born, it latches onto its mother’s nipple and the nipple expands in its mouth. The baby cannot let go for 100 days.', NULL, 0, NULL, '2015-07-11 23:19:54', '2015-07-11 17:49:54', '', b'1', ''),
(354, '', '63', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', 'Top 10 Myths About Sex debunked ', 'assets/uploads/1436637141_penisshoesize.png', 0, NULL, '2015-07-11 23:22:21', '2015-07-11 17:52:21', '', b'1', ''),
(355, '', '63', 'http://www.magzim.com/15-things-you-never-knew-about-how-i-met-your-mother/', '15 Things You Never Knew About ‘How I Met ', 'assets/uploads/1436641463_HIMYM-How-I-Met-Your-Mother.jpg', 0, NULL, '2015-07-12 00:34:23', '2015-07-11 19:04:23', '2015-07-12 04:00:54', b'1', 'Yes'),
(356, '', '63', '', '19% of surveyed transgender people have been denied health care due to their transgender or gender non-conforming status.', NULL, 0, NULL, '2015-07-12 00:36:55', '2015-07-11 19:06:55', '', b'1', ''),
(357, '', '63', '', '5-10% of internet users are actually unable to control how much time they spend online because of psychological addiction.', NULL, 0, NULL, '2015-07-12 00:37:35', '2015-07-11 19:07:35', '2015-07-12 02:00:26', b'1', 'Yes'),
(358, '', '63', '', 'It was revealed in Season 4 of Spongebob Squarepants that Gary, Spongebob''s pet, is actually Patrick''s cousin.', NULL, 0, NULL, '2015-07-12 00:38:02', '2015-07-11 19:08:02', '2015-07-12 03:00:57', b'1', 'Yes'),
(359, '', '63', '', 'You should delete your browser cookies before buying airline tickets - Ticket fares go up when you’ve visited a site multiple times.', NULL, 0, NULL, '2015-07-12 00:38:50', '2015-07-11 19:08:50', '2015-07-12 03:30:44', b'1', 'Yes'),
(360, '', '63', 'http://www.magzim.com/12-amazing-facts-about-apple/', 'Top 12 Amazing Facts About Apple iPhones You would love to know ', 'assets/uploads/1436642182_apple-logo-money.png', 0, NULL, '2015-07-12 00:46:22', '2015-07-11 19:16:22', '2015-07-12 04:00:53', b'1', 'Yes'),
(361, '', '63;66', 'http://www.magzim.com/10-types-of-women-men-like-the-most/', '10 Types Of Women That Really Turn Men On', 'assets/uploads/1436734517_diva.jpg', 0, NULL, '2015-07-13 02:25:17', '2015-07-12 20:55:17', '', b'1', ''),
(362, '', '65', 'http://www.magzim.com/10-types-of-women-men-like-the-most/', '10 Types Of Women That Really Turn Men On', 'assets/uploads/1436734662_diva.jpg', 0, NULL, '2015-07-13 02:27:42', '2015-07-12 20:57:42', '', b'1', ''),
(363, '', '63', '', 'The "Pinky Promise" originally indicated that the person who breaks the promise must cut off their pinky finger', NULL, 0, NULL, '2015-07-13 02:39:08', '2015-07-12 21:09:08', '2015-07-13 03:00:00', b'1', 'Yes'),
(364, '', '63', '', 'Mosquito bite? Press a *hot* spoon onto the spot. The heat will destroy the protein that caused the reaction and the itching will stop.', NULL, 0, NULL, '2015-07-13 02:39:40', '2015-07-12 21:09:40', '2015-07-13 03:30:34', b'1', 'Yes'),
(365, '', '63', '', 'Why do they call it "The mall"? Instead of going to one store, you go to "them all". Them all = The Mall. Clever.', NULL, 0, NULL, '2015-07-13 02:40:21', '2015-07-12 21:10:21', '2015-07-13 03:45:08', b'1', 'Yes'),
(366, '', '63', '', 'People who eat nuts 7 or more times a week have a 20% lower death rate than those who don''t.', NULL, 0, NULL, '2015-07-13 02:41:09', '2015-07-12 21:11:09', '2015-07-13 03:15:44', b'1', 'Yes'),
(367, '', '63;65;66', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', 'Top 10 Myths About Sex debunked ', 'assets/uploads/1436735694_PENIS-SIZE.jpg', 0, NULL, '2015-07-13 02:44:54', '2015-07-12 21:14:54', '2015-07-13 04:10:28', b'1', 'Yes'),
(368, '', '56', 'http://www.magzim.com/top-10-mind-blowing-facts-about-snakes/', '', NULL, 0, NULL, '2015-07-13 17:45:26', '2015-07-13 12:15:26', '', b'1', ''),
(369, '', '65', 'http://www.magzim.com/i-am-amsterdam/', 'Best Reasons why one should visit Amsterdam ', 'assets/uploads/1436792627_iamamsterdam.jpg', 0, NULL, '2015-07-13 18:33:47', '2015-07-13 13:03:47', '', b'1', ''),
(370, '', '63;65', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', '99% Girls do not reach Orgasm bcoz of these silly mistake', 'assets/uploads/1436807475_kissing_calories.jpg', 0, NULL, '2015-07-13 22:41:15', '2015-07-13 17:11:15', '', b'1', ''),
(371, '', '65', 'http://www.magzim.com/top-10-love-quotes-from-famous-romantic-novels/', '10 Romantic Quotes to make Your Girl fall in love with you in seconds', 'assets/uploads/1436874239_Kissing_Happy.jpg', 0, NULL, '2015-07-14 17:13:59', '2015-07-14 11:43:59', '', b'1', ''),
(372, '', '63', 'http://www.magzim.com/10-types-of-women-men-like-the-most/', '10 Types Of Women That Really Turn Men On ', 'assets/uploads/1436874438_sexy_girl.jpg', 0, NULL, '2015-07-14 17:17:18', '2015-07-14 11:47:18', '', b'1', ''),
(373, '', '63', 'http://www.magzim.com/10-types-of-women-men-like-the-most/', '10 Types Of Women That Really Turn Men On ', 'assets/uploads/1436874766_sexy_girl.jpg', 0, NULL, '2015-07-14 17:22:46', '2015-07-14 11:52:46', '', b'1', ''),
(374, '', '64', '', 'Hi test', NULL, 0, NULL, '2015-07-14 19:43:21', '2015-07-14 14:13:21', '', b'1', ''),
(375, '', '63;65', 'http://www.magzim.com/10-types-of-women-men-like-the-most/', '10 Types Of Women That Really Turn On Men for S3X..', 'assets/uploads/1436992657_trophygirl.jpg', 0, NULL, '2015-07-16 02:07:37', '2015-07-15 20:37:37', '2015-07-16 04:00:26', b'1', 'Yes'),
(376, '', '63', '', 'Spending more time outdoors can decrease your chances of depression.', NULL, 0, NULL, '2015-07-16 02:08:36', '2015-07-15 20:38:36', '2015-07-16 02:30:28', b'1', 'Yes'),
(377, '', '63', '', 'When someone tickles you, your laughter is actually a sign of panic, anxiety and nervousness.', NULL, 0, NULL, '2015-07-16 02:09:16', '2015-07-15 20:39:16', '2015-07-16 03:00:07', b'1', 'Yes'),
(378, '', '63', '', 'People at the same level of attractiveness are more likely to end up together.', NULL, 0, NULL, '2015-07-16 02:10:08', '2015-07-15 20:40:08', '2015-07-16 03:30:02', b'1', 'Yes'),
(379, '', '63', '', '80% of people remain quiet even when they really want to say something in order to avoid an argument with someone they care about.', NULL, 0, NULL, '2015-07-16 02:11:27', '2015-07-15 20:41:27', '2015-07-16 03:59:58', b'1', 'Yes'),
(380, '', '63', 'http://www.magzim.com/amazing-facts-las-vegas/', 'Mind Blowing Facts about Las Vegas ', 'assets/uploads/1437023086_vegas1.jpg', 0, NULL, '2015-07-16 10:34:46', '2015-07-16 05:04:46', '2015-07-16 11:00:35', b'1', 'Yes'),
(381, '', '53', '', 'fgdfggf', NULL, 0, NULL, '2015-07-16 14:44:38', '2015-07-16 09:14:38', '', b'1', ''),
(382, '', '64', '', 'test ', NULL, 0, NULL, '2015-07-16 14:58:42', '2015-07-16 09:28:42', '', b'1', ''),
(383, '', '64', '', 'test', NULL, 0, NULL, '2015-07-16 20:03:03', '2015-07-16 14:33:03', '', b'1', ''),
(384, '', '51', 'http://www.123.com', 'test', NULL, 0, NULL, '2015-07-17 14:15:22', '2015-07-17 08:45:22', '', b'1', ''),
(385, '', '63', 'http://www.magzim.com/diet-not/', 'This Trick has doctors shocked!???? easily shed 20lbs a month approved for men & women both >> ', 'assets/uploads/1437318633_diet.jpg', 2, 'Code: 186 Error: Status is over 140 characters.', '2015-07-19 20:40:33', '2015-07-19 15:10:33', '', b'1', ''),
(386, '', '63', '', 'test', NULL, 0, NULL, '2015-07-19 20:41:17', '2015-07-19 15:11:17', '', b'1', ''),
(387, '', '63', 'http://www.magzim.com/diet-not/', 'This Trick has doctors shocked! Easily shed 20lbs a month approved for men & women both >>', 'assets/uploads/1437318764_diet.jpg', 0, NULL, '2015-07-19 20:42:44', '2015-07-19 15:12:44', '', b'1', ''),
(388, '', '63', 'http://www.magzim.com/titanic-facts-10-outstanding-facts-about-titanic/', '10 Outstanding facts about Titanic that will blow your mind >> ', 'assets/uploads/1437337636_Titanic_california.jpg', 0, NULL, '2015-07-20 01:57:16', '2015-07-19 20:27:16', '', b'1', ''),
(389, '', '69', 'http://www.magzim.com/10-types-of-women-men-like-the-most/', '10 Types Of Women That Really Turn Men On ', 'assets/uploads/1437420042_trophygirl.jpg', 0, NULL, '2015-07-21 00:50:42', '2015-07-20 19:20:42', '', b'1', ''),
(390, '', '69', 'http://www.magzim.com/8-biggest-mistakes-men-make-in-bed/', '', NULL, 0, NULL, '2015-07-21 00:53:29', '2015-07-20 19:23:29', '', b'1', ''),
(391, '', '67', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', 'Terrible Mistakes Couples Make After Doing SEX ', 'assets/uploads/1437420921_sleepyaftersex.jpg', 0, NULL, '2015-07-21 01:05:21', '2015-07-20 19:35:21', '', b'1', ''),
(392, '', '68', 'http://www.magzim.com/10-types-of-women-men-like-the-most/', '10 Types Of Women That Really Turn On Men for Sex ', 'assets/uploads/1437421147_trophygirl.jpg', 0, NULL, '2015-07-21 01:09:07', '2015-07-20 19:39:07', '', b'1', ''),
(393, '', '63', 'http://www.magzim.com/8-biggest-mistakes-men-make-in-bed/', 'Never ever do these stupid mistakes during SEX ', 'assets/uploads/1437426905_Mistakes-Men-Lovemaking-Orgasm.jpg', 0, NULL, '2015-07-21 02:45:05', '2015-07-20 21:15:05', '', b'1', ''),
(394, '', '69', 'http://www.magzim.com/top-10-myths-about-sex-debunked/', '', NULL, 0, NULL, '2015-07-21 10:25:15', '2015-07-21 04:55:15', '', b'1', ''),
(395, '', '63;65;66;67;68', 'http://www.magzim.com/10-types-of-women-men-like-the-most/', '99% Guys get horny when they see these kinda girls ', 'assets/uploads/1437514760_xxx.jpg', 0, NULL, '2015-07-22 03:09:20', '2015-07-21 21:39:20', '2015-07-22 04:00:06', b'1', 'Yes'),
(396, '', '63;65;66;67;68', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', '99% Girls do not reach Orgasm bcoz of these silly mistake ', 'assets/uploads/1437514922_Mistakes-Men-Lovemaking-Orgasm.jpg', 0, NULL, '2015-07-22 03:12:02', '2015-07-21 21:42:02', '2015-07-22 07:00:45', b'1', 'Yes'),
(397, '', '63;65;66', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', '99% Girls do not reach Orgasm bcoz of these silly mistake men do in bed', 'assets/uploads/1437587725_Mistakes-Men-Lovemaking-Go-Easy-with-Her.jpg', 2, 'Code: 89 Error: Invalid or expired token.', '2015-07-22 23:25:25', '2015-07-22 17:55:25', '', b'1', ''),
(398, '', '63', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', '99% Girls do not reach Orgasm bcoz of these silly mistake men do in bed ', 'assets/uploads/1437587979_Mistakes-Men-Lovemaking-Go-Easy-with-Her.jpg', 2, 'Code: 89 Error: Invalid or expired token.', '2015-07-22 23:29:39', '2015-07-22 17:59:39', '', b'1', ''),
(399, '', '68', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', '99% Girls do not reach Orgasm bcoz of these silly mistake men do in bed  ', 'assets/uploads/1437588611_Mistakes-Men-lovemaking-.jpeg', 0, NULL, '2015-07-22 23:40:11', '2015-07-22 18:10:11', '', b'1', ''),
(400, '', '63', '', 'Crying keeps you healthy by literally flushing away harmful bacteria and reducing stress.', NULL, 0, NULL, '2015-07-23 14:08:14', '2015-07-23 08:38:14', '', b'1', ''),
(401, '', '63', 'http://www.magzim.com', '"Crying keeps you healthy by literally flushing away harmful bacteria and reducing stress."', 'assets/uploads/1437640767_Mistakes-Men-Lovemaking-Go-Easy-with-Her.jpg', 0, NULL, '2015-07-23 14:09:27', '2015-07-23 08:39:27', '', b'1', ''),
(402, '', '63', '', 'test', NULL, 0, NULL, '2015-07-23 18:06:17', '2015-07-23 12:36:17', '', b'1', ''),
(403, '', '66;69', 'http://www.magzim.com/diet-not/', 'Foods and Dieting Habits that will turn your mood on ', 'assets/uploads/1437913378_eatingaftersex_Kim.jpg', 0, NULL, '2015-07-26 17:52:59', '2015-07-26 12:22:59', '', b'1', ''),
(404, '', '65;66;68;75', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college.Dont believe? Try #4 n thnk me later', 'assets/uploads/1438117863_Dating_Tips_Don’t_Overshare.jpg', 0, NULL, '2015-07-29 02:41:03', '2015-07-28 21:11:03', '2015-07-29 04:00:49', b'1', 'Yes'),
(405, '', '63', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that will get u laid with girl of ur dreams in minutes. Try it once >>', 'assets/uploads/1438118039_Dating_Tips_There_are_always_More_Fish.jpg', 0, NULL, '2015-07-29 02:43:59', '2015-07-28 21:13:59', '2015-07-29 05:00:01', b'1', 'Yes'),
(406, '', '72', 'http://www.magzim.com/the-worlds-most-strongest-beers/', '99% people still don''t know which is the world''s strongest beer. See here to know >>', 'assets/uploads/1438118273_Strongest_Beers_world_Snake_Venom.jpg', 0, NULL, '2015-07-29 02:47:53', '2015-07-28 21:17:53', '2015-07-29 05:00:38', b'1', 'Yes'),
(407, '', '63;65;66;68;75', 'http://www.magzim.com/10-interesting-marijuana-facts-and-myths/', '10 Incredible Facts and Myths about Marijuana Debunked ', 'assets/uploads/1438118892_marijuana-money-istock.jpg', 0, NULL, '2015-07-29 02:58:12', '2015-07-28 21:28:12', '2015-07-29 07:00:56', b'1', 'Yes'),
(408, '', '63', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college. Dont believe? Try #2 n thnk me late', 'assets/uploads/1438134715_Dating_Tips_There_are_always_More_Fish.jpg', 2, 'Code: 186 Error: Status is over 140 characters.', '2015-07-29 07:21:55', '2015-07-29 01:51:55', '', b'1', ''),
(409, '', '63', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that will get u laid with girl of ur dreams in minutes. Try it once >>', 'assets/uploads/1438134946_Dating_Tips_There_are_always_More_Fish.jpg', 0, NULL, '2015-07-29 07:25:46', '2015-07-29 01:55:46', '', b'1', ''),
(410, '', '63', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college.Dont believe? Try #2 n thnk me later', 'assets/uploads/1438135375_Dating_Tips_Don’t_Overshare.jpg', 2, 'Code: 186 Error: Status is over 140 characters.', '2015-07-29 07:32:55', '2015-07-29 02:02:55', '', b'1', ''),
(411, '', '63', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college.Dont believe?Try #5 n thnk me later', 'assets/uploads/1438135653_Dating_Tips_Don’t_Overshare.jpg', 2, 'Code: 186 Error: Status is over 140 characters.', '2015-07-29 07:37:33', '2015-07-29 02:07:33', '', b'1', ''),
(412, '', '63', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college.Dont believe?Try#2 n thnk me later', 'assets/uploads/1438135811_Dating_Tips_Watch_Their_Eyes.jpg', 0, NULL, '2015-07-29 07:40:11', '2015-07-29 02:10:11', '', b'1', ''),
(413, '', '63', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college.Dont believe?Try #2 n thnk me later', 'assets/uploads/1438136312_Dating_Tips_Don’t_Overshare.jpg', 2, 'Code: 186 Error: Status is over 140 characters.', '2015-07-29 07:48:32', '2015-07-29 02:18:32', '', b'1', ''),
(414, '', '63', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college.Dont believe?Try#2 n thnk me later', 'assets/uploads/1438136420_Dating_Tips_Don’t_Overshare.jpg', 0, NULL, '2015-07-29 07:50:20', '2015-07-29 02:20:20', '', b'1', ''),
(415, '', '63;65;66;68;75', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college.Dont believe?Try#2 n thnk me later', 'assets/uploads/1438136491_Dating_Tips_Don’t_Overshare.jpg', 0, NULL, '2015-07-29 07:51:31', '2015-07-29 02:21:31', '2015-07-29 10:00:19', b'1', 'Yes'),
(416, '', '60', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college. Dont believe? Try #2 n thnk me lat', 'assets/uploads/1438150989_cover-121-nobody-perfect-facebook-cover-1388015470.jpg', 0, NULL, '2015-07-29 11:53:09', '2015-07-29 06:23:09', '', b'1', ''),
(417, '', '60', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college. Dont believe? Try #2 n thnk me lat', 'assets/uploads/1438151077_cover-121-nobody-perfect-facebook-cover-1388015470.jpg', 0, NULL, '2015-07-29 11:54:37', '2015-07-29 06:24:37', '', b'1', ''),
(418, '', '63', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating tips that''ll get u laid with any girl of ur college.Dont believe?Try#2 n thnk me later', 'assets/uploads/1438166004_Dating_Tips_Watch_Their_Eyes.jpg', 0, NULL, '2015-07-29 16:03:24', '2015-07-29 10:33:24', '', b'1', ''),
(419, '', '65', '', 'Stop looking for reasons to be unhappy. Focus on the things you do have, and the reasons you should be happy.', NULL, 0, NULL, '2015-07-29 22:13:52', '2015-07-29 16:43:52', '', b'1', ''),
(420, '', '65;68;75', 'http://www.magzim.com/10-crazy-and-curious-kissing-facts/', '99% people do not know these incredible facts about KISSING. Checkout before Kissing ur GF>> ', 'assets/uploads/1438189165_Kiss_Compatibility.jpg', 0, NULL, '2015-07-29 22:29:25', '2015-07-29 16:59:25', '2015-07-29 23:00:05', b'1', 'Yes'),
(421, '', '72', 'http://www.magzim.com/10-incredible-health-benefits-of-kissing/', '10 ways of masturbation for way more fun & excitement >>', 'assets/uploads/1438261172_orgasm-620x400.jpg', 0, NULL, '2015-07-30 18:29:32', '2015-07-30 12:59:32', '', b'1', ''),
(422, '', '65', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', 'Terrible Mistakes Men Do In Bed During Sex >>', 'assets/uploads/1438261421_Mistakes-Men-lovemaking-.jpeg', 0, NULL, '2015-07-30 18:33:41', '2015-07-30 13:03:41', '', b'1', ''),
(423, '', '63;65;66;68;72;75', 'http://www.magzim.com/unhealthy-habits-that-make-you-age-faster/', 'Our everyday bad n unhealthy habits that are making us OLD Faster. Check do u have any >>', 'assets/uploads/1438373978_Unhealthy_habits_Ageing_faster.jpg', 0, NULL, '2015-08-01 01:49:38', '2015-07-31 20:19:38', '2015-08-01 04:05:16', b'1', 'Yes'),
(424, '', '65;68;72;75', 'http://www.magzim.com/20-unknown-facts-you-didnt-know-about-adolf-hitler/', '20 Unknown Facts You Didn’t Know about Adolf Hitler. Checkout to know more >>', 'assets/uploads/1438374672_feminise-hitler.jpg', 0, NULL, '2015-08-01 02:01:12', '2015-07-31 20:31:12', '2015-08-01 07:00:53', b'1', 'Yes'),
(425, '', '63;65;66;68;72', 'http://www.magzim.com/10-types-of-women-men-like-the-most/', '13 types of sex you should have atleast once in life >> ', 'assets/uploads/1438462853_xc.png', 0, NULL, '2015-08-02 02:30:53', '2015-08-01 21:00:53', '2015-08-02 04:30:36', b'1', 'Yes'),
(426, '', '63', 'http://www.magzim.com/15-signs-youre-dating-an-immature-guy/', '10 signs to know he is gud in bed or not See today n dont regret later:', 'assets/uploads/1438633159_He_considers_his_own_opinion_first.jpg', 0, NULL, '2015-08-04 01:49:19', '2015-08-03 20:19:19', '', b'1', ''),
(427, '', '63;65;72', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', '13 types of Sex u should have at least once in life >>  ', 'assets/uploads/1438697653_xc.png', 2, 'Code: 89 Error: Invalid or expired token.', '2015-08-04 19:44:13', '2015-08-04 14:14:13', '', b'1', ''),
(428, '', '72', 'http://www.magzim.com/10-crazy-and-curious-kissing-facts/', 'Best ways to make her crazy for having sex right away >>', 'assets/uploads/1438726918_CLI-.png', 2, 'Code: 89 Error: Invalid or expired token.', '2015-08-05 03:51:58', '2015-08-04 22:21:58', '', b'1', ''),
(429, '', '63', 'http://www.magzim.com/10-crazy-and-curious-kissing-facts/', 'Best ways to make her crazy for having sex right away >>', 'assets/uploads/1438727199_CLI-.png', 0, NULL, '2015-08-05 03:56:39', '2015-08-04 22:26:39', '2015-08-05 05:30:21', b'1', 'Yes'),
(430, '', '66;68', 'http://www.magzim.com/10-crazy-and-curious-kissing-facts/', 'Best ways to make her crazy for having sex right away >>', 'assets/uploads/1438727490_CLI-.png', 0, NULL, '2015-08-05 04:01:30', '2015-08-04 22:31:30', '2015-08-05 04:30:07', b'1', 'Yes'),
(431, '', '63', 'http://www.magzim.com/unhealthy-habits-that-make-you-age-faster/', '99% people do not know these stupid habits are making them OLD faster>> ', 'assets/uploads/1439062018_Unhealthy_habits_Ageing_faster.jpg', 0, NULL, '2015-08-09 00:56:58', '2015-08-08 19:26:58', '', b'1', ''),
(432, '', '63', 'http://www.magzim.com/10-interesting-marijuana-facts-and-myths/', 'Some Crazy facts about Marijuana you never knew. Checkout >> ', 'assets/uploads/1439111722_marijuana-money-istock.jpg', 0, NULL, '2015-08-09 14:45:22', '2015-08-09 09:15:22', '', b'1', ''),
(433, '', '63', 'http://www.magzim.com/15-interesting-facts-you-didnt-know-about-friends-tv-show/', '15 Interserting Facts You Never knew About FRIENDS TV show ', 'assets/uploads/1439121695_Friends.jpg', 0, NULL, '2015-08-09 17:31:35', '2015-08-09 12:01:35', '', b'1', ''),
(434, '', '63', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', 'assets/uploads/1439127489_Brazen_Bull_brutal_torture.jpg', 0, NULL, '2015-08-09 19:08:09', '2015-08-09 13:38:09', '', b'1', ''),
(435, '', '63', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', '10 Most Brutal Torture Techniques Ever Devised in History >>', 'assets/uploads/1439127578_Brazen_Bull_brutal_torture.jpg', 0, NULL, '2015-08-09 19:09:38', '2015-08-09 13:39:38', '', b'1', ''),
(436, '', '69', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', '10 Most Brutal Torture Techniques Ever Devised in History >>', 'assets/uploads/1439127670_Rat_torture.jpg', 0, NULL, '2015-08-09 19:11:10', '2015-08-09 13:41:10', '', b'1', ''),
(437, '', '66', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', '10 Most Brutal Torture Techniques Ever Devised in History >>  ', 'assets/uploads/1439133181_Guillotine.jpg', 0, NULL, '2015-08-09 20:43:01', '2015-08-09 15:13:01', '', b'1', ''),
(438, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds In the World >>', 'assets/uploads/1439134518_siberian-huskies.jpg', 0, NULL, '2015-08-09 21:05:18', '2015-08-09 15:35:18', '', b'1', ''),
(439, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds In the World >>', 'assets/uploads/1439144552_P?rr?_d?_Pr???_C?n?ri?.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-09 23:52:32', '2015-08-09 18:22:32', '', b'1', ''),
(440, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds In the World >>', 'assets/uploads/1439144804_P?rr?_d?_Pr???_C?n?ri?.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-09 23:56:44', '2015-08-09 18:26:44', '', b'1', ''),
(441, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds In the World >>', 'assets/uploads/1439144942_P?rr?_d?_Pr???_C?n?ri?.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-09 23:59:02', '2015-08-09 18:29:02', '', b'1', ''),
(442, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds In the World >>', 'assets/uploads/1439145838_P?rr?_d?_Pr???_C?n?ri?.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 00:13:58', '2015-08-09 18:43:58', '', b'1', ''),
(443, '', '63', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', '10 Most Brutal Torture Techniques Ever Devised in History >> ', 'assets/uploads/1439152888_Rat_torture.jpg', 0, NULL, '2015-08-10 02:11:28', '2015-08-09 20:41:28', '', b'1', ''),
(444, '', '53', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', '', 'assets/uploads/1439185326_image472.png', 0, NULL, '2015-08-10 11:12:06', '2015-08-10 05:42:06', '', b'1', ''),
(445, '14', '', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', '', 'assets/uploads/1439185432_image472.png', 0, NULL, '2015-08-10 11:13:52', '2015-08-10 05:43:53', '2015-08-10 11:30:28', b'1', 'Yes'),
(446, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439185544_image472.png', 0, NULL, '2015-08-10 11:15:44', '2015-08-10 05:45:44', '', b'1', ''),
(447, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439188577_1439145838_P?rr?_d?_Pr???_C?n?ri?__1_.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 12:06:17', '2015-08-10 06:36:17', '', b'1', ''),
(448, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439188957_1439145838_P?rr?_d?_Pr???_C?n?ri?__1_.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 12:12:37', '2015-08-10 06:42:37', '', b'1', ''),
(449, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439189135_1439145838_P?rr?_d?_Pr???_C?n?ri?__1_.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 12:15:35', '2015-08-10 06:45:35', '', b'1', ''),
(450, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439189250_1439145838_P?rr?_d?_Pr???_C?n?ri?__1_.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 12:17:30', '2015-08-10 06:47:30', '', b'1', ''),
(451, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439189395_1439145838_P?rr?_d?_Pr???_C?n?ri?__1_.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 12:19:55', '2015-08-10 06:49:55', '', b'1', ''),
(452, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439189544_1439145838_P?rr?_d?_Pr???_C?n?ri?__1_.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 12:22:24', '2015-08-10 06:52:24', '', b'1', ''),
(453, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439189831_1439145838_P?rr?_d?_Pr???_C?n?ri?__1_.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 12:27:11', '2015-08-10 06:57:11', '', b'1', ''),
(454, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439190023_1439145838_PÐµrrÐ¾_dÐµ_PrÐµÑ•Ð°_CÐ°nÐ°riÐ¾__1_.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 12:30:23', '2015-08-10 07:00:23', '', b'1', ''),
(455, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'image.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 12:35:11', '2015-08-10 07:05:11', '', b'1', ''),
(456, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439190023_1439145838_P?rr?_d?_Pr???_C?n?ri?__1_.png', 2, 'Code: 189 Error: Error creating status.', '2015-08-10 12:36:48', '2015-08-10 07:06:48', '', b'1', ''),
(457, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439190023_1439145838_Pеrrо_dе_Prеѕа_Cаnаriо__1_.png', 0, NULL, '2015-08-10 12:48:26', '2015-08-10 07:18:26', '', b'1', ''),
(458, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439192022_1439145838_Pеrrо_dе_Prеѕа_Cаnаriо__1_.png', 0, NULL, '2015-08-10 13:03:42', '2015-08-10 07:33:42', '', b'1', ''),
(459, '', '51', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '', 'assets/uploads/1439192153_1439145838_Pеrrо_dе_Prеѕа_Cаnаriо__1_.png', 0, NULL, '2015-08-10 13:05:53', '2015-08-10 07:35:53', '', b'1', ''),
(460, '', '63', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', '10 Most Brutal Torture Techniques Ever Devised in History >>', 'assets/uploads/1439205142_Rat_torture.jpg', 0, NULL, '2015-08-10 16:42:22', '2015-08-10 11:12:22', '', b'1', ''),
(461, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds In the World >>', 'assets/uploads/1439212974_great_dane.jpg', 0, NULL, '2015-08-10 18:52:54', '2015-08-10 13:22:54', '', b'1', ''),
(462, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds In the World >>', 'assets/uploads/1439554663_siberian-huskies.jpg', 0, NULL, '2015-08-14 17:47:43', '2015-08-14 12:17:43', '', b'1', ''),
(463, '', '63', 'http://www.magzim.com/20-unknown-facts-you-didnt-know-about-adolf-hitler/', '20 Unknown Facts You Didn’t Know about Adolf Hitler >>', 'assets/uploads/1439592936_Adolf_Hitler_as_a_child.jpg', 0, NULL, '2015-08-15 04:25:36', '2015-08-14 22:55:36', '', b'1', ''),
(464, '', '63', 'http://www.magzim.com/15-things-you-never-knew-about-how-i-met-your-mother/', '15 Things You Never Knew About ‘How I Met Your Mother >>', 'assets/uploads/1439595756_HIMYM-How-I-Met-Your-Mother.jpg', 0, NULL, '2015-08-15 05:12:36', '2015-08-14 23:42:36', '', b'1', ''),
(465, '', '63', 'http://www.magzim.com/15-signs-youre-dating-an-immature-guy/', '15 Signs You’re Dating An Immature Guy >> ', 'assets/uploads/1439600669_Dating_Tips_Don’t_Overshare.jpg', 0, NULL, '2015-08-15 06:34:29', '2015-08-15 01:04:29', '', b'1', ''),
(466, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds In the World >> ', 'assets/uploads/1439606443_great_dane.jpg', 0, NULL, '2015-08-15 08:10:43', '2015-08-15 02:40:43', '', b'1', ''),
(467, '', '63', 'http://www.magzim.com/15-things-you-never-knew-about-how-i-met-your-mother/', '15 Things You Never Knew About ‘How I Met Your Mother >>', 'assets/uploads/1439644171_HIMYM-How-I-Met-Your-Mother-Props.jpg', 0, NULL, '2015-08-15 18:39:31', '2015-08-15 13:09:31', '', b'1', ''),
(468, '', '63', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', 'Terrible Mistakes Couples Make during Lovemaking Session >> ', 'assets/uploads/1439646926_Mistakes-Men-lovemaking-Disrespect-Her.jpg', 0, NULL, '2015-08-15 19:25:26', '2015-08-15 13:55:26', '', b'1', ''),
(469, '', '63', 'http://www.magzim.com/facts-about-breaking-bad/', 'I Bet You Didn''t Know These Weird Facts About Breaking Bad >> ', 'assets/uploads/1439660206_good_bye_breaking_bad.jpg', 0, NULL, '2015-08-15 23:06:46', '2015-08-15 17:36:46', '', b'1', ''),
(470, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds In the World >>', 'assets/uploads/1439665043_siberian-huskies.jpg', 0, NULL, '2015-08-16 00:27:23', '2015-08-15 18:57:23', '', b'1', ''),
(471, '', '63', 'http://www.magzim.com/10-interesting-marijuana-facts-and-myths/', '10 Interesting Marijuana Facts and Myths Debunked >>', 'assets/uploads/1439669478_us42.jpg', 0, NULL, '2015-08-16 01:41:18', '2015-08-15 20:11:18', '', b'1', ''),
(472, '', '63', 'http://www.magzim.com/facts-about-breaking-bad/', 'Amazing facts about Breaking Bad which you never knew >>', 'assets/uploads/1439680838_Breaking_Bad_Poster.jpg', 0, NULL, '2015-08-16 04:50:38', '2015-08-15 23:20:38', '', b'1', ''),
(473, '', '63', 'http://www.magzim.com/terrible-mistakes-couples-make-post-lovemaking-session/', 'Terrible Mistakes Couple Make During Love Making!! Checkout >>', 'assets/uploads/1439690668_Mistakes-Men-lovemaking-.jpeg', 0, NULL, '2015-08-16 07:34:28', '2015-08-16 02:04:28', '', b'1', ''),
(474, '', '63', 'http://www.magzim.com/dating-tips-that-will-transform-your-love-life/', 'Dating Tips That Will Transform Your Love Life >>', 'assets/uploads/1439722068_Dating_Tips_There_are_always_More_Fish.jpg', 0, NULL, '2015-08-16 16:17:48', '2015-08-16 10:47:48', '', b'1', ''),
(475, '', '63', 'http://www.magzim.com/10-types-of-women-men-like-the-most/', '10 Types Of Women That Really Turn Men On >>', 'assets/uploads/1439722813_trophygirl.jpg', 0, NULL, '2015-08-16 16:30:13', '2015-08-16 11:00:13', '', b'1', ''),
(476, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds In the World >>', 'assets/uploads/1439732518_Boxer_Dogs.jpg', 0, NULL, '2015-08-16 19:11:58', '2015-08-16 13:41:58', '', b'1', ''),
(477, '', '63', 'http://www.magzim.com/foods-that-are-killing-your-libido/', 'Foods That Are Killing Your Libido >>', 'assets/uploads/1439740817_redmeat.jpg', 0, NULL, '2015-08-16 21:30:17', '2015-08-16 16:00:17', '', b'1', ''),
(478, '', '63', 'http://www.magzim.com/foods-that-are-killing-your-libido/', 'Food that are killing your sex drive >> ', 'assets/uploads/1439746773_couple-eating-strawberry.jpg', 0, NULL, '2015-08-16 23:09:33', '2015-08-16 17:39:33', '', b'1', ''),
(479, '', '63', 'http://www.magzim.com/20-unknown-facts-you-didnt-know-about-adolf-hitler/', '20 Unknown Facts You Didn’t Know about Adolf Hitler >>', 'assets/uploads/1439764516_Bundesarchiv_Bild_183-1989-0322-506,_Adolf_Hitler,_Kinderbild_retouched.jpg', 0, NULL, '2015-08-17 04:05:16', '2015-08-16 22:35:16', '', b'1', ''),
(480, '', '63', 'http://www.magzim.com/ten-amazing-facts-bill-gates/', 'Bill Gates shares his biggest REGRET of life.. checkout #5 >>', 'assets/uploads/1439771693_bill_jail.jpg', 0, NULL, '2015-08-17 06:04:53', '2015-08-17 00:34:53', '', b'1', ''),
(481, '', '66', 'http://www.magzim.com/amazing-facts-las-vegas/', 'Best Places and Some Amazing Facts about Las Vegas >>', 'assets/uploads/1439820585_vegas1.jpg', 0, NULL, '2015-08-17 19:39:45', '2015-08-17 14:09:45', '', b'1', ''),
(482, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', 'Top 15 Most Dangerous Dog Breeds in the World >>', 'assets/uploads/1439823790_Pеrrо_dе_Prеѕа_Cаnаriо.png', 0, NULL, '2015-08-17 20:33:10', '2015-08-17 15:03:10', '', b'1', ''),
(483, '', '63', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', '10 Most Brutal Torture Techniques Ever Devised in History >> ', 'assets/uploads/1439829645_spanish_donkey_brutal_torture.jpg', 0, NULL, '2015-08-17 22:10:45', '2015-08-17 16:40:45', '', b'1', ''),
(484, '', '63', 'http://www.magzim.com/20-unknown-facts-you-didnt-know-about-adolf-hitler/', '20 Unknown Facts You Didn''t Know about Adolf Hitler', 'assets/uploads/1439835291_Adolf_Hitler_as_a_child.jpg', 0, NULL, '2015-08-17 23:44:51', '2015-08-17 18:14:51', '', b'1', ''),
(485, '', '63', 'http://www.magzim.com/15-most-dangerous-dog-breeds-in-the-world/', '15 Most Dangerous Dog Breeds in the World >>', 'assets/uploads/1439842129_siberian-huskies.jpg', 0, NULL, '2015-08-18 01:38:49', '2015-08-17 20:08:49', '', b'1', ''),
(486, '', '63', 'http://www.magzim.com/foods-that-are-killing-your-libido/', 'Foods that will Kill Your Mood. Stop Eating them instant >> ', 'assets/uploads/1439847213_Canned_foods.jpg', 0, NULL, '2015-08-18 03:03:33', '2015-08-17 21:33:33', '', b'1', ''),
(487, '', '63', 'http://www.magzim.com/10-most-brutal-torture-techniques-ever-devised-in-history/', '10 Most Brutal Torture Techniques Ever Devised in History >> ', 'assets/uploads/1439851045_spanish_donkey_brutal_torture.jpg', 0, NULL, '2015-08-18 04:07:25', '2015-08-17 22:37:26', '', b'1', '');

-- --------------------------------------------------------

--
-- Table structure for table `transaction_details`
--

CREATE TABLE IF NOT EXISTS `transaction_details` (
  `id` int(11) NOT NULL,
  `user_id` varchar(20) DEFAULT NULL,
  `ref_id` varchar(255) DEFAULT NULL,
  `pay_for_month_year` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `transection_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `paypal_trans_id` int(50) DEFAULT NULL,
  `mode_type` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction_details`
--

INSERT INTO `transaction_details` (`id`, `user_id`, `ref_id`, `pay_for_month_year`, `amount`, `transection_date`, `paypal_trans_id`, `mode_type`) VALUES
(1, '51', '51_sulbha14', 'June 2015', '1.14', '2015-06-29 07:23:14', 1, ''),
(9, '69', '69_Magzimontherock', 'August 2015', '0.77', '2015-08-08 09:44:30', 7, 'paypal');

-- --------------------------------------------------------

--
-- Table structure for table `TwitterUpdate`
--

CREATE TABLE IF NOT EXISTS `TwitterUpdate` (
  `user_id` int(11) NOT NULL,
  `uname` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_token` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_token_secret` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `TwitterUpdate`
--

INSERT INTO `TwitterUpdate` (`user_id`, `uname`, `name`, `oauth_token`, `oauth_token_secret`) VALUES
(1, 'sulbha14', 'sulbha shrivastava', '456543101-lojBU0wRymm2RJfMsTwNE5spEzYjFxPlOVnX99ok', 'POaajMRp5EfkJr4TUqlITFolf3CG89r4JgTgAQAFcIbW4'),
(2, 'aminkhan1990', 'aminkhan', '342306792-oZIsWVlPTgDZp7DtJvoP710mCNLpgdQm9HcIeFIY', 'H2gho1uzrStKQgfqaSym7oMW0ralZhRaM1bFkEUgo1KbB'),
(3, 'sulbha14shri', 'sulbha', '3223150730-3ts1YVmdjpGk1iivMapooe4fZmMWb92KvnwmKjO', 'A4CCdrqyViRjcyIlKZzldLW3RDRY0Qq5OOiPj4l6bX5hB');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `social_id` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL COMMENT '0 for delete , 1 not delete',
  `verification_token` varchar(225) DEFAULT NULL,
  `type` enum('1','2') NOT NULL DEFAULT '2' COMMENT '1 for admin type , 2 for user',
  `activation_token` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `login_token` text,
  `login_token_secreat` text,
  `outh_provider` varchar(255) DEFAULT NULL,
  `referal_link` varchar(255) DEFAULT NULL,
  `percent_cut` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ac_holder_name` varchar(255) DEFAULT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `ifsc` varchar(255) DEFAULT NULL,
  `swift_bic` varchar(255) DEFAULT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `paypal_email` varchar(255) DEFAULT NULL,
  `twitter_id` varchar(255) DEFAULT NULL,
  `facebook_id` varchar(255) DEFAULT NULL,
  `schedule_frequency` varchar(225) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `social_id`, `firstname`, `middlename`, `lastname`, `email`, `password`, `gender`, `is_deleted`, `verification_token`, `type`, `activation_token`, `active`, `login_token`, `login_token_secreat`, `outh_provider`, `referal_link`, `percent_cut`, `create_date`, `update_date`, `ac_holder_name`, `bank_name`, `ifsc`, `swift_bic`, `account_no`, `paypal_email`, `twitter_id`, `facebook_id`, `schedule_frequency`) VALUES
(1, '0', 'Admin', NULL, 'admin', 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', 'male', '0', 'xOUmzwsJSXB2K8j9GyPcWYDfb3Iv6E', '1', '', b'0', '', NULL, '', NULL, '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, '456543101', 'sulbha', '', 'shrivastava', '', '', '', '0', NULL, '2', '456543101', b'0', '456543101-XF1NM0cpb2lFICzfUBy7U4g4QMixfyi907tJeclf', '723LPIGmtVjbDCc1F4nmbBmmIQNoiKLsrjqTGZWh0vwKk', 'twitter', '51_sulbha14', '1', '2015-06-25 11:24:19', '2015-06-25 05:54:19', '', '', '', '', '', 'sulbha@paypal.com', 'sulbha14', NULL, '6 Hours'),
(52, '102258373449386', 'Elizabethaaaa', NULL, 'soni', '', '', 'female', '0', NULL, '2', '102258373449386', b'0', 'CAAXgiZCZAWNzIBAIqfbfkVTjqGqP7OI87iYE4p6lm96lWzcW9tKdayOnTSbaRVZCav5AxGEGvLr8ZBrS8JT0AtLsjhdULf61lyttp7ZCmHceraAljpVRtOpCsnRxnaqjcvyH0xRICahj1eUp3uohycJRHdRtOP7C4JbZBkyVgj6N20snA2jSG41pKbaAZCD2dEUQptzOlf6kU3k6vaxXRwg', NULL, 'facebook', '52_Elizabeth', NULL, '2015-06-25 11:31:49', '2015-06-25 06:01:49', 'aaaa', 'aaa', 'aaa', 'aaa', 'aaa', 'aaaaa@aa.aa', NULL, '', NULL),
(53, '102613803409576', 'Sulbha', NULL, 'Shrivastava', '', '', 'female', '0', NULL, '2', '102613803409576', b'0', 'CAAXgiZCZAWNzIBAEuVVwr7eSHfv6s6h9yYhLpNO63mZAAE379n5uSLVJAZAiimHRQG1Ctl5kNX2Fvy5Bnkv2MFKPWeVyKTTqLTGSZAjO8aFJXu3K4z80waWLC5JjFz0l0Oc2ZCQz3bsqNUBHXBMuhscjnuSz8HWQm1KuXZBkmwA10ZBZCWiUKzFZCgXuZBUbLGDbxuZBJuL5pqrk7T9tKZCjoVqRB', NULL, 'facebook', '53_Sulbha', '2', '2015-06-25 11:37:37', '2015-06-25 06:07:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL),
(55, '1042906272', 'mihtra', NULL, '', '', '', '', '0', NULL, '2', '1042906272', b'0', '1042906272-WMcsK4t5iQZikblmC61VvF2PGCXu1jHskSGLqq3', 'xUtsvZAhfxVp8nbjsUhQL6fRM5guVLuyoNkHp2ab2fDjp', 'twitter', '55_mihtra', '10', '2015-06-27 18:08:37', '2015-06-27 12:38:37', NULL, NULL, NULL, NULL, NULL, NULL, 'mihtra', NULL, NULL),
(56, '95621715', 'dewanishan', NULL, '', '', '', '', '0', NULL, '2', '95621715', b'0', '95621715-nlB9MtjYmU8QpfZVaQIIf3wppIg7KhqCLxKFl58TD', 'wyVvL4ARImTK6uQqSJmLxJzUISW70M1WYmYLVUKVNtt3W', 'twitter', '56_dewanishan', '50', '2015-06-30 18:12:05', '2015-06-30 12:42:05', NULL, NULL, NULL, NULL, NULL, NULL, 'dewanishan', NULL, NULL),
(58, '1424052347918674', 'Tom', NULL, 'Seligsteinman', '', '', 'male', '0', NULL, '2', '1424052347918674', b'0', 'CAAXgiZCZAWNzIBAMJZAZBf1t8xZBSi5ZBXCkNZCLnMyZACM9T9gSoTZBpZCdpANFNPQ4i3Qey2aYRNm4ZBpnnW1Ey0sLM0jFhP5MruoGZCM1hC2dNezKp4VXonOleWZAX23TS1QwACCLYBGysTTjrDSUlMK67l5ZBDA2486zIyNo8atp8B3wvv4QETdSwSoTWHPcE1VPCUyDNvra4rmpHJ6OGhTFsV', NULL, 'facebook', '58_Tom', NULL, '2015-07-06 15:38:53', '2015-07-06 10:08:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL),
(60, '3223150730', 'sulbha14shri', NULL, '', '', '', '', '0', NULL, '2', '3223150730', b'0', '3223150730-k5RMyTczCIbt4vCdGgbU5qKZY4zNvxDUskw1ASH', '3D2CXQ1fLlLrpyUezJvsGLRd8XJc0w8P3nKHi7GzxqA2I', 'twitter', '60_sulbha14shri', NULL, '2015-07-08 15:54:36', '2015-07-08 10:24:36', NULL, NULL, NULL, NULL, NULL, NULL, 'sulbha14shri', NULL, NULL),
(61, '3263103410', 'irbaaz_shaikh', NULL, '', '', '', '', '0', NULL, '2', '3263103410', b'0', '3263103410-kg30OattXWm6pkUVJ4wYzHfk6ZfhvnWY1MVgucJ', 'UR6FV4vcIUqMnK2DZoOMrGPuDo8SjvAaivRCM3s0dKcVO', 'twitter', '61_irbaaz_shaikh', NULL, '2015-07-08 15:55:36', '2015-07-08 10:25:36', NULL, NULL, NULL, NULL, NULL, NULL, 'irbaaz_shaikh', NULL, NULL),
(62, '708684258', 'patelash212', NULL, '', '', '', '', '0', NULL, '2', '708684258', b'0', '708684258-Ous8ikLOQxxvTRtzdwG1KrDUNLAaO71s6yf0wyx8', 'Z76dvtBDlyHzDOeSlTaw8wbBnp4tcTQvaBN8ZgPK4bEy6', 'twitter', '62_patelash212', NULL, '2015-07-08 16:05:54', '2015-07-08 10:35:54', NULL, NULL, NULL, NULL, NULL, NULL, 'patelash212', NULL, NULL),
(63, '525060895', 'UnusualFactPage', NULL, '', '', '', '', '0', NULL, '2', '525060895', b'0', '525060895-SeOqfhFhWjKfikKXjHlgfFMsrH7eHh38wQGTQgb5', 'JL52ebVAlNo8lFGgLDmVcg9MRnkNGxCpoFsxClMG7T1pP', 'twitter', '63_UnusualFactPage', '', '2015-07-09 17:08:46', '2015-07-09 11:38:46', NULL, NULL, NULL, NULL, NULL, NULL, 'UnusualFactPage', NULL, '4 Hours'),
(64, '983572294994467', 'Ishan', NULL, 'Dewan', '', '', 'male', '0', NULL, '2', '983572294994467', b'1', 'CAAWsT8P6LWYBAMlacEnRgFWbA62nc0kV4G3wthxy6iM4DSjXN1PovQMInMtZAdiFB9SyfyU6QpOYHyRaiKPrGFw2SoqpZB4yX0EPETb9ZCy0PK3NrpggIAfpl3YBOgqndWd9qZCyoNn9fRq7nYZA0nMBZA0N9jEfTtYrNLuMEjh7pAiK58pS38uNeRBIM6ZAzKT9dP88Rg7VTCoRSgjppJNe3RLPiNfmjIZD', NULL, 'facebook', NULL, NULL, '2015-07-12 15:52:33', '2015-07-12 10:22:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL),
(65, '2541999782', 'QuotesDetail', NULL, '', '', '', '', '0', NULL, '2', '2541999782', b'0', '2541999782-7zh78QOELpMoJ8ekA37a6Zera07hYN6qmvyKo04', 'NYPnPN6fQISAoSzPqNA8cXOjAEh3cWvjPjuqeZ3Qvevj9', 'twitter', '65_QuotesDetail', '5', '2015-07-12 23:38:24', '2015-07-12 18:08:24', 'vaibhav purohit', 'Union bank of India', 'UTBI0RJG689', 'UBININBBRAI', '549402010000056', 'evaibhaw@gmail.com', 'QuotesDetail', NULL, NULL),
(66, '1053367598', 'Nishant ', NULL, 'oberoi', '', '', '', '0', NULL, '2', '1053367598', b'0', '1053367598-46aWZuYClBsf9UoVDW736Ox8KXvhZjNJ1bzmcUX', 'RJ1JIEuP6DEsn10NXlNUKa1kxz8zjUBfh75EjLP4caqUg', 'twitter', '66_Hidden_Stories', '10', '2015-07-13 00:10:36', '2015-07-12 18:40:36', '', '', '', '', '', 'nickoberoi007@gmail.com', 'Hidden_Stories', NULL, NULL),
(67, '412937157', 'Bhaumik', NULL, 'Karathia', '', '', '', '0', NULL, '2', '412937157', b'0', '412937157-6sgExyvY1wTaOIy54zYGeRpV95jNmiZSh3EYmrCT', 'BwjHVY7kfGM6Ma2HwTDohuzLy1O4gyq8OswVnHePHxvV2', 'twitter', '67_BhaumikKarathia', NULL, '2015-07-20 20:16:34', '2015-07-20 14:46:34', '', '', '', '', '', 'bhaumikhardik@gmail.com', 'BhaumikKarathia', NULL, NULL),
(68, '1860220441', 'Bhaumik', NULL, 'Karathia', '', '', '', '0', NULL, '2', '1860220441', b'0', '1860220441-i8T0jmrE6a3ngob4ZFRSLi8kDR9A8PVd1hJVduM', 'psjhZwThWvPkMWfYzMfrkbN2pUAY4Wa1PVF4XkGEWGkrg', 'twitter', '68_Adultjoks', '10', '2015-07-20 20:41:50', '2015-07-20 15:11:51', '', '', '', '', '', 'bhaumikhardik@gmail.com', 'Adultjoks', NULL, NULL),
(69, '1498565142', 'Magzimontherock', NULL, '', '', '', '', '0', NULL, '2', '1498565142', b'0', '1498565142-BNQ7peoUNtbSpuKpwqiQodJ1I64JZRIDujFifIF', 'vn1ZGn5WovSTaL12dADsyNJaxFVMew2Mkm9mu4REAGocS', 'twitter', '69_Magzimontherock', '50', '2015-07-21 00:20:44', '2015-07-20 18:50:44', '', '', '', '', '', 'Singh84a@gmail.com', 'Magzimontherock', NULL, '8 Hours'),
(70, '10203280859781577', 'Munish', NULL, 'Malhotra', '', '', 'male', '0', NULL, '2', '10203280859781577', b'0', 'CAAWsT8P6LWYBAMFxbfszWVF8Vz2ocrMGLNJhPZCzIzFqXA4ThdfmYl8omZCZCXYvBGZAZCVNvPsMwhs1YvT6WCVTjIYfmEKdpZB2hcyrPLsfYkiNHk6tA50zG8obkVptaWhaqcWcLFEQZBxT9fP3E75dtI34td6LvaEGVsklLiIYqcsoe5ay02v2WeQ0FtKfVGvPWN4LyED0CYaSzn6vNQa', NULL, 'facebook', '70_Munish', NULL, '2015-07-23 14:18:38', '2015-07-23 08:48:38', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL),
(71, '3047261886', 'MINDBIASTING', NULL, '', '', '', '', '0', NULL, '2', '3047261886', b'0', '3047261886-GwnTkR7pj3oPzy9SJW3484dIspROIOkAO9aEUhG', 'CFDCx7JTWdBW56gzSn7tiVzOPR9lELKIwlf5fcfW14Esh', 'twitter', '71_MINDBIASTING', NULL, '2015-07-23 19:45:35', '2015-07-23 14:15:35', NULL, NULL, NULL, NULL, NULL, NULL, 'MINDBIASTING', NULL, NULL),
(72, '2856085429', 'AshleyLily', NULL, '', '', '', '', '0', NULL, '2', '2856085429', b'0', '2856085429-upu7R8CoVWooggCejaB9XzFHFRQR9INaojJDpd2', '0osVAXjjiHNpRtrg3oXdBv6k33LQceVOx9M3W9mQhHfwy', 'twitter', '72_AshleyLily_', '', '2015-07-24 14:08:08', '2015-07-24 08:38:08', 'JALENDRA KUMAR', 'STATE BANK OF INDIA ', 'SBIN0012599', 'SWIFT Code STATE BANK OF INDIA (SBI)', '31991589030', 'chhotudaughton710@gmail.com', 'AshleyLily_', NULL, ''),
(73, '2856561182', 'Macy', NULL, 'Jenna', '', '', '', '0', NULL, '2', '2856561182', b'0', '2856561182-vsivbjJT2WBIYZgSx8DG1xHFaTrNz3nL37rEKXg', 'um8K0bIwOoClWX60De4IxiwOfNA8wbC7kTfHZ7CPUEP0j', 'twitter', '73_Zoejenna_', NULL, '2015-07-24 14:55:33', '2015-07-24 09:25:33', 'JALENDRA KUMAR', 'STATE BANK OF INDIA', 'SBIN0012599', 'SWIFT Code State Bank of India (SBI)', '31991589030', 'chhotudaughton710@gmail.com', 'Zoejenna_', NULL, NULL),
(74, '2523327685', 'ComedyTextings', NULL, '', '', '', '', '0', NULL, '2', '2523327685', b'0', '2523327685-Ji3z3kBFCwqQdqZpBSBO0UZXY0YRig1mP9lvRa4', 'c8k9mUUu2H9KjD2cnDIW3Y7j5k8oqY6IDwHW0LOnC5dUU', 'twitter', '74_ComedyTextings', NULL, '2015-07-24 17:22:55', '2015-07-24 11:52:55', '', '', '', '', '', 'jaykayartpress@yahoo.co.in', 'ComedyTextings', NULL, NULL),
(75, '1215691526', 'hardik', NULL, 'vinchhi', '', '', '', '0', NULL, '2', '1215691526', b'0', '1215691526-gnxb33v4yq4N5fKJOg4z9MKiilwvzn8nKtAbu8s', 'VHLygShvrgAs6xHQ0tjZpgvIoRc8ly903SUu6vpn77qMf', 'twitter', '75_KicksTheBucket', NULL, '2015-07-24 23:44:28', '2015-07-24 18:14:28', 'hardik vinchhi', 'hardik kiritbhai vinchhi', 'BKID0002127', 'BKIDINBBKLD', '312710110001051', 'hardiksco@gmail.com', 'KicksTheBucket', NULL, NULL),
(76, '2944126932', 'facts_1k', NULL, '', '', '', '', '0', NULL, '2', '2944126932', b'0', '2944126932-oeCeFZWIZ3ZcD7fJil0JQwtEDi1fY3BomZtnbOZ', 'lnvSCvAfvRmIHZCzCFA0tMFnBRwedm6OtJAAtR8lFqVMl', 'twitter', '76_facts_1k', NULL, '2015-08-01 07:41:10', '2015-08-01 02:11:10', NULL, NULL, NULL, NULL, NULL, NULL, 'facts_1k', NULL, '2 Hours'),
(77, '2570563530', '0hh_Shit', NULL, '', '', '', '', '0', NULL, '2', '2570563530', b'0', '2570563530-f2alxnvHNgjTxNSnMyTmNcoMK3awTn7lyhKi9E5', 'ws9LdSPzBZh7mDhrRRYE3NXZqE3jP5oHPYEMiceQWNKT7', 'twitter', '77_0hh_Shit', NULL, '2015-08-09 00:30:23', '2015-08-08 19:00:23', NULL, NULL, NULL, NULL, NULL, NULL, '0hh_Shit', NULL, '8 Hours');

-- --------------------------------------------------------

--
-- Table structure for table `user_25june`
--

CREATE TABLE IF NOT EXISTS `user_25june` (
  `id` int(11) NOT NULL,
  `social_id` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL COMMENT '0 for delete , 1 not delete',
  `type` enum('1','2') NOT NULL DEFAULT '2' COMMENT '1 for admin type , 2 for user',
  `activation_token` varchar(255) DEFAULT NULL,
  `active` bit(1) NOT NULL,
  `login_token` text,
  `login_token_secreat` text,
  `outh_provider` varchar(255) DEFAULT NULL,
  `referal_link` varchar(255) DEFAULT NULL,
  `percent_cut` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_25june`
--

INSERT INTO `user_25june` (`id`, `social_id`, `firstname`, `middlename`, `lastname`, `email`, `password`, `gender`, `is_deleted`, `type`, `activation_token`, `active`, `login_token`, `login_token_secreat`, `outh_provider`, `referal_link`, `percent_cut`, `create_date`, `update_date`) VALUES
(1, '0', 'Admin', 'aa', 'aa', 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', 'male', '0', '1', '', b'0', '', NULL, '', NULL, '10', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, '103406843331677', 'James', NULL, 'Putnamstein', NULL, '', 'male', '0', '2', '103406843331677', b'0', 'CAAWrpVdes54BAAIqlohTviky8bJZBrmj62dEV4d5RMbrrDuiBKhg659kgvZC46wzzCbJEdLSP0a92D8gANFZBzUcWeZAVmq13Dx8Y4c6z39vMKdAGC5ZA0jqhTKFu7wmPTuZACTz9KQvVEQon7T3xxNUmRTOydSdUAqdMK9dzHw1l3XYJRuqXSWtZAlBE7DgfZBjtXFpHvhFYA9f0ZBn1khSt', NULL, 'facebook', NULL, '30', '2015-06-05 15:20:16', '2015-06-05 09:50:16'),
(38, '3223150730', 'sulbha14shri', NULL, '', 'sulbha14shri', '', '', '0', '2', '3223150730', b'0', '3223150730-k5RMyTczCIbt4vCdGgbU5qKZY4zNvxDUskw1ASH', '3D2CXQ1fLlLrpyUezJvsGLRd8XJc0w8P3nKHi7GzxqA2I', 'twitter', '38_sulbha14shri', '10', '2015-06-12 17:30:18', '2015-06-12 12:00:18'),
(39, '102613803409576', 'Sulbha', NULL, 'Shrivastava', NULL, '', 'female', '0', '2', '102613803409576', b'0', 'CAAXgiZCZAWNzIBAAuWQBPVMGgPnLKFqPtL2wyMgbEBucnFRr98oQnb0ins5XbKiY7CViv4nJEgmRCpV5mnSboHtG6FhpusogPyFl6WBEFa2PpWZCUGB99qWWn3gmUL0hIdH9lPfFV6HvU8ydjSmJFPQnHZAwSTB1wvMDh2VHQimNupBARkYOq7cg1ubGQCZA42ekT7r8TGo4h1PdLN4TQ', NULL, 'facebook', '39_Sulbha', '25', '2015-06-12 18:40:00', '2015-06-12 13:10:00'),
(42, '1443245619329735', 'Tom', NULL, 'Narayananson', '', '', 'male', '0', '2', '1443245619329735', b'0', 'CAAXgiZCZAWNzIBAPqUZBPGw1Py12YmqKwM9jmf4SGR4vRyZB8BU04ZAfMSRO4TqXdVX5CW6ZBZBT47ibTdQMJZAi61WH7nIZCJ23AZCPXPThEKW4u8yS514EyhX9p4F480SZAw0tZAIXjVZCWQDa8MZCYTqL9R3NziAGNtSTPKVkRTiYIz7YVEyaaX7TugVJQb8SnNuNeD5q5dKWNuqOwkVOsZBVGZAx', NULL, 'facebook', '42_Tom', '10', '2015-06-12 19:01:50', '2015-06-12 13:31:50'),
(43, '102258373449386', 'Elizabeth', NULL, 'Baoescu', '', '', 'female', '0', '2', '102258373449386', b'0', 'CAAXgiZCZAWNzIBAGanP9EDY9zBk74OTLwDZCOPrs760lGg2CB8UO3tAA6VSDrcZBE12T0wjWiHEwCgMyHgmgxMbDlzEXsnQCsHMBZAQDE1o6FNlN4rrrIf5W41ZBTmR5EOZCntzNt0TIrEvrZCZCCgZCJRQq1NlAnyuYvSZCvYyqD5XNofZBZAESGWZC4z4ZBoTtTuxespDRzGZCNgBgH7iHAgGSxxXk', NULL, 'facebook', '43_Elizabeth', '20', '2015-06-12 19:03:07', '2015-06-12 13:33:07'),
(44, '456543101', 'sulbha14', NULL, '', 'sulbha14', '', '', '0', '2', '456543101', b'0', '456543101-XF1NM0cpb2lFICzfUBy7U4g4QMixfyi907tJeclf', '723LPIGmtVjbDCc1F4nmbBmmIQNoiKLsrjqTGZWh0vwKk', 'twitter', '44_sulbha14', '10', '2015-06-13 12:49:25', '2015-06-13 07:19:25'),
(45, '1424052347918674', 'Tom', NULL, 'Seligsteinman', '', '', 'male', '0', '2', '1424052347918674', b'0', 'CAAXgiZCZAWNzIBAOZARo89Ogc4jtUZCij1LU3YsnnBrbvzVlcbj9MR5mwfH0IMX8rbG0TrmZB562r4sTk1iktw0jyIkxFHuYRpfjIhXQsE2wRAz8gjtHM91eNy296KbJs9oxkvd0aS08JgQbu0mIUEfNvZA4ARD5i32SrH8ZCSHHuSZCkOz1gTiS4Ie60ZAE3sMFhPCGuBRFC7OxN48c0Xoxh', NULL, 'facebook', '45_Tom', '10', '2015-06-16 13:26:40', '2015-06-16 07:56:40'),
(46, '3237800810', 'anshu_ibr', NULL, '', 'anshu_ibr', '', '', '0', '2', '3237800810', b'0', '3237800810-trLaGjZOCcFa0MBDJ9uXnFzbb7aPO2xIFWNB7Sz', 'YfWXutyuVoWswqxQBkuOkYpnhbxbd9UdFX5fZc9DCH1Q3', 'twitter', '46_anshu_ibr', '10', '2015-06-16 17:08:26', '2015-06-16 11:38:26'),
(47, '95621715', 'dewanishan', '', '', 'dewanishan', '', '', '0', '2', '95621715', b'0', '95621715-nlB9MtjYmU8QpfZVaQIIf3wppIg7KhqCLxKFl58TD', 'wyVvL4ARImTK6uQqSJmLxJzUISW70M1WYmYLVUKVNtt3W', 'twitter', '47_dewanishan', '10', '2015-06-19 11:58:09', '2015-06-19 06:28:09'),
(48, '1042906272', 'mihtra', NULL, '', 'mihtra', '', '', '0', '2', '1042906272', b'0', '1042906272-WMcsK4t5iQZikblmC61VvF2PGCXu1jHskSGLqq3', 'xUtsvZAhfxVp8nbjsUhQL6fRM5guVLuyoNkHp2ab2fDjp', 'twitter', '48_mihtra', '10', '2015-06-20 18:19:51', '2015-06-20 12:49:51');

-- --------------------------------------------------------

--
-- Table structure for table `user_backup`
--

CREATE TABLE IF NOT EXISTS `user_backup` (
  `id` int(11) NOT NULL,
  `social_id` varchar(255) NOT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `middlename` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL COMMENT '0 for delete , 1 not delete',
  `type` enum('1','2') NOT NULL DEFAULT '2' COMMENT '1 for admin type , 2 for user',
  `activation_token` varchar(255) NOT NULL,
  `active` bit(1) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_backup`
--

INSERT INTO `user_backup` (`id`, `social_id`, `firstname`, `middlename`, `lastname`, `email`, `password`, `gender`, `is_deleted`, `type`, `activation_token`, `active`, `create_date`, `update_date`) VALUES
(1, '0', 'Admin', 'Admin', 'Admin', 'admin@admin.com', 'e10adc3949ba59abbe56e057f20f883e', 'male', '0', '1', '', b'0', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, '854531801308988', 'Sulbha', NULL, 'Shrivastava', 'sulbha2814@gmail.com', '', 'female', '0', '2', '854531801308988', b'0', '0000-00-00 00:00:00', '2015-05-24 13:56:24'),
(18, '102772173393739', 'Sulbha', NULL, 'Shrivastava', NULL, '', 'female', '0', '2', '102772173393739', b'0', '0000-00-00 00:00:00', '2015-05-24 14:01:37'),
(19, '827775913968820', 'Mohit', NULL, 'Niwadunge', 'mohit.niwadunge@gmail.com', '', 'male', '0', '2', '827775913968820', b'0', '0000-00-00 00:00:00', '2015-05-25 09:24:27'),
(20, '883151818410900', 'Anshu', NULL, 'Meghawat', 'anshumeghawat@gmail.com', '', 'female', '0', '2', '883151818410900', b'0', '0000-00-00 00:00:00', '2015-05-25 09:28:29'),
(21, '910384699035046', 'Er Amin', NULL, 'Khan', 'aminkhan.1990@yahoo.com', '', 'male', '0', '2', '910384699035046', b'0', '0000-00-00 00:00:00', '2015-05-25 10:31:06'),
(22, '3223150730', 'sulbha14shri', NULL, '', 'sulbha14shri', '', '', '0', '2', '3223150730', b'0', '0000-00-00 00:00:00', '2015-05-25 17:51:32'),
(23, '883978708330111', 'Pradeep', NULL, 'Sharma', 'sharma.pradeep1989@gmail.com', '', 'male', '0', '2', '883978708330111', b'0', '0000-00-00 00:00:00', '2015-05-26 10:29:24'),
(24, '342306792', 'aminkhan1990', NULL, '', 'aminkhan1990', '', '', '0', '2', '342306792', b'1', '0000-00-00 00:00:00', '2015-05-27 09:51:04'),
(25, '103750459962577', 'Sulbha', NULL, 'Shrivastava', NULL, '', 'female', '0', '2', '103750459962577', b'0', '0000-00-00 00:00:00', '2015-06-03 08:13:30'),
(26, '856601217768713', 'Sulbha', NULL, 'Shrivastava', 'sulbha2814@gmail.com', '', 'female', '0', '2', '856601217768713', b'0', '0000-00-00 00:00:00', '2015-06-03 09:39:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_category`
--

CREATE TABLE IF NOT EXISTS `user_category` (
  `user_category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_category`
--

INSERT INTO `user_category` (`user_category_id`, `user_id`, `category_id`, `created_date`, `updated_date`) VALUES
(2, 38, '', '2015-06-12 13:58:39', '2015-06-12 08:28:39'),
(3, 43, '', '2015-06-13 11:28:16', '2015-06-13 05:58:16'),
(4, 44, '', '2015-06-13 12:49:57', '2015-06-13 07:19:57'),
(23, 45, '', '2015-06-16 14:51:00', '2015-06-16 09:21:00'),
(24, 39, '', '2015-06-17 22:57:35', '2015-06-17 17:27:35'),
(25, 47, '', '2015-06-19 11:58:24', '2015-06-19 06:28:24'),
(26, 52, '', '2015-06-27 18:14:11', '2015-06-27 12:44:11'),
(27, 54, '', '2015-07-01 12:56:05', '2015-07-01 07:26:05'),
(28, 51, '14;16', '2015-07-01 15:34:03', '2015-07-01 10:04:03'),
(29, 57, '', '2015-07-01 17:06:06', '2015-07-01 11:36:06'),
(30, 53, '', '2015-07-03 17:53:56', '2015-07-03 12:23:56'),
(31, 56, '', '2015-07-07 13:07:23', '2015-07-07 07:37:23'),
(32, 61, '', '2015-07-08 16:18:24', '2015-07-08 10:48:24'),
(33, 65, '', '2015-07-12 23:39:00', '2015-07-12 18:09:00'),
(34, 69, '15;16;17', '2015-07-23 17:11:20', '2015-07-23 11:41:20'),
(35, 72, '', '2015-07-24 14:12:12', '2015-07-24 08:42:12'),
(36, 76, '14;15;16;17;18;19', '2015-08-01 07:42:17', '2015-08-01 02:12:17'),
(37, 77, '14;15;16;17;18;19', '2015-08-09 00:31:28', '2015-08-08 19:01:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `analytics_detail`
--
ALTER TABLE `analytics_detail`
  ADD PRIMARY KEY (`analytics_detail_id`);

--
-- Indexes for table `api_configration`
--
ALTER TABLE `api_configration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `paypal_details`
--
ALTER TABLE `paypal_details`
  ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`), ADD KEY `user_id_2` (`user_id`);

--
-- Indexes for table `paypal_transaction_details`
--
ALTER TABLE `paypal_transaction_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `share_link`
--
ALTER TABLE `share_link`
  ADD PRIMARY KEY (`sharelink_id`), ADD KEY `category_id` (`category_id`), ADD KEY `category_id_2` (`category_id`);

--
-- Indexes for table `transaction_details`
--
ALTER TABLE `transaction_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `TwitterUpdate`
--
ALTER TABLE `TwitterUpdate`
  ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `uname` (`uname`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_25june`
--
ALTER TABLE `user_25june`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_backup`
--
ALTER TABLE `user_backup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_category`
--
ALTER TABLE `user_category`
  ADD PRIMARY KEY (`user_category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `analytics_detail`
--
ALTER TABLE `analytics_detail`
  MODIFY `analytics_detail_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `api_configration`
--
ALTER TABLE `api_configration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `paypal_details`
--
ALTER TABLE `paypal_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `paypal_transaction_details`
--
ALTER TABLE `paypal_transaction_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `share_link`
--
ALTER TABLE `share_link`
  MODIFY `sharelink_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=488;
--
-- AUTO_INCREMENT for table `transaction_details`
--
ALTER TABLE `transaction_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `TwitterUpdate`
--
ALTER TABLE `TwitterUpdate`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `user_25june`
--
ALTER TABLE `user_25june`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `user_backup`
--
ALTER TABLE `user_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `user_category`
--
ALTER TABLE `user_category`
  MODIFY `user_category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
