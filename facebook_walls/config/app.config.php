<?php

return array(
    "app_id" => "1596101507330974", // Your App ID
    "app_secret" => "bcbc67b5f1c10d8e66cc7d081200e66f", // Your App Secret
    "redirect_url" => "http://localhost/test/", // Your App Redirect URL - Can be set by "Settings -> Advanced"

    "scope" => array( // Your App Scope -  Read here: https://developers.facebook.com/docs/facebook-login/permissions/v2.3
        "publish_actions",
        "manage_pages",
        "user_groups"
		//'email'
    )
);