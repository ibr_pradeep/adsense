<?php
echo '<pre>';
function getService()
{
  // Creates and returns the Analytics service object.

  // Load the Google API PHP Client Library.
  require_once 'src/Google/autoload.php';

  // Use the developers console and replace the values with your
  // service account email, and relative location of your key file.
  $service_account_email = '539518501004-q7ljpvkjla1cv653c11u0uqvfrb0b9k2@developer.gserviceaccount.com';
  $key_file_location = 'client_secrets.p12';

  // Create and configure a new client object.
  $client = new Google_Client();
  $client->setApplicationName("HelloAnalytics");
  $analytics = new Google_Service_Analytics($client);

  // Read the generated client_secrets.p12 key.
  $key = file_get_contents($key_file_location);
  $cred = new Google_Auth_AssertionCredentials(
      $service_account_email,
      array(Google_Service_Analytics::ANALYTICS_READONLY),
      $key
  );
  $client->setAssertionCredentials($cred);
  if($client->getAuth()->isAccessTokenExpired()) {
    $client->getAuth()->refreshTokenWithAssertion($cred);
  }

  return $analytics;
}

function getFirstprofileId(&$analytics) {
  // Get the user's first view (profile) ID.

  // Get the list of accounts for the authorized user.
  $accounts = $analytics->management_accounts->listManagementAccounts();

  if (count($accounts->getItems()) > 0) {
    $items = $accounts->getItems();
    $firstAccountId = $items[0]->getId();

    // Get the list of properties for the authorized user.
    $properties = $analytics->management_webproperties
        ->listManagementWebproperties($firstAccountId);

    if (count($properties->getItems()) > 0) {
      $items = $properties->getItems();
      $firstPropertyId = $items[0]->getId();

      // Get the list of views (profiles) for the authorized user.
      $profiles = $analytics->management_profiles
          ->listManagementProfiles($firstAccountId, $firstPropertyId);

      if (count($profiles->getItems()) > 0) {
        $items = $profiles->getItems();		
        // Return the first view (profile) ID.
        return $items[0]->getId();

      } else {
        throw new Exception('No views (profiles) found for this user.');
      }
    } else {
      throw new Exception('No properties found for this user.');
    }
  } else {
    throw new Exception('No accounts found for this user.');
  }
}

function getResults(&$analytics, $profileId) {
  // Calls the Core Reporting API and queries for the number of sessions
  // for the last seven days.
  try {
	  $optParams = array(
		'dimensions' => 'ga:referralPath,ga:pagePath',	
		//'filters' => 'ga:medium==referral'
		);
	  return $results = $analytics->data_ga->get(
		   'ga:'.$profileId,
		   '365daysAgo',
		   'today',
		   'ga:adsenseRevenue,ga:adsenseAdsClicks,ga:adsensePageImpressions',
			$optParams);
	  // Success. 
	} catch (apiServiceException $e) {
	  // Handle API service exceptions.
	  $error = $e->getMessage();
	}
}

function printResults(&$results) {
  // Parses the response from the Core Reporting API and prints
  // the profile name and total sessions.
  if (count($results->getRows()) > 0) {

    // Get the profile name.
    $profileName = $results->getProfileInfo()->getProfileName();

    // Get the entry for the first entry in the first row.
    $rows = $results->getRows();
    $sessions = $rows[0][0];

    // Print the results.
	$user_rows = [];
	foreach($rows as $row){
		$string = $row[1];    
		if(strpos($string, '?ref') !== false){			
			$string = substr($string, strpos($string, "?") + 1);    
			$string = explode('=', $string);			
			$row['Referral Path'] = $row[0];
			unset($row[0]);
			$row['Page'] = $row[1];
			unset($row[1]);
			$row['AdSense Revenue'] = $row[2];
			unset($row[2]);
			$row['AdSense Ads Clicked'] = $row[3];
			unset($row[3]);
			$row['AdSense Page Impressions'] = $row[4];
			unset($row[4]);
			$user_rows[$string[1]][] = $row;
		}		
	}
	print_r($user_rows);
   // return ($rows); //Result sequence according dimension and metrics 
  } else {
    print "No results found.\n";
  }
}

$analytics = getService();
$profile = getFirstProfileId($analytics);
printResults(getResults($analytics, $profile));

?>
