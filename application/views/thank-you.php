<!DOCTYPE html>
<html lang='en'>
<head>
  <meta charset="UTF-8" /> 
  <title>
      Affiliate Web Application Forgot Password  
  </title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/login_style.css" />
</head>
<body>
  <div id="wrapper">  
  <!-- Navigation -->
    <div id="page-wrapper">    
      <div class="container-fluid">    
      <!-- Page Heading -->
        <div class="row"> 
        	<div class="" style="  width: 19%;  margin: 0 auto;  margin-top: 100px;">
                <div class="alert alert-success fade in">                
                    <strong>Success!</strong> <?php echo $this->session->flashdata('message'); ?>.
                </div>
            </div>
        </div>      
      </div>
    </div>
  </div>
</body>
</html>
