<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{

	$this->load->view('includes/header');
}
?>
<?php $type = is_admin(); 
$user_session = getCurrentUserSession();
?>
<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Profile </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Profile </li>
          </ol>
        </div>
      </div>
      
      <!-- /.row -->
      
      <?php if ($this->session->flashdata('insert') || $this->session->flashdata('delete') || $this->session->flashdata('update') ){?>
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('insert') ; 

								  echo $this->session->flashdata('delete');  

								  echo $this->session->flashdata('update'); 

							?> </div>
        </div>
      </div>
      <?php } ?>
       <div class="row">
      
        <div class="col-lg-6">
          <form role="form" method="post" action="<?php echo base_url(); ?>userlist/profile">
            <input type="hidden" name="edit_id" id="edit_id" value="<?php echo isset($userdata['id'])?$userdata['id']:'' ?>" />
         <div class="form-group row-fluid" >
         	<div class="span6">
              <label>First Name</label>
              <input class="form-control" name="firstname" id="firstname" value="<?php echo isset($userdata['firstname'])?$userdata['firstname']:' '   ; ?>" >
              </div>
              <div class="span6">
              <label>Last Name</label>
              <input class="form-control" name="lastname" id="lastname"  value="<?php echo isset($userdata['lastname'])?$userdata['lastname']:''; ?>" >
          
              </div>
             </div>
              <?php if ($user_session['usertype']!= 1){ 
		  ?>
            <div class="form-group row-fluid" >
         	<div class="span6">
             <label>User Name</label>
              <input class="form-control" name="username" id="username"  value="<?php echo isset($userdata['twitter_id'])?$userdata['twitter_id']:''; ?>" >
            </div>
            </div>
            <?php } ?>
             <div class="form-group">
              <label>Email</label>
              <input class="form-control" <?php if($user_session['usertype']==1){ ?> readonly="readonly" <?php } ?>  name="email" id="email" value="<?php echo isset($userdata['email'])?$userdata['email']:''; ?>" >
            </div>
             <?php if ($user_session['usertype']!= 1){ 
		  ?>
            <div class="form-group row-fluid" >
         	<div class="span6">
              <label>Twitter ID</label>
              <input class="form-control" name="twitterid" id="twitterid" value="<?php echo isset($userdata['twitter_id'])?$userdata['twitter_id']:' '  ; ?>" >
              </div>
              <div class="span6">
              <label>Facebook ID</label>
              <input class="form-control" name="facebookid" id="facebookid"  value="<?php echo isset($userdata['facebook_id'])?$userdata['facebook_id']:''; ?>" >
          
              </div>
             </div>
            
            
            <?php } ?>
            <?php if ($user_session['usertype']!= 1){ 
		  ?>
        	<div class="form-group">
              <label>Paypal Email address</label>
              <?php $payapal_email = isset($userdata['paypal_email']) ? $userdata['paypal_email']:''; ?>
              <input class="form-control" type="email" name="paypalemail" id="paypalemail" required="required" value="<?php echo $payapal_email; ?>" >
            </div>
             <div class="form-group">
              <label>Bank Details for Direct Bank Transfers</label>
              <p>Note : Indian publishers will be paid by wire transfer only.</p>
             </div>
             <fieldset>
             <legend></legend>
            <div class="form-group">
              <label>Account Holder Name</label>
              <input class="form-control account_detail" type="text" name="holder_name" id="holder_name" <?php echo (!$payapal_email) ? 'required="required"' : ''?> value="<?php echo isset($userdata['ac_holder_name'])?$userdata['ac_holder_name']:''; ?>">
            </div>
             <div class="form-group">
              <label>Bank Name</label>
              <input class="form-control account_detail" type="text" name="bank_name" id="bank_name" <?php echo (!$payapal_email) ? 'required="required"' : ''?> value="<?php echo isset($userdata['bank_name'])?$userdata['bank_name']:''; ?>" >
            </div>
             <div class="form-group">
              <label>IFSC</label>
              <input class="form-control account_detail" type="text" name="ifsc" id="ifsc" <?php echo (!$payapal_email) ? 'required="required"' : ''?> value="<?php echo isset($userdata['ifsc'])?$userdata['ifsc']:''; ?>" >
            </div>
            <div class="form-group">
              <label>SWIFT - BIC</label>
              <input class="form-control account_detail" type="text" name="swift_bic" id="swift_bic" <?php echo (!$payapal_email) ? 'required="required"' : ''?> value="<?php echo isset($userdata['swift_bic'])?$userdata['swift_bic']:''; ?>" >
            </div>
            <div class="form-group">
              <label>Account No.</label>
              <input class="form-control account_detail" type="text" name="account_no" id="account_no" <?php echo (!$payapal_email) ? 'required="required"' : ''?> value="<?php echo isset($userdata['account_no'])?$userdata['account_no']:''; ?>" >
            </div>
            </fieldset>
          
          <?php
		  
		  }?>
             <input type="submit" name="submit" class="btn btn-default" value="Update"  />
          </form>
        </div>
      </div>
      
      <!-- /.row --> 
      
      <br />
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<script>
$(document).ready(function(e) {
	payPalRequired();
	bankDetailRequired();
    $('#paypalemail').change(function(e) {
      payPalRequired();  
    });
	
	$('#holder_name, #bank_name, #ifsc, #swift_bic, #account_no').change(function(e) {
     	bankDetailRequired();
    });
});

function payPalRequired(){
	if($('#paypalemail').val()){
		$('.account_detail').removeAttr('required');
	}else{
		$('.account_detail').attr('required', 'required');
	}	
}
function bankDetailRequired(){
	var holder_name = $('#holder_name').val();
	var bank_name = $('#bank_name').val();
	var ifsc = $('#ifsc').val();
	var swift_bic = $('#swift_bic').val();
	var account_no = $('#account_no').val();
	if(holder_name || bank_name || ifsc || swift_bic || account_no){
		$('#paypalemail').removeAttr('required');		
	}else{
		$('#paypalemail').attr('required', 'required');		
	}	
}
</script>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
