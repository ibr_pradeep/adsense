<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}
?>

<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Paypal/account details </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Paypal/account details </li>
          </ol>
        </div>
      </div>
      
      <!-- /.row -->
      
     
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title"> Paypal/account details </h3>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
            
                <table id="mytables" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th> SNo.</th>
                      <th> Name </th>
                      <th>Paypal Email</th>
                      <th>Bank Name</th>
                      <th>Ac Holder Name</th>
                      <th>Account No</th>
                      <th>IFSC</th>
                      <th>SWIFT - BIC</th>
                      <th style="display:none"> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  
                      <?php //echo"<pre>"; print_r($paypals); die;
					  $i=1;
					   if (isset($paypals) && (count($paypals) > 0 )){

														foreach ($paypals as $paypal) { ?>
                                                          <tr>
                                                          <td><?php echo $i; ?></td>
                      <td><?php echo $paypal->firstname .' '. $paypal->middlename .'' .$paypal->lastname ?></td>
                      <td><?php echo isset($paypal->paypal_email)?$paypal->paypal_email:'' ?></td>
                      
                      <td><?php echo isset($paypal->bank_name)?$paypal->bank_name:'' ?></td>
                      <td><?php echo isset($paypal->ac_holder_name)?$paypal->ac_holder_name:'' ?></td>
                      <td><?php echo isset($paypal->account_no)?$paypal->account_no:'' ?></td>
                      <td><?php echo isset($paypal->ifsc)?$paypal->ifsc:'' ?></td>
                      <td><?php echo isset($paypal->swift_bic)?$paypal->swift_bic:'' ?></td>
                      
                    
                      <td style="display:none"><a href="<?php echo base_url()?>">[ Edit ] </a> &nbsp;&nbsp;<a onclick="return confirm('Are you sure you want to delete ? ')" href="">[ Delete ] </a></td>
                    </tr>
                    <?php $i++; } ?>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
