
<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}

?>
<script>

	
function getsharelink(str){
		if(str == 'category'){
			$("#categories_div").show();
			$("#users_div").hide();	
		}
		if(str == 'user'){
			 $("#users_div").show();
			$("#categories_div").hide();
		}
	}

</script>

    <div id="wrapper">



        <!-- Navigation -->

        

	<?php $this->load->view('includes/navbar');?>

        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">

                            Share Link

                        </h1>

                        <ol class="breadcrumb">

                            <li class="active">

                                <i class="fa fa-dashboard"></i> Share Link

                            </li>

                        </ol>

                    </div>

                </div>

                <!-- /.row -->

				<?php if ($this->session->flashdata('insert') || $this->session->flashdata('delete') || $this->session->flashdata('update') ){?>

                <div class="row">

                    <div class="col-lg-12">

                        <div class="alert alert-info alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <i class="fa fa-info-circle"></i> 

							<?php echo $this->session->flashdata('insert') ; 

								  echo $this->session->flashdata('delete');  

								  echo $this->session->flashdata('update'); 

							?> 

                        </div>

                    </div>

                </div>

                <?php } ?>

                <div class="row">

                    <div class="col-lg-6">



                        <form role="form" method="post" action="<?php echo base_url(); ?>index.php/sharelink/addUpdate">

							<input type="hidden" name="link_id" id="link_id" value="<?php echo isset($sharelinks['sharelink_id'])?$sharelinks['sharelink_id']:''; ?>" />
							
                            <div class="form-group">

                                <label>Share Link</label>
                             	</div>
                                <div class="form-group" > 
                                <input type="radio" required="required" value="category" <?php if(isset($sharelinks['category_id'])){ if($sharelinks['category_id'] != 0){ echo 'checked="checked"'; } }else{ echo 'checked="checked"'; }?>  name="sharelink" id="sharelink_category" onclick="getsharelink(this.value)" />Categories
                                <input type="radio" required="required" value="user" name="sharelink" id="sharelink_user" <?php if(isset($sharelinks['user_id'])){ if($sharelinks['user_id'] != 0){ echo 'checked="checked"'; } }?> onclick="getsharelink(this.value)"  />Users

                            </div>

                              <div class="form-group"  id="categories_div" <?php if(isset($sharelinks['category_id'])){ if($sharelinks['category_id'] != 0){ echo 'style="display:block"'; }else{  echo 'style="display:none"';} }?> >

                                <label>Category</label>

                                 <select name="category" id="category" class="form-control"   >

                                	<option value="" > --Select-- </option>

                                    <?php if($categories){

										foreach($categories as $category)

										{

											if($category->category_id == $sharelinks['category_id'])

											{

											?>

                                            <option value="<?php echo $category->category_id; ?>" selected="selected"><?php echo $category->title; ?></option>

                                            <?php 	

											}else

											{

											?>

                                            <option value="<?php echo $category->category_id; ?>"><?php echo $category->title; ?></option>

                                            <?php 

											}

										}	

									}?>

                                </select>

                            </div>
							<div class="form-group" id="users_div" <?php if(isset($sharelinks['user_id'])){ if($sharelinks['user_id'] != 0){ echo 'style="display:block"'; }else{  echo 'style="display:none"';} }else{ ?> style="display:none" <?php }?>>

                                <label>Users</label>

                                 <select  name="users_id" id="users" class="form-control"   >

                                	<option value="" > --Select-- </option>

                                    <?php if($users){

										foreach($users as $user)

										{

											if($user->id == $sharelinks['user_id'])

											{

											?>

                                            <option value="<?php echo $user->id; ?>" selected="selected"><?php echo $user->firstname." ".$user->middlename.' '.$user->lastname; ?></option>

                                            <?php 	

											}else

											{

											?>

                                            <option value="<?php echo $user->id; ?>"><?php echo $user->firstname." ".$user->middlename.' '.$user->lastname; ?></option>

                                            <?php 

											}

										}	

									}?>

                                </select>

                            </div>
                             <div class="form-group">

                                <label>Share link</label>

                                <input  type="url" class="form-control" name="link" id="link" required="required" value="<?php echo isset($sharelinks['link'])?$sharelinks['link']:''; ?>" >

                               

                            </div>
                         <?php //echo "<PRE>"; print_r($sharelinks); ?>
                  <div class="form-group">
                   <div class="checkbox">
                      <label>
                          <input type="checkbox"  id="Scheduler_Time" <?php  if(!empty($sharelinks['Scheduler_Set'])){echo'checked';} ?> value="Yes" name="Scheduler_Set">Scheduler 
                      </label>
                  </div>
                  </div>
                           

                            <div class="form-group " id="Scheduler_Time_hide"> 
                            	<div class="col-xs-6"   >       
                                <label>Date And Time</label>
                                <input  type="text" class="form-control" name="scheduler_datetime" id="datetimepicker_start_time"  value="<?php echo isset($sharelinks['scheduler_datetime']) ? $sharelinks['scheduler_datetime']:''; ?>" >
								</div>
                              
                                <br /><br /><br />
                            </div>
							
						  <div class="form-group">
                          
                           <input type="submit" name="submit" class="btn btn-default" value="Submit"  />

                            <button type="reset" class="btn btn-default">Reset Button</button>

							</div>

                        </form>



                    </div>

                    

                </div>

                <!-- /.row -->

                <br />

 				<?php if(!isset($sharelinks['category_id'])){?>

              	<div class="row">
				 <div class="col-lg-12">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <h3 class="panel-title">Manage Share Link</h3>

                            </div>

                            <div class="panel-body">

                                <div class="table-responsive">

                                    <table width="100%" id="mytables" class="table table-bordered table-hover table-striped">

                                        <thead>

                                            <tr>

                                                <th width="5%"> #</th>

                                                <th  width="15%"> URL </th>

                                                <th  width="10%"> Category </th>
                                                <th  width="15%"> User</th>
                                                <th width="15%"> Scheduler </th>

                                              

                                                <th  width="30%"> Action </th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                            <tr>

                                            	<?php if (isset($results) && (count($results) > 0 )){

														foreach ($results as $result) { ?>

                                                       

															<td><?php echo isset($result->sharelink_id)?$result->sharelink_id:'' ?></td>

                                                            <td><?php echo isset($result->link)?$result->link:'' ?></td>

															<td><?php echo isset($result->title)?$result->title:'' ?></td>
                                                            
                                                            <td><?php echo  $result->firstname.' '.$result->middlename.' '.$result->lastname ?></td>
                                                            
                                                            <td><?php echo isset($result->scheduler_datetime)?$result->scheduler_datetime:'' ?></td>

															

															<td><a href="<?php echo base_url()?>index.php/sharelink/addUpdate/<?php echo $result->sharelink_id; ?>">[Edit] </a>

																&nbsp;&nbsp;<a onclick="return confirm('Are you sure you want to delete ? ')" href="<?php echo base_url()?>index.php/sharelink/deleteSharelink/<?php echo $result->sharelink_id; ?>">[Delete] </a>

                                                    	</tr>

														<?php } ?>

													<?php } ?>

                                            

                                          </tbody>

                                    </table>

                                </div>

                                

                            </div>

                        </div>

                    </div>

                </div>

                 <?php } ?>   

                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    



<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>

	 
    
<script>

$(document ).ready(function() { 

	
	
		if ($("#Scheduler_Time").is(":checked")){
		$("#Scheduler_Time_hide").show();
		}
		else
		{
		$("#Scheduler_Time_hide").hide();
		$("#datetimepicker_start_time").val('');
		}
	$("#Scheduler_Time").click(function(){
		//$("#Scheduler_Time").this.val();
		if ($("#Scheduler_Time").is(":checked")){
			
		$("#Scheduler_Time_hide").show();
		}
		else
		{
			$("#Scheduler_Time_hide").hide();
			$("#datetimepicker_start_time").val('');
		}
	});
	
	});

</script>

<script>
jQuery('#datetimepicker').datetimepicker();

jQuery('#datetimepicker2').datetimepicker({
  datepicker:false,
  format:'H:i'
});
jQuery('#datetimepicker_start_time').datetimepicker({
 // startDate:'+2015-05-01',//or 1986/12/08,
    format: 'Y-m-d H:i:s',
   allowTimes:[
  '00:00' ,'00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00',  '22:30', '23:00','23:30']
});
</script>
