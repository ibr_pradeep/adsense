<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Usercategory extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */

	 

	public function __construct()

	{

		parent::__construct();

		$this->load->model('categoryuser_model');

	}

	public function index()
	{
		
		$data['categories'] = $this->categoryuser_model->getallCategory();
		$data['usercategories'] = $this->categoryuser_model->getmyCategory();

		$this->load->view('user_category',$data);

	}

	/** Add and update particuler category value

		

	**/

	public function addUpdate($categoryid = '')

	{

			$cat_ids = array(); 
			foreach($_POST['category_id'] as $ids){
					$cat_ids[] = $ids; 
			}
			$categoryIDs = implode($cat_ids ,';');
			$edit_id = $this->input->post('edit_id');
			
			if($this->input->post('submit')){			
				if($this->input->post('edit_id')){

				
						$data = array(
						'user_id'		=> $this->session->userdata('userID'),
						'category_id' 	=> $categoryIDs,
						);
				$response = $this->categoryuser_model->updateUserCategory($data , $edit_id );
				if($response){

					$this->session->set_flashdata('update','Category Updated Sucessfully..!!');	

					redirect(base_url().'index.php/usercategory');

				}

				

			}else{


					$data = array(
						'user_id'		=> $this->session->userdata('userID'),
						'category_id' 	=> $categoryIDs,
						'created_date'   => date('Y-m-d H:i:s')

						);
						
						$response = $this->categoryuser_model->insertUserCategory($data);
		
						if($response){
		
							$this->session->set_flashdata('insert','Category Inserted Sucessfully..!!');	
		
							redirect(base_url().'index.php/usercategory');
		
						}	

			}

			}

			$datacat['category'] = $this->categoryuser_model->getSingleUserCategory($categoryIDs);	

			

			$this->load->view('category',$datacat);

	}

	/* Delete single category */

	public function deleteCategory($categorID)

	{

		$response = $this->categoryuser_model->deleteCategory($categorID);

		if($response)

		{

			$this->session->set_flashdata('delete','Category deleted Sucessfully..!!');	

			redirect(base_url().'index.php/category');

		}

	}

	

	

}

