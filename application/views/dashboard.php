<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}
$user_session = getCurrentUserSession();
?>

<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Dashboard  </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Dashboard </li>
          </ol>
        </div>
      </div>
      <!-- /.row -->
      
      <div class="row" >
        <div class="col-lg-12">
          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i> <strong>Well Come To <?php echo  $user_session['username'] ;?></strong> </div>
        </div>
      </div>
      <!-- /.row -->
      <fieldset >
      
      <?php if($user_session['usertype'] == 2){
		 ?>
      <div class="row">
        <div class="col-lg-6">
          <?php $categoty_ids = '';  if(isset($usercategories->category_id)){ $categoty_ids = explode(';',$usercategories->category_id ); } ?>
          <form role="form" method="post" action="<?php echo base_url(); ?>usercategory/addUpdate">
            <input type="hidden" name="edit_id" id="edit_id" value="<?php echo isset($usercategories->user_category_id)?$usercategories->user_category_id:''; ?>" />
            <div class="form-group">
              <label>Select Categories</label>
              <select  class="form-control chzn-select"  name="category_id[]" id="category_id" multiple="multiple">
                <option value=""> --Select--</option>
                <?php 
									if(sizeof($categories)>0){
										foreach($categories as $category){
											if(in_array($category->category_id , $categoty_ids)){
												echo '<option value="'.$category->category_id.'" selected >'.$category->title.'</option>'; 	
											}else
											{
											echo '<option value="'.$category->category_id.'" >'.$category->title.'</option>'; 	
											}
										}	
									}
									?>
              </select>
            </div>
            <?php if(isset($usercategories->user_category_id)){
	?>
            <input type="submit" name="submit" class="btn btn-default" value="Update"  />
            <?php 
	}else
	{?>
            <input type="submit" name="submit" class="btn btn-default" value="Add"  />
            <?php }?>
          </form>
        </div>
      </div>
      <?php } ?>
      </fieldset>
      <!-- /.row -->
      
      
      <!-- /.row -->
      
      
      <!-- /.row --> 
      
    </div>
    <!-- /.container-fluid --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
