<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}





$user_session = getCurrentUserSession();

?>



    <div id="wrapper">



        <!-- Navigation -->

        

	<?php $this->load->view('includes/navbar');?>

        <div id="page-wrapper">



            <div class="container-fluid">



                <!-- Page Heading -->

                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">

                            Category

                        </h1>

                        <ol class="breadcrumb">

                            <li class="active">

                                <i class="fa fa-dashboard"></i> Category

                            </li>

                        </ol>

                    </div>

                </div>

                <!-- /.row -->

				<?php if ($this->session->flashdata('insert') || $this->session->flashdata('delete') || $this->session->flashdata('update') ){?>

                <div class="row">

                    <div class="col-lg-12">

                        <div class="alert alert-info alert-dismissable">

                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

                            <i class="fa fa-info-circle"></i> 

							<?php echo $this->session->flashdata('insert') ; 

								  echo $this->session->flashdata('delete');  

								  echo $this->session->flashdata('update'); 

							?> 

                        </div>

                    </div>

                </div>

                <?php } ?>
					
      
      <?php if($user_session['usertype'] == 2){
		 ?>
      <div class="row">
        <div class="col-lg-6">
          <?php $categoty_ids = '';  if(isset($usercategories->category_id)){ $categoty_ids = explode(';',$usercategories->category_id ); } ?>
          <form role="form" method="post" action="<?php echo base_url(); ?>usercategory/addUpdate">
            <input type="hidden" name="edit_id" id="edit_id" value="<?php echo isset($usercategories->user_category_id)?$usercategories->user_category_id:''; ?>" />
            <div class="form-group">
              <label>Select Categories</label>
              <select  class="form-control chzn-select"  name="category_id[]" id="category_id" multiple="multiple">
                <option value=""> --Select--</option>
                <?php 
									if(sizeof($categories)>0){
										foreach($categories as $category){
											if(in_array($category->category_id , $categoty_ids)){
												echo '<option value="'.$category->category_id.'" selected >'.$category->title.'</option>'; 	
											}else
											{
											echo '<option value="'.$category->category_id.'" >'.$category->title.'</option>'; 	
											}
										}	
									}
									?>
              </select>
            </div>
            <?php if(isset($usercategories->user_category_id)){
	?>
            <input type="submit" name="submit" class="btn btn-default" value="Update"  />
            <?php 
	}else
	{?>
            <input type="submit" name="submit" class="btn btn-default" value="Add"  />
            <?php }?>
          </form>
        </div>
      </div>
      <?php }else{ ?>
    
      
      
      
                <div class="row">

                    <div class="col-lg-6">
                    
  					  <form role="form" method="post" action="<?php echo base_url(); ?>category/addUpdate">

							<input type="hidden" name="category_id" id="category_id" value="<?php echo isset($category['category_id'])?$category['category_id']:''; ?>" />

                             <div class="form-group">

                                <label>Add New Category</label>

                                <input class="form-control" name="title" id="title" required="required" value="<?php echo isset($category['title'])?$category['title']:''; ?>" >

                               

                            </div>


	<?php if(!isset($category['category_id'])){?>
                           <input type="submit" name="submit" class="btn btn-default btn-primary" value="Add"  />

                            <button type="reset" class="btn btn-default">Reset Button</button>

<?php } else {
	?>
     <input type="submit" name="submit" class="btn btn-default btn-primary" value="Update"  />
    <?php 
	}?>

                        </form>

  					 </div>

                    

                </div>
		
                <!-- /.row -->

                <br />

 				<?php if(!isset($category['category_id'])){?>

              	<div class="row">

                    

                    

                    <div class="col-lg-10">

                        <div class="panel panel-default">

                            <div class="panel-heading">

                                <h3 class="panel-title">Manage Category</h3>

                            </div>

                            <div class="panel-body">

                                <div class="table-responsive">

                                    <table id="mytables" class="table table-bordered table-hover table-striped">

                                        <thead>

                                            <tr>

                                                <th> SNo.</th>

                                                <th> Title </th>

                                                <th>Create date</th>

                                                <th> Action </th>

                                            </tr>

                                        </thead>

                                        <tbody>

                                            <tr>

                                            	<?php if (isset($results) && (count($results) > 0 )){

														foreach ($results as $result) { ?>

                                                   

															<td><?php echo isset($result->category_id)?$result->category_id:'' ?></td>

															<td><?php echo isset($result->title)?$result->title:'' ?></td>

															<td><?php echo isset($result->create_date)?$result->create_date:'' ?></td>

															<td><a href="<?php echo base_url()?>category/addUpdate/<?php echo $result->category_id; ?>">[ Edit ] </a>

																&nbsp;&nbsp;<a onclick="return confirm('Are you sure you want to delete ? ')" href="<?php echo base_url()?>category/deleteCategory/<?php echo $result->category_id; ?>">[ Delete ] </a>

                                                    	</tr>

														<?php } ?>

													<?php } ?>

                                            

                                          </tbody>

                                    </table>

                                </div>

                                

                            </div>

                        </div>

                    </div>

                </div>

                 <?php } ?>   
  <?php } ?>   
                </div>

                <!-- /.row -->



            </div>

            <!-- /.container-fluid -->



        </div>

        <!-- /#page-wrapper -->



    </div>

    



<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>