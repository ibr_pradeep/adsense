<!DOCTYPE html>
<html lang='en'>
<head>
  <meta charset="UTF-8" /> 
  <title>
      Affiliate Web Application Forgot Password  
  </title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/login_style.css" />
</head>
<body>
  <div id="wrapper">  
  <!-- Navigation -->
    <div id="page-wrapper">    
      <div class="container-fluid">    
      <!-- Page Heading -->
        <div class="row">   
          <form action="<?php echo base_url('user/admin/set_new_password');?>" method="post">
            <?php if($this->session->flashdata('error')){ ?>
            <div class="alert alert-danger fade in">               
                <strong>Error!</strong> <?php echo $this->session->flashdata('error');?>
            </div>
            <?php } ?>
            <div class="inset">
              <p>
                <label for="password">New password </label>
                <input type="password" name="password" id="password">
              </p> 
              <p>
                <label for="Confirm password">Confirm password </label>
                <input type="password" name="c_password" id="c_password">
              </p>                         
            </div>
            <p class="p-container">
            	<input type="hidden" name="token" value="<?php echo isset($token) ? $token : ''; ?>">
                <input type="hidden" name="email" value="<?php echo isset($email) ? $email : ''; ?>">
              <input type="submit" name="go" id="go" value="Go">
            </p>          
          </form>
        </div>      
      </div>
    </div>
  </div>
</body>
</html>
