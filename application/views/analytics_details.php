<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}


?>
<script type="text/javascript">
$(document).ready(function() {
    $('#myanalytics').dataTable( {
        //"order": [[ 2, "desc" ]]
    } );
} );
</script>

<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Adsense Report </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Adsense Report </li>
          </ol>
        </div>
      </div>
      
      <!-- /.row -->
      
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Manage Adsense</h3>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <div class="form-group " id="Scheduler_Time_hide">
                  <div class="col-lg-8 row-no-padding "   >
                  <form method="post" action="<?php echo base_url('analyticsreport')?>">
                	<div class="row-fluid">
                		<label>Search: </label>
                   <div class="span12">
                    <input type="text"  class="form-control span3 " name="date_filter_start" id="date_filter_start" placeholder="Start Date"  value="<?php echo isset($start_date) ? $start_date : ''; ?>">
                    <input  type="text" class="form-control span3 " name="date_filter_end" id="date_filter_end" placeholder="End Date"  value="<?php echo isset($end_date) ? $end_date : ''; ?>" >
                    <input type="submit" name="search" id="search"  value="Search" class="form-control span3 btn btn-sm btn-primary" />
                    </div>
                    
                   </div>
                    </form>
                    
                  </div>
                  <br />
                  <br />
                  <br />
                </div>
                <table id="myanalytics" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th> #</th>
                      <th> Username </th>
                      <th> Analytic Account</th>
                      <th> Referral ID </th>
                      <th> Share Link </th>
                     
                      <th> AdSense Revenue </th>
                      <th>AdSense Ads Clicked</th>
                      <th>AdSense Page Impressions</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
					if(!empty($analytics)){
					  foreach ($analytics as $service_acc => $analytics_) {
						if(!empty($analytics_)){					  
						 /* foreach ($analytics_ as $analytic_) {
							if(!empty($analytic_)){*/
							$accountname = analyticAccountName($service_acc);
							 $i=1;
							  foreach($analytics_ as $key => $analytic){
								if(!empty($analytic)){	
								if(getUsernamebyRef($key)){						
								  echo '<tr>
										<td></td>
										<td style="border-right:none">'.getUsernamebyRef($key).'</td>
										<td style="border:none"></td>
										<td style="border:none"></td>
										<td style="border:none"></td>
										<td style="border:none"></td>
										<td style="border:none"></td>
										<td style="border:none"></td>
									  </tr>';
								 
								  foreach($analytic as $val){
									  if($val){
									$ref = explode('?ref=',$val['share_url']); 
									if(isset($ref[0])){	
									$rr = isset($ref[1]) ? str_replace('/', '', $ref[1]) : '';													
									echo '<tr>
										  <td></td>
										  <td></td>
										  <td>'.$accountname.'</td>
										  <td>'.$rr.'</td>
										  <td>'.$val['share_url'].'</td>
										  <td>$ '.number_format($val['AdSense_Revenue'], 2, '.', ',').'</td>
										  <td>'.$val['AdSense_Ads_Clicked'].'</td>
										  <td>'.$val['AdSense_Page_Impressions'].'</td>
										</tr>';
									$i++;
									}
									  }
								  }
								  }
								}
							  }
							/*}
						  }*/
						}
					  }  
					}
					?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
<script>
$('#date_filter_start').datetimepicker({
	//yearOffset:222,
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#date_filter_end').datetimepicker({
	//yearOffset:222,
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
</script>
<script type="text/javascript">
function getdatefileter(filter)
{
	window.location = "<?php echo base_url()?>analyticsreport?filter="+filter; 	
}
</script>