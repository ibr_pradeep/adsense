<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}

?>
<script>

	
function getsharelink(str){
		if(str == 'category'){
			$("#categories_div").show();
			$("#fbusers_div").hide();
			$("#twusers_div").hide();
			$("#fbcontent").removeAttr('maxlength')
			$('#char-limit').html('');
			
		}else if(str == 'fbuser'){
			$("#fbusers_div").show();
			$("#categories_div").hide();
			$("#twusers_div").hide();
			$('#users_chosen').css({'display':'inline-block'});
			$("#fbcontent").removeAttr('maxlength')
			$('#char-limit').html('');
	
			
		}else if(str == 'twuser'){
			$("#twusers_div").show();
			$("#fbusers_div").hide();
			$("#categories_div").hide();			
			$('#users_chosen').css({'display':'inline-block'});
			
			var linkChar = $('#link').val().length;
			var imageChar = ($('#image').val().length) ? 23 : 0;
			var contentChar = $('#fbcontent').val().length;
			
			if(linkChar){
				if ($('#link').val().indexOf("https") >= 0){
					linkChar = 24;
				}else{
					linkChar = 23	
				}			
			}else{
				linkChar = 0;	
			}
			//linkChar = (linkChar > 22) ? 22 : linkChar;
			var maxlength = 140 - linkChar;
			maxlength = maxlength - imageChar;
			if(maxlength < contentChar){
				var content  = $('#fbcontent').val().slice(0, maxlength);
				$('#fbcontent').val(content);
				contentChar = $('#fbcontent').val().length;
			}
			maxlength_ = maxlength - contentChar;
			
			$('#fbcontent').attr('maxlength',maxlength);
			$('#char-limit').html('Character limit '+maxlength_);
			//allert;
			
			
			
		}
	}
$(document).ready(function() {
	$('#link').keyup(function(){
		
		if($('#sharelink_twuser').is(':checked')) { 
			var linkChar = $('#link').val().length;
			var imageChar = ($('#image').val().length) ? 23 : 0;
			var contentChar = $('#fbcontent').val().length;
			
			if(linkChar){
				if ($('#link').val().indexOf("https") >= 0){
					linkChar = 24;
				}else{
					linkChar = 23	
				}			
			}else{
				linkChar = 0;	
			}
			
			/*linkChar = (linkChar > 22) ? 22 : linkChar;*/
			var maxlength = 140 - linkChar;
			maxlength = maxlength - imageChar;
			if(maxlength < contentChar){
				var content  = $('#fbcontent').val().slice(0, maxlength);
				$('#fbcontent').val(content);
				contentChar = $('#fbcontent').val().length;
			}
			maxlength_ = maxlength - contentChar;
			//alert(maxlength);
			$('#fbcontent').attr('maxlength',maxlength);
			$('#char-limit').html('Character limit '+maxlength_);
		}
    		
	
	});
	
	$('#image').change(function(){
		
		if($('#sharelink_twuser').is(':checked')) { 
			var linkChar = $('#link').val().length;
			var imageChar = ($('#image').val().length) ? 23 : 0;
			var contentChar = $('#fbcontent').val().length;
			
			if(linkChar){
				if ($('#link').val().indexOf("https") >= 0){
					linkChar = 24;
				}else{
					linkChar = 23	
				}			
			}else{
				linkChar = 0;	
			}
			
			/*linkChar = (linkChar > 22) ? 22 : linkChar;*/
			
			var maxlength = 140 - linkChar;
			maxlength = maxlength - imageChar;
			if(maxlength < contentChar){
				var content  = $('#fbcontent').val().slice(0, maxlength);
				$('#fbcontent').val(content);
				contentChar = $('#fbcontent').val().length;
			}
			maxlength_ = maxlength - contentChar;
			//alert(maxlength);
			$('#fbcontent').attr('maxlength',maxlength);
			$('#char-limit').html('Character limit '+maxlength_);
		}
    		
	
	});
	
	$('#fbcontent').keyup(function(){
		
		if($('#sharelink_twuser').is(':checked')) { 
			var linkChar = $('#link').val().length;
			
			if(linkChar){
				if ($('#link').val().indexOf("https") >= 0){
					linkChar = 24;
				}else{
					linkChar = 23	
				}			
			}else{
				linkChar = 0;	
			}
			
			/*linkChar = (linkChar > 22) ? 22 : linkChar;*/
			
			var maxlength = 140 - linkChar;
			var imageChar = ($('#image').val().length) ? 23 : 0;
			maxlength = maxlength - imageChar;
			
			var remainingChar = $('#fbcontent').val().length;
			var newlength = maxlength - remainingChar;
			$('#char-limit').html('Character limit '+newlength);
		}
    		
	
	});
   // $('#mytables').DataTable();
   
   
} );
$(document).ready(function() {
	$('#mytables').DataTable( { "order": [[ 0, "desc" ]] });
});
</script>

<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Share Link </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Share Link </li>
          </ol>
        </div>
      </div>
      
      <!-- /.row -->
      
      <?php if ($this->session->flashdata('insert') || $this->session->flashdata('delete') || $this->session->flashdata('update') ){?>
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('insert') ; 

								  echo $this->session->flashdata('delete');  

								  echo $this->session->flashdata('update'); 

							?> </div>
        </div>
      </div>
      <?php } ?>
      
      <?php if ($this->session->flashdata('error')){?>
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-info alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('error') ; ?> 
          </div>
        </div>
      </div>
      <?php } ?>
    
      <div class="row">
        <div class="col-lg-6">
          <form role="form" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>sharelink/addUpdate">
            <input type="hidden" name="link_id" id="link_id" value="<?php echo isset($sharelinks['sharelink_id'])?$sharelinks['sharelink_id']:''; ?>" />
            <div class="form-group">
              <label>Share Link</label>
            </div>
            <div class="form-group" >
              <input type="radio" required="required" value="category" <?php if(isset($sharelinks['category_id'])){ if($sharelinks['category_id'] != 0){ echo 'checked="checked"'; } }else{ echo 'checked="checked"'; }?>  name="sharelink" id="sharelink_category" onclick="getsharelink(this.value)" />
              Category
              
              <input type="radio" required="required" value="fbuser" name="sharelink" id="sharelink_fbuser" <?php echo ($show_fbuser) ? 'checked="checked"' : ''; ?> onclick="getsharelink(this.value)"  />
              Facebook User
              
              <input type="radio" required="required" value="twuser" name="sharelink" id="sharelink_twuser" <?php echo ($show_twuser) ? 'checked="checked"' : ''; ?> onclick="getsharelink(this.value)"  />
              Twitter User
            </div>
              
            <div class="form-group"  id="categories_div" <?php if(isset($sharelinks['category_id'])){ if($sharelinks['category_id'] != 0){ echo 'style="display:block"'; }else{  echo 'style="display:none"';} }?> >
              <label>Category</label>
              <?php $categoryIDs[] =  isset($sharelinks['category_id']) ? explode(';',$sharelinks['category_id']) : '';
			  ?>
              <select name="category[]" id="category" class="span4 form-control chzn-select"  multiple="multiple"  >
                <option value="" > --Select-- </option>
                <?php if($categories){

										foreach($categories as $category)

										{

											if(in_array($category->category_id , $categoryIDs ))
											{

											?>
                <option value="<?php echo $category->category_id; ?>" selected="selected"><?php echo $category->title; ?></option>
                <?php 	

											}else

											{

											?>
                <option value="<?php echo $category->category_id; ?>"><?php echo $category->title; ?></option>
                <?php 

											}

										}	

									}?>
              </select>
            </div>
            
            <div class="form-group" id="fbusers_div" <?php echo (!$show_fbuser) ? 'style="display:none"' : ''; ?>>
              <label>Facebook Users</label>
              <?php $useryIDs = isset($sharelinks['user_id']) ? explode(';', $sharelinks['user_id']) : ''; ?>
              <select  name="fbusers[]" id="fbusers" class="span4 form-control chzn-select"  multiple="multiple">
                <option value="all">All</option>
                <?php 
				if($fb_users){
				  foreach($fb_users as $fb_user){	
				  	$fb_user_id = isset($fb_user->id) ? $fb_user->id:'';
					$fb_user_name = isset($fb_user->firstname) ? $fb_user->firstname : '';
					$fb_user_name .= isset($fb_user->middlename) ? ' '.$fb_user->middlename : '';
					$fb_user_name .= isset($fb_user->lastname) ? ' '.$fb_user->lastname : '';
					$selected = '';
					if(is_array($useryIDs)){
						if(in_array($fb_user_id, $useryIDs)){
							$selected = 'selected="selected"';	
						}
					}
					echo '<option value="'.$fb_user_id.'" '.$selected.'>'.$fb_user_name.'</option>';                   
				  }
				}?>
              </select>
            </div>
            
            <div class="form-group" id="twusers_div" <?php echo (!$show_twuser) ? 'style="display:none"' : ''; ?>>
              <label>Twitter Users</label>
              <?php $useryIDs = isset($sharelinks['user_id']) ? explode(';', $sharelinks['user_id']) : ''; ?>
              <select  name="twusers[]" id="twusers" class="span4 form-control chzn-select"  multiple="multiple">                
                <option value="all">All</option>
                <?php 
				if($twt_users){
				  foreach($twt_users as $twt_user){	
				  	$tw_user_id = isset($twt_user->id) ? $twt_user->id:'';
					$tw_user_name = isset($twt_user->firstname) ? $twt_user->firstname : '';
					$tw_user_name .= isset($twt_user->middlename) ? ' '.$twt_user->middlename : '';
					$tw_user_name .= isset($twt_user->lastname) ? ' '.$twt_user->lastname : '';
					$selected = '';
					if(is_array($useryIDs)){
						if(in_array($tw_user_id, $useryIDs)){
							$selected = 'selected="selected"';	
						}
					}
					echo '<option value="'.$tw_user_id.'" '.$selected.'>'.$tw_user_name.'</option>';                   
				  }
				}?>
              </select>
            </div>
            
            <div class="form-group" >
            <div class="row-fluid">
            <div class="span6">
              <label>Share link</label>
              <input  type="url" class=" twittercheck form-control" name="link" id="link" value="<?php echo isset($sharelinks['link'])?$sharelinks['link']:''; ?>" >
              </div>
               <div class=" span6 ">
               <label>Share Image</label>
              <input type="file" name="image" id="image"  class="form-control"  />
              </div>
            </div>
            </div>
            
            <div class="form-group">
              <label>Share Text</label>
              <span id="char-limit" style="float: right;font-weight: bold;"></span>
              <textarea name="fbcontent" id="fbcontent"  class="form-control" ></textarea>
            </div>
            
            <div class="form-group  span3" >
             
            </div>
            <div class="row-fluid ">
              <div class="checkbox span6">
                <label>
                  <input type="checkbox"  id="Scheduler_Time" <?php  if(!empty($sharelinks['Scheduler_Set'])){echo'checked';} ?> value="Yes" name="Scheduler_Set">
                  Schedule </label>
                  
              </div>
            
              <div class=" span6"  id="Scheduler_Time_hide" >
                <label>Date And Time</label>
                <input  type="text" class="form-control" name="scheduler_datetime" id="datetimepicker_start_time"  value="<?php echo isset($sharelinks['scheduler_datetime']) ? $sharelinks['scheduler_datetime']:''; ?>" >
              </div>
              <br />
              <br />
              <br />
            </div>
          
            
            
            
            <div class="form-group">
            <?php if(isset($sharelinks['sharelink_id'])){
				?>
                 <input type="submit" name="submit" class="btn btn-default btn-primary" value="Update"  />
                 
                <?php
				}else{?>
              <input type="submit" name="submit" class="btn btn-default btn-primary" value="Submit"  />
              <button type="reset" class="btn btn-default">Reset Button</button>
              <?php } ?>
            </div>
          </form>
        </div>
      </div>
      
      <!-- /.row --> 
      
      <br />
      <?php if(!isset($sharelinks['category_id'])){
		  
		   // print_r($results);
		  ?>
      
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Manage Share Link</h3>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table width="100%" id="mytables" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th width="5%"> #</th>
                      <th  width="15%"> Create Date</th>
                      <th  width="15%"> URL </th>
                      <th  width="10%"> Category </th>
                      <th  width="15%"> User</th>
                      <th width="15%"> Scheduler </th>
                      <th width="15%"> Status </th>
                      <th  width="30%"> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php 
					 if (isset($results) && (count($results) > 0 )){

					foreach ($results as $result) { ?>
                      <td width="10%"><?php echo $result->sharelink_id?></td>
                      <td width="20%"><?php echo isset($result->create_date)?$result->create_date:'' ?></td>
                      <td width="20%"><div class="link" ><?php echo isset($result->link)?$result->link:'' ?></div></td>
                      <td width="10%"><?php   $category_id = explode(';',$result->category_id); echo getcategory($category_id) ;?></td>
                      <td width="10%"><?php $userIds = explode(';',$result->user_id); echo getUsername($userIds); ?></td>
                      <td width="10%"><?php echo isset($result->scheduler_datetime)?$result->scheduler_datetime:'' ?></td>
                      <td width="10%">
					  <?php  if ($result->is_deleted == 1 || $result->is_deleted == 2 ){ 
					  echo getStatus('', $result->is_deleted);  
					  }else {  
					  echo getStatus($result->scheduler_status);
					   } ?>
                       </td>
                      <td  width="10%"><a href="<?php echo base_url()?>sharelink/addUpdate/<?php echo $result->sharelink_id; ?>">[Edit] </a> &nbsp;&nbsp;<a onclick="return confirm('Are you sure you want to delete ? ')" href="<?php echo base_url()?>sharelink/deleteSharelink/<?php echo $result->sharelink_id; ?>">[Delete] </a>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
<script>

$(document ).ready(function() { 

	
	
		if ($("#Scheduler_Time").is(":checked")){
		$("#Scheduler_Time_hide").show();
		}
		else
		{
		$("#Scheduler_Time_hide").hide();
		$("#datetimepicker_start_time").val('');
		}
	$("#Scheduler_Time").click(function(){
		//$("#Scheduler_Time").this.val();
		if ($("#Scheduler_Time").is(":checked")){
			
		$("#Scheduler_Time_hide").show();
		}
		else
		{
			$("#Scheduler_Time_hide").hide();
			$("#datetimepicker_start_time").val('');
		}
	});
	
	});

</script> 
<script>
jQuery('#datetimepicker').datetimepicker();

jQuery('#datetimepicker2').datetimepicker({
  datepicker:false,
  format:'H:i'
});
jQuery('#datetimepicker_start_time').datetimepicker({
 // startDate:'+2015-05-01',//or 1986/12/08,
    format: 'Y-m-d H:i:s',
   allowTimes:[
  '00:00' ,'00:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', '21:00', '21:30', '22:00',  '22:30', '23:00','23:30']
});
$('#fbusers, #twusers').change(function(e) {	
	var values = $(this).val();
	if($.inArray("all", values) != -1){
	  $(this).find('option').prop('selected', true);
	  $(this).find('option:eq(0)').prop('selected', false);
	  $(this).find('option:eq(1)').prop('selected', false);
	  $(this).find('option:eq(1)').val('deselectall');
	  $(this).find('option:eq(1)').text('deselectAll');
	  $(this).trigger("liszt:updated");	 
	}else if($.inArray("deselectall", values) != -1){
	  $(this).find('option').prop('selected', false);
	  $(this).find('option:eq(1)').val('all');
	  $(this).find('option:eq(1)').text('All');
	  $(this).trigger("liszt:updated");	  
	}
});
</script> 
