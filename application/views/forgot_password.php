<!DOCTYPE html>
<html lang='en'>
<head>
  <meta charset="UTF-8" /> 
  <title>
      Affiliate Web Application Forgot Password  
  </title>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/css/login_style.css" />
</head>
<body>
  <div id="wrapper">  
  <!-- Navigation -->
    <div id="page-wrapper">    
      <div class="container-fluid">    
      <!-- Page Heading -->
        <div class="row">   
          <form action="<?php echo base_url('user/admin/forgot_password');?>" method="post">
            <?php if($this->session->flashdata('error')){ ?>
            <div class="alert alert-danger fade in">                
                <strong>Error!</strong> <?php echo $this->session->flashdata('error');?>
            </div>
            <?php } ?>
            <div class="inset">
              <p>
                <label for="email">EMAIL </label>
                <input type="text" name="email" id="email">
              </p>                         
            </div>
            <p class="p-container">    
              <input type="submit" name="go" id="go" value="Go">
            </p>          
          </form>
        </div>      
      </div>
    </div>
  </div>
</body>
</html>
