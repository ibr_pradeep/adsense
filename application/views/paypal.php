<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}







?>

<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Paypal Details </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Paypal Details </li>
          </ol>
        </div>
      </div>
     
      <!-- /.row -->
      
      <?php if ($this->session->flashdata('insert') || $this->session->flashdata('delete') || $this->session->flashdata('update') ){?>
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('insert') ; 

								  echo $this->session->flashdata('delete');  

								  echo $this->session->flashdata('update'); 

							?> </div>
        </div>
      </div>
      <?php } ?>
      <div class="row" style="height:350px">
        <div class="col-lg-6">
          <form role="form" method="post" action="<?php echo base_url(); ?>paypal/addUpdate">
            <input type="hidden" name="edit_id" id="edit_id" value="<?php echo isset($paypal['id'])?$paypal['id']:''; ?>" />
           	
           
            <div class="form-group">
              <label>Paypal Email address</label>
              <input class="form-control" type="email" name="paypalemail" id="paypalemail" required="required" value="<?php echo isset($paypal['paypal_email'])?$paypal['paypal_email']:''; ?>" >
            </div>
            <?php if (isset($paypal['id'])){
				?>
                <input type="submit" name="submit" class="btn btn-default" value="Update"  />
                <?php	
			} else{?>
            <input type="submit" name="submit" class="btn btn-default" value="Submit"  />
            <?php } ?>
            <button type="reset" class="btn btn-default">Reset Button</button>
          </form>
        </div>
      </div>
      
      <!-- /.row --> 
      
      <br />
     
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
