<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}







?>

<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Api Configration </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Api Configration </li>
          </ol>
        </div>
      </div>
      
      <!-- /.row -->
      
      <?php if ($this->session->flashdata('insert') || $this->session->flashdata('delete') || $this->session->flashdata('update') ){?>
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('insert') ; 

								  echo $this->session->flashdata('delete');  

								  echo $this->session->flashdata('update'); 

							?> </div>
        </div>
      </div>
      <?php }
				if($this->session->flashdata('exist')){
					?>
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('exist') ; ?> </div>
        </div>
      </div>
      <?php 	
				}
	 ?>
     	<?php 
			if(isset($config['api_id'])){
		?>           
      <div class="row">
        <div class="col-lg-6">
          <form role="form" method="post" action="<?php echo base_url('apiconfig/addUpdate'); ?>">
            <input type="hidden" name="edit_id" id="edit_id" value="<?php echo isset($config['id'])?$config['id']:''; ?>" />
            <div class="form-group">
              <label>Service Provider</label>
              <select name="service_provider" id="service_provider" class="form-control" onchange="getApiconfig(this.value)" <?php if($config['service_provider']){ echo ' disabled="disabled"' ; } ?> >
                <option value=""> -- Select -- </option>
                <option <?php if(isset($config['service_provider'])){ if($config['service_provider'] == 'Facebook'){ echo 'selected="selected"' ; }} ?>  >Facebook</option>
                <option  <?php if(isset($config['service_provider'])){ if($config['service_provider'] == 'Twitter'){ echo 'selected="selected"'   ; }} ?>>Twitter</option>
              </select>
              <span id="provider-error"></span> </div>
            <div class="form-group">
              <label>Api ID</label>
              <input  class="form-control" name="apiid" id="apiid" required="required" value="<?php echo isset($config['api_id'])?$config['api_id']:''; ?>" >
            </div>
            <div class="form-group">
              <label>App Secret</label>
              <input class="form-control" name="app_secreat" id="app_secreat" required="required" value="<?php echo isset($config['app_secret'])?$config['app_secret']:''; ?>" >
            </div>
             <?php if($config['service_provider'] != 'Twitter'){ ?>
            <div class="form-group">
              <label>Redirect URL</label>
              <input class="form-control" name="redirect_url" id="redirect_url"  value="<?php echo isset($config['redirect_url'])?$config['redirect_url']:''; ?>" >
            </div>
          
            <div class="form-group">
              <label>Permission</label>
              <input class="form-control" name="permission" id="permission" value="<?php echo isset($config['permissions'])?$config['permissions']:''; ?>" >
            </div>
              <?php } ?>
             <?php if(!isset($config['edit_id'])){?>
              <input type="submit" name="submit" class="btn btn-default btn-primary" value="Update"  />
             <?php } else { ?>
            <input type="submit" name="submit" class="btn btn-default btn-primary" value="Add"  />
            <button type="reset" class="btn btn-default">Reset Button</button>
            <?php } ?>
          </form>
        </div>
      </div>
      
      <!-- /.row --> 
      	<?php } ?>
      <br />
      <?php if(!isset($config['id'])){?>
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Manage Api Configration</h3>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table id="mytables" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th width="10%"> SNo.</th>
                      <th  width="20%"> Api ID </th>
                      <th  width="15%">Api Secreat</th>
                      <th  width="20%">Permission</th>
                      <th  width="10%">Service Provider</th>
                      <th  width="25%"> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  
                      <?php if (isset($results) && (count($results) > 0 )){
								foreach ($results as $result) { ?>
                                  <tr>
                      <td ><?php echo isset($result->id)?$result->id:'' ?></td>
                      <td><?php echo isset($result->api_id)?$result->api_id:'' ?></td>
                      <td><div class="link" ><?php echo isset($result->app_secret)?$result->app_secret:'' ?></div></td>
                      <td><?php echo isset($result->permissions)?$result->permissions:'' ?></td>
                       <td><?php echo isset($result->service_provider)?$result->service_provider:'' ?></td>
                      <td><a href="<?php echo base_url()?>apiconfig/addUpdate/<?php echo $result->id; ?>">[ Edit ] </a> &nbsp;&nbsp;<a onclick="return confirm('Are you sure you want to delete ? ')" href="<?php echo base_url()?>apiconfig/deleteAppconfig/<?php echo $result->id; ?>">[ Delete ] </a>
                    		</tr>
                    <?php } ?>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
<script type="text/javascript">
function getApiconfig(service_prov)
{
	$("#provider-error").html(""); 
	 $.ajax({
						url: '<?php echo base_url('apiconfig/getApiconfig');?>',
						type: 'post',
						data : {service_prov: service_prov},
						success: function(json) {
								var sresult_obj = JSON.parse(json);
								if(sresult_obj){
									if(sresult_obj['service_provider'] == service_prov)
									{
										$("#provider-error").html("<font color='red'>Service prvider already exist ! You can update it </font>"); 
										
									}
									
								}
								
						}
	 })
	 
}
</script>