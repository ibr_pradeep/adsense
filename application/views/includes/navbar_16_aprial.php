<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">

            <!-- Brand and toggle get grouped for better mobile display -->
  <?php $type = is_admin(); ?>
            <div class="navbar-header" >

                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>

                <a class="navbar-brand" href="<?php echo base_url()?>index.php/dashboard">Affiliate Web Application Admin</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->session->userdata('userfname');?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <?php if ($type == 1){?>
                        <li>
                            <a href="<?php echo base_url()?>index.php/userlist/profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                      
                        <li>
                            <a href="<?php echo base_url()?>index.php/dashboard/changepwd"><i class="fa fa-fw  fa-gears"></i> Change password</a>
                        </li>
                        <?php } ?>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo base_url()?>index.php/user/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>

            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <div class="collapse navbar-collapse navbar-ex1-collapse">

                <ul class="nav navbar-nav side-nav">
              
                <li class="active">
                <a href="<?php echo base_url()?>index.php/dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
                <?php if($type == 1){?>
                <li>
                <a href="<?php echo base_url()?>index.php/sharelink"><i class="fa fa-fw fa-share-alt"></i>Share link</a>
                </li>
                <?php } ?>
                 <?php if($type == 1){?>
                <li>
                <a href="<?php echo base_url()?>index.php/sharelink/scheduler"><i class="fa fa-fw fa-edit"></i> Scheduler </a>
                </li>
               <?php } ?>
                
                <li>
                 <?php if($type == 1){?>
                <a href="<?php echo base_url()?>index.php/category"><i class="fa fa-fw fa-table"></i>Category</a>
                <?php }else
				{
			?>
            <a href="<?php echo base_url()?>index.php/usercategory"><i class="fa fa-fw fa-table"></i>Category</a>
            <?php 		
				} ?>
                
                </li>
                 <?php if($type ==1){?>
                <li>
                <a href="#"><i class="fa fa-fw fa-dollar"></i>Billing</a>
                </li>
                <li>
                <a href="<?php echo base_url()?>index.php/analyticsreport"><i class="fa fa-fw fa-wrench"></i>Manage adsense</a>
                </li>
                <li>
                <a href="<?php echo base_url()?>index.php/userlist"><i class="fa fa-fw fa-group"></i>User list</a>
                </li>
                
                <?php } ?>
                <li>
                <a href="<?php echo base_url()?>index.php/analyticsreport"><i class="fa fa-fw fa-file-pdf-o"></i> Reporting</a>
                </li>
                 <?php if($type ==1){?>
                <li>
                 <a href="<?php echo base_url()?>index.php/userlist/profile"><i class="fa fa-fw fa-user"></i>Profile page </a>
				 </li>
                 <?php } ?>
                <li>
                  <?php if($type ==1){?>
                <a href="<?php echo base_url()?>index.php/paypaldeails"><i class="fa fa-fw fa-credit-card"></i>Paypal details </a>
                 <?php } else
				{ ?>
                 <a href="<?php echo base_url()?>index.php/paypal"><i class="fa fa-fw fa-credit-card"></i>Paypal details </a>
                 <?php }?>
                </li>
                </ul>

            </div>

            <!-- /.navbar-collapse -->

        </nav>