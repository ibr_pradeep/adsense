<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">



            <!-- Brand and toggle get grouped for better mobile display -->

  <?php 
  $type = is_admin(); 
  $user_session = getCurrentUserSession();
  ?>

            <div class="navbar-header" >



                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">



                    <span class="sr-only">Toggle navigation</span>



                    <span class="icon-bar"></span>



                    <span class="icon-bar"></span>



                    <span class="icon-bar"></span>



                </button>



                <a class="navbar-brand" href="<?php echo base_url('dashboard')?>">ShareNBag</a>

            </div>

            <!-- Top Menu Items -->

            <ul class="nav navbar-right top-nav">
            
              

                <li class="dropdown">

                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
				  <?php  echo 'Welcome '.$user_session['userfname'];
				  $outh_provider = isset($user_session['outh_provider']) ? $user_session['outh_provider'] : '';
				  if($outh_provider == 'facebook'){ 
				  	echo "<img class='socialicon' src='".base_url()."../assets/images/facebook.png'>"; 
				  }if($outh_provider == 'twitter'){ 
				  	echo "<img class='socialicon' src='".base_url()."../assets/images/twitter.png'>";  
				  }  ?>   <b class="caret"></b></a>

                    <ul class="dropdown-menu">

                      <?php if ($type == 1){?>
                      
                   

                        <li>

                            <a href="<?php echo base_url()?>userlist/profile"><i class="fa fa-fw fa-user"></i> Profile</a>

                        </li>

                      

                        <li>

                            <a href="<?php echo base_url()?>dashboard/changepwd"><i class="fa fa-fw  fa-gears"></i> Change password</a>

                        </li>

                        <?php } ?>

                        <li class="divider"></li>

                        <li>

                            <a href="<?php echo base_url()?>user/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>

                        </li>

                    </ul>

                </li>

            </ul>



            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->



            <div class="collapse navbar-collapse navbar-ex1-collapse">



                <ul class="nav navbar-nav side-nav">

              
				<?php if($type == 1){?>
                <li class="<?php echo ($this->uri->segment(2) == 'dashboard') ? 'active' : ''; ?>" >

                <a href="<?php echo base_url()?>dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                </li>
<?php } else { ?>
				<li class="<?php echo ($this->uri->segment(2) == 'dashboard') ? 'active' : ''; ?>">
                  <a href="<?php echo base_url()?>dashboard/showDetails"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
              	</li>
				<li class="<?php echo ($this->uri->segment(2) == 'userlist' && $this->uri->segment(3) == 'profile') ? 'active' : ''; ?>" >
              <a href="<?php echo base_url()?>userlist/profile"><i class="fa fa-fw fa-user"></i> Profile</a>
              <?php } 
?>
                </li>

                <?php if($type == 1){?>

                <li class="<?php echo ($this->uri->segment(2) == 'sharelink') ? 'active' : ''; ?>">

                <a href="<?php echo base_url()?>sharelink"><i class="fa fa-fw fa-share-alt"></i>Share link</a>

                </li>

                <?php } ?>

                

                <li style="display:none;">

                <a href="<?php echo base_url()?>sharelink/scheduler" style="display:none"><i class="fa fa-fw fa-edit"></i> Scheduler </a>

                </li>

             

                

                <li class="<?php echo ($this->uri->segment(2) == 'category') ? 'active' : ''; ?>">

                 <?php if($type == 1){?>

                <a href="<?php echo base_url()?>category"><i class="fa fa-fw fa-table"></i>Category</a>

                <?php }?>

                

                </li>

                 <?php if($type ==1){?>

                <li class="<?php echo ($this->uri->segment(2) == 'billing') ? 'active' : ''; ?>">

                <a href="<?= base_url(); ?>billing"><i class="fa fa-fw fa-dollar"></i>Billing</a>

                </li>

                <li class="<?php echo ($this->uri->segment(2) == 'manageads') ? 'active' : ''; ?>">

                <a href="<?php echo base_url()?>manageads"><i class="fa fa-fw fa-wrench"></i>Manage adsense</a>

                </li>

                <li class="<?php echo ($this->uri->segment(2) == 'userlist' && $this->uri->segment(3) == '') ? 'active' : ''; ?>">

                <a href="<?php echo base_url()?>userlist"><i class="fa fa-fw fa-group"></i>User list</a>

                </li>

                

                <?php } ?>

                 <?php if($type ==1){?>

                <li class="<?php echo ($this->uri->segment(2) == 'analyticsreport') ? 'active' : ''; ?>">

                <a href="<?php echo base_url()?>analyticsreport"><i class="fa fa-fw fa-file-pdf-o"></i> Reporting</a>

                </li>

                  <?php } ?>

                 <?php if($type ==1){?>

                <li class="<?php echo ($this->uri->segment(2) == 'userlist' && $this->uri->segment(3) == 'profile') ? 'active' : ''; ?>">

                 <a href="<?php echo base_url()?>userlist/profile"><i class="fa fa-fw fa-user"></i>Profile page </a>

				 </li>

                 <?php } ?>

                <li class="<?php echo ($this->uri->segment(2) == 'paypaldeails') ? 'active' : ''; ?>">

                  <?php if($type ==1){?>

                <a href="<?php echo base_url()?>paypaldeails"><i class="fa fa-fw fa-credit-card"></i>Paypal/account details</a>
	</li>
    			
                 <?php } else

				{ ?>
				
				<!-- <li class="<?php echo ($this->uri->segment(2) == 'earnings') ? 'active' : ''; ?>">
                 <a href="<?php echo base_url('earnings')?>" ><i class="fa fa-fw fa-file-pdf-o"></i> Earnings</a>
                 </li>-->
				<li class="<?php echo ($this->uri->segment(2) == 'user' && $this->uri->segment(3) == 'report') ? 'active' : ''; ?>">
                 <a href="<?php echo base_url('user/report')?>"><i class="fa fa-fw fa-credit-card"></i> Reports  </a>
				</li>
                <li class="<?php echo ($this->uri->segment(2) == 'payment') ? 'active' : ''; ?>">
                 <a href="<?php echo base_url('payment')?>"><i class="fa fa-fw fa-credit-card"></i> Payment History  </a>
				</li>
				<!--<li class="<?php echo ($this->uri->segment(2) == 'category') ? 'active' : ''; ?>">
                 <a href="<?php echo base_url()?>category"><i class="fa fa-fw fa-credit-card"></i>Select Category</a>
				</li>-->
                 <?php }?>

                </li>

                 <?php if($type ==1){?>

                <li class="<?php echo ($this->uri->segment(2) == 'apiconfig') ? 'active' : ''; ?>">

                 <a href="<?php echo base_url()?>apiconfig"><i class="fa fa-fw fa-user"></i>Api Config</a>

				 </li>

                 <?php } ?>

                </ul>



            </div>



            <!-- /.navbar-collapse -->



        </nav>