<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');
}



?>

    <div id="wrapper">

        <!-- Navigation -->
        
	<?php $this->load->view('includes/navbar');?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Change Password
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Change Password
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<?php if ($this->session->flashdata('sucess')|| $this->session->flashdata('error')){?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i> 
							<?php echo $this->session->flashdata('sucess') ; 
								  echo $this->session->flashdata('error');  
							?> 
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" method="post" action="<?php echo base_url(); ?>dashboard/changepwd">
							<input type="hidden" name="category_id" id="category_id" />
                             <div class="form-group">
                                <label>Old password</label>
                                <input type="password" class="form-control" name="oldpwd" id="oldpwd" required="required"  >
                               
                            </div>
				 			<div class="form-group">
                                <label>New Password</label>
                                <input type="password" class="form-control" name="newpwd" id="newpwd" required="required" oninput="form.confpwd.pattern = escapeRegExp(this.value)" >
                               
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control" name="confpwd" id="confpwd" required="required"  >
                               
                            </div>
                           <input type="submit" name="submit" class="btn btn-default" value="Submit"  />
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>

                    </div>
                    
                </div>
                <!-- /.row -->
               
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    

<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/footer');

}

?>
<script>
    function escapeRegExp(str) {
      return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }
</script>