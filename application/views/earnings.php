<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest'){
	$this->load->view('includes/header');
}
?>
<div id="wrapper">  
  <!-- Navigation -->  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid">      
      <!-- Page Heading -->      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Earning</h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Earning</li>
          </ol>
        </div>
      </div>     
     <!-- /.row -->      
      <div class="row">
        <div class="col-lg-10">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Earning</h3>
            </div>
              <div class="row-fluid">
           		
                 <a class="btn btn-lg btn-link" href="<?php base_url(); ?>earnings?req=daily">Today</a>
               
                 <a class="btn btn-lg btn-link" href="<?php base_url(); ?>earnings?req=month">Current month</a>
                
               
                 <a class="btn btn-lg btn-link" href="<?php base_url(); ?>earnings?req=last_month">Last Month</a>
               
                 </div>
               
         
           
            <div class="panel-body">
              <div class="table-responsive">
              
                <table id="myanalytics" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th> #</th>
                      <th>Month</th>
                      <th>Amount </th>
                      <th>Last Paid Date </th>
                     
                    </tr>
                  </thead>
                  <tbody>
                      
                  <?php
                  if(!empty($user_billings)){ $i = 1;
					foreach($user_billings as $user_billing){
						if(!empty($user_billing)){
							foreach($user_billing as $user_billing_){
								if(!empty($user_billing_)){
								echo '<tr>
									  <td>'.$i.'</td>
									  <td>'.$user_billing_['month'].'</td>
									  <td>'.$user_billing_['payment_amount'].'</td>
									  <td></td>
									</tr>';
									 $i++;
								}
							}
						}                     
					}
				  }
				  ?>                       
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
<script>
$('#date_filter_start').datetimepicker({
	//yearOffset:222,
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#date_filter_end').datetimepicker({
	//yearOffset:222,
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
</script>
<script type="text/javascript">
function getdatefileter(filter)
{
	window.location = "<?php echo base_url()?>analyticsreport?filter="+filter; 	
}
</script>