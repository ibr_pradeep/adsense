<?php 
if(strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest'){
	$this->load->view('includes/header');
}
?>


<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Payment history </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Payment history </li>
          </ol>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Payment history</h3>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table width="100%" id="mytables" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th width="5%"> #</th>
                      <th  width="15%">Payment Date</th>
                      <th width="15%">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php
                    if(!empty($rows)){
						$i = 1;
						foreach($rows as $row){
							echo '<tr>                     
									  <td>'.$i.'</td>
									  <td>'.date('dM Y h:i a', strtotime($row->transection_date)).'</td>
									  <td>'.$row->amount.'</td>
								   </tr>';
							$i++;
						}
					}
					?>                                       
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest'){
	$this->load->view('includes/footer');
}
?>