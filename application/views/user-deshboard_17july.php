<?php
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}

?>

<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
		  <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
       
      </div>
      <?php
	 // echo"<pre>";
//print_r($user_billings); die;
$TodayErning 	= isset($user_billings['today'][0]['earning']) ? $user_billings['today'][0]['earning'] : 0;
$currentMonth 	= isset($user_billings['currentMonth'][0]['earning']) ? $user_billings['currentMonth'][0]['earning'] : 0;
$lastMonth 		= isset($user_billings['lastMonth'][0]['earning']) ? $user_billings['lastMonth'][0]['earning'] : 0;
$TodayErning = number_format($TodayErning, 2, '.', ',');
$currentMonth = number_format($currentMonth, 2, '.', ',');
$lastMonth = number_format($lastMonth, 2, '.', ',');
	  ?>
<div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-money fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $TodayErning;?></div>
                                    <div></div>
                                </div>
                            </div>
                        </div>
                       <!-- <a href="#">-->
                            <div class="panel-footer">
                                <span class="pull-left">Today Earning!</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                      <!--  </a>-->
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-money fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $currentMonth;?></div>
                                    <div></div>
                                </div>
                            </div>
                        </div>
                       <!-- <a href="#">-->
                            <div class="panel-footer">
                               <span class="pull-left">Current Month Earning!</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                       <!-- </a>-->
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-money fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><?php echo $lastMonth;?></div>
                                    <div></div>
                                </div>
                            </div>
                        </div>
                      <!--  <a href="#">-->
                            <div class="panel-footer">
                                <span class="pull-left">Last Month Earning!</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                       <!-- </a>-->
                    </div>
                </div>
                
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">Category</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        
        <div class="row">

        <div class="col-lg-6">

          <?php $categoty_ids = '';  if(isset($usercategories->category_id)){ $categoty_ids = explode(';',$usercategories->category_id ); } ?>

          <form role="form" method="post" action="<?php echo base_url(); ?>dashboard/addUpdate">

            <input type="hidden" name="edit_id" id="edit_id" value="<?php echo isset($usercategories->user_category_id)?$usercategories->user_category_id:''; ?>" />

            <div class="form-group">

              <label>Select Categories</label>

              <select  class="form-control chzn-select"  name="category_id[]" id="category_id" multiple="multiple">

                <option value=""> --Select--</option>

                <?php 

									if(sizeof($categories)>0){

										foreach($categories as $category){

											if(in_array($category->category_id , $categoty_ids)){

												echo '<option value="'.$category->category_id.'" selected >'.$category->title.'</option>'; 	

											}else

											{

											echo '<option value="'.$category->category_id.'" >'.$category->title.'</option>'; 	

											}

										}	

									}

									?>

              </select>

            </div>

            <?php if(isset($usercategories->user_category_id)){

	?>

            <input type="submit" name="submit" class="btn btn-default" value="Update"  />

            <?php 

	}else

	{?>

            <input type="submit" name="submit" class="btn btn-default" value="Add"  />

            <?php }?>

          </form>

        </div>

      </div>

      
            </div>
            
    
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/footer');
}
?>
