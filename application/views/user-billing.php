<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest'){
	$this->load->view('includes/header');
}

$user_session = getCurrentUserSession();
?>
<script>
$(document).ready(function() {
    $('#myanalytics').DataTable();
} );
</script>
<div id="wrapper">  
  <!-- Navigation -->  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid">      
      <!-- Page Heading -->      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"><?php  echo ($user_session['usertype'] == 1) ? 'Billing Details' : 'Report'; ?></h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> <?php  echo ($user_session['usertype'] == 1) ? 'Billing Details' : 'Report'; ?></li>
          </ol>
        </div>
      </div>    
      <?php if ($this->session->flashdata('error') ){?>
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
           <i class="fa fa-info-circle"></i> <?= $this->session->flashdata('error') ; ?> 
          </div>
        </div>
      </div>
      <?php } ?>  
      <!-- /.row -->      
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title"><?php  echo ($user_session['usertype'] == 1) ? 'Billing Details' : 'Report'; ?></h3>
            </div>
        
            <div class="panel-body">
              <div class="table-responsive">
                <div class="form-group " id="Scheduler_Time_hide">
                  <div class="col-lg-8 row-no-padding "   >
                  <form method="post" action="<?php echo ($user_session['usertype'] == 1) ? base_url('billing') : base_url('user/report'); ?>">
                    <div class="row-fluid">
                		<label>Search: </label>
                       <!-- <select name="user" id="user">
                        <option></option>
                        
                        </select>-->
                       <?php if($user_session['usertype'] == 1){ ?>
                       <div class="span12"> 
                       	<div class="span2">
                          <label>Months</label>
                          <select name="month" class="span12">
                          <?php
                          foreach (months() as $m) {
							  $selected = (str_replace(' ', '', strtolower($m)) == str_replace(' ', '', strtolower($month))) ? 'selected="selected"' : '';
                            echo '<option value="'.str_replace(' ', '', $m).'" '.$selected.'>'.$m.'</option>';
                          }
                          ?>
                          </select>
                        </div>
                        <div class="span2">
                          <label>Years</label>
                          <select name="year" class="span12">
                              <?php
                              for($i=2014;$i<=date('Y');$i++){
								  $selected = ($i == $year) ? 'selected="selected"' : '';
                                  echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
                              }
                              ?>
                          </select>
                        </div>
                        <div class="span2">
                          <label>&nbsp;</label>
                          <input type="submit" name="search" id="search"  value="Search" class="btn btn-primary" style="margin-top: 22px;margin-bottom: 11px;" />
                        </div>
                       </div>
                       <?php } else{?>
                       <div class="span12">
                        <input type="text" class="form-control span3 " name="date_start" id="date_filter_start" placeholder="Start Date" value="<?php echo isset($start_date) ? $start_date : ''; ?>">
                        <input type="text" class="form-control span3 " name="date_end" id="date_filter_end" placeholder="End Date" value="<?php echo isset($end_date) ? $end_date : ''; ?>">
                        <input type="submit" name="search" id="search" value="Search" class="form-control span3 btn btn-sm btn-primary">
                        </div>
                        <?php } ?>
                   </div>
                    </form>
                  </div>
                  <br/>
                  <br/>
                  <br/>
                </div>
               
                <table id="myanalytics" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th> #</th>
                      <th>Username </th>                     
                      <th>Share Link </th> 
                      <th>Paypal email</th>
                      <th>Total Payment Amount </th>                                    
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if(!empty($analytics)){
				  	foreach ($analytics as $service_acc => $analytics_) {
						if(!empty($analytics_)){
							$accountname = analyticAccountName($service_acc);
							$i=1;
							foreach($analytics_ as $key => $analytic){
								if(!empty($analytic)){	
									if(getUsernamebyRef($key)){	
										foreach($analytic as $val){
									  		if($val){
												$ref = explode('?ref=',$val['share_url']); 
												if(isset($ref[0])){	
												$rr = isset($ref[1]) ? str_replace('/', '', $ref[1]) : '';	
												  ?>
												  <tr>
													<td><?php echo $i; ?></td>
													<td><?= getUsernamebyRef($key); ?></td>
													<td><?php echo $val['share_url']; ?> </td>
												   
													<td><?= getUserPaypalEmail($user_session['userID']) ?></td>                       
													<td>
													$ <?php
													$percent_cut =  getUserpercentcut( $key );
													$revenue = getPerCut($percent_cut, $val['AdSense_Revenue']);
													echo number_format($revenue, 2, '.', ',')
													?>
													</td>
												  </tr>
												  <?php
												  $i++;
												}
											}
										}
									}
								}
							}
						}
					}
				  }
				  ?>                       
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
<script>
$('#date_filter_start').datetimepicker({
	//yearOffset:222,
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#date_filter_end').datetimepicker({
	//yearOffset:222,
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
</script>
<script type="text/javascript">
function getdatefileter(filter)
{
	window.location = "<?php echo base_url()?>analyticsreport?filter="+filter; 	
}
</script>