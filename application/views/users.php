<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{
	$this->load->view('includes/header');
}



?>

    <div id="wrapper">

        <!-- Navigation -->
        
	<?php $this->load->view('includes/navbar');?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Category
                        </h1>
                        <ol class="breadcrumb">
                            <li class="active">
                                <i class="fa fa-dashboard"></i> Category
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->
				<?php if ($this->session->flashdata('insert') || $this->session->flashdata('delete') || $this->session->flashdata('update') ){?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-info alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <i class="fa fa-info-circle"></i> 
							<?php echo $this->session->flashdata('insert') ; 
								  echo $this->session->flashdata('delete');  
								  echo $this->session->flashdata('update'); 
							?> 
                        </div>
                    </div>
                </div>
                <?php } ?>
                <div class="row">
                    <div class="col-lg-6">

                        <form role="form" method="post" action="<?php echo base_url(); ?>category/addUpdate">
							<input type="hidden" name="category_id" id="category_id" value="<?php echo isset($category['category_id'])?$category['category_id']:''; ?>" />
                             <div class="form-group">
                                <label>Title</label>
                                <input class="form-control" name="title" id="title" required="required" value="<?php echo isset($category['title'])?$category['title']:''; ?>" >
                               
                            </div>

                           <input type="submit" name="submit" class="btn btn-default" value="Submit"  />
                            <button type="reset" class="btn btn-default">Reset Button</button>

                        </form>
                        
                        <a href="https://graph.facebook.com/oauth/authorize?client_id=823228824429542&redirect_uri=<?php echo base_url('login');?>&scope=user_photos,email,user_birthday,user_hometown" class="facebook"></a>
 			
            
            	<a href="?login&oauth_provider=twitter"><img src="<?php echo base_url();?>assets/images/tw_login.png"></a>&nbsp;&nbsp;&nbsp;
    			<a href="?login&oauth_provider=facebook"><img src="<?php echo base_url();?>assets/images/fb_login.png"></a> <br />
                    </div>
                    
                </div>
                <!-- /.row -->
                <br />
 				<?php if(!isset($category['category_id'])){?>
              	<div class="row">
                    
                    
                    <div class="col-lg-10">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Manage Category</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table id="category" class="table table-bordered table-hover table-striped">
                                        <thead>
                                            <tr>
                                                <th> #</th>
                                                <th> Title </th>
                                                <th>Create date</th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                            	<?php if (isset($results) && (count($results) > 0 )){
														foreach ($results as $result) { ?>
                                                        <tr>
															<td><?php echo isset($result->category_id)?$result->category_id:'' ?></td>
															<td><?php echo isset($result->title)?$result->title:'' ?></td>
															<td><?php echo isset($result->create_date)?$result->create_date:'' ?></td>
															<td><a href="<?php echo base_url()?>category/addUpdate/<?php echo $result->category_id; ?>">[ Edit ] </a>
																&nbsp;&nbsp;<a onclick="return confirm('Are you sure you want to delete ? ')" href="<?php echo base_url()?>category/deleteCategory/<?php echo $result->category_id; ?>">[ Delete ] </a>
                                                    	</tr>
														<?php } ?>
													<?php } ?>
                                            
                                          </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                 <?php } ?>   
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    

<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/footer');

}

?>