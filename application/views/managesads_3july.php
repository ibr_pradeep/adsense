<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')
{

	$this->load->view('includes/header');

}

?>


<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Manage Adsanse  </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Manage Adsanse </li>
          </ol>
        </div>
      </div>
      
      <!-- /.row -->
      
      <?php if ($this->session->flashdata('insert') || $this->session->flashdata('delete') || $this->session->flashdata('update') ){?>
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('insert') ; 

								  echo $this->session->flashdata('delete');  

								  echo $this->session->flashdata('update'); 

							?> </div>
        </div>
      </div>
      <?php } ?>
    
      <div class="row">
        <div class="col-lg-6">
          <form role="form" enctype="multipart/form-data" method="post" action="<?php echo base_url(); ?>index.php/manageads/addUpdate">
            <input type="hidden" name="edit_id" id="edit_id" value="<?php echo isset($account['analytics_detail_id'])?$account['analytics_detail_id']:''; ?>" />
             <div class="form-group">
              <label>Service Account Email</label>
              <textarea name="accountemail" id="accountemail"  class="form-control" ><?php echo isset($account['service_email'])?$account['service_email']:''; ?></textarea>
            </div>
            <div class="form-group">
             <label>Upload File [ client_secrets.p12 ]</label>
              <input type="file" name="file" id="file"  class="form-control"  /><span><?php echo isset($account['key_filename'])?$account['key_filename']:''; ?></span>
             
            </div>
              <div class="form-group" style="display:none">
             <label>Status</label>
             
             	<input type="radio" name="status" <?php if(isset($account['status'])){ if($account['status'] == 0 ){ echo 'checked="checked"' ; }}else { echo 'checked="checked"' ; } ?> value="0"  />Active
             	<input type="radio" name="status" value="1" <?php if(isset($account['status'])){ if($account['status'] == 1 ){ echo 'checked="checked"' ; }} ?> />Deactive
            </div>
            <div class="form-group">
              <input type="submit" name="submit" class="btn btn-default btn-primary" value="Submit"  />
              <button type="reset" class="btn btn-default">Reset Button</button>
            </div>
          </form>
        </div>
      </div>
      
      <!-- /.row --> 
      
      <br />
    	<?php if(empty($account['analytics_detail_id'])){?>
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Manage Share Link</h3>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table width="100%" id="mytables" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th width="5%"> #</th>
                      <th  width="15%"> Create Date</th>
                      <th  width="15%"> Service Email </th>
                      <th  width="10%"> Status </th>
                      <th  width="30%"> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <?php 
					  $i = 1;
					  if (isset($results) && (count($results) > 0 )){

														foreach ($results as $result) { ?>
                      <td width="10%"><?php echo $i ?></td>
                      <td width="20%"><?php echo isset($result->created_date)?$result->created_date:'' ?></td>
                      <td width="20%"><div class="link" ><?php echo isset($result->service_email)?$result->service_email:'' ?></div></td>
                      <td width="20%"><?php echo checkStatus($result->status) ?></td>
                    
                      <td width="20%"><a href="<?php echo base_url()?>index.php/manageads/addUpdate/<?php echo $result->analytics_detail_id; ?>">[Edit] </a> &nbsp;&nbsp;<a onclick="return confirm('Are you sure you want to delete ? ')" href="<?php echo base_url()?>index.php/manageads/deleteaccount/<?php echo $result->analytics_detail_id; ?>">[Delete] </a>
                    </tr>
                    <?php 
					$i++;} ?>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
     	<?php } ?>
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
 
 
