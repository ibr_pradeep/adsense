<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest'){
	$this->load->view('includes/header');
}
?>
<div id="wrapper">  
  <!-- Navigation -->  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid">      
      <!-- Page Heading -->      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header">Billing Details</h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Billing Details</li>
          </ol>
        </div>
      </div>      
      <!-- /.row -->      
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Billing Details</h3>
            </div>
        
            <div class="panel-body">
              <div class="table-responsive">
                <div class="form-group " id="Scheduler_Time_hide">
                  <div class="col-lg-8 row-no-padding "   >
                  <form method="post" action="<?php echo base_url('billing')?>">
                    <div class="row-fluid">
                		<label>Search: </label>
                       <!-- <select name="user" id="user">
                        <option></option>
                        
                        </select>-->
                       <div class="span12">
                        <input type="text"  class="form-control span3 " name="date_filter_start" id="date_filter_start" placeholder="Start Date" value="<?= isset($start_date) ? $start_date : ''?>">
                        <input  type="text" class="form-control span3 " name="date_filter_end" id="date_filter_end" placeholder="End Date" value="<?= isset($end_date) ? $end_date : ''?>">
                        <input type="submit" name="search" id="search"  value="Search" class="btn btn-primary form-control span3" />
                       </div>
                   </div>
                    </form>
                  </div>
                  <br/>
                  <br/>
                  <br/>
                </div>
               
                <table id="myanalytics" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th> #</th>
                      <th>Username </th>
                       <th>Share Link </th>
                        <?php if($this->session->userdata('usertype') == 1){?>
                      <th>Referral ID </th>
                      <?php } ?>
                      <th>Paypal email</th>
                      <?php if($this->session->userdata('usertype') == 1){?>
                      <th>% cut</th>
                      
                      <th>Earning</th>
                      <?php } ?>
                      <th>Total Payment Amount </th>
                      <th>Paid Amount </th>
                      <th>Unpaid Amount </th>
                      <th>Last Paid Date </th>
                        <?php if($this->session->userdata('usertype') == 1){?>
                       <th>Action </th>
                         <?php } ?>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  if(!empty($user_billings)){ $i = 1;
					foreach($user_billings as $user_billing){
						  $user_id = getUserIdbyRef($user_billing['referal_id']); 
					  ?>
                      <tr>
                        <td><?php echo $i; ?></td>
                        <td><?= $user_billing['username']; ?></td>
                           <td><?php echo isset($user_billing['share_url'])?$user_billing['share_url']:'' ?> </td>
                          <?php if($this->session->userdata('usertype') == 1){?>
                        <td><?= $user_billing['referal_id']; ?></td>
                        <?php } ?>
                        <td><?= $user_billing['paypal_email']; ?></td>
                         <?php if($this->session->userdata('usertype') == 1){?>
                        <td><?= $user_billing['per_cut']; ?></td>
                       <td><?= $user_billing['earning']; ?></td>
                         <?php } ?>
                        <td><?= $user_billing['payment_amount']; ?></td>
                        <?php $paid = getPaymentDetails(  $user_id , $user_billing['referal_id']  ); ?>
                        
                        <td><?php echo getPaidAmount(  $user_id ,$user_billing['payment_amount'] ); ?></td>
                        <?php $unpaidamt =  getUnPaidAmount(  $user_id ,$user_billing['payment_amount'] ); ?>
                        <td><?php echo $unpaidamt; ?></td>
                        <td> <?php if($paid){ echo $paid->transection_date; } ?></td>
                         <?php if($this->session->userdata('usertype') == 1){?>
                        <td>
                         <?php if(!empty($unpaidamt)){
							  ?>
						<form method="post" action="<?PHP echo site_url(); ?>/paypal">
                        	
                        <input type="hidden" name="user_id" value="<?php echo $user_id ; ?>"  />
                        <input type="hidden" name="ref_id" value="<?php echo $user_billing['referal_id'] ; ?>"  />
                         <input type="hidden" name="amount" value="<?php echo $unpaidamt; ?>"  />
                          <input class="btn btn-success" type="submit" name="pay" value="Pay" /></form>
                           <?php 
							}else if(!isset($paid)){ ?>
						<form method="post" action="<?PHP echo site_url(); ?>/paypal">
                        	
                        <input type="hidden" name="user_id" value="<?php echo $user_id ; ?>"  />
                        <input type="hidden" name="ref_id" value="<?php echo $user_billing['referal_id'] ; ?>"  />
                         <input type="hidden" name="amount" value="<?php echo $user_billing['payment_amount']; ?>"  />
                          <input class="btn btn-success" type="submit" name="pay" value="Pay" <?php if($user_billing['paypal_email']){ echo 'disabled="disabled"';  }?>  /></form>
                           <?php } else {
							   ?>
                               <input class="btn btn-danger" disabled="disabled" type="submit" name="pay" value="Paid" />
                               <?php
							  }?>
                              </td>
                               <?php
							  }?>
                      </tr>
                      <?php
                      $i++;
					}
				  }
				  ?>                       
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
<script>
$('#date_filter_start').datetimepicker({
	//yearOffset:222,
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#date_filter_end').datetimepicker({
	//yearOffset:222,
	lang:'en',
	timepicker:false,
	format:'Y-m-d',
	formatDate:'Y-m-d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
</script>
<script type="text/javascript">
function getdatefileter(filter)
{
	window.location = "<?php echo base_url()?>index.php/analyticsreport?filter="+filter; 	
}
</script>