<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}







?>

<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Manage Users </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Manage Users </li>
          </ol>
        </div>
      </div>
      
      <!-- /.row -->
      
      <?php if ($this->session->flashdata('message')) {?>
      <div class="row">
        <div class="col-lg-12">
          <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <i class="fa fa-info-circle"></i> <?php echo $this->session->flashdata('message') ; ?> </div>
        </div>
      </div>
      <?php } ?>
      
      <!-- /.row -->
     
      <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Manage Category</h3>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table id="mytables" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                   		
                      <th> id</th>
                      <th> Name </th>
                      <th>Email</th>
                      <th>Social Platform</th>
                      <th> % cut</th>
                       <th>Category</th>
                      <th>Status</th>
                      <th> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  
                    <tr>
                      <?php if (isset($results) && (count($results) > 0 )){

														foreach ($results as $result) { ?>
                   
               
                      <td><?php echo isset($result->id)?$result->id:'' ?></td>
                      <td><?php echo $result->firstname.' '.$result->middlename.' '.$result->lastname  ?></td>
                      <td><?php echo isset($result->email)?$result->email:'' ?></td>
                      <td><?php echo isset($result->outh_provider) ? $result->outh_provider : ''; ?></td>
                      <td>
                       <div class="row-fluid">
                      <form method="post" action="<?php echo base_url('userlist/savePercentage'); ?>" >
                      <input  type="hidden" name="userid" value="<?php echo isset($result->id)?$result->id:'' ?>" />
                    <div class="span11">  <input required="required" class="span6 margin-left-cus" type="text" name="percentage" maxlength="3" value="<?php echo isset($result->percent_cut)?$result->percent_cut:'' ?>" />
                   
                    
                    <input class="btn btn-sm btn-warning" type="submit" name="save" value=" <?php if(isset($result->percent_cut)){ echo "Update" ; }else { echo "Save";  } ?>" /> </div></form></div></td>
                      <td><?= isset($result->category) ? $result->category : ''; ?></td>
                      <td>
					  	<?php 
						if (isset($result->active)){ 
						  if ($result->active == 0){ 
						  	echo "<span  id='active'>Active</span>"; 
						  }else{ 
						  	echo "<span  id='deactive'>Blocked</span>"; 
						  }  
						} ?>
                      </td>                     
                      <td>
                      <a href="javascript:void(0);" class="block_unblock" data-id="<?= $result->id; ?>" data-status="<?= isset($result->active) ? $result->active : ''; ?>">
                      <?php 
						if (isset($result->active)){ 
						  if ($result->active == 0){ 
						  	echo "[ Block ]"; 
						  }else{ 
						  	echo "[ Unblock ]"; 
						  }  
						} ?>
                        </a>
                     </td>                   
                    </tr>
                    <?php } ?>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<script>
$(document).ready(function(e) {
    $('.block_unblock').click(function(e) {
		var elem = $(this);
    	var user_id = $(this).attr('data-id');
		var status = $(this).attr('data-status');
		status = parseInt(status);		
		var alert_text = (status) ? 'unblock' : 'block';
		var n_status = (status) ? 0 : 1;
		var html_status = (status) ? 'Active' : 'Blocked';
		var status_id = (status) ? 'active' : 'deactive';
		var allow = confirm('Are you sure you want to '+alert_text+' this user ?');
		if(allow){
			$.ajax({
				url: '<?= base_url('userlist/blockUnblockUser'); ?>',
				type: 'post',
				data: {user_id:user_id,status:n_status},
				success:function(response){
					if(response){						
						elem.parent().prev().html('<span id="'+status_id+'">'+html_status+'</span>');	
						if(n_status){
							elem.text('[ Unblock ]');	
							elem.attr('data-status', 1)	
						}else{
							elem.text('[ Block ]');	
							elem.attr('data-status', 0);
						}
					}
				},
				error: function (error) {
                  alert('error; ' + eval(error));
              	}
			});	
		}
    });
});
</script>
<?php 
if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest'){
	$this->load->view('includes/footer');
}
?>
