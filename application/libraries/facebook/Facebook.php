<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if ( session_status() == PHP_SESSION_NONE ) {
    session_start();
}
 
require_once( APPPATH . 'libraries/facebook/Facebook/GraphObject.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/GraphSessionInfo.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookSession.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookCurl.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookHttpable.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookCurlHttpClient.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookResponse.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookSDKException.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookRequestException.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookAuthorizationException.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookRequest.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookRedirectLoginHelper.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/GraphUser.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/GraphObject.php' );
//echo  "<br>".APPPATH . 'libraries/facebook/Profile.php'  ; die('mypath');
require_once( APPPATH . 'libraries/facebook/Profile.php' );

use Facebook\GraphSessionInfo;
use Facebook\FacebookSession;
use Facebook\FacebookCurl;
use Facebook\FacebookHttpable;
use Facebook\FacebookCurlHttpClient;
use Facebook\FacebookResponse;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\GraphObject;
use Facebook\GraphUser;

class Facebook {
    var $ci;
    var $helper;
    var $session;
 var $redirect_url = '';
 var $permission = '';
 
    public function __construct() {
		
		
		
        $this->ci =& get_instance();
		
		// fetch facbook api credential details from api_configration table 
  		$query_fbconfig = $this->ci->db->get_where('api_configration' , array('is_deleted'=>0,'service_provider'=>'Facebook' )); 
		$row_config = $query_fbconfig->row_array();
		
		$api_id = $row_config['api_id'];
		
		$app_secret = $row_config['app_secret'];
		$permission = $row_config['permissions'];
		$redirect_url = $row_config['redirect_url'];
$this->redirect_url = $row_config['redirect_url'];
$this->permission = $permission;
//print_r($this->permission);
        FacebookSession::setDefaultApplication($api_id , $app_secret );
        $this->helper = new FacebookRedirectLoginHelper( $redirect_url );
 
        if ( $this->ci->session->userdata('fb_token') ) {
            $this->session = new FacebookSession( $this->ci->session->userdata('fb_token') );
 
            // Validate the access_token to make sure it's still valid
            try {
                if ( ! $this->session->validate() ) {
                  $this->session = false;
                }
            } catch ( Exception $e ) {
                // Catch any exceptions
                $this->session = false;
            }
        } else {
            try {
                $this->session = $this->helper->getSessionFromRedirect();
            } catch(FacebookRequestException $ex) {
                // When Facebook returns an error
            } catch(\Exception $ex) {
                // When validation fails or other local issues
            }
        }
 
        if ( $this->session ) {
            $this->ci->session->set_userdata( 'fb_token', $this->session->getToken() );
 
            $this->session = new FacebookSession( $this->session->getToken() );
        }
    }
 
    public function get_login_url() {
    //echo "per ".$this->permission ;
        return $this->helper->getLoginUrl( $this->permission  );
    }
 
    public function get_logout_url() {
        if ( $this->session ) {
            return $this->helper->getLogoutUrl( $this->session, site_url( 'logout' ) );
        }
        return false;
    }
 
    public function get_user($token ='') {
        if ( $token ) {
            try {
                $request1 = (new FacebookRequest( $token, 'GET', '/me' ));
				$request = $request1 ->execute();
                $user = $request->getGraphObject()->asArray();
 
                return $user;
 
            } catch(FacebookRequestException $e) {
                return false;
 
            }
        }
    }
	
	
	public function getAccessToken()
    {
//print_r($this->redirect_url);
        $helper = new FacebookRedirectLoginHelper($this->redirect_url);
   		
        $session = $helper->getSessionFromRedirect_1();
	
		//print_r($session); 
        if ($session) {
			//$aa = $session->getAccessToken();
			// print_r($aa);
            return (string) $session ;//$session->getAccessToken();
        }
        return null;
    }
	
	/**
     * Check the access token
     *
     * @param string $token
     * @return bool
     */
    public function checkAccess($token)
    {
		
        $this->session = new FacebookSession($token);
		try {
			
            return $this->session->validate();
        } catch(FacebookSDKException $ex) {
            return false;
        }
        return false;
    }
	
	/**
     * Send request against facebook graph
     *
     * @param string $method
     * @param string $path
     * @param array|null $params
     *
     * @return \Facebook\GraphObject
     *
     * @throws \Facebook\FacebookRequestException
     */
    private function sendRequest($method, $path, $params = null)
    {
		$token = $this->ci->session->userdata('login_token');
        $request = new FacebookRequest($token , $method, $path, $params);
        $response = $request->execute();
        return $response->getGraphObject();
    }
	/**
     * Retrieve user's profile
     *
     * @return Profile
     */
    public function getProfile()
    {
        $graph = $this->sendRequest("GET", "/me")->cast(GraphUser::className());
		
        $profile = new Profile($graph->getProperty("id"));
        $profile->setName($graph->getProperty("name"));
        $profile->setLink($graph->getProperty("link"));
        $profile->setLocale($graph->getProperty("locale"));
        $profile->setApp($this);
		//print_r($profile); die;
        return $profile;
    }


	 
  public function postfacebook($token = '' ,$profile_id = '' , $link = '', $message = '' ,$image = '')
	{
		
		//echo "message ".$message ."<br>";
		//echo "link ".$link ."<br>";
		//echo "image ".'http://216.172.184.121/~ibruser/adsense/'.$image."<br>";
		
		$page_post = (new FacebookRequest( $token , 'POST', '/'.$profile_id.'/feed', array(
			'access_token' 	=> $token,
			'link' 			=> $link, 
		   	'message' 		=> $message,
			'picture' 		=> base_url($image) /*"http://dev.ibrinfotech.com/images/1434620594_mission_thum.jpg";*/
			//'source'    	=> "@" . 'http://216.172.184.121/~ibruser/adsense/'.$image
		  ) ));
		  try {
		 	return $page_post->execute()->getGraphObject()->asArray();
		  } catch(Exception $e){
			return $e->getMessage();  
		  }
		// die;
		//return;
		// return post_id
		
	}
	
	// daummy function for passing more perameter
	public function postfacebook1()
	{
		$path ='D:\myxamp\htdocs\test_back\assets\uploads';
		
		$token = 'CAAXgiZCZAWNzIBAIhI5lS2Kfjp1qZBO1LhXqCNBD762GkG5XsUFZBSrr6UwmCSeyMf61aarsmRGWfq6enbKtEjiJK30DKWzsjSmxnZBjz5Bfypo9WMCurgooKQMurZCQ9GUevTZCABCQfO1LowsWHBPWOEXLCsYKCEVFBh1iX3caoZA84yywHcTOLPkdyqoZCp7jcSYdtXPvlp4b2fEV5ZBxNs';
			$page_post = (new FacebookRequest( $token , 'POST', '/103750459962577/feed', array(
			'access_token' 	=> $token,
			'name' 			=> 'Facebook API: Posting As A Page using Graph API v2.x and PHP SDK 4.0.x',
			'link' 			=> 'https://www.ibrinfotech.com/',
		   	'message' 		=> 'hello php developer auto post blog',
			'source' 		=> new CURLFile( realpath('assets/uploads/1434531043_s1.jpg'), 'image/jpg'),
		  ) ));
		return $postdata = $page_post->execute()->getGraphObject()->asArray();
		
		// return post_id
		
	}
}
 