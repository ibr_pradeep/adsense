<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if ( session_status() == PHP_SESSION_NONE ) {
    session_start();
}
 
require_once( APPPATH . 'libraries/facebook/Facebook/GraphObject.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/GraphSessionInfo.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookSession.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookCurl.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookHttpable.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookCurlHttpClient.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookResponse.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookSDKException.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookRequestException.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookAuthorizationException.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookRequest.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/FacebookRedirectLoginHelper.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/GraphUser.php' );
require_once( APPPATH . 'libraries/facebook/Facebook/GraphObject.php' );
//echo  "<br>".APPPATH . 'libraries/facebook/Profile.php'  ; die('mypath');
require_once( APPPATH . 'libraries/facebook/Profile.php' );

use Facebook\GraphSessionInfo;
use Facebook\FacebookSession;
use Facebook\FacebookCurl;
use Facebook\FacebookHttpable;
use Facebook\FacebookCurlHttpClient;
use Facebook\FacebookResponse;
use Facebook\FacebookAuthorizationException;
use Facebook\FacebookRequestException;
use Facebook\FacebookRequest;
use Facebook\FacebookSDKException;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\GraphObject;
use Facebook\GraphUser;

class Facebook {
    var $ci;
    var $helper;
    var $session;
 
    public function __construct() {
        $this->ci =& get_instance();
 
        FacebookSession::setDefaultApplication( $this->ci->config->item('api_id', 'facebook'), $this->ci->config->item('app_secret', 'facebook') );
        $this->helper = new FacebookRedirectLoginHelper( $this->ci->config->item('redirect_url', 'facebook') );
 
        if ( $this->ci->session->userdata('fb_token') ) {
            $this->session = new FacebookSession( $this->ci->session->userdata('fb_token') );
 
            // Validate the access_token to make sure it's still valid
            try {
                if ( ! $this->session->validate() ) {
                  $this->session = false;
                }
            } catch ( Exception $e ) {
                // Catch any exceptions
                $this->session = false;
            }
        } else {
            try {
                $this->session = $this->helper->getSessionFromRedirect();
            } catch(FacebookRequestException $ex) {
                // When Facebook returns an error
            } catch(\Exception $ex) {
                // When validation fails or other local issues
            }
        }
 
        if ( $this->session ) {
            $this->ci->session->set_userdata( 'fb_token', $this->session->getToken() );
 
            $this->session = new FacebookSession( $this->session->getToken() );
        }
    }
 
    public function get_login_url() {
        return $this->helper->getLoginUrl( $this->ci->config->item('permissions', 'facebook') );
    }
 
    public function get_logout_url() {
        if ( $this->session ) {
            return $this->helper->getLogoutUrl( $this->session, site_url( 'logout' ) );
        }
        return false;
    }
 
    public function get_user($token ='') {
        if ( $token ) {
            try {
                $request1 = (new FacebookRequest( $token, 'GET', '/me' ));
				$request = $request1 ->execute();
                $user = $request->getGraphObject()->asArray();
 
                return $user;
 
            } catch(FacebookRequestException $e) {
                return false;
 
            }
        }
    }
	
	
	public function getAccessToken()
    {
        $helper = new FacebookRedirectLoginHelper($this->ci->config->item('redirect_url', 'facebook'));
   		
        $session = $helper->getSessionFromRedirect_1();
	
		//print_r($session); 
        if ($session) {
			//$aa = $session->getAccessToken();
			// print_r($aa);
            return (string) $session ;//$session->getAccessToken();
        }
        return null;
    }
	
	/**
     * Check the access token
     *
     * @param string $token
     * @return bool
     */
    public function checkAccess($token)
    {
		
        $this->session = new FacebookSession($token);
		try {
			
            return $this->session->validate();
        } catch(FacebookSDKException $ex) {
            return false;
        }
        return false;
    }
	
	/**
     * Send request against facebook graph
     *
     * @param string $method
     * @param string $path
     * @param array|null $params
     *
     * @return \Facebook\GraphObject
     *
     * @throws \Facebook\FacebookRequestException
     */
    private function sendRequest($method, $path, $params = null)
    {
		$token = $this->ci->session->userdata('login_token');
        $request = new FacebookRequest($token , $method, $path, $params);
        $response = $request->execute();
        return $response->getGraphObject();
    }
	/**
     * Retrieve user's profile
     *
     * @return Profile
     */
    public function getProfile()
    {
        $graph = $this->sendRequest("GET", "/me")->cast(GraphUser::className());
		
        $profile = new Profile($graph->getProperty("id"));
        $profile->setName($graph->getProperty("name"));
        $profile->setLink($graph->getProperty("link"));
        $profile->setLocale($graph->getProperty("locale"));
        $profile->setApp($this);
		//print_r($profile); die;
        return $profile;
    }


	 
  public function postfacebook($token = '' ,$profile_id = '' , $link = '')
	{
		echo "token ".$token  .'profile_id '.$profile_id .' link '. $link ; 
		
			$page_post = (new FacebookRequest( $token , 'POST', '/'.$profile_id.'/feed', array(
			'access_token' 	=> $token,
			//'name' 			=> $name, //'Facebook API: Posting As A Page using Graph API v2.x and PHP SDK 4.0.x',
			'link' 			=> $link, //'https://www.ibrinfotech.com/',
		   //	'message' 		=> $message //'sulbha php developer auto post blog',
		  ) ));
		return $postdata = $page_post->execute()->getGraphObject()->asArray();
		
		// return post_id
		
	}
	
	// daummy function for passing more perameter
	public function postfacebook1()
	{
		echo "token ". $token ."<br>";
		
		$token = 'CAAWrpVdes54BAHoQ8JgNyZA389iC5prdpju0X86aZC1ZBreQoe5j5nZAZBOqlut3z5dGjs7ZAFUT3gAzkhlYOfyDLNuLsw2BhtSJlSD7Y207GvWrj4gSt1Kww8J3hO05dd0GfwIsLzZBvfQfpIPQQ3swfNEjOpGvKsUGx4F1CmhzNau1bGaChmiTd5dfYythJoiyUHhCqTtUAH8Xze51mel';
			$page_post = (new FacebookRequest( $token , 'POST', '/103750459962577/feed', array(
			'access_token' 	=> $token,
			'name' 			=> 'Facebook API: Posting As A Page using Graph API v2.x and PHP SDK 4.0.x',
			'link' 			=> 'https://www.ibrinfotech.com/',
		   	'message' 		=> 'hello php developer auto post blog',
		  ) ));
		return $postdata = $page_post->execute()->getGraphObject()->asArray();
		
		// return post_id
		
	}
}
 
