<?php
class CURLFile {
/* Properties */
public $name ;
public $mime ;
public $postname ;
/* Methods */
public __construct ( string $filename [, string $mimetype [, string $postname ]] )
public string getFilename ( void )
public string getMimeType ( void )
public string getPostFilename ( void )
public void setMimeType ( string $mime )
public void setPostFilename ( string $postname )
public void __wakeup ( void )
}
?>