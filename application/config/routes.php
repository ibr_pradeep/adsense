<?php

defined('BASEPATH') OR exit('No direct script access allowed');



/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|	example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|	http://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There are three reserved routes:

|

|	$route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|	$route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router which controller/method to use if those

| provided in the URL cannot be matched to a valid route.

|

|	$route['translate_uri_dashes'] = FALSE;

|

| This is not exactly a route, but allows you to automatically route

| controller and method names that contain dashes. '-' isn't a valid

| class or method name character, so it requires translation.

| When you set this option to TRUE, it will replace ALL dashes in the

| controller and method URI segments.

|

| Examples:	my-controller/index	-> my_controller/index

|		my-controller/my-method	-> my_controller/my_method

*/
if($_SERVER['SERVER_NAME'] == '216.172.184.121'){
	$url = $_SERVER['REQUEST_URI'];
	$url = explode('/', $url);
	$url_ = $url;
	unset($url[0]);
	unset($url[1]);
	unset($url[2]); 
	if($url[3] == 'index.php'){
		unset($url[3]);	
	}
	$lhs_url = implode('/', $url);
	if($url_[3] == 'index.php'){
		unset($url[4]);	
	}else{
		unset($url[3]);	
	}
	$rhs_url = implode('/', $url);		
}else{
	$url = $_SERVER['REQUEST_URI'];
	$url = explode('/', $url);
	$url_ = $url;
	unset($url[0]);
	unset($url[1]); 
	if($url[2] == 'index.php'){
		unset($url[2]);	
	}
	$lhs_url = implode('/', $url);
	if($url_[2] == 'index.php'){
		unset($url[3]);	
	}else{
		unset($url[2]);	
	}
	$rhs_url = implode('/', $url);
}
$route['twitter/auth'] = 'twitter/auth';
$route[$lhs_url] = $rhs_url;
if($rhs_url == 'user/report'){
	$route[$lhs_url] = 'billing/userReport';	
}
$route['default_controller'] = 'user';
//$route['user/report'] = 'billing/userReport';
$route['admin'] = 'user/admin';
$route['user/admin/forgot_password'] = 'user/forgot_password';
$route['user/admin/set_new_password'] = 'user/set_new_password';

$route['user/admin/thankyou'] = 'user/thankyou';

$route['twitter/auth'] = 'twitter/auth';
$route['user/logout'] = 'user/logout';
$route['user/checkUserlogin'] = 'user/checkUserlogin';

$route['mycrone/crone'] = 'mycrone/crone';
$route['404_override'] = '';

$route['translate_uri_dashes'] = FALSE;

