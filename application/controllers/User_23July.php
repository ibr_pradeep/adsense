<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
		$this->load->model('user_model');
		$user_session = getCurrentUserSession();
		$user_info = isset($user_session['username']) ? $user_session['username'] : '';
	}
	 
	public function index()
	{
		$user_session = getCurrentUserSession();
		$blank_session = getBlankSession();
		$data = array();
		$this->load->library('facebook/facebook');
		if(!isset($blank_session['twitter_user_id'])){
			if($this->input->get('code')){
				$data['login_url'] = $this->fblogin();
			}else{
				$login_url = $this->facebook->get_login_url();
				$data['login_url'] = $login_url;	
			}
			//die;
		}else{
			//this function will login user through facebook account 
			$data['login_url'] = $this->fblogin();
			//this function will login user through twitter account 
			$this->twitterlogin();
		}
		$this->load->view('login',$data);
		$user_info = isset($user_session['username']) ? $user_session['username'] : '';
		if(isset($user_info)){
		//	redirect(base_url()."index.php/dashboard");		
		}
		else
		{
			//redirect(base_url()."index.php/user");	
		}
	}
	
	public function admin()
	{
		$data = array();
		$this->load->view('admin',$data);
			
		//echo "<PRE>";print_r($this->session->all_userdata());
	}
	
	public function forgot_password(){
		if($this->input->post('go')){
			$email = $this->input->post('email');
			if($email){
				$this->db->where(array('email' => $email, 'type' => 1));
				$query = $this->db->get('user');
				$row = $query->row();
				if(empty($row)){
					$this->session->set_flashdata('error', 'email address does not exists');
					redirect(base_url('user/admin/forgot_password'));
				}else{
					$this->load->helper('string');
					$hash_token = random_string('alnum',30);
					$this->db->where(array('email' => $email, 'type' => 1));
					$this->db->update('user', array('verification_token' => $hash_token));
					
					//load email helper
					/*$this->load->helper('email');
					//load email library
					$this->load->library('email');
					
					$this->email->set_mailtype("html");
					$this->email->from('' , 'Affiliate Web Application');
				    $this->email->to('ashvin@ibrinfotech.com'); 
				    $this->email->subject('Forgot password');
				    $this->email->message('<p><a href="'.base_url('user/admin/set_new_password?token='.$hash_token.'&email='.$email).'">Please click here to set new password</a></p>');  */
				  
				    // try send mail ant if not able print debug
					srand((double)microtime()*1000000);
					$marker = md5(uniqid(rand()));
				
					$receiver  = "admin@admin.com";
					$title = "Mail";
					$sender  = 'Affiliate Web Application';
					$sender_mail = 'admin@admin.com';
				
					$message = '<p>Hi,</p><p><a href="'.base_url('user/admin/set_new_password?token='.$hash_token.'&email='.$email).'">Please click here to set new password</a></p>';
				
					$headers  = "From: $sender <$sender_mail>\n";
					$headers .= "MIME-Version: 1.0\n";
					$headers .= "Content-Type: multipart/mixed;\n";
					$headers .= "\tboundary=\"___$marker==\"";
				
					$content ="--___$marker==\n";
					$content .="Content-Type: text/html; charset=\"iso-8859-2\"\n";
					$content .="Content-Transfer-Encoding: 8bit\n";
					$content .="\n$message\n";
				
					if (mail($receiver,$title,$content,$headers))
					{
						//print "Your message is sent.";
						$this->session->set_flashdata('message', 'Mail sent. please check your email');
						redirect(base_url('user/admin/thankyou'));
					}
					else
					{
						print "Your message is not sent.
						<br>Please go <a href=\"javascript:history.back();\">back</a> and send again.";
					}
				}
			}else{
				$this->session->set_flashdata('error', 'Enter email address');
				redirect(base_url('user/admin/forgot_password'));
			}
		}else{
			$this->load->view('forgot_password');
		}
	}
	
	public function set_new_password(){
		if($this->input->post('go')){
			$token = $this->input->post('token');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$c_password = $this->input->post('c_password');
			if($password == $c_password){
				$this->db->where(array('email' => $email, 'type' => 1, 'verification_token' => $token));
				$this->db->update('user', array('password' => md5($password), 'verification_token' => NULL));
				$this->session->set_flashdata('message', 'Password updated succesfully');
				redirect(base_url('user/admin/thankyou'))	;
			}else{
				$this->session->set_flashdata('error', 'Confirm password not match!');
				redirect(base_url('user/admin/set_new_password?token='.$token.'&email='.$email.''))	;
			}
		}else{
			$token = $this->input->get('token');
			$email = $this->input->get('email');
			
			$this->db->where(array('email' => $email, 'type' => 1, 'verification_token' => $token));
			$query = $this->db->get('user');
			$row = $query->row();
			if(empty($row)){
				echo '!invalid token';	
			}else{
				$data['token'] = $token;
				$data['email'] = $email;
				$this->load->view('set_new_password', $data);	
			}
		}
	}
	
	public function thankyou(){
		$this->load->view('thank-you');	
	}
	public function checkUserlogin()
	{
		
		$rows = $this->login_model->checkUserlogin();
		if($rows){
			/*$this->session->set_userdata('userID' , $rows->id);
			$this->session->set_userdata('userfname' , $rows->firstname);
			$this->session->set_userdata('username' , $rows->email);
			$this->session->set_userdata('usertype' , $rows->type);*/
			$logindata =array(
				'admin_web' => array(
							'userID' 		=> $rows->id,
							'userfname' 	=> $rows->firstname,
							'username' 		=> $rows->email,
							'usertype' 		=> $rows->type
							)
			);
			$this->session->set_userdata($logindata);
			redirect(base_url('admin_web/dashboard'));
		}else{
			$this->session->set_flashdata('error','Invalid username or password..!!');
			redirect(base_url('admin'));
		}
			
	}
	/** function for distroy admin session variable
		return boolen value
	*/
	public function logout(){		
		$userseg = $this->uri->segment(1);
		$this->session->unset_userdata($userseg);		
		redirect(base_url('user'));
		
	}
	/** This function will login user through facebook account 
	
	*/
	public function fblogin()
	{
		
		$this->load->library('facebook/facebook');
		
		 $token = $this->facebook->getAccessToken();		
		 $user = $this->facebook->get_user( $token);		
		// $user = $this->facebook->get_user();
		//$user = $this->facebook->getUser();
		
        if ($user) {
            try {
             // $fbdata = $this->facebook->api('/me');
				 $fbdata = $this->facebook->get_user( $token);
				
				
 				//$token = $this->facebook->getAccessToken();
 				//print_r($fbdata ); die;
			 //check if already exist facebook user 
			  $respnse = $this->user_model->alredyExist($fbdata['id']);
			 
			  if (sizeof($respnse) >0){
				  if($respnse['active'] == 1){
					   $this->session->set_flashdata('loginerror','your account blocked by admin..!!');
					   redirect(base_url("user")); 
				  }
				  else
				  {
					$up_data['login_token'] =  $token ;
					$this->user_model->update_login_token($up_data , $fbdata['id']);
					$ses_name = $respnse['id'].'_'.$respnse['firstname'];
				 	$logindata =array(
								$ses_name => array(
											'userID' 		=> $respnse['id'],
											'userfname' 	=> $respnse['firstname'],
											'username' 		=> isset($respnse['email'])?$respnse['email']:$respnse['social_id'],
											'usertype' 		=> $respnse['type'],
											'outh_provider' => $respnse['outh_provider'],
											'login_token'   =>  $token
											)
							);
					 $this->session->set_userdata($logindata);
					 $this->session->unset_userdata('blank');
					 redirect(base_url("".$ses_name."/userlist/profile")); 
				  }
					
			  }
			  else
			  {
					
					
					if (isset($fbdata['id'])){
							 $data_fb = array(
							  'social_id' 		=> $fbdata['id'],
							  'email' 			=> isset($fbdata['email'])?$fbdata['email']:'',
							  'firstname' 		=> $fbdata['first_name'],
							  'lastname' 		=> $fbdata['last_name'],
							  'gender'  		=> $fbdata['gender'],
							  'activation_token'=> $fbdata['id'],
							  'facebook_id' 	=> isset($fbdata['email'])?$fbdata['email']:'',
					  		  'login_token'		=>  $token ,
							  'outh_provider'	=> 'facebook', 
							  'create_date'  	=> date('Y-m-d H:i:s')
							  );
						  
						  $fblogin = $this->user_model->insertData($data_fb);
						  $ses_name = $fblogin['id'].'_'.$fblogin['firstname'];
						  $logindata = array(
						  			$ses_name =>  array(
													'userID' 		=> $fblogin['id'],
													'userfname' 	=> $fblogin['firstname'],
													'username' 		=> isset($fblogin['email'])?$fblogin['email']:$fblogin['social_id'],
													'usertype' 		=> $fblogin['type'],
													'outh_provider' => 'facebook', 
													'login_token'   =>  $token
													)
								);
								
						 $this->session->set_userdata($logindata);
						 $this->session->unset_userdata('blank');
						 $ses_userdata = $this->session->userdata($ses_name);
				  $this->db->where('id',$ses_userdata['userID']);
				  
				  $data['referal_link'] =  $ses_userdata['userID'].'_'.$ses_userdata['userfname'];
				 // print_r($this->session->all_userdata());
				  $this->db->update('user',$data);
						 redirect(base_url("".$ses_name."/userlist/profile"));
					}
			  }
			} catch (FacebookApiException $e) {
                $user = null;
            }
        }

        if ($user) {

           // $data['logout_url'] = site_url('welcome/logout'); // Logs off application
           

        } else {
			
			$login_url = $this->facebook->get_login_url();
            return $login_url;
        }
	}
	
	/** this function will login user through twitter 
	
	*/
	public function twitterlogin()
	{
		//print_r($this->session->all_userdata());die;
		 $blank_session = getBlankSession();
		 $social_id = isset($blank_session['twitter_user_id']) ? $blank_session['twitter_user_id'] : '';	
		 $respnse = $this->user_model->alredyExist($social_id);	
		 if (sizeof($respnse) >0){
			 
				  if($respnse['active'] == 1){
					   $this->session->set_flashdata('loginerror','your account blocked by admin..!!');
					  redirect("user"); 
				  }
				  else
				  {
					$blank_session = getBlankSession();
					$upt_data = array(
								 'login_token'			=> $blank_session['access_token'],
					  			 'login_token_secreat'	=> $blank_session['access_token_secret']
								);
					$this->user_model->update_login_token($upt_data , $social_id);
					$ses_name = $respnse['id'].'_'.$respnse['firstname'];
				 	$logindata = array(
							$ses_name => array(
											'userID' 		=> $respnse['id'],
											'userfname' 	=> $respnse['firstname'],
											'username' 		=> isset($respnse['email'])?$respnse['email']:$respnse['social_id'],
											'usertype' 		=> $respnse['type'],
											'outh_provider' => 'twitter', 
											)
							);
					 $this->session->set_userdata($logindata);
					 $this->session->unset_userdata('blank');
					redirect(''.$ses_name.'/userlist/profile'); 
				  }
					
		 }
		 else
		 {
			 $blank_session = getBlankSession();
			 $referal_link_twi = md5(isset($blank_session['twitter_screen_name']) ? $blank_session['twitter_screen_name'] : '');
			 $twitter_user_id = isset($blank_session['twitter_user_id']) ? $blank_session['twitter_user_id'] : '';
			 if($twitter_user_id != ''){
			   $data_tw = array(
					  'social_id' 			=> $blank_session['twitter_user_id'],
					  'email' 				=> '',
					  'firstname' 			=> $blank_session['twitter_screen_name'],
					  'lastname' 			=> '',
					
					  'twitter_id' 			=> $blank_session['twitter_screen_name'],
					  'activation_token'	=> $blank_session['twitter_user_id'],
					  'login_token'			=> $blank_session['access_token'],
					  'login_token_secreat'	=> $blank_session['access_token_secret'],
					 
					  'create_date'  		=> date('Y-m-d H:i:s'),
					  'outh_provider'		=> 'twitter'
					  );				  
				
				  $twlogin = $this->user_model->insertData($data_tw);
				  $this->session->unset_userdata('request_token');
				  $this->session->unset_userdata('request_token_secret');
				  $this->session->unset_userdata('blank');
				  $ses_name = $twlogin['id'].'_'.$twlogin['firstname'];
				  $logindata = array( 
				  				$ses_name => array(
											'userID' 		=> $twlogin['id'],
											'userfname' 	=> $twlogin['firstname'],
											'username' 		=> isset($twlogin['email'])?$twlogin['email']:$twlogin['social_id'],
											'usertype' 		=> $twlogin['type'],
											'outh_provider' => 'twitter', 
								)
							   );
						
				 $this->session->set_userdata($logindata);
				  
				 $ses_userdata = $this->session->userdata($ses_name);
				  $this->db->where('id',$ses_userdata['userID']);
				  $data['referal_link'] =  $ses_userdata['userID'].'_'.$ses_userdata['userfname'];
				  $this->db->update('user',$data);
				  
				redirect(''.$ses_name.'/userlist/profile');
			 }
			/* else
			 {
				redirect("user"); 
			 }*/
		 }
		 
	}
	public function getfbtoken()
	{
		
	}
	
}
