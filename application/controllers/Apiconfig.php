<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Apiconfig extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */

	 

	public function __construct()

	{

		parent::__construct();

		$this->load->model('apiconfig_model');
		$userseg = $this->uri->segment(1);	
		newBaseurl();
		if(!$this->session->userdata($userseg)){
			redirect(base_url());
		}

	}

	public function index()

	{

		$data['results'] = $this->apiconfig_model->getallConfig();

		$this->load->view('apiconfig',$data);

	}

	/** Add and update particuler category value

		

	**/

	public function addUpdate($edit_id = '')

	{

		
		if($this->input->post('submit')){			

			if($this->input->post('edit_id')){

				$data = array(

						'api_id' 				=> $this->input->post('apiid'),
						'app_secret' 			=> $this->input->post('app_secreat'),
						'redirect_url' 			=> $this->input->post('redirect_url'),
						'permissions' 			=> $this->input->post('permission'),

						);


				$edit_id = $this->input->post('edit_id');

				$response = $this->apiconfig_model->updateAppconfig($data , $edit_id );

				if($response){

					$this->session->set_flashdata('update','Configration Updated Sucessfully..!!');	

					redirect(base_url('apiconfig'));

				}

				

			}else

			{

				$data = array(

						'service_provider' 		=> $this->input->post('service_provider'),
						'api_id' 				=> $this->input->post('apiid'),
						'app_secret' 			=> $this->input->post('app_secreat'),
						'redirect_url' 			=> $this->input->post('redirect_url'),
						'permissions' 			=> $this->input->post('permission'),
						'created_date'   		=> date('Y-m-d H:i:s')

						);

				
				$serviceproider = $this->apiconfig_model->getApiconfig($this->input->post('service_provider'));
				if($serviceproider)
				{
					$this->session->set_flashdata('exist','Service prvider already exist ! You can update it');	
	
					redirect(base_url('apiconfig'));
				}
				else
				{
				$response = $this->apiconfig_model->insertAppconfig($data);

					if($response){
	
						$this->session->set_flashdata('insert','Configration Inserted Sucessfully..!!');	
	
						redirect(base_url('apiconfig'));
	
					}
				}

			}

			}

			$datacat['config'] = $this->apiconfig_model->getSingleConfig($edit_id);	

			$this->load->view('apiconfig',$datacat);

	}

	/* Delete single category */

	public function deleteAppconfig($ID)

	{

		$response = $this->apiconfig_model->deleteAppconfig($ID);

		if($response)

		{

			$this->session->set_flashdata('delete','App Configration deleted Sucessfully..!!');	

			redirect(base_url('apiconfig'));

		}

	}

	public function getApiconfig()
	{
		$service_prov = $this->input->post('service_prov');	
		$response = $this->apiconfig_model->getApiconfig($service_prov);
		echo json_encode($response); 
	}	

	

}

