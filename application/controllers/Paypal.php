<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Paypal extends CI_Controller {

	public function __construct()

	{

		parent::__construct();

		$this->load->model('paypal_model');
		$this->load->model('sharelink_model');
		$userseg = $this->uri->segment(1);	
		newBaseurl();
		if(!$this->session->userdata($userseg)){
			redirect(base_url());
		}
	}

	public function index()
	{
		
		if($this->input->post('direct_pay')){
			$month_year = date('F Y');
			$data_trans = array(
								'user_id' 				=> $this->input->post('user_id'),
								'ref_id'  			   	=> $this->input->post('ref_id'),
								'pay_for_month_year'  	=> str_replace(' ', '', $this->input->post('pay_month_year')),
								'amount'				=> round($this->input->post('amount'), 3),
								'paypal_trans_id'		=> '',
								'mode_type'				=> 'direct'
							);
		
			$success_id = $this->paypal_model->transaction_details($data_trans); 
			if($success_id ){
				redirect(base_url('billing')); 		
			}
		}else if($this->input->post('pay')){
			$month_year = date('F Y');
			$data_session = array(
				'pay_userid' => $this->input->post('user_id'),
				'month_year' =>	str_replace(' ', '', $this->input->post('pay_month_year')),
				'ref_id'  	 => $this->input->post('ref_id'),
			);
			$this->session->set_userdata($data_session);
			 $amount = $this->input->post('amount'); 
			$amount = round($amount, 3);
			//echo "amt ".$amount ; die;
			$user_id = $this->input->post('user_id');
			$marchent_id = getUserPaypalEmail($user_id);
			if(!$marchent_id){
				$this->session->set_flashdata('error','PayPal email not set');	
				redirect(base_url('billing'));
			}
			$user_name = get_username($user_id);
			$data=array(
					'merchant_email'	=> $marchent_id,
					'product_name'		=> $user_name,
					'amount'			=>  $amount, //0.50,
					'currency_code'		=> 'USD',
					'thanks_page'		=> base_url('paypal/thank'),
					'notify_url'		=>"http://".$_SERVER['HTTP_HOST'].'paypal/ipn.php',
					'cancel_url'		=>"http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
					'paypal_mode' 		=>false,
					);	
		
			$paypalform = $this->infotutsPaypal( $data);
			echo $paypalform ; 	
			
		}else if($this->input->post('Sandbox')){
			//'Sandbox' => $this->config->item('Sandbox')
			$amount = $this->input->post('amount'); 
			$amount = round($amount, 2);
			$month_year = date('F Y');
			$data_session = array(
				'pay_userid' => $this->input->post('user_id'),
				'month_year' =>	str_replace(' ', '', $this->input->post('pay_month_year')),
				'ref_id'  	 => $this->input->post('ref_id'),
				'amount'  	 => $amount,
			
			);
			$this->session->set_userdata($data_session);
			
		
			//echo "amt ".$amount ; die;
			$user_id = $this->input->post('user_id');
			$marchent_id = getUserPaypalEmail($user_id);
			
			if(!$marchent_id){
				$this->session->set_flashdata('error','PayPal email not set');	
				redirect(base_url('billing'));
			}
			$user_name = get_username($user_id);
			
			//set PayPal Endpoint to sandbox
//$url  = trim("https://svcs.sandbox.paypal.com/AdaptivePayments/Pay");
$url  = $this->config->item('API_url_ADP');
$form_url  = $this->config->item('API_FORM_url_ADP');
//PayPal API Credentials
/*
$API_UserName = "dewanishan-facilitator_api1.gmail.com"; //TODO dewanishan_api1.gmail.com
$API_Password = "NTPJPKNUSPGVT65Q"; //TODO QNFCNF7S7JZKHRGE
$API_Signature = "An5ns1Kso7MWUdW4ErQKJJJ4qi4-A5eoVVzvUQ.OrWC4qQhIDi4Mo.8F"; //TODO AFcWxV21C7fd0v3bYYYRCpSSRl31AW2-bBVdUeFKqxZS1Y44tLXl.-5N
*/
$API_UserName = $this->config->item('API_UserName_ADP');//TODO dewanishan_api1.gmail.com
$API_Password = $this->config->item('API_Password_ADP'); //TODO QNFCNF7S7JZKHRGE
$API_Signature = $this->config->item('API_Signature_ADP'); //TODO AFcWxV21C7fd0v3bYYYRCpSSRl31AW2-bBVdUeFKqxZS1Y44tLXl.-5N
//Default App ID for Sandbox  
 
$API_AppID = $this->config->item('API_AppID_ADP');
$API_RequestFormat = "NV";
$API_ResponseFormat = "NV";
//Create request payload with minimum required parameters
$bodyparams = array (   "requestEnvelope.errorLanguage" => "en_US",
                  "actionType" => "PAY",
                  "cancelUrl" => "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
                  "returnUrl" => base_url('paypal/thank'),
                  "currencyCode" => "USD",
                  "receiverList.receiver.email" => $marchent_id,
                  "receiverList.receiver.amount" => $amount
    );
// convert payload array into url encoded query string
$body_data = http_build_query($bodyparams, "", chr(38));
try
{
//create request and add headers
$params = array("http" => array(
    "method" => "POST",                                                 
    "content" => $body_data,                                             
    "header" =>  "X-PAYPAL-SECURITY-USERID: " . $API_UserName . "\r\n" .
                 "X-PAYPAL-SECURITY-SIGNATURE: " . $API_Signature . "\r\n" .
                 "X-PAYPAL-SECURITY-PASSWORD: " . $API_Password . "\r\n" .
                 "X-PAYPAL-APPLICATION-ID: " . $API_AppID . "\r\n" .
                 "X-PAYPAL-REQUEST-DATA-FORMAT: " . $API_RequestFormat . "\r\n" .
                 "X-PAYPAL-RESPONSE-DATA-FORMAT: " . $API_ResponseFormat . "\r\n"
));
//create stream context
 $ctx = stream_context_create($params);
//open the stream and send request
 $fp = @fopen($url, "r", false, $ctx);
//get response
 $response = stream_get_contents($fp);
//check to see if stream is open
 if ($response === false) {
    throw new Exception("php error message = " . "$php_errormsg");
 }
//close the stream
 fclose($fp);
//parse the ap key from the response
$keyArray = explode("&", $response);

foreach ($keyArray as $rVal){    list($qKey, $qVal) = explode ("=", $rVal);       $kArray[$qKey] = $qVal;}
$payKey='';
//print the response to screen for testing purposes
if ( $kArray["responseEnvelope.ack"] == "Success") {
	
  $payKey=$kArray['payKey'];

 }else { echo 'ERROR Code: ' .  $kArray["error(0).errorId"] ." <br/>"; echo 'ERROR Message: ' . urldecode($kArray["error(0).message"])."<br/>";}
}
catch(Exception $e) {echo "Message: ||" .$e->getMessage()."||";}

    	$script_url = 'https://www.paypalobjects.com/js/external/dg.js';
		$form = '';
		 $form .= '<script src="'.$script_url.'">';
		 $form .= '</script>';	
		 $form .=' <script type="text/javascript" charset="utf-8">
            var embeddedPPFlow = new PAYPAL.apps.DGFlow({trigger: "myForm"});
             </script>';
		 $form .= '<form  action="'.$form_url.'"  id="myForm" >';
		 $form .= '<input type="hidden" name="paykey" value="'.$payKey.'" />';
		 // Instant Payment Notification & Return Page Details /
		 $form .= '<input type="hidden" name="expType" value="light" />';		 
		 // Configures Basic Checkout Fields -->
		 $form .= '<script>';
		 $form .= ' window.onload=function(){setTimeout(function(){			
                     document.getElementById("myForm").submit();
                    }, 200);}';
		 $form .= '</script>';
		 
		 
			echo $form ; 	
			
		}
	}

	/** This function will Add and update paypal details
		@param $editid 
		return boolean value if sucess and failuer 
	*/

	public function addUpdate()
	{

		

						
			$user_session = getCurrentUserSession();
			
			if($this->input->post('submit')){			

			if($this->input->post('edit_id')){
				$editid = $this->input->post('edit_id');
				$data = array(
							'paypal_email' 	=>  $this->input->post('paypalemail'),
						);

				$response = $this->paypal_model->updatePaypal($data , $editid );

				if($response){

					$this->session->set_flashdata('update','Paypal Detail Updated Sucessfully..!!');	

					redirect('userlist/profile');

				}

				

			}else

			{

				
				$data = array(

						'paypal_email' 		=>  $this->input->post('paypalemail'),
						'user_id' 			=>  isset($user_session['userID']) ? $user_session['userID'] : '',
						'created_date'   	=>  date('Y-m-d H:i:s')
						);

				

				$response = $this->paypal_model->insertPaypal($data);
				
				if($response){

					$this->session->set_flashdata('insert','Paypal Detail Inserted Sucessfully..!!');	

					redirect('userlist/profile');

				}	

			}

			}

			$datapay['paypal'] = $this->paypal_model->getSinglepaypal($editid);	

			

			$this->load->view('paypal',$datapay);

	}

	/* Delete single category */

	public function deleteCategory($categorID)

	{

		$response = $this->category_model->deleteCategory($categorID);

		if($response)

		{

			$this->session->set_flashdata('delete','Category deleted Sucessfully..!!');	

			redirect(base_url().'index.php/category');

		}

	}

	public function infotutsPaypal( $data) {
		
		define( 'SSL_URL', 'https://www.paypal.com/cgi-bin/webscr' );
		define( 'SSL_SAND_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr' );
		$action = '';
		 //Is this a test transaction?
		$action = ($data['paypal_mode']) ? SSL_SAND_URL : SSL_URL;
		$data['merchant_email'] = "imodify021-facilitator@gmail.com";
		$action = SSL_SAND_URL;
		//$data['amount'] = 1;
	
		$form = '';
		
		$form .= '<form name="frm_payment_method" action="' . $action . '" method="post">';
		 $form .= '<input type="hidden" name="business" value="' . $data['merchant_email'] . '" />';
		 // Instant Payment Notification & Return Page Details /
		 $form .= '<input type="hidden" name="notify_url" value="' . $data['notify_url'] . '" />';
		 $form .= '<input type="hidden" name="cancel_return" value="' . $data['cancel_url'] . '" />';
		 $form .= '<input type="hidden" name="return" value="' . $data['thanks_page'] . '" />';
		 $form .= '<input type="hidden" name="rm" value="2" />';
		 // Configures Basic Checkout Fields -->
		 $form .= '<input type="hidden" name="lc" value="" />';
		 $form .= '<input type="hidden" name="no_shipping" value="1" />';
		 $form .= '<input type="hidden" name="no_note" value="1" />';
		 // <input type="hidden" name="custom" value="localhost" />-->
		 $form .= '<input type="hidden" name="currency_code" value="' . $data['currency_code'] . '" />';
		 $form .= '<input type="hidden" name="page_style" value="paypal" />';
		 $form .= '<input type="hidden" name="charset" value="utf-8" />';
		 $form .= '<input type="hidden" name="item_name" value="' . $data['product_name'] . '" />';
		 $form .= '<input type="hidden" value="_donations" name="cmd"/>';
		 $form .= '<input type="hidden" name="amount" value="' . number_format($data['amount'], 2, '.', '') . '" />';
		 $form .= '<script>';
		 $form .= 'setTimeout("document.frm_payment_method.submit()", 2);';
		 $form .= '</script>';
		 $form .= '</form>';
		 return $form;
	}	
	public function thank()
	{
				$data =array(
				'mc_gross' => $this->input->post('mc_gross'), 
				'protection_eligibility' => $this->input->post('protection_eligibility'), 
				'payer_id' => $this->input->post('payer_id'), 
				'tax' => $this->input->post('tax'), 
				'payment_date' => $this->input->post('payment_date'), 
				'payment_status' => $this->input->post('payment_status'), 
				'charset' => $this->input->post('charset'), 
				'first_name' => $this->input->post('first_name'), 
				'mc_fee' => $this->input->post('mc_fee'), 
				'notify_version' => $this->input->post('notify_version'), 
				'custom' => $this->input->post('custom'), 
				'payer_status' => $this->input->post('payer_status'), 
				'business' => $this->input->post('business'), 
				'quantity' => $this->input->post('quantity'), 
				'payer_email' => $this->input->post('payer_email'), 
				'verify_sign' => $this->input->post('verify_sign'), 
				'txn_id' => $this->input->post('txn_id'), 
				'payment_type' => $this->input->post('payment_type'), 
				'last_name' => $this->input->post('last_name'), 
				'receiver_email' => $this->input->post('receiver_email'), 
				'payment_fee' => $this->input->post('payment_fee'), 
				'receiver_id' => $this->input->post('receiver_id'), 
				'txn_type' => $this->input->post('txn_type'), 
				'item_name' => $this->input->post('item_name'), 
				'mc_currency' => $this->input->post('mc_currency'), 
				'item_number' => $this->input->post('item_number'), 
				'residence_country' => $this->input->post('residence_country'), 
				'test_ipn' => $this->input->post('test_ipn'), 
				'handling_amount' => $this->input->post('handling_amount'), 
				'transaction_subject' => $this->input->post('transaction_subject'), 
				'payment_gross' => $this->input->post('payment_gross'), 
				'shipping' => $this->input->post('shipping'), 
				'auth' => $this->input->post('auth'), 
			
		); 
		$this->paypal_model->paypal_tansaction_details($data); 
		$insert_id = $this->db->insert_id();
		
		$data_trans = array(
				'user_id' 				=> $this->session->userdata('pay_userid'),
				'ref_id'  			   	=> $this->session->userdata('ref_id'),
				'pay_for_month_year'  	=> $this->session->userdata('month_year'),
				'amount'				=> $this->session->userdata('amount'),
				'paypal_trans_id'		=> $insert_id,
				'mode_type'				=> 'paypal'
				
		);
		
		$this->paypal_model->transaction_details($data_trans); 
		
		
		if($insert_id ){
			redirect(base_url('billing')); 		
		}
	}
	

}

