<?php 

if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')

{

	$this->load->view('includes/header');

}
?>

<div id="wrapper"> 
  
  <!-- Navigation -->
  
  <?php $this->load->view('includes/navbar');?>
  <div id="page-wrapper">
    <div class="container-fluid"> 
      
      <!-- Page Heading -->
      
      <div class="row">
        <div class="col-lg-12">
          <h1 class="page-header"> Paypal Deatils </h1>
          <ol class="breadcrumb">
            <li class="active"> <i class="fa fa-dashboard"></i> Paypal Deatils </li>
          </ol>
        </div>
      </div>
      
      <!-- /.row -->
      
     
      <div class="row">
        <div class="col-lg-10">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title"> Paypal Deatils</h3>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
            
                <table id="mytables" class="table table-bordered table-hover table-striped">
                  <thead>
                    <tr>
                      <th> SNo.</th>
                      <th> Name </th>
                      <th>Paypal Email</th>
                      <th style="display:none"> Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  
                      <?php
					  $i=1;
					   if (isset($paypals) && (count($paypals) > 0 )){

														foreach ($paypals as $paypal) { ?>
                                                          <tr>
                                                          <td><?php echo $i; ?></td>
                      <td><?php echo $paypal->firstname .' '. $paypal->middlename .'' .$paypal->lastname ?></td>
                      <td><?php echo isset($paypal->paypal_email)?$paypal->paypal_email:'' ?></td>
                    
                      <td style="display:none"><a href="<?php echo base_url()?>">[ Edit ] </a> &nbsp;&nbsp;<a onclick="return confirm('Are you sure you want to delete ? ')" href="">[ Delete ] </a></td>
                    </tr>
                    <?php $i++; } ?>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    
    </div>
    
    <!-- /.row --> 
    
  </div>
  
  <!-- /.container-fluid --> 
  
</div>

<!-- /#page-wrapper -->

</div>
<?php 



if (strtolower($this->input->server('HTTP_X_REQUESTED_WITH')) != 'xmlhttprequest')



{



	$this->load->view('includes/footer');



}



?>
