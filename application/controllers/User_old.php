<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
		$this->load->model('user_model');
		if($this->session->userdata('username')){
			//redirect('dashboard');		
		}
		else
		{
			//redirect('user');	
		}	
	}
	 
	public function index()
	{
		$data = array();
		//this function will login user through facebook account 
		$data['login_url'] = $this->fblogin();
		//this function will login user through twitter account 
		$this->twitterlogin();
		
		$this->load->view('login',$data);
	}
	
	public function checkUserlogin()
	{
		
		$rows = $this->login_model->checkUserlogin();
		if($rows){
			$this->session->set_userdata('userID' , $rows->id);
			$this->session->set_userdata('userfname' , $rows->firstname);
			$this->session->set_userdata('username' , $rows->email);
			$this->session->set_userdata('usertype' , $rows->type);
			redirect(base_url()."index.php/dashboard");
		}else
		{
			$this->session->set_flashdata('error','Invalid username or password..!!');
			redirect(base_url()."index.php/user");
		}
			
	}
	/** function for distroy admin session variable
		return boolen value
	*/
	public function logout()
	{
		 $this->session->sess_destroy();
		 redirect(base_url()."index.php/user");
		
	}
	/** This function will login user through facebook account 
	
	*/
	public function fblogin()
	{
		
		$this->load->library('facebook');
		$user = $this->facebook->getUser();
		
        if ($user) {
            try {
              $fbdata = $this->facebook->api('/me');
			  //check if already exist facebook user 
			  $respnse = $this->user_model->alredyExist($fbdata['id']);
			 
			  if (sizeof($respnse) >0){
				  if($respnse['active'] == 1){
					   $this->session->set_flashdata('loginerror','your account blocked by admin..!!');
					   redirect("index.php/user"); 
				  }
				  else
				  {
				 	$logindata = array(
								'userID' 		=> $respnse['id'],
								'userfname' 	=> $respnse['firstname'],
								'username' 		=> isset($fblogin['email'])?$fblogin['email']:$fblogin['social_id'],
								'usertype' 		=> $respnse['type'],
							);
					 $this->session->set_userdata($logindata);
					 redirect("dashboard"); 
				  }
					
			  }
			  else
			  {
				
					if (isset($fbdata['id'])){
							 $data_fb = array(
							  'social_id' 		=> $fbdata['id'],
							  'email' 			=> $fbdata['email'],
							  'firstname' 		=> $fbdata['first_name'],
							  'lastname' 		=> $fbdata['last_name'],
							  'gender'  		=> $fbdata['gender'],
							  'activation_token'=> $fbdata['id'],
							  //'create_date'  	=> date('Y-m-d H:i:s')
							  );
						  
						  $fblogin = $this->user_model->insertData($data_fb);
						  $logindata = array(
									'userID' 		=> $fblogin['id'],
									'userfname' 	=> $fblogin['firstname'],
									'username' 		=> isset($fblogin['email'])?$fblogin['email']:$fblogin['social_id'],
									'usertype' 		=> $fblogin['type'],
								);
						 $this->session->set_userdata($logindata);
						 redirect("dashboard");
					}
			  }
			} catch (FacebookApiException $e) {
                $user = null;
            }
        }

        if ($user) {

            $data['logout_url'] = site_url('welcome/logout'); // Logs off application
           

        } else {
			
            $login_url = $this->facebook->getLoginUrl(array(
                'redirect_uri' => site_url('user'), 
                'scope' => array("email") // permissions here
            ));
			
			return $login_url;
        }
	}
	
	/** this function will login user through twitter 
	
	*/
	public function twitterlogin()
	{
		
		 $social_id = $this->session->userdata('twitter_user_id');	
		 $respnse = $this->user_model->alredyExist($social_id);	
		 if (sizeof($respnse) >0){
			 
			
				  if($respnse['active'] == 1){
					   $this->session->set_flashdata('loginerror','your account blocked by admin..!!');
					  redirect("user"); 
				  }
				  else
				  {
					
				 	$logindata = array(
								'userID' 		=> $respnse['id'],
								'userfname' 	=> $respnse['firstname'],
								'username' 		=> isset($fblogin['email'])?$fblogin['email']:$fblogin['social_id'],
								'usertype' 		=> $respnse['type'],
							);
					 $this->session->set_userdata($logindata);
					redirect(base_url()."index.php/dashboard"); 
				  }
					
		 }
		 else
		 {
			 
			 if($this->session->userdata('twitter_user_id') != ''){
			   $data_tw = array(
					  'social_id' 		=> $this->session->userdata('twitter_user_id'),
					  'email' 			=> $this->session->userdata('twitter_screen_name'),
					  'firstname' 		=> $this->session->userdata('twitter_screen_name'),
					  'lastname' 		=> '',
					  'gender'  		=> '',
					  'activation_token'=> $this->session->userdata('twitter_user_id'),
					  //'create_date'  	=> date('Y-m-d H:i:s')
					  );
				  
				  $twlogin = $this->user_model->insertData($data_tw);
				  $logindata = array(
			  				'userID' 		=> $twlogin['id'],
							'userfname' 	=> $twlogin['firstname'],
							'username' 		=> isset($twlogin['email'])?$twlogin['email']:$twlogin['social_id'],
							'usertype' 		=> $twlogin['type'],
			  			);
				 $this->session->set_userdata($logindata);
				redirect(base_url()."index.php/dashboard");
			 }
		 }
		 
	}
	
}
