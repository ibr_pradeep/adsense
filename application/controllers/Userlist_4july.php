<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userlist extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('paypal_model');
		if(!$this->session->userdata('userID'))
		{
			redirect(base_url());
		}
	}
	
	
	public function index()
	{
		$results = $this->user_model->getallusers();
		
		//echo '<pre>';
		
		if(!empty($results)){
			foreach($results as $key => $result){
				$user_id = $result->id;
				$categories = $this->getUserCategory($user_id);
				$results[$key]->category = is_array($categories) ? implode(', ', $categories) : '';
			}
		}		
		$data['results'] = $results;
		$this->load->view('userlist',$data);
	}
	
	public function getUserCategory($user_id=''){
		if($user_id){
		  $this->db->where('user_id', $user_id);
		  $query = $this->db->get('user_category');	
		  $result = $query->row();
		  $cat_data = [];
		  if(isset($result->category_id)){
			$categories = explode(';', $result->category_id);  
			if($categories){
			  foreach($categories as $categorie){
				$this->db->where('category_id', $categorie);
				$query = $this->db->get('category');	
				$result = $query->row();
				$cat_data[] = isset($result->title) ? $result->title : '';
			  }
			}
		  }
		  return $cat_data;
		}		
	}
	
	
	/** This function will block a particuler user from user table 
		@param userID 
		@return  boolen value when sucess and failure 
	*/
	public function profile()
	{
		
	
		$data['paypal'] = $this->paypal_model->getSinglepaypal();
	
		if ($this->input->post('submit') && $this->input->post('edit_id')){
		
		
			$editid = $this->input->post('edit_id');
			
			$fullname = $this->input->post('firstname');
			
		
			$data = array(
					'firstname' 	=>  $this->input->post('firstname'),
					
					'lastname' 		=>  $this->input->post('lastname'),
					'paypal_email'  => $this->input->post('paypalemail'),
					'ac_holder_name'=> $this->input->post('holder_name'),
					'bank_name' 	=> $this->input->post('bank_name'),
					'ifsc' 			=> $this->input->post('ifsc'),
					'swift_bic' 	=> $this->input->post('swift_bic'),
					'account_no' 	=> $this->input->post('account_no'),
				
					);
					
			$response = $this->user_model->getUpdateProfile($data);
			if($response )
			{
				$this->session->set_flashdata('update','Profile details updated sucessfully');
				redirect('userlist/profile');		
			}
			
				
		}
		$data['userdata'] = $this->user_model->getUserInfo();
		$this->load->view('profile',$data);
			
	}
	
	/*
	* Block/Unblock user
	* 
	* @return status
	* Ashvin Patel 20June2015
	*/
	public function blockUnblockUser(){
		$user_id = $this->input->post('user_id');
		$status = $this->input->post('status');
		$this->db->where('id', (int)$user_id);
		echo $this->db->update('user', array('active' => (int)$status));
		//echo $this->db->last_query();
	}	
	
	/** This function will save percentage cut value 
		
	*/
	public function savePercentage()
	{
		if($this->input->post('save'))	
		{
			$userid = $this->input->post('userid');
			$data['percent_cut'] = $this->input->post('percentage');	
			$response = $this->user_model->savePercentage($data , $userid);
			if($response){
				redirect('userlist');	
			}
		}
	}
		
}
