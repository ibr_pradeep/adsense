<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AnalyticsReport extends CI_controller {
		
	public function __construct(){
		parent::__construct();
		$userseg = $this->uri->segment(1);	
		newBaseurl();
		if(!$this->session->userdata($userseg)){
			redirect(base_url());
		}
	}
	
	
	/*
	* Anayltic report index function it willl load report view
	* 
	* @return null
	* Ashvin Patel 3/July/2015
	*/
	public function index(){
		
		$this->load->model('Analytics');
		if($this->input->post('search')){
			$data['start_date'] = $this->input->post('date_filter_start');
			$data['end_date']   = $this->input->post('date_filter_end');
		}else{
			$data['start_date'] = date('Y-m-01');
			$data['end_date'] = date('Y-m-t'); 
		}		
		$analytics = $this->Analytics->getAnalyticReport($data);
		$analytics_1 = [];
		//$share_url = '';
		$referal = '';
		
		//print_r()
		/*echo '<pre>';	
		print_r($analytics);
		die;*/	
		if(!empty($analytics)){
		  foreach ($analytics as $service_acc => $analytics_) {
			if(!empty($analytics_)){					  
			  foreach ($analytics_ as $analytic_) {
				if(!empty($analytic_)){
					$share_url = '';
					$adImpresion = 0;
				  foreach($analytic_ as $key => $analytic){
					
					$share_url = [];
					if(!empty($analytic)){
						$i = 0;
					  foreach($analytic as $j => $val){
						 // echo $val['share_url']; echo '<br>';
						  if(strpos($val['share_url'], 'nggallery') !== false){
							$ag_link = explode('/nggallery', $val['share_url']);
							//print_r($ag_link);
							$match_link = $ag_link[0].'/?ref='.$key;
						  }else{
							$match_link = $val['share_url'];
						 }
						  if(!in_array($match_link, $share_url)){
							$revenue = 0;
							$ad_impression = 0;
							$ad_clicks = 0;
						  	$analytics_1[$service_acc][$key][$i] = $val;
							$i++;							
						  }	
						  					  
						  $share_url[] = $val['share_url']; 
						  $revenue = $revenue+EtoNumber($val['AdSense_Revenue']);						 
						  $analytics_1[$service_acc][$key][$i-1]['AdSense_Revenue'] = $revenue;
						  
						  $ad_clicks = $ad_clicks+$val['AdSense_Ads_Clicked'];						 
						  $analytics_1[$service_acc][$key][$i-1]['AdSense_Ads_Clicked'] = $ad_clicks;
						  
						  $ad_impression = $ad_impression+$val['AdSense_Page_Impressions'];						 
						  $analytics_1[$service_acc][$key][$i-1]['AdSense_Page_Impressions'] = $ad_impression;
						  /*if($j == 10){
							break;  
						  }*/
					  }
					}
				  }
				}
			  }
			}
		  }
		}
		
		//print_r($analytics);
		//print_r($analytics_1);
		//die;
		$data['analytics'] = $analytics_1;
		$this->load->view('analytics_details',$data);				
	}
}