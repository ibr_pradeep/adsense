<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Earnings extends CI_controller {
		
	public function __construct()
	{
		parent::__construct();
		$userseg = $this->uri->segment(1);	
		newBaseurl();
		if(!$this->session->userdata($userseg)){
			redirect(base_url());
		}
	}
	
	
	public function index(){	
		$user_billings = [];	
		if($this->input->get('req') == 'daily'){			
		  $start_date = date('Y-m-d');
		  $end_date = date('Y-m-d');
		  $month = date('M');
		  $user_billings[] = $this->getAnalyticsDetails($start_date , $end_date, $month);
		}elseif($this->input->get('req') == 'month'){			
		  $monthdata = array();
		  //foreach(months() as $m_){	
			$Y = date('Y');	
			$month = date('M');		
			$start_date = date('Y-m-01');
			$end_date   = date('Y-m-t');
			$user_billings[] = $this->getAnalyticsDetails($start_date , $end_date, $month);			
			/*if(!empty($m_data)){
			  $monthdata[] =  $m_data;
			}*/
		 // }
		  //$user_billings = $monthdata;		
		}elseif($this->input->get('req') == 'last_month'){			
		  $start_date = date('Y-m-d', strtotime('first day of last month'));
		  $end_date = date('Y-m-d', strtotime('last day of last month'));	
		  $month = date('M', strtotime($end_date));		
		  $user_billings[] = $this->getAnalyticsDetails($start_date , $end_date, $month);
		}else{
		  $start_date  = date('Y-m-d');
		  $end_date	 = date('Y-m-d');
		  $month = date("M"); 
		  $user_billings[] = $this->getAnalyticsDetails($start_date , $end_date, $month);
		}
		/*print_r($user_billings);
		die;*/
		$data['user_billings'] = $user_billings;
		$this->load->view('earnings', $data);				
	}
	
	/*
	* User report
	* 
	* Show user repots and earning
	* Ashvin Patel 19/June/2015
	*/
	public function userReport(){
		$this->load->model('Analytics');
		if($this->input->post('search')){
			$start_date = $this->input->post('date_filter_start');
			$end_date   = $this->input->post('date_filter_end');
		}else{
			$start_date = '' ;
			$end_date = ''; 
		}
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		
		$data['analytics'] = $this->Analytics->getAnalyticReport($data);
		$user_billings = [];
		
		$user_session = getCurrentUserSession();
		if($analytics){
		  foreach($analytics as $analytics_){
			if(!empty($analytics_)){
			  foreach($analytics_ as $analytic_){
				if(!empty($analytic_)){
				  foreach($analytic_ as $key => $analytic){
					if(!empty($analytic)){
					  $revenue = 0;					
					  $refral_id = $key;
					  $user_data = explode('_', $key);
					  $user_id = $user_data[0];
					  if($user_session['userID'] == $user_id){
						$paypal_email = getUserPaypalEmail($user_id);
						$user_name = getUsernamebyRef($key);
						foreach($analytic as $analytic_){									
						  $revenue = $revenue+$analytic_['AdSense_Revenue'];
						}
					  }					
					  $user_billings[] = ['username' => $user_name, 'referal_id' => $key, 'paypal_email' => $paypal_email, 'per_cut' => 10, 'earning' => $revenue, 'payment_amount' => getPerCut(10, $revenue),'month'=>$month  ];
					}
				  }
				}
			  }
			}
		  }
		}		
		$data['user_billings'] = $user_billings;		
		$this->load->view('billing', $data);
	}
	public function getAnalyticsDetails($start_date ='' , $end_date = '', $month=''){
		$this->load->model('Analytics');
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		$analytics = $this->Analytics->getAnalyticReport($data);
		
		$user_billings = [];
		// paresent month user earning details 
		if($analytics){
		  foreach($analytics as $analytics_){
			if(!empty($analytics_)){
			  foreach($analytics_ as $analytic_){
				if(!empty($analytic_)){
				  foreach($analytic_ as $key => $analytic){
					if(!empty($analytic)){
					  $revenue = 0;					
					  $refral_id = $key;
					  $user_data = explode('_', $key);
					  $user_id = $user_data[0];
					  $paypal_email = getUserPaypalEmail($user_id);
					  $user_name = getUsernamebyRef($key);
					  foreach($analytic as $analytic_){
						//$month = 	$analytic_['month'];							
						$revenue = $revenue+$analytic_['AdSense_Revenue'];
					  }					   
					  $percent_cut =  getUserpercentcut($key);					  
					  $user_billings[] = ['username' => $user_name, 'referal_id' => $key, 'paypal_email' => $paypal_email, 'per_cut' => $percent_cut , 'earning' => $revenue, 'payment_amount' => getPerCut($percent_cut , $revenue),'month'=>$month ];
					}
				  }
				}
			  }
			}
		  }
		}
		return $user_billings; 
	}
}