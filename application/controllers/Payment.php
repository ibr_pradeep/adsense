<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payment extends CI_controller {
		
	public function __construct(){
		parent::__construct();
		$userseg = $this->uri->segment(1);	
		newBaseurl();
		if(!$this->session->userdata($userseg)){
			redirect(base_url());
		}
	}
	
	public function index(){
		$user_session = getCurrentUserSession();
		$userID = isset($user_session['userID']) ? $user_session['userID'] : '';
		$this->db->where('user_id', $userID);
		$query = $this->db->get('transaction_details');
		$result = $query->result();
		$data['rows'] = $result;
		$this->load->view('payment-history', $data);	
	}
}