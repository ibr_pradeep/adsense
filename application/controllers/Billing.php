<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends CI_controller {
		
	public function __construct()
	{
		parent::__construct();
		$userseg = $this->uri->segment(1);	
		newBaseurl();
		if(!$this->session->userdata($userseg))
		{
			redirect(base_url());
		}
	}
	
	
	public function index(){
		//$paid = getPaymentDetails(72 , '72_AshleyLily_');
       //	echo "<pre>"; print_r($paid); die;
		set_time_limit(0);
		$this->load->model('Analytics');
		if($this->input->post('search')){
			$month = $this->input->post('month');
			$year   = $this->input->post('year');
			$date = $month.'-'.$year;
			$start_date = date('Y-m-01',strtotime($date));
			$end_date = date('Y-m-t',strtotime($date));
			$data['month'] = $month;
			$data['year'] = $year;
		}else{
			$start_date = '';
			$end_date = ''; 
			$data['month'] = date('F');
			$data['year'] = date('Y');			
		}
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;

		$analytics = $this->Analytics->getAnalyticReport($data);	
		/*echo '<pre>';
		print_r($analytics);
		echo '</pre>';
	die;*/
		$user_billings = [];
		$user_earning = [];
		if(!empty($analytics)){
		  foreach($analytics as $analytics_){
			if(!empty($analytics_)){
			  foreach($analytics_ as $analytic_){
				if(!empty($analytic_)){
				  foreach($analytic_ as $key => $analytic){
					if(!empty($analytic)){
						$revenue = 0;					
						$refral_id = $key;
						$user_data = explode('_', $key);
						$user_id = $user_data[0];
						$paypal_email = getUserPaypalEmail($user_id);
						$user_name = getUsernamebyRef($key);
						
						foreach($analytic as $analytic_){
							//if($user_name == 'UnusualFactPage'){
							  //echo 'befor '.$revenue; echo '<br>';
							  //echo 'Revenue: '.$analytic_['AdSense_Revenue']; echo '<br>';
							 // echo EtoNumber($analytic_['AdSense_Revenue']); echo '<br>';
							  //die;
							  if($user_name){
								  $share_url = $analytic_['share_url'];
								  $user_earning[$user_name] = isset($user_earning[$user_name]) ? $user_earning[$user_name] : 0;
								 
								  $user_earning[$user_name] = $user_earning[$user_name]+EtoNumber($analytic_['AdSense_Revenue']);  
								  //echo 'after '.$revenue; echo '<br>';
								 // echo 'Sum: '.$user_earning[$user_name]; echo '<br>';
								  $percent_cut =  getUserpercentcut( $key );												
								  $user_billings[$user_name] = ['userId'	=> $user_id,
													'username' 	=> $user_name, 
													'referal_id'=> $key, 
												'paypal_email' => $paypal_email, 
												'per_cut' => $percent_cut , 
												'earning' => $user_earning[$user_name], 
												'payment_amount' => getPerCut($percent_cut , $user_earning[$user_name]) ,
												'share_url'=> $share_url ];
							  }
						}
						//}						
						
					}
				  }
				}
			  }
			}
		  }
		}
		/*print_r($user_earning);
		die;*/
		$data['user_billings'] = $user_billings;		
		$this->load->view('billing', $data);
				
	}	
	
	
	/*
	* User report
	* 
	* Show user repots and earning
	* Ashvin Patel 19/June/2015
	*/
	public function userReport(){
		$user_session = getCurrentUserSession();
		$this->load->model('Analytics');
		if($this->input->post('search')){
			if($user_session['usertype'] == 1){ 
			  $month = $this->input->post('month');
			  $year   = $this->input->post('year');
			  $date = $month.'-'.$year;
			  $start_date = date('Y-m-01',strtotime($date));
			  $end_date = date('Y-m-t',strtotime($date));
			  $data['month'] = $month;
			  $data['year'] = $year;
			}else{
			  $start_date = $this->input->post('date_start');
			  $end_date = $this->input->post('date_end');
			}
		}else{
			$start_date = date('Y-m-01');
			$end_date = date('Y-m-t'); 
			$data['month'] = date('F');
			$data['year'] = date('Y');	
		}
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		
		$analytics = $this->Analytics->getAnalyticReport($data);
		$user_billings = [];
		
		
		$analytics_1 = [];
		//$share_url = '';
		$referal = '';
		
		//print_r()
		/*echo '<pre>';	
		print_r($analytics);
		die;*/	
		if(!empty($analytics)){
		  foreach ($analytics as $service_acc => $analytics_) {
			if(!empty($analytics_)){					  
			  foreach ($analytics_ as $analytic_) {
				if(!empty($analytic_)){
					$share_url = '';
					$adImpresion = 0;
				  foreach($analytic_ as $key => $analytic){
					
					$share_url = [];
					if(!empty($analytic)){
						$i = 0;
					  foreach($analytic as $j => $val){
						 // echo $val['share_url']; echo '<br>';
						  if(strpos($val['share_url'], 'nggallery') !== false){
							$ag_link = explode('/nggallery', $val['share_url']);
							//print_r($ag_link);
							$match_link = $ag_link[0].'/?ref='.$key;
						  }else{
							$match_link = $val['share_url'];
						 }
						  if(!in_array($match_link, $share_url)){
							$revenue = 0;
							$ad_impression = 0;
							$ad_clicks = 0;
						  	$analytics_1[$service_acc][$key][$i] = $val;
							$i++;							
						  }	
						  					  
						  $share_url[] = $val['share_url']; 
						  $revenue = $revenue+EtoNumber($val['AdSense_Revenue']);						 
						  $analytics_1[$service_acc][$key][$i-1]['AdSense_Revenue'] = $revenue;
						  
						  $ad_clicks = $ad_clicks+$val['AdSense_Ads_Clicked'];						 
						  $analytics_1[$service_acc][$key][$i-1]['AdSense_Ads_Clicked'] = $ad_clicks;
						  
						  $ad_impression = $ad_impression+$val['AdSense_Page_Impressions'];						 
						  $analytics_1[$service_acc][$key][$i-1]['AdSense_Page_Impressions'] = $ad_impression;
						  /*if($j == 10){
							break;  
						  }*/
					  }
					}
				  }
				}
			  }
			}
		  }
		}
		
		/*if($analytics){
		  foreach($analytics as $analytics_){
			if(!empty($analytics_)){
			  foreach($analytics_ as $analytic_){
				if(!empty($analytic_)){
				  foreach($analytic_ as $key => $analytic){
					if(!empty($analytic)){
						$revenue = 0;					
						$refral_id = $key;
						$user_name = '';
						$paypal_email = '';
						$share_url = '';
						$user_data = explode('_', $key);
						//print_r($user_data);
						$user_id = $user_data[0];
						//if($user_session['userID'] == $user_id){
						$paypal_email = getUserPaypalEmail($user_id);
						$user_name = getUsernamebyRef($key);
						if($user_name){
							foreach($analytic as $analytic_){
								$share_url = $analytic_['share_url'];						
								$revenue = $revenue+$analytic_['AdSense_Revenue'];
					//}
								$percent_cut =  getUserpercentcut( $key );
								$user_billings[] = ['username' => $user_name, 'referal_id' => $key, 'paypal_email' => $paypal_email, 'per_cut' => $percent_cut , 'earning' => $revenue, 'payment_amount' => getPerCut($percent_cut, $revenue), 'share_url' => $share_url];
							}
						}
					}
				  }
				}
			  }
			}
		  }
		}*/
		//die;
		$data['analytics'] = $analytics_1;		
		$this->load->view('user-billing', $data);
	}
	public function paypal()
	{	
		$data=array(
					'merchant_email'=>'sulbha2814-facilitator@gmail.com',
					'product_name'=>'Demo Product',
					'amount'=>20.50,
					'currency_code'=>'USD',
					'thanks_page'=>"http://".$_SERVER['HTTP_HOST'].'paypal/thank.php',
					'notify_url'=>"http://".$_SERVER['HTTP_HOST'].'paypal/ipn.php',
					'cancel_url'=>"http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
					'paypal_mode'=>true,
					);	
				
			$paypalform = $this->infotutsPaypal( $data);
			echo $paypalform ; 
	}
	/*
	 * Update Percentagecut in billing section 
	 * 
	 */
	public function savePercentage()
	{	$this->load->model('Analytics');
		if($this->input->post('save'))	
		{
			$userid = $this->input->post('userid');
			$data['percent_cut'] = $this->input->post('percentage');	
			$response = $this->Analytics->savePercentage($data , $userid);
			if($response){
				redirect(base_url('billing'));	
			}
		}
	}
}