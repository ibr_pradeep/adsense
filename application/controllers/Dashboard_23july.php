<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
		$this->load->model('categoryuser_model');	
		
		$userseg = $this->uri->segment(1);	
		newBaseurl();
		if(!$this->session->userdata($userseg))
		{
			redirect(base_url());
		}
		
	}
	public function index()
	{
		$data['usercategories'] = $this->categoryuser_model->getmyCategory();
		$data['categories'] = $this->categoryuser_model->getallCategory();
		//print_r($data['usercategories']);
		$this->load->view('dashboard',$data);
	}
	/** this function for edit password 
		in this function take old passpwd 
		@param  old password 
		@return boolen value
	*/
	public function changepwd()
	{
		if	($this->input->post('submit')){
			$oldpwd = md5($this->input->post('oldpwd'));
			$newpwd = md5($this->input->post('newpwd'));
			$response = $this->login_model->editPassword($oldpwd , $newpwd );
			if ($response)
			{
				$this->session->set_flashdata('sucess','Password chnaged sucessfully..!!');	
				redirect('dashboard/changepwd');
					
			}
			else
			{
				$this->session->set_flashdata('error','Old Password not match..!!');	
				redirect('dashboard/changepwd');
				
			}
			
		}
		$this->load->view('changepwd');
	}
	
	public function showDetails(){
		
		$user_session = getCurrentUserSession();
		$user_billings = [];	
		  	$start_date = date('Y-m-d');
			$end_date = date('Y-m-d');
		  	$month = date('M');
		 	$user_billings['today'] = $this->getAnalyticsDetails($start_date , $end_date, $month);
		
		
		  $monthdata = array();
		  //foreach(months() as $m_){	
			$Y = date('Y');	
			$month = date('M');		
			$start_date = date('Y-m-01');
			$end_date   = date('Y-m-t');
			$user_billings['currentMonth'] = $this->getAnalyticsDetails($start_date , $end_date, $month);			
		
		
		  $start_date = date('Y-m-d', strtotime('first day of last month'));
		  $end_date = date('Y-m-d', strtotime('last day of last month'));	
		  $month = date('M', strtotime($end_date));		
		  $user_billings['lastMonth'] = $this->getAnalyticsDetails($start_date , $end_date, $month);
		  $data['user_billings'] = $user_billings;
		
		
		$this->load->model('category_model');
		$data['usercategories'] = $this->categoryuser_model->getmyCategory();
		$data['categories'] = $this->categoryuser_model->getallCategory();
		$data['results'] = $this->category_model->getallCategory();
		$data['per_cut'] = getUserPerCutById(isset($user_session['userID']) ? $user_session['userID'] : '');
		$this->load->view('user-deshboard.php',  $data);
		//echo"hello"	; die;
	}
	
	public function getAnalyticsDetails($start_date ='' , $end_date = '', $month=''){
		$this->load->model('Analytics');
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		$analytics = $this->Analytics->getAnalyticReport($data);
		
		$user_billings = [];
		// paresent month user earning details 
		if($analytics){
		  foreach($analytics as $analytics_){
			if(!empty($analytics_)){
			  foreach($analytics_ as $analytic_){
				if(!empty($analytic_)){
				  foreach($analytic_ as $key => $analytic){
					if(!empty($analytic)){
					  $revenue = 0;					
					  $refral_id = $key;
					  $user_data = explode('_', $key);
					  $user_id = $user_data[0];
					  $paypal_email = getUserPaypalEmail($user_id);
					  $user_name = getUsernamebyRef($key);
					  foreach($analytic as $analytic_){
						//$month = 	$analytic_['month'];							
						$revenue = $revenue+$analytic_['AdSense_Revenue'];
					  }					   
					  $percent_cut =  getUserpercentcut($key);					  
					  $user_billings[] = ['username' => $user_name, 'referal_id' => $key, 'paypal_email' => $paypal_email, 'per_cut' => $percent_cut , 'earning' => $revenue, 'payment_amount' => getPerCut($percent_cut , $revenue),'month'=>$month ];
					}
				  }
				}
			  }
			}
		  }
		}
		return $user_billings; 
	}
	
	public function addUpdate($categoryid = '')
	{
		$this->load->model('categoryuser_model');
		$user_session = getCurrentUserSession();
		$cat_ids = array(); 

			foreach($_POST['category_id'] as $ids){

					$cat_ids[] = $ids; 

			}

			$categoryIDs = implode($cat_ids ,';');

			$edit_id = $this->input->post('edit_id');
			
			if($this->input->post('submit')){			
				if($this->input->post('edit_id')){
						$data = array(
						'user_id'		=> $user_session['userID'],
						'category_id' 	=> $categoryIDs,
						);
				$response = $this->categoryuser_model->updateUserCategory($data , $edit_id );
				if($response){
					$this->session->set_flashdata('update','Category Updated Sucessfully..!!');	
					redirect(base_url('dashboard/showDetails'));
				}
			}else{
					$data = array(
						'user_id'		=> $user_session['userID'],
						'category_id' 	=> $categoryIDs,
						'created_date'   => date('Y-m-d H:i:s')
						);
						$response = $this->categoryuser_model->insertUserCategory($data);
					if($response){
							$this->session->set_flashdata('insert','Category Inserted Sucessfully..!!');	

		echo "<script>window.location= '".base_url('dashboard/showDetails')."</script>";
						}	
			}
			}
		$datacat['category'] = $this->categoryuser_model->getSingleUserCategory($categoryIDs);	
		redirect(base_url('dashboard/showDetails'));



	}
	
}
