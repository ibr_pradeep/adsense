<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Scheduler extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */

	 

	public function __construct()

	{

		parent::__construct();

		$this->load->model('Scheduler_model');
		$userseg = $this->uri->segment(1);	
		newBaseurl();
		if(!$this->session->userdata($userseg)){
			redirect(base_url());
		}

	}

	

	public function index()

	{

		

		$data['categories'] = $this->Scheduler_model->getallcategory();

		$data['results'] = $this->Scheduler_model->getallScheduler();

		$this->load->view('scheduler',$data);

	}

	/** Add and update particuler category value

		

	**/

	public function addUpdate($editid = '')

	{

		

			

			

			if($this->input->post('submit')){			

			if($this->input->post('link_id')){

				

				$editid = $this->input->post('link_id');

				$data = array(

						'category_id' 	=> $this->input->post('category'),

						'curler_datetime'			=> $this->input->post('curler_datetime'),

						);

				$response = $this->Scheduler_model->updateScheduler($data , $editid );

				if($response){

					$this->session->set_flashdata('update','Scheduler Updated Sucessfully..!!');	

					redirect(base_url().'index.php/scheduler');

				}

				

			}else

			{

				

				$data = array(

						'category_id' 	=> $this->input->post('category'),

						'curler_datetime'			=> $this->input->post('curler_datetime')
					
						);

				$response = $this->Scheduler_model->insertScheduler($data);

				if($response){

					$this->session->set_flashdata('insert','Scheduler Inserted Sucessfully..!!');	

					redirect(base_url().'index.php/scheduler');

				}	

			}

			}

			$datas['categories']  = $this->Scheduler_model->getallcategory();

			$datas['scheduler'] = $this->Scheduler_model->getSingleScheduler($editid);	

			

			$this->load->view('scheduler',$datas);

	}

	/* Delete single category */

	public function deleteScheduler($sharelinkID)

	{

		$response = $this->Scheduler_model->deleteScheduler($sharelinkID);

		if($response)

		{

			$this->session->set_flashdata('delete','Share Link deleted Sucessfully..!!');	

			redirect(base_url().'index.php/scheduler');

		}

	}

	

	

}

