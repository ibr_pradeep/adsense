<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

	

class Manageads_model extends CI_Model {

	

	public function insert_data($data)

	{

		$this->db->insert('analytics_detail',$data);

		return $this->db->insert_id();

	}

	

	public function update_data($data , $edit_id )

	{

		$this->db->where('analytics_detail_id',$edit_id);

		return $this->db->update('analytics_detail',$data);
		
		

	}

	public function getallanalytics()
	{

		$this->db->where('is_deleted',0);
		$query = $this->db->get('analytics_detail');	
		return $query->result();

	}

	

	public function getSingleaccount($editid)
	{
		
		$query = $this->db->get_where('analytics_detail'

					, array('analytics_detail_id' => $editid)

					);	

		$rows = $query->row_array();
		return $rows;
	}

	public function deleteaccount($accountid)
	{

		$this->db->where('analytics_detail_id',$accountid);

		$data['is_deleted'] = '1'; 

		return $this->db->update('analytics_detail',$data);	

	}
	
	

	

}

?>