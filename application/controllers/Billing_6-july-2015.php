<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Billing extends CI_controller {
		
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('userID'))
		{
			redirect(base_url());
		}
	}
	
	
	public function index(){
		$this->load->model('Analytics');
		if($this->input->post('search')){
			$month = $this->input->post('month');
			$year   = $this->input->post('year');
			$date = $month.'-'.$year;
			$start_date = date('Y-m-01',strtotime($date));
			$end_date = date('Y-m-t',strtotime($date));
			$data['month'] = $month;
			$data['year'] = $year;
		}else{
			$start_date = '';
			$end_date = ''; 
			$data['month'] = date('F');
			$data['year'] = date('Y');			
		}
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;

		$analytics = $this->Analytics->getAnalyticReport($data);	
		
		$user_billings = [];
		if(!empty($analytics)){
		  foreach($analytics as $analytics_){
			if(!empty($analytics_)){
			  foreach($analytics_ as $analytic_){
				if(!empty($analytic_)){
				  foreach($analytic_ as $key => $analytic){
					if(!empty($analytic)){
						$revenue = 0;					
						$refral_id = $key;
						$user_data = explode('_', $key);
						$user_id = $user_data[0];
						$paypal_email = getUserPaypalEmail($user_id);
						$user_name = getUsernamebyRef($key);
						foreach($analytic as $analytic_){
						  $share_url = $analytic_['share_url'];			
						  $revenue = $revenue+$analytic_['AdSense_Revenue'];
						}						 
						$percent_cut =  getUserpercentcut( $key );						
						$user_billings[] = ['username' => $user_name, 'referal_id' => $key, 'paypal_email' => $paypal_email, 'per_cut' => $percent_cut , 'earning' => $revenue, 'payment_amount' => getPerCut($percent_cut , $revenue) ,'share_url'=> $share_url ];
					}
				  }
				}
			  }
			}
		  }
		}
		//die;
		$data['user_billings'] = $user_billings;		
		$this->load->view('billing', $data);
				
	}	
	
	/*
	* User report
	* 
	* Show user repots and earning
	* Ashvin Patel 19/June/2015
	*/
	public function userReport(){
		$this->load->model('Analytics');
		if($this->input->post('search')){
			$month = $this->input->post('month');
			$year   = $this->input->post('year');
			$date = $month.'-'.$year;
			$start_date = date('Y-m-01',strtotime($date));
			$end_date = date('Y-m-t',strtotime($date));
			$data['month'] = $month;
			$data['year'] = $year;
		}else{
			$start_date = '';
			$end_date = ''; 
			$data['month'] = date('F');
			$data['year'] = date('Y');	
		}
		$data['start_date'] = $start_date;
		$data['end_date'] = $end_date;
		
		$analytics = $this->Analytics->getAnalyticReport($data);
		$user_billings = [];		
		
		if($analytics){
		  foreach($analytics as $analytics_){
			if(!empty($analytics_)){
			  foreach($analytics_ as $analytic_){
				if(!empty($analytic_)){
				  foreach($analytic_ as $key => $analytic){
					if(!empty($analytic)){
						$revenue = 0;					
						$refral_id = $key;
						$user_name = '';
						$paypal_email = '';
						$share_url = '';
						$user_data = explode('_', $key);
						$user_id = $user_data[0];
						if($this->session->userdata('userID') == $user_id){
							$paypal_email = getUserPaypalEmail($user_id);
							$user_name = getUsernamebyRef($key);
							foreach($analytic as $analytic_){
								$share_url = $analytic_['share_url'];						
								$revenue = $revenue+$analytic_['AdSense_Revenue'];
							}
						}
						$percent_cut =  getUserpercentcut( $key );
						$user_billings[] = ['username' => $user_name, 'referal_id' => $key, 'paypal_email' => $paypal_email, 'per_cut' => $percent_cut , 'earning' => $revenue, 'payment_amount' => getPerCut(10, $revenue), 'share_url' => $share_url];
					}
				  }
				}
			  }
			}
		  }
		}
		$data['user_billings'] = $user_billings;		
		$this->load->view('billing', $data);
	}
	public function paypal()
	{
		$data=array(
					'merchant_email'=>'sulbha2814-facilitator@gmail.com',
					'product_name'=>'Demo Product',
					'amount'=>20.50,
					'currency_code'=>'USD',
					'thanks_page'=>"http://".$_SERVER['HTTP_HOST'].'paypal/thank.php',
					'notify_url'=>"http://".$_SERVER['HTTP_HOST'].'paypal/ipn.php',
					'cancel_url'=>"http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
					'paypal_mode'=>true,
					);	
				
			$paypalform = $this->infotutsPaypal( $data);
			echo $paypalform ; 
	}
	
}