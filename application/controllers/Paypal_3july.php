<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Paypal extends CI_Controller {

	public function __construct()

	{

		parent::__construct();

		$this->load->model('paypal_model');
		$this->load->model('sharelink_model');
		if(!$this->session->userdata('userID'))
		{
			redirect(base_url());
		}
	}

	public function index()
	{
		if($this->input->post('pay')){
			$month_year = date('F Y');
			$data_session = array(
				'pay_userid' => $this->input->post('user_id'),
				'month_year' =>	$month_year,
				'ref_id'  	 => $this->input->post('ref_id'),
			);
			$this->session->set_userdata($data_session);
			$amount = $this->input->post('amount'); 
			$amount = round($amount, 3);
			//echo "amt ".$amount ; die;
			$data=array(
					'merchant_email'	=> 'sulbha2814-facilitator@gmail.com',
					'product_name'		=>	$this->input->post('user_id'),
					'amount'			=>  $amount, //0.50,
					'currency_code'		=> 'USD',
					'thanks_page'		=> site_url().'/paypal/thank',
					'notify_url'		=>"http://".$_SERVER['HTTP_HOST'].'paypal/ipn.php',
					'cancel_url'		=>"http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
					'paypal_mode' 		=>true,
					);	
		
			$paypalform = $this->infotutsPaypal( $data);
			echo $paypalform ; 	
			
		}
	}

	/** This function will Add and update paypal details
		@param $editid 
		return boolean value if sucess and failuer 
	*/

	public function addUpdate()
	{

		

						

			if($this->input->post('submit')){			

			if($this->input->post('edit_id')){
				$editid = $this->input->post('edit_id');
				$data = array(
							'paypal_email' 	=>  $this->input->post('paypalemail'),
						);

				$response = $this->paypal_model->updatePaypal($data , $editid );

				if($response){

					$this->session->set_flashdata('update','Paypal Detail Updated Sucessfully..!!');	

					redirect('userlist/profile');

				}

				

			}else

			{

				
				$data = array(

						'paypal_email' 		=>  $this->input->post('paypalemail'),
						'user_id' 			=>  $this->session->userdata('userID'),
						'created_date'   	=>  date('Y-m-d H:i:s')
						);

				

				$response = $this->paypal_model->insertPaypal($data);
				
				if($response){

					$this->session->set_flashdata('insert','Paypal Detail Inserted Sucessfully..!!');	

					redirect('userlist/profile');

				}	

			}

			}

			$datapay['paypal'] = $this->paypal_model->getSinglepaypal($editid);	

			

			$this->load->view('paypal',$datapay);

	}

	/* Delete single category */

	public function deleteCategory($categorID)

	{

		$response = $this->category_model->deleteCategory($categorID);

		if($response)

		{

			$this->session->set_flashdata('delete','Category deleted Sucessfully..!!');	

			redirect(base_url().'index.php/category');

		}

	}

	public function infotutsPaypal( $data) {
		
		define( 'SSL_URL', 'https://www.paypal.com/cgi-bin/webscr' );
		 define( 'SSL_SAND_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr' );
		
		$action = '';
		 //Is this a test transaction?
		 $action = ($data['paypal_mode']) ? SSL_SAND_URL : SSL_URL;
	
		$form = '';
		
		$form .= '<form name="frm_payment_method" action="' . $action . '" method="post">';
		 $form .= '<input type="hidden" name="business" value="' . $data['merchant_email'] . '" />';
		 // Instant Payment Notification & Return Page Details /
		 $form .= '<input type="hidden" name="notify_url" value="' . $data['notify_url'] . '" />';
		 $form .= '<input type="hidden" name="cancel_return" value="' . $data['cancel_url'] . '" />';
		 $form .= '<input type="hidden" name="return" value="' . $data['thanks_page'] . '" />';
		 $form .= '<input type="hidden" name="rm" value="2" />';
		 // Configures Basic Checkout Fields -->
		 $form .= '<input type="hidden" name="lc" value="" />';
		 $form .= '<input type="hidden" name="no_shipping" value="1" />';
		 $form .= '<input type="hidden" name="no_note" value="1" />';
		 // <input type="hidden" name="custom" value="localhost" />-->
		 $form .= '<input type="hidden" name="currency_code" value="' . $data['currency_code'] . '" />';
		 $form .= '<input type="hidden" name="page_style" value="paypal" />';
		 $form .= '<input type="hidden" name="charset" value="utf-8" />';
		 $form .= '<input type="hidden" name="item_name" value="' . $data['product_name'] . '" />';
		 $form .= '<input type="hidden" value="_xclick" name="cmd"/>';
		 $form .= '<input type="hidden" name="amount" value="' . $data['amount'] . '" />';
		 $form .= '<script>';
		 $form .= 'setTimeout("document.frm_payment_method.submit()", 2);';
		 $form .= '</script>';
		 $form .= '</form>';
		 return $form;
	}	
	public function thank()
	{
		$data =array(
				'mc_gross' => $this->input->post('mc_gross'), 
				'protection_eligibility' => $this->input->post('protection_eligibility'), 
				'payer_id' => $this->input->post('payer_id'), 
				'tax' => $this->input->post('tax'), 
				'payment_date' => $this->input->post('payment_date'), 
				'payment_status' => $this->input->post('payment_status'), 
				'charset' => $this->input->post('charset'), 
				'first_name' => $this->input->post('first_name'), 
				'mc_fee' => $this->input->post('mc_fee'), 
				'notify_version' => $this->input->post('notify_version'), 
				'custom' => $this->input->post('custom'), 
				'payer_status' => $this->input->post('payer_status'), 
				'business' => $this->input->post('business'), 
				'quantity' => $this->input->post('quantity'), 
				'payer_email' => $this->input->post('payer_email'), 
				'verify_sign' => $this->input->post('verify_sign'), 
				'txn_id' => $this->input->post('txn_id'), 
				'payment_type' => $this->input->post('payment_type'), 
				'last_name' => $this->input->post('last_name'), 
				'receiver_email' => $this->input->post('receiver_email'), 
				'payment_fee' => $this->input->post('payment_fee'), 
				'receiver_id' => $this->input->post('receiver_id'), 
				'txn_type' => $this->input->post('txn_type'), 
				'item_name' => $this->input->post('item_name'), 
				'mc_currency' => $this->input->post('mc_currency'), 
				'item_number' => $this->input->post('item_number'), 
				'residence_country' => $this->input->post('residence_country'), 
				'test_ipn' => $this->input->post('test_ipn'), 
				'handling_amount' => $this->input->post('handling_amount'), 
				'transaction_subject' => $this->input->post('transaction_subject'), 
				'payment_gross' => $this->input->post('payment_gross'), 
				'shipping' => $this->input->post('shipping'), 
				'auth' => $this->input->post('auth'), 
			
		); 
		$this->paypal_model->paypal_tansaction_details($data); 
		$insert_id = $this->db->insert_id();
		
		$data_trans = array(
				'user_id' 				=> $this->session->userdata('pay_userid'),
				'ref_id'  			   	=> $this->session->userdata('ref_id'),
				'pay_for_month_year'  	=> $this->session->userdata('month_year'),
				'amount'				=> $this->input->post('payment_gross')
				
		);
		
		$this->paypal_model->transaction_details($data_trans); 
		$this->session->unsetuserdata('pay_userid');
		$this->session->unsetuserdata('ref_id');
		$this->session->unsetuserdata('month_year');
		
		if($insert_id ){
			redirect('billing'); 		
		}
	}
	

}

