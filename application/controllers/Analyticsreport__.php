<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AnalyticsReport extends CI_controller {
		
	public function __construct()
	{
		parent::__construct();
		if(!$this->session->userdata('userID'))
		{
			redirect(base_url());
		}
	}
	
	
	public function index()
	{
		
		if($this->input->post('search')){
			$start_date = $this->input->post('date_filter_start');
			$end_date   = $this->input->post('date_filter_end');
		}
		else
		{
			$start_date = '' ;
			$end_date = ''; 
		}
		//echo "asda ".$start_date;die;
		
		$analytics = $this->getService();
		$profile = $this->getFirstProfileId($analytics);
		$results = $this->getResults($analytics, $profile ,$start_date ,$end_date);
		$data['analytics'] = $this->printResults($results);
		$this->load->view('analytics_details',$data);
				
	}
	public function getService()
	{
	  // Creates and returns the Analytics service object.
	
	  // Load the Google API PHP Client Library.
	  require_once 'anlytics/src/Google/autoload.php';
	
	  // Use the developers console and replace the values with your
	  // service account email, and relative location of your key file.
	  $this->db->where('is_deleted',0);
	  $this->db->where('status',0);
	  $query = $this->db->get('analytics_detail');
	  $rows_result = $query->row();
	  $service_email 		= $rows_result->service_email; 
	  $fileclient_secrets 	= $rows_result->key_filename;
	  $service_account_email =  $service_email ; // '539518501004-q7ljpvkjla1cv653c11u0uqvfrb0b9k2@developer.gserviceaccount.com';
	  $key_file_location =  $fileclient_secrets; //'anlytics/client_secrets.p12';
	
	  // Create and configure a new client object.
	  $client = new Google_Client();
	  $client->setApplicationName("HelloAnalytics");
	  $analytics = new Google_Service_Analytics($client);
	
	  // Read the generated client_secrets.p12 key.
	  $key = file_get_contents($key_file_location);
	  $cred = new Google_Auth_AssertionCredentials(
		  $service_account_email,
		  array(Google_Service_Analytics::ANALYTICS_READONLY),
		  $key
	  );
	  $client->setAssertionCredentials($cred);
	  if($client->getAuth()->isAccessTokenExpired()) {
		$client->getAuth()->refreshTokenWithAssertion($cred);
	  }
	
	  return $analytics;
	}
	public function getFirstprofileId(&$analytics) {
  // Get the user's first view (profile) ID.

  // Get the list of accounts for the authorized user.
  $accounts = $analytics->management_accounts->listManagementAccounts();

  if (count($accounts->getItems()) > 0) {
    $items = $accounts->getItems();
    $firstAccountId = $items[0]->getId();

    // Get the list of properties for the authorized user.
    $properties = $analytics->management_webproperties
        ->listManagementWebproperties($firstAccountId);

    if (count($properties->getItems()) > 0) {
      $items = $properties->getItems();
      $firstPropertyId = $items[0]->getId();

      // Get the list of views (profiles) for the authorized user.
      $profiles = $analytics->management_profiles
          ->listManagementProfiles($firstAccountId, $firstPropertyId);

      if (count($profiles->getItems()) > 0) {
        $items = $profiles->getItems();		
        // Return the first view (profile) ID.
        return $items[0]->getId();

      } else {
        throw new Exception('No views (profiles) found for this user.');
      }
    } else {
      throw new Exception('No properties found for this user.');
    }
  } else {
    throw new Exception('No accounts found for this user.');
  }
	}
	
	public function getResults(&$analytics, $profileId , $start_date = '' , $end_date = '') {
	  // Calls the Core Reporting API and queries for the number of sessions
	  // for the last seven days.
	 // echo  "stt ".$start_date; die;
	//echo "aasdas " .$start_date .''. $end_date ;die;
	  try {
		  $optParams = array(
			'dimensions' => 'ga:referralPath,ga:pagePath,ga:socialActivityContentUrl',	
			'filters' => 'ga:medium==referral'
			);
			$current_start_dt = date('Y-m-01',strtotime('this month'));
			$current_end_dt = date('Y-m-t',strtotime('this month'));
			$results = $analytics->data_ga->get(
			   'ga:'.$profileId,
			   !empty($start_date)?$start_date:$current_start_dt,//'365daysAgo',
			   !empty($end_date)?$end_date:$current_end_dt,//'today',
			   'ga:adsenseRevenue,ga:adsenseAdsClicks,ga:adsensePageImpressions',
				$optParams);
			
			
			
		/*$results = $analytics->data_ga->get(
			   'ga:'.$profileId,
			   !empty($start_date)?$start_date:$current_start_dt,//'365daysAgo',
			   !empty($end_date)?$end_date:$current_end_dt,//'today',
			   'ga:adsenseRevenue,ga:adsenseAdsClicks,ga:adsensePageImpressions',
				$optParams);*/
				//echo "<pre>";
				//print_r($results);die;
				  return $results;
		  // Success. 
		} catch (apiServiceException $e) {
		  // Handle API service exceptions.
		  $error = $e->getMessage();
		}
	}

	public function printResults(&$results) {
  // Parses the response from the Core Reporting API and prints
  // the profile name and total sessions.
  if (count($results->getRows()) > 0) {

    // Get the profile name.
    $profileName = $results->getProfileInfo()->getProfileName();

    // Get the entry for the first entry in the first row.
    $rows = $results->getRows();
	
	
	//print_r( $rows); die; 
    $sessions = $rows[0][0];

    // Print the results.
	$user_rows = [];
	
	foreach($rows as $row){
		$string = $row[1];  
		 // print_r( $row); die;
		if(strpos($string, '?ref') !== false){			
			$string = substr($string, strpos($string, "?") + 1);    
			$string = explode('=', $string);
			$row['Page'] = $row[1];
			//print_r($row['Page']);die;
			$user = explode('/?ref=',$row['Page']);
			
			$referal = $this->session->userdata('userID').'_'.$this->session->userdata('userfname');
			if ($this->session->userdata('usertype') != 1){
				
				if($referal == $user[1]){
				$row['Referral_Path'] = $row[0];
				unset($row[0]);
				$row['Page'] = $row[1];
				unset($row[1]);
				$row['share_url'] = $row[2];
				unset($row[2]);
				$row['AdSense_Revenue'] = $row[3];
				unset($row[3]);
				$row['AdSense_Ads_Clicked'] = $row[4];
				unset($row[4]);
				$row['AdSense_Page_Impressions'] = $row[5];
				unset($row[5]);
				$user_rows[$string[1]][] = $row;
				}
			}else{
				
				if($referal == $user[1]){
				$row['Referral_Path'] = $row[0];
				unset($row[0]);
				$row['Page'] = $row[1];
				unset($row[1]);
				$row['share_url'] = $row[2];
				unset($row[2]);
				$row['AdSense_Revenue'] = $row[3];
				unset($row[3]);
				$row['AdSense_Ads_Clicked'] = $row[4];
				unset($row[4]);
				$row['AdSense_Page_Impressions'] = $row[5];
				unset($row[5]);
				$user_rows[$string[1]][] = $row;
				}
				
			}
			
			
		}		
	}
	
	return $user_rows;
	//Result sequence according dimension and metrics 
  } else {
    print "No results found.\n";
  }
	}

}