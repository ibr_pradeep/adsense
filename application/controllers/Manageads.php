<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Manageads extends CI_Controller {

	public function __construct()

	{

		parent::__construct();

		
		$this->load->model('manageads_model');
		$userseg = $this->uri->segment(1);	
		newBaseurl();
		if(!$this->session->userdata($userseg))
		{
			redirect(base_url());
		}
	}

	public function index()
	{
			
		$data['results'] = $this->manageads_model->getallanalytics();
		$this->load->view('managesads', $data); //,$data

	}
	
	/** Add and update particuler category value	
		@param $eidtid
		return boolean 
	**/

	public function addUpdate($editid = '')
	{
		
		
		if($this->input->post('submit')){
			
			if(!$this->input->post('accountemail')){				
				$this->session->set_flashdata('error','Please enter service account email');	
				redirect(base_url('manageads'));						
			}
			if(!$this->input->post('edit_id')){
				if(empty($_FILES['file']['name'])){
					$this->session->set_flashdata('accountemail', $this->input->post('accountemail'));
					$this->session->set_flashdata('error','Please select key file');	
					redirect(base_url('manageads'));		
				}
			}
			if(!$this->input->post('edit_id')){
			  if(!empty($_FILES['file']['name'])){
				  $filename = $_FILES['file']['name'];
				  $ext = pathinfo($filename, PATHINFO_EXTENSION);
				  if( $ext !== 'p12'){
					  $this->session->set_flashdata('accountemail', $this->input->post('accountemail'));
					  $this->session->set_flashdata('error','Please select correct file file');	
					  redirect(base_url('manageads'));	
				  }
				  $date = date('ymdHi');
				  if(mkdir('anlytics/'.$date, 0777, true))
				  {
					  $uploaddir = 'anlytics/'.$date.'/';
					  $uploadfile = $uploaddir .$_FILES['file']['name'];
					  move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);	
				  }
			  }
			}
			
			if($this->input->post('edit_id')){
				
				$edit_id = $this->input->post('edit_id');
				if(!empty($uploadfile)){
					$data['key_filename'] =  $uploadfile ;
				}
				$data['title']	       = $this->input->post('title');
				$data['service_email'] =  $this->input->post('accountemail') ;
				$data['status'] 	   =  $this->input->post('status') ;
				//print_r($data); die;
				$response= $this->manageads_model->update_data($data , $edit_id); 
			
				if($response){
					
					$this->session->set_flashdata('update','Account Updated Sucessfully..!!');	

					redirect(base_url('manageads'));

				}

				

			}
			else
			{	
			
				
				$data = array(
							'title'				=> $this->input->post('title'),
							
							'service_email' 	=> $this->input->post('accountemail') ,
							
							'key_filename' 		=> $uploadfile,
							
							'status' 			=> $this->input->post('status') ,
	
							'created_date'		=> date('Y-m-d H:i:s'),
						);

				$response= $this->manageads_model->insert_data($data); 
				
				if($response){

					$this->session->set_flashdata('insert','Account Details Inserted Sucessfully..!!');	

					redirect(base_url('manageads'));

				}	
				}
			}
			
			$datas['account'] = $this->manageads_model->getSingleaccount($editid);
			
			$this->load->view('managesads',$datas);
	}
	
	/** This function will delete a single category 
		@param $accountid
		@return boolean value 
	*/
	public function deleteaccount($accountid)
	{

		$response = $this->manageads_model->deleteaccount($accountid);
		if ($response)
		{
			$this->session->set_flashdata('delete','Account Deleted Sucessfully..!!');	
			redirect(base_url('manageads'));
		}

	}
}

