<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Sharelink extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */

	 

	public function __construct()

	{

		parent::__construct();
		
	
		// Loading twitter configuration.
		$this->config->load('twitter');
		$this->load->library('facebook/facebook');
		$this->load->model('sharelink_model');
		//$this->load->helper('my_fbautoload');
		if(!$this->session->userdata('userID'))
		{
			redirect(base_url());
		}


	}

	

	public function index()
	{
		$data['categories'] = $this->sharelink_model->getallcategory();// get all category from category table
		$data['users'] = $this->sharelink_model->getallUsers(); // get all users from user table
		$data['results'] = $this->sharelink_model->getallShareLink();

		$this->load->view('sharelink',$data);

	}

	/** Add and update particuler category value

		

	**/

	public function addUpdate($editid = '')
	{
		
		if($this->input->post('submit')){	
			
			
			if($this->input->post('sharelink') == 'category'){
				if(!$this->input->post('category')){
					//echo 'hi cat';
					$this->session->set_flashdata('error','category not selected..!');
					redirect(base_url('sharelink'));
				}
			}elseif($this->input->post('sharelink') == 'user'){
				if(!$this->input->post('users_id')){
					//echo 'hi user';
					$this->session->set_flashdata('error','user not selected..!');
					redirect(base_url('sharelink'));
				}
			}
			
			$uploaddir = 'assets/uploads/';
			$uploadfile = '';
			if($_FILES['image']['name']){
				$file_name = str_replace(' ', '_', $_FILES['image']['name']);
				$file_name = str_replace('(', '_', $file_name);
				$file_name = str_replace(')', '_', $file_name);
				$uploadfile = $uploaddir . time().'_'.$file_name;
				move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile);
			}
			if($this->input->post('link_id')){

				
				
				
				
				$editid = $this->input->post('link_id');
				
				$Scheduler_Set1		= $this->input->post('Scheduler_Set');
				$Scheduler_Set		= isset($Scheduler_Set1) ? $Scheduler_Set1 : '';
				$scheduler_datet	= $this->input->post('scheduler_datetime');
				$scheduler_datetime = isset($scheduler_datet) ? $scheduler_datet : date('Y-m-d H:i:s');
				if(!empty($uploadfile)){
					
					$data['image'] = 	$uploadfile;	
				}
				
				$categorIDS = array();
				if(isset($_POST['category'])){
					foreach($_POST['category'] as $categoryids)
					{
							$categorIDS[] = $categoryids;
					}
				}
				$cate = implode($categorIDS ,';');
				
				
				$userIDs = array(); 
				if(isset($_POST['users_id'])){
					foreach($_POST['users_id'] as $userids)
					{
							$userIDs[] = $userids;
					}
				}
				$user = implode($userIDs ,';');
				
				$data = array(

						'category_id' 			=> isset($cate)?$cate:'' ,
						
						'user_id' 				=> isset($user)?$user:'',

						'link'					=> $this->input->post('link'),
						'content'				=> $this->input->post('fbcontent'),
						
						'scheduler_datetime'	=> $this->input->post('scheduler_datetime'),
						
						'Scheduler_Set'			=> $this->input->post('Scheduler_Set')

						);
						
				$response = $this->sharelink_model->updateSharelink($data , $editid );
				if (!($this->input->post('scheduler_datetime'))){
						if($this->input->post('sharelink') == 'user'){
							$this->postWithDirectUser();
						}elseif($this->input->post('sharelink') == 'category'){
							$this->postdirectbycategory($editid);
						}										
					}
				if($response){
					
					$this->session->set_flashdata('update','Share Link Updated Sucessfully..!!');	

					redirect(base_url().'index.php/sharelink',$data);

				}

				

			}
			else
			{	
			
				$Scheduler_Set1		= $this->input->post('Scheduler_Set');
				$Scheduler_Set		= isset($Scheduler_Set1) ? $Scheduler_Set1 : '';
				$scheduler_datet	= $this->input->post('scheduler_datetime');
				$scheduler_datetime = isset($scheduler_datet) ? $scheduler_datet : date('Y-m-d H:i:s');
				$categorIDS = array();
				if( isset($_POST['category'])){
					foreach($_POST['category'] as $categoryids)
					{
							$categorIDS[] = $categoryids;
					}
				}
				$cate = implode($categorIDS ,';');
				
				
				$userIDs = array(); 
				if(isset($_POST['users_id'])){
					foreach($_POST['users_id'] as $userids)
					{
							$userIDs[] = $userids;
					}
				}
				$user = implode($userIDs ,';');
				$data = array(

						'category_id' 			=> isset($cate)?$cate:'' ,
						
						'user_id' 				=> isset($user)?$user:'',

						'link'				=> $this->input->post('link'),
						
						'content'				=> $this->input->post('fbcontent'),
						
						'scheduler_datetime'=> $scheduler_datetime,
						
						'Scheduler_Set'		=> $Scheduler_Set,

						'create_date'   	=> date('Y-m-d H:i:s')

						);
				if(!empty($uploadfile)){
					$data['image'] = 	$uploadfile;
				}
				$link_id = $this->sharelink_model->insertSharelink($data);
					if (!($this->input->post('scheduler_datetime'))){						
						if($this->input->post('sharelink') == 'user'){
							$this->postWithDirectUser();
						}elseif($this->input->post('sharelink') == 'category'){
							$this->postdirectbycategory($link_id);
						}						
					}
				if($link_id){

					$this->session->set_flashdata('insert','Share Link Inserted Sucessfully..!!');	

					redirect(base_url().'index.php/sharelink');

				}	

			}

			}
			$datas['categories']  = $this->sharelink_model->getallcategory();
			$datas['users'] = $this->sharelink_model->getallUsers(); // get all users from user table
			
			$datas['sharelinks'] = $this->sharelink_model->getSingleSharelink($editid);	
			$this->load->view('sharelink',$datas);
	}

	/* Delete single category */

	public function deleteSharelink($sharelinkID)
	{
		$response = $this->sharelink_model->deleteSharelink($sharelinkID);

		if($response)

		{

			$this->session->set_flashdata('delete','Share Link deleted Sucessfully..!!');	

			redirect(base_url().'index.php/sharelink');

		}

	}
	
	//----------------Scheduler----------------------//
	public function Scheduler()

	{

		$data['categories'] = $this->sharelink_model->getallcategory();

		$data['results'] = $this->sharelink_model->getallShareLink();

		$this->load->view('scheduler',$data);

	}
	public function postWithDirectUser()
	{
		
		$results = $this->sharelink_model->getShareuser();		
		foreach($results as $result )
		{
			
			//echo "userids ".$result->user_id ;
			$users = explode(';',$result->user_id );			
			
			foreach($users  as $user){
				
				$user_ids = $user; 
				$key = $this->sharelink_model->getRefral_link($user_ids );
				$post_link = $result->link.'?ref='.$key; 
				$message   = isset($result->content)?$result->content :'';
				$image     = isset($result->image)?$result->image:'';
				
				/*echo "message ".$message ."<br>";
		echo "link ".$post_link ."<br>";
		echo "image ".$image ."<br>";
		die;*/
				$data['scheduler_status'] = 1;
				$sharelink_id  = $result->sharelink_id; 
				$schetime 	= date('Y-m-d H:i',strtotime($result->scheduler_datetime));
				$currnet_date 	= date('Y-m-d H:i');
				if(strtotime($schetime) == strtotime($currnet_date))
				{
					$response = $this->sharelink_model->updateCron_set($data , $sharelink_id );
					$this->twitterpost($post_link, $message, $image, $user_ids );
					$this->facebookPost($post_link ,$message,$image,$user_ids );
				}
				else
				{
					
					$response = $this->sharelink_model->updateCron_set($data , $sharelink_id );
					$this->twitterpost($post_link, $message, $image, $user_ids );
					$this->facebookPost($post_link ,$message,$image,$user_ids );
				}
			}
		}
		
	}
public function facebookPost($link = '' , $message ='' ,$image = ''  ,$user_ids ='')
	{
		
		$fbusers = $this->sharelink_model->getToken('facebook',$user_ids );
		
		//die;
		foreach($fbusers as $fbuser){
				$login_token = $fbuser->login_token;	
				$profile_id =  $fbuser->social_id;
				$response = $this->facebook->postfacebook(
											$login_token ,
											$profile_id ,
											$link,
											$message,
											$image
										);				
				if ($response)
				{
					//echo "post"; 	
				}						
										
			
		}	
	}
	
	public function twitterpost($link='', $message='', $image='', $user_id='')	{
		
		
		$twusers = $this->sharelink_model->getToken('twitter',  $user_id );		
		
		foreach($twusers as $twuser){
			$oauth_token 		= $twuser->login_token;
			$oauth_token_secret = $twuser->login_token_secreat;
			$param = array('consumer_key' => $this->config->item('twitter_consumer_token'), 'consumer_secret' => $this->config->item('twitter_consumer_secret'), 'oauth_token' => $oauth_token, 'oauth_token_secret' => $oauth_token_secret);
			$this->load->library('Twitteroauth', $param);
			
			
			$connection = new TwitterOAuth($param);
			$content = $connection->get('account/verify_credentials');
			$image_ = $image;
			$msg = $message.'  '.$link;
			$status_message = $msg;
			$status = '';
			if($image){
				$status = $connection->upload('statuses/update_with_media', array('status' => $status_message, 'media[]' => file_get_contents($image_)));
			}else{
				$status = $connection->post('statuses/update', array('status' => $status_message));	
			}			
			
			json_encode($status);			
			if ($status)
			{
				//echo "post"; 	
			}
		}
	}
	public function postdirectbycategory($link_id=''){			
	  if($link_id){
		  $post_users = array();
		  $sharelink_detail  =$this->sharelink_model->getShareLinkById($link_id);
		  $sharelink_category = isset($sharelink_detail->category_id) ? explode(';', $sharelink_detail->category_id) : '';
		  $post_link = isset($sharelink_detail->link) ? $sharelink_detail->link : '';
		  $message   = isset($sharelink_detail->content) ? $sharelink_detail->content:'';
		  $image     = isset($sharelink_detail->image) ? $sharelink_detail->image:'';	
		  $schetime  = isset($sharelink_detail->scheduler_datetime) ? $sharelink_detail->scheduler_datetime : '';
		  $schetime  = ($schetime) ? date('Y-m-d H:i',strtotime($schetime)) : '';
		  $currnet_date 	= date('Y-m-d H:i');
		  if(!empty($sharelink_category)){
			  foreach($sharelink_category as $category){
				  $users = $this->sharelink_model->getUsersByCategory($category);
				  if(!empty($users)){
					  foreach($users as $user){
						  $user_id = $user->user_id;
						  if(!in_array($user_id, $post_users)) {								 
							$post_users[] = $user_id;
							if($this->sharelink_model->checkUserIsBlocked($user_id)){
							  $key = $this->sharelink_model->getRefral_link($user_id);
							  $share_link = '';
							  if($post_link){					
								  $share_link = $post_link.'?ref='.$key; 
							  }
							  if(strtotime($schetime) == strtotime($currnet_date)){
								  $data['scheduler_status'] = 1;
								  $response = $this->sharelink_model->updateCron_set($data , $link_id);
								  $this->facebookPost($share_link, $message, $image, $user_id);
								  $this->twitterpost($share_link, $message, $image, $user_id);												
								  if($response){
									  //echo "update";	
								  }										
							  }elseif($schetime == ''){									
								  $data['scheduler_status'] = 1;
								  $response = $this->sharelink_model->updateCron_set($data , $link_id);									
								  $this->facebookPost($share_link, $message, $image, $user_id);
								  $this->twitterpost($share_link, $message, $image, $user_id);
								  if($response){
									  //echo "update";	
								  }
							  }
							}
						  }
					  }
				  }
			  }
		  }		  
	   }			
	}
	public function postdd()
	{
		
		//$this->facebook->postfacebook1();
		 $this->twitteroauth->postTwiterStatus_11(
											$this->config->item('twitter_consumer_token'),
											$this->config->item('twitter_consumer_secret')
											
										);
		// $this->twitteroauth->postTwiterStatus_11();
	}
}

