<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Category extends CI_Controller {



	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see http://codeigniter.com/user_guide/general/urls.html

	 */

	 

	public function __construct()

	{

		parent::__construct();

		$this->load->model('category_model');
		$this->load->model('categoryuser_model');
		$userseg = $this->uri->segment(1);	
		newBaseurl();
		if(!$this->session->userdata($userseg)){
			redirect(base_url());
		}

	}

	public function index()

	{

		$data['usercategories'] = $this->categoryuser_model->getmyCategory();
		$data['categories'] = $this->categoryuser_model->getallCategory();
		$data['results'] = $this->category_model->getallCategory();

		$this->load->view('category',$data);

	}

	/** Add and update particuler category value

		

	**/

	public function addUpdate($categoryid = '')

	{

		

			

			

			
						

			if($this->input->post('submit')){			

			if($this->input->post('category_id')){

				$data = array(

						'title' 		=> $this->input->post('title'),

						);


				$category_id = $this->input->post('category_id');

				$response = $this->category_model->updateCategory($data , $category_id );

				if($response){

					$this->session->set_flashdata('update','Category Updated Sucessfully..!!');	

					redirect(base_url('category'));

				}

				

			}else

			{

				$data = array(

						'title' 		=> $this->input->post('title'),

						'create_date'   => date('Y-m-d H:i:s')

						);


				$response = $this->category_model->insertCategory($data);

				if($response){

					$this->session->set_flashdata('insert','Category Inserted Sucessfully..!!');	

					redirect(base_url('category'));

				}	

			}

			}

			$datacat['category'] = $this->category_model->getSingleCategory($categoryid);	

			

			$this->load->view('category',$datacat);

	}

	/* Delete single category */

	public function deleteCategory($categorID)
	{

		$this->category_model->checkordeleteShareCategory($categorID);
		$this->category_model->checkorDeleteUserCategory($categorID);
		$response = $this->category_model->deleteCategory($categorID);

		if($response)

		{

			$this->session->set_flashdata('delete','Category deleted Sucessfully..!!');	

			redirect(base_url('category'));

		}

	}

	

	

}

