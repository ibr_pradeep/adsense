<?php

/**

 * User_model Class extends CI_Model

 *

 * @package    USER

 * @category   Adminnistrator

 * @author     Gary

 * @link http://www.example.com/user/user_model.html

 */



class User_model extends CI_Model {
	
	/** this function will insert value from facebook login 
	    @param $data
		return boolen value if sucess and feailr
	*/
	public function insertData($data)
	{
		$this->db->insert('user',$data);
		$insertid = $this->db->insert_id();
		return $this->getUserdata($insertid);			
		
	}
	
	
	/** this function will fetch user data 
	    @param $userid
		return boolen value if sucess and feailr
	*/
	public function getUserdata($userid)
	{
		$query = $this->db->get_where('user' 
									,array('id' => $userid )
							);
		return $query->row_array();
	}
	
	/** This function will fetch if facebook user already in user table
	    @param $facebookid
		return boolen value if sucess and feailr
	*/
	public function alredyExist($facebookid)
	{
		$query = $this->db->get_where('user' 
									,array('social_id' => $facebookid)
							);
		$row = $query->row_array();
		return $row;
	}
	
	/** This function will fetch if facebook user already in user table
	    @param $facebookid
		return boolen value if sucess and feailr
	*/
	public function getallusers()
	{
		$this->db->where('type','2');
		$this->db->where('is_deleted','0');
		$query = $this->db->get('user');
		$rows = $query->result();
		return $rows;
	}
	
	/** This function will block a particuler user from user table 
		@param userID 
		@return  boolen value when sucess and failure 
	*/
	public function blockUser($userID)
	{
		$this->db->where('id',$userID);
		$data['active'] = 1 ; 
		$this->db->update('user' , $data);
		return $this->db->affected_rows();
	}
	
	/** This function will get user who logged in from user table 
		@param from session userid
		@return  boolen value when sucess and failure 
	*/
	public function getUserInfo()	
	{
		$this->db->select('user.id,social_id, firstname, middlename, lastname, email, gender , is_deleted , type ,paypal_email  , ac_holder_name,bank_name,ifsc,swift_bic,account_no,twitter_id,facebook_id'); 
		
		$this->db->where('user.id',$this->session->userdata('userID'));
		//$this->db->join('paypal_details','user_id = user.id','left');
	//	$this->db->group_by('social_id');
		$query = $this->db->get('user');
		$this->db->last_query();
		return $query->row_array();
		
	}
	
	/** This function will update user information who logged in 
		@param from session userid
		@return  boolen value when sucess and failure 
	*/
	public function getUpdateProfile($data)
	{
		$this->db->where('id',$this->session->userdata('userID'));
		return $this->db->update('user',$data);
	}
	
	public function update_login_token($up_data , $socialID)
	{
		$this->db->where('social_id',$socialID);
		return $this->db->update('user',$up_data);
	}	
	
	
	public function savePercentage($data , $userid)
	{
		$this->db->where('id',$userid);
		return $this->db->update('user',$data);
	}	
	
}