<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

	

class Apiconfig_model extends CI_Model {

	
	/** This function will insert data into 
		@param $data 
		@return boolean value 
	*/
	public function insertAppconfig($data)

	{

		$this->db->insert('api_configration',$data);

		return $this->db->insert_id();

	}

	

	public function updateAppconfig($data , $edit_id )

	{

		$this->db->where('id',$edit_id);

		return $this->db->update('api_configration',$data);

		

	}

	public function getallConfig()

	{

		$this->db->where('is_deleted','0');

		$query = $this->db->get('api_configration');	

		return $query->result();

	}

	

	public function getSingleConfig($edit_id)

	{

		$query = $this->db->get_where('api_configration'

					, array('id' => $edit_id)

					);	
 		$rows = $query->row_array();
		return $rows = $query->row_array();

	}

	public function deleteAppconfig($edit_id)

	{

		$this->db->where('id',$edit_id);

		$data['is_deleted'] = '1'; 

		return $this->db->update('api_configration',$data);	

	}

	/*  This function will get the details of particuler service provider 
		@param $service_prov
		$return boolean value if sucess or failuer
	*/
	public function getApiconfig($service_prov)
	{
		$this->db->select('id, service_provider');
		$this->db->where('service_provider', $service_prov); 	
		$this->db->where('is_deleted','0');
		$query = $this->db->get('api_configration');	
		return $query->row();
	}

}

?>