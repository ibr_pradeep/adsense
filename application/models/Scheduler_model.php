<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

	

class Scheduler_model extends CI_Model {

	

	

	/** 

	   This function for get all category  value

	   return all category value

	*/

	public function getallcategory()

	{

		$this->db->where('is_deleted','0');

		$query = $this->db->get('category');

		return $query->result();

	}

	

	/** 

	   This function for get all ShareLink  value

	   return all ShareLink value

	*/

	public function getallScheduler()

	{

		$this->db->select('*');

		//$this->db->join('category','category.category_id = scheduler.category_id');
		$this->db->join('share_link','share_link.sharelink_id = scheduler.scheduler_id');
		$this->db->join('category','category.category_id = share_link.category_id');
		
		$this->db->from('scheduler');

		$this->db->where('scheduler.status','0');

		$query = $this->db->get();

		return $query->result();

		

	}

	

	public function insertScheduler($data)

	{

		return $this->db->insert('scheduler',$data);

	}

	

	public function updateScheduler($data , $edit_id )

	{

		$this->db->where('sharelink_id',$edit_id);

		return $this->db->update('share_link',$data);

		

	}

	

	public function getSingleScheduler($sharelinkid)

	{

		$query = $this->db->get_where('share_link'

					, array('sharelink_id' => $sharelinkid)

					);	

		return $rows = $query->row_array();

	}

	public function deleteScheduler($sharelinkID)

	{

		$this->db->where('sharelink_id',$sharelinkID);

		$data['is_deleted'] = '1';

		return $this->db->update('share_link' , $data);	

	}

	

}

?>