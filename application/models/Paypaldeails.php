<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Paypaldeails extends CI_Controller {

	public function __construct()

	{

		parent::__construct();

		$this->load->model('paypal_model');
		$this->load->model('sharelink_model');
		if(!$this->session->userdata('username'))
		{
			redirect(base_url());
		}
	}

	public function index()

	{
			
		
		
		$data['paypals'] = $this->paypal_model->getallpaypal();
		$this->load->view('paypaldeatils',$data);

	}

	/** This function will Add and update paypal details
		@param $editid 
		return boolean value if sucess and failuer 
	*/

	public function addUpdate()
	{

		

						

			if($this->input->post('submit')){			

			if($this->input->post('edit_id')){
				$editid = $this->input->post('edit_id');
				$data = array(
							'paypal_email' 	=>  $this->input->post('paypalemail'),
						);

				$response = $this->paypal_model->updatePaypal($data , $editid );

				if($response){

					$this->session->set_flashdata('update','Paypal Detail Updated Sucessfully..!!');	

					redirect('paypal');

				}

				

			}else

			{

				$user_session = getCurrentUserSession();
				$data = array(

						'paypal_email' 		=>  $this->input->post('paypalemail'),
						'user_id' 			=>  $user_session['userID'],
						'created_date'   	=>  date('Y-m-d H:i:s')
						);

				

				$response = $this->paypal_model->insertPaypal($data);
				
				if($response){

					$this->session->set_flashdata('insert','Paypal Detail Inserted Sucessfully..!!');	

					redirect('paypal');

				}	

			}

			}

			$datapay['paypal'] = $this->paypal_model->getSinglepaypal($editid);	

			

			$this->load->view('paypal',$datapay);

	}

	/* Delete single category */

	public function deleteCategory($categorID)

	{

		$response = $this->category_model->deleteCategory($categorID);

		if($response)

		{

			$this->session->set_flashdata('delete','Category deleted Sucessfully..!!');	

			redirect(base_url().'index.php/category');

		}

	}

	

	

}

