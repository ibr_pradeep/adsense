<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

	

class Category_model extends CI_Model {

	

	public function insertCategory($data)

	{

		

		$this->db->insert('category',$data);

		return $this->db->insert_id();

	}

	

	public function updateCategory($data , $category_id )

	{

		$this->db->where('category_id',$category_id);

		return $this->db->update('category',$data);

		

	}

	public function getallCategory()
	{

		$this->db->where('is_deleted','0');

		$query = $this->db->get('category');	

		return $query->result();

	}

	

	public function getSingleCategory($categoryid)

	{

		$query = $this->db->get_where('category'

					, array('category_id' => $categoryid)

					);	

		return $rows = $query->row_array();

	}

	public function deleteCategory($categorID)

	{

		
		$this->db->where('category_id',$categorID);

		$data['is_deleted'] = '1'; 

		return $this->db->update('category',$data);	

	}
	
	public function checkordeleteShareCategory($categorID)
	{
		
		$this->db->like('category_id',$categorID);
		$this->db->order_by('category_id','desc');  
		$query = $this->db->get('share_link');
		$rows = $query->result();
		foreach($rows as $row){
		$categories = explode(';',$row->category_id );
		
		if(in_array($categorID ,$categories))
		{
			  unset($categories[array_search($categorID,$categories)]);
		}
		$cate = implode($categories ,';');
		$data['category_id'] = $cate ;
		$this->db->where('sharelink_id',$row->sharelink_id  );
		$this->db->update('share_link' , $data); 
		}
		
	}
	
	public function checkorDeleteUserCategory($categorID)
	{
		
		$this->db->like('category_id',$categorID);
		$this->db->order_by('category_id','desc');  
		$query = $this->db->get('user_category');
		$rows = $query->result();
		foreach($rows as $row){
		$categories = explode(';',$row->category_id );
		
		if(in_array($categorID ,$categories))
		{
			  unset($categories[array_search($categorID,$categories)]);
		}
		$cate = implode($categories ,';');
		$data['category_id'] = $cate ;
		$this->db->where('user_category_id',$row->user_category_id  );
		$this->db->update('user_category' , $data); 
		}
		
		
	}

}

?>