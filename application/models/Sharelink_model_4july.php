<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

	

class Sharelink_model extends CI_Model {

	/** 

	   This function for get all category  value

	   return all category value

	*/

	public function getallcategory()

	{

		$this->db->where('is_deleted','0');

		$query = $this->db->get('category');

		return $query->result();

	}

	

	/** 

	   This function for get all ShareLink  value

	   return all ShareLink value

	*/

	public function getallShareLink()

	{

		$this->db->select('sharelink_id ,category_id,user_id,link,share_link.is_deleted ,share_link.create_date ,share_link.scheduler_datetime ,share_link.scheduler_status ,share_link.Scheduler_Set,share_link.create_date,share_link.scheduler_status');

		//$this->db->join('category','category.category_id = share_link.category_id', 'left');
		
		//$this->db->join('user','user.id = share_link.user_id', 'left');
		
		$this->db->from('share_link');

		//$this->db->where('share_link.is_deleted','0');
		
		$this->db->order_by('sharelink_id','desc');

		$query = $this->db->get();

		return $query->result();

		

	}

	

	public function insertSharelink($data){
		$this->db->insert('share_link',$data);
		return $this->db->insert_id(); //return last insert id --Ashvin Patel 20June2015--
	}

	

	public function updateSharelink($data , $edit_id )

	{
		//print_r($edit_id);die;

		$this->db->where('sharelink_id',$edit_id);

		return $this->db->update('share_link',$data);
	//	print_r($this->db->last_query());

		

	}

	public function updateCron_set($data , $sharelink_id )
	{
		
		$this->db->where('sharelink_id',$sharelink_id);
		$this->db->update('share_link',$data);
		return  true; 
	}

	public function getSingleSharelink($sharelinkid)

	{

		$query = $this->db->get_where('share_link'

					, array('sharelink_id' => $sharelinkid)

					);	

		return $rows = $query->row_array();

	}

	public function deleteSharelink($sharelinkID)

	{

		$this->db->where('sharelink_id',$sharelinkID);

		$data['is_deleted'] = '1';

		return $this->db->update('share_link' , $data);	

	}

	public function getlinks()
	{
		$currnet_date = date('Y-m-d H:i:s');
		$this->db->where('scheduler_status',0);
		$this->db->where('user_id',0);
		$this->db->order_by('sharelink_id','desc');
		$query = $this->db->get('share_link');	
		return $query->result();
		
	}
	public function getShareuser()
	{
		$this->db->where('scheduler_status',0);
		$this->db->where('category_id',0);
		$query = $this->db->get('share_link');	
		return $query->result();
	}
	
	public function getRefral_link($user_ids)
	{
		$this->db->select('referal_link');
		$this->db->where('id',$user_ids );
		$query = $this->db->get('user');
		
		$ref_links = $query->row();
		return $key = isset($ref_links->referal_link) ? $ref_links->referal_link : '';	
	}
	public function getToken($outhprovider , $userid )
	{
		$this->db->where('outh_provider' ,$outhprovider);
		$this->db->where('id', $userid);
		$query = $this->db->get('user');	
		$rows = $query->result();
		return $rows; 
	}
	/** 

	   This function for get all category  value

	   return all category value

	*/

	public function getallusers()
	{
		$this->db->where('is_deleted','0');
		$this->db->where('active', 0);
		$this->db->where('type','2');
		$query = $this->db->get('user');

		return $query->result();

	}
	
	public function getUserbycategory()
	{
		
		$query = $this->db->get('user_category');

		return $query->result();

	}
	
	public function getShareLinkById($id=''){
		if($id){
			$this->db->where('sharelink_id', $id);
			$query = $this->db->get('share_link');
			return $query->row();
		}
	}
	public function getUsersByCategory($category_id=''){
	  if($category_id){
		$users = $this->db->query("SELECT user_id FROM user_category WHERE FIND_IN_SET($category_id, REPLACE(category_id, ';', ','))");
		//echo $this->db->last_query();
		return $users->result();;     	
	  }
	}
	
	public function checkUserIsBlocked($user_id=''){
		if($user_id){
			$this->db->where('id', $user_id);
			$this->db->select('active');
			$query = $this->db->get('user');
			$row = $query->row();
			if(isset($row->active)){
				return ($row->active == 1) ? false : true;	
			}
		}
		return false;
	}
}

?>