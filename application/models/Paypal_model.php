<?php



if (!defined('BASEPATH'))exit('No direct script access allowed');



	



class Paypal_model extends CI_Model {



	

	/** This function will insert data in paypal table

		@param $data

		return insert id 

	*/

	public function insertPaypal($data)

	{



		$this->db->insert('paypal_details',$data);

		return $this->db->insert_id();



	}

	

	/** This function will update data in paypal table

		@param $data

		$param $editid

		return boolean value if sucess and failuer

	*/

	public function updatePaypal($data , $editid )

	{

		$this->db->where('user_id',$editid); 

			$query_pay = $this->db->get('paypal_details');

			$numrow = $query_pay->num_rows();

			

			if ( $numrow == 1 ){

					$this->db->where('user_id',$editid);

			

					$this->db->update('paypal_details',$data);

				

				}else

				{

					$data['user_id'] = $editid; 

					$this->db->insert('paypal_details',$data);

				}

	}

	

	/** This function will get single record from paypal table

		$param $editid

		return $rows array that contain paypal information

	*/

	public function getSinglepaypal(){
		$user_session = getCurrentUserSession();
		$query = $this->db->get_where('paypal_details', array('user_id' => $user_session['userID']));

		return $rows = $query->row_array();
	}



	

	public function getallpaypal()

	{

		//$this->db->select('id,paypal_email,firstname,middlename,lastname');
		$this->db->select('*');
		$query = $this->db->get('user');	

		

		return $query->result();

	}



	/** This function will insert data in paypal table

		@param $data

		return insert id 

	*/

	public function paypal_tansaction_details($data)

	{



		$this->db->insert('paypal_transaction_details',$data);

		return $this->db->insert_id();



	}

	

	/** This function will insert data in transaction_details table

		@param $data

		return insert id 

	*/

	public function transaction_details($data)

	{



		$this->db->insert('transaction_details',$data);

		return $this->db->insert_id();



	}

	



}



?>