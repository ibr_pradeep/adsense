<?php

if (!defined('BASEPATH'))exit('No direct script access allowed');

	

class Categoryuser_model extends CI_Model {

	

	public function insertUserCategory($data)

	{

		$this->db->insert('user_category',$data);
		//echo $this->db->last_query();
		return $this->db->insert_id();

	}

	

	public function updateUserCategory($data , $edit_id )

	{

		$this->db->where('user_category_id',$edit_id);

		return $this->db->update('user_category',$data);

		

	}

	public function getallCategory()

	{

		$this->db->where('is_deleted','0');

		$query = $this->db->get('category');	

		return $query->result();

	}

	public function getmyCategory()
	{
		$user_session = getCurrentUserSession();
		$this->db->where('user_id', $user_session['userID']);
		$this->db->select('user_category_id,category_id');
		$this->db->from('user_category');
		$query = $this->db->get();	

		return $query->row();

	}
	

	public function getSingleUserCategory($categoryid)

	{

		$query = $this->db->get_where('category'

					, array('category_id' => $categoryid)

					);	

		return $rows = $query->row_array();

	}

	public function deleteCategory($categorID)

	{

		$this->db->where('category_id',$categorID);

		$data['is_deleted'] = '1'; 

		return $this->db->update('category',$data);	

	}
	
	

	

}

?>