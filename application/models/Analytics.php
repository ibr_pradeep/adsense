<?php
if (!defined('BASEPATH'))exit('No direct script access allowed');
class Analytics extends CI_Model {
	
	/*
	* Get analytics report by all acconts
	* 
	* @return analytic data
	* Ashvin Patel 3/July/2015
	*/
	public function getAnalyticReport($data = ''){
		if($data){			
			$users = $this->getAllUserRferral();			
			$data['start_date'] = ($data['start_date']) ? date('Y-m-d', strtotime($data['start_date'])) : '';
			$data['end_date'] = ($data['end_date']) ? date('Y-m-d', strtotime($data['end_date'])) : '';
			$analytic_accounts = $this->getAnalyticAccounts();
			$retun_data = [];
			if(!empty($analytic_accounts)){
				foreach($analytic_accounts as $analytic_account){
				  $analytics = $this->getService($analytic_account);
				  if($analytics){
					  $profile = $this->getFirstProfileId($analytics);
					  if(!empty($users)){
						  foreach($users as $user){
							  if($user->referal_link){
								  $results = $this->getResults($analytics, $profile ,$data['start_date'] ,$data['end_date'], $user->referal_link);
								  $retun_data[$analytic_account->service_email][] = $this->printResults($results);
							  }
						  }
					  }
				  }
				}
			}			
			return ($retun_data);
		}
	}
	
	/*
	* Get analytic all acconts
	* 
	* @return accounts
	* Ashvin Patel 3/July/2015
	*/
	public function getAnalyticAccounts(){
	  $this->db->select('service_email, key_filename');
	  $this->db->where('is_deleted',0);
	  $this->db->where('status',0);
	  $query = $this->db->get('analytics_detail');
	  $result = $query->result();
	  return $result;		
	}
	
	/*
	* Authorise service account and get service
	* 
	* @return service
	* Ashvin Patel 3/July/2015
	*/
	public function getService($service_account){
	  // Creates and returns the Analytics service object.	
	  // Load the Google API PHP Client Library.
	  require_once 'anlytics/src/Google/autoload.php';
	
	  // Use the developers console and replace the values with your
	  // service account email, and relative location of your key file.	
	  $service_email 		= $service_account->service_email; 
	  $fileclient_secrets 	= $service_account->key_filename;
	  $service_account_email =  $service_email ; // '539518501004-q7ljpvkjla1cv653c11u0uqvfrb0b9k2@developer.gserviceaccount.com';
	  $key_file_location =  $fileclient_secrets; //'anlytics/client_secrets.p12';
	
	  // Create and configure a new client object.
	  $client = new Google_Client();
	  $client->setApplicationName("HelloAnalytics");
	  $analytics = new Google_Service_Analytics($client);
	
	  // Read the generated client_secrets.p12 key.
	 /* if (!file_exists($key_file_location)) {   
	    return;
	  }*/		  
	  if (!file_exists($key_file_location)) {   
		log_message('error', 'Key file not found at Application\Model\Analytics.php Line: 71');
		return;
	  }else{
	   $key = file_get_contents($key_file_location); 
	  }
	  
	  $cred = new Google_Auth_AssertionCredentials(
		  $service_account_email,
		  array(Google_Service_Analytics::ANALYTICS_READONLY),
		  $key
	  );
	  $client->setAssertionCredentials($cred);
	  if($client->getAuth()->isAccessTokenExpired()) {
		$client->getAuth()->refreshTokenWithAssertion($cred);
	  }
	
	  return $analytics;
	}
	
	/*
	* Get analytics profile ID
	* 
	* @return profidleID
	* Ashvin Patel 3/July/2015
	*/
	public function getFirstprofileId(&$analytics) {
	  // Get the user's first view (profile) ID.
	
	  // Get the list of accounts for the authorized user.
	  $accounts = $analytics->management_accounts->listManagementAccounts();
	
	  if (count($accounts->getItems()) > 0) {
		$items = $accounts->getItems();
		//print_r ($items);
		//die;
		$firstAccountId = $items[0]->getId();
	
		// Get the list of properties for the authorized user.
		$properties = $analytics->management_webproperties
			->listManagementWebproperties($firstAccountId);
	
		if (count($properties->getItems()) > 0) {
		  $items = $properties->getItems();
		  $firstPropertyId = $items[0]->getId();
	
		  // Get the list of views (profiles) for the authorized user.
		  $profiles = $analytics->management_profiles
			  ->listManagementProfiles($firstAccountId, $firstPropertyId);
	
		  if (count($profiles->getItems()) > 0) {
			$items = $profiles->getItems();		
			// Return the first view (profile) ID.
			return $items[0]->getId();
	
		  } else {
			throw new Exception('No views (profiles) found for this user.');
		  }
		} else {
		  throw new Exception('No properties found for this user.');
		}
	  } else {
		throw new Exception('No accounts found for this user.');
	  }
	}
	
	/*
	* Get Analytic result by profile ID
	* 
	* @return result
	* Ashvin Patel 3/July/2015
	*/
	public function getResults(&$analytics, $profileId , $start_date = '' , $end_date = '', $handle = '') {
	  // Calls the Core Reporting API and queries for the number of sessions
	  // for the last seven days.
	 // echo  "stt ".$start_date; die;
	//echo "aasdas " .$start_date .''. $end_date ;die;
	//echo 'Profile ID'.$profileId; echo '<br>';
	  try {
		  $optParams = array(
			'dimensions' => 'ga:referralPath,ga:pagePath,ga:socialActivityContentUrl',	
			'filters' => 'ga:socialActivityContentUrl=@'.$handle.'',
			'sort' => 'ga:socialActivityContentUrl',
			'max-results' => 10000000 
		/*it will not return more than 10,000 record https://developers.google.com/analytics/devguides/reporting/core/v3/reference#maxResults*/
			);
		  $current_start_dt = date('Y-m-01');
		  $current_end_dt = date('Y-m-t');
		  $results = $analytics->data_ga->get(
			 'ga:'.$profileId,
			 !empty($start_date)?$start_date:$current_start_dt,//'2015-06-01',
			 !empty($end_date)?$end_date:$current_end_dt,//'2015-07-31',
			 'ga:adsenseRevenue,ga:adsenseAdsClicks,ga:adsensePageImpressions',
			  $optParams);				
		  return $results;
		  // Success. 
		} catch (apiServiceException $e) {
		  // Handle API service exceptions.
		  $error = $e->getMessage();
		}
	}
	
	
	/*
	* Prepare Result for printing
	* 
	* @return result
	* Ashvin Patel 3/July/2015
	*/
	public function printResults(&$results) {
	  $user_session = getCurrentUserSession();
	  // Parses the response from the Core Reporting API and prints
	  // the profile name and total sessions.
	  if (count($results->getRows()) > 0) {	
		// Get the profile name.
		$profileName = $results->getProfileInfo()->getProfileName();	
		// Get the entry for the first entry in the first row.
		$rows = $results->getRows();	
		//echo '<pre>';
		//return($rows);
		//die;	
		$sessions = $rows[0][0];	
		// Print the results.
		$user_rows = [];
		//echo '<pre>';
		foreach($rows as $row){
			//$string = $row[1]; 
			$string = $row[2]; 			 
			if(strpos($string, '?ref') !== false ){			
				$string = substr($string, strpos($string, "?") + 1);
				$string = str_replace('/', '', $string);
				$string = explode('=', $string);
				$row['Page'] = $row[1];
				//print_r($row); 
				$user = explode('?ref=',$row['Page']);				
				$referal = $user_session['userID'].'_'.$user_session['userfname'];
				/*if ($user_session['usertype'] != 1){	
				 	$user[1] = isset($user[1]) ? str_replace('/', '',  $user[1]) : '';				
					if($referal == $user[1]){
					  $row['Referral_Path'] = $row[0];
					  unset($row[0]);
					  $row['Page'] = $row[1];
					  unset($row[1]);
					  $row['share_url'] = $row[2];
					  unset($row[2]);
					  $row['AdSense_Revenue'] = $row[3];
					  unset($row[3]);
					  $row['AdSense_Ads_Clicked'] = $row[4];
					  unset($row[4]);
					  $row['AdSense_Page_Impressions'] = $row[5];
					  unset($row[5]);
					  $user_rows[$string[1]][] = $row;
					}
				}else{		*/	
					$row['Referral_Path'] = $row[0];
					unset($row[0]);
					$row['Page'] = $row[1];
					unset($row[1]);
					$row['share_url'] = $row[2];
					unset($row[2]);
					$row['AdSense_Revenue'] = $row[3];
					unset($row[3]);
					$row['AdSense_Ads_Clicked'] = $row[4];
					unset($row[4]);
					$row['AdSense_Page_Impressions'] = $row[5];
					unset($row[5]);
					$user_rows[$string[1]][] = $row;					
				/*}*/
			}		
		}
		return $user_rows;
		//Result sequence according dimension and metrics 
	  } else {
		return '';
	  }
	}
	
	public function savePercentage($data , $userid)
	{
		$this->db->where('id',$userid);
		return $this->db->update('user',$data);
	}
	
	public function getAllUserRferral(){
		$user_session = getCurrentUserSession();	
		
		$this->db->where(array('is_deleted' => '0', 'type' => '2', 'active' => '0'));
		if(isset($user_session['usertype'])){
			if($user_session['usertype'] == 2){
				$this->db->where(array('id' => $user_session['userID']));
				//$this->db->where(array('id' => 63));
			}
		}
		$this->db->select('referal_link');
		$query = $this->db->get('user');
		//echo $this->db->last_query();
		$users = $query->result();
		return $users;
	}

}

?>